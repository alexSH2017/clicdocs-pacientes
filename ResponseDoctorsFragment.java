package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.DividerItemDecoration;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterPost;
import com.clicdocs.clicdocspacientes.adapters.SwipeRecyclerViewAdapterDoctor;
import com.clicdocs.clicdocspacientes.beans.ModelFilters;
import com.clicdocs.clicdocspacientes.fragments.ResponseSearchPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogLogin;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.BackendThreat;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.CustomViewPager;
import com.clicdocs.clicdocspacientes.utils.DoctorsFind;
import com.clicdocs.clicdocspacientes.utils.Forums;
import com.clicdocs.clicdocspacientes.utils.Geocoder;
import com.clicdocs.clicdocspacientes.utils.KSOAPHelper;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.daimajia.swipe.util.Attributes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ResponseDoctorsFragment extends Fragment implements ResponseSearchPagerFragment.FragmentLifecycle {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RecyclerView myRecycler;
    private View view;
    private MapView mapView;
    private GoogleMap mMap;
    private ProgressDialog loading;
    private AlertDialog DLnoconnection;
    private LocationManager location;
    private Marker mMarker;
    private ArrayList<Marker> mMarkerArrayList;
    private ArrayList<String> arrayDoctor;
    private Paint p = new Paint();
    private Bundle b = new Bundle();
    private ArrayList<DoctorsFind> arrayDoctors = new ArrayList<>();
    private double Slat=0.0,Slng=0.0;
    private TextView TVnoResponse;
    private EditText ETsearch;
    private TextInputLayout TILsearch;
    private Button BTNsearch;
    private Spinner SPspecliaties, SPstates, SPmunicipalities, SPclinics, SPservices, SPseguros, SPschools;
    private JSONObject jsonFilters;
    private  BottomSheetDialog bottomSheetDialog;
    private ArrayList<ModelFilters> arrayFilters = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.response_doctors_fragment,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        myRecycler      = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        TVnoResponse    = (TextView) view.findViewById(R.id.TVnoResponse);
        arrayDoctor     = new ArrayList<>();
        session         = new SessionManager(ctx);
        mapView         = (MapView) view.findViewById(R.id.mapView);
        arrayDoctors    = (ArrayList<DoctorsFind>)boostrap.resultSearch.get("doctors");
        mapView.onCreate(savedInstanceState);
        fillRecyclerViewDoctor(arrayDoctors);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

    }

    private void seeMapDoctor (){

        mapView.onResume();
        try {
            MapsInitializer.initialize(boostrap.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGooglemap) {
                mMap = mGooglemap;
                mMap.clear();
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ctx,"Por favor de activar  el GPS para mejor ubicacion.",Toast.LENGTH_LONG).show();
                    return ;
                }
                mMap.setMyLocationEnabled(true);
                double lat=0.0;
                double lng=0.0;
                String office = "", name="";
                for (int i=0; i<arrayDoctors.size();i++) {
                    for (int j = 0; j <arrayDoctors.get(i).getMyoffices().size(); j++) {
                        if (arrayDoctors.get(i).getMyoffices().get(j).getLatitude() != "") {
                            lat = Double.parseDouble(arrayDoctors.get(i).getMyoffices().get(j).getLatitude());
                            lng = Double.parseDouble(arrayDoctors.get(i).getMyoffices().get(j).getLongitude());
                            office = arrayDoctors.get(i).getMyoffices().get(j).getName();
                            name = ucFirst(arrayDoctors.get(i).getName())+" "+ucFirst(arrayDoctors.get(i).getFname())+" "+ucFirst(arrayDoctors.get(i).getMname());
                            addMarker(lat, lng, name, office);
                        }
                    }
                }
                new GetCoordinates().execute(session.getSearch().get("state").replace(" ","+"));

            }
        });
    }

    private void addMarker (double lat, double lng, String name, String consultorio){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,12);
        if (mMarker!= null){
        }
        mMarker=mMap.addMarker(new MarkerOptions()
                .position(coordenadas)
                .title(name)
                .snippet(consultorio));
        mMap.animateCamera(ubication);
    }

    private void setLocation (double lat, double lng){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,12);
        mMap.animateCamera(ubication);
    }

    public void doSearchWhiteFilters (){
        String aux1 = jsonFilters.toString().replace("{","");
        final String aux2 = aux1.replace("}","");
        Log.v("jason",aux2);
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            loading = ProgressDialog.show(ctx, getString(R.string.status), "Buscando registros...");
            BackendThreat threat = new BackendThreat(ctx, new BackendThreat.ObjectTask() {
                @Override
                public Object doInBackground() {
                    final String METHOD = "findDoctorsAndPharmacies";
                    SoapObject request = new SoapObject(KSOAPHelper.NAMESPACE_AUX, "findDoctorsAndPharmacies");
                    request.addProperty("string", "juan");
                    request.addProperty("estado", "");
                    request.addProperty("filter", aux2.toString());
                    return KSOAPHelper.getResponceAux(request, METHOD, ctx);
                }
                @Override
                public void onPostExecute(Object response) {
                    loading.dismiss();
                    if(response != null) {
                        HashMap<String, String> result = KSOAPHelper.parseToHashMap(response);
                        switch (Integer.parseInt(result.get(Constants.Code))) {
                            case 200:
                                Log.v("printresponse",""+response);
                                boostrap.resultSearch = null;
                                boostrap.resultSearch = KSOAPHelper.parseToArrayList2(response);

                                arrayDoctors =  (ArrayList<DoctorsFind>)boostrap.resultSearch.get("doctors");

                                session.setSearch(ETsearch.getText().toString().trim(), Miscellaneous.getState("Veracruz"));
                                //SwipeRecyclerViewAdapterDoctor adapterDoctor = new SwipeRecyclerViewAdapterDoctor(boostrap,arrayDoctors,ctx,null);
                                //adapterDoctor.clear();
                                fillRecyclerViewDoctor(arrayDoctors);

                                break;
                            case 400:
                                AlertDialog error = new AlertDialog.Builder(ctx)
                                        .setTitle("Error")
                                        .setMessage(result.get(Constants.Code))
                                        .setPositiveButton(getString(R.string.gotit), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                        }
                    }
                }
            });
        } else {
            DLnoconnection = new AlertDialog.Builder(ctx)
                    .setMessage(getString(R.string.noconnection))
                    .setPositiveButton(getString(R.string.gotit), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
    }


    private void FilterSearch (){
        bottomSheetDialog = new BottomSheetDialog(ctx);
        View parentView     = getLayoutInflater(b).inflate(R.layout.filters_dialog,null);
        ETsearch            = (EditText) parentView.findViewById(R.id.ETsearch);
        SPstates            = (Spinner) parentView.findViewById(R.id.SPstates);
        SPmunicipalities    = (Spinner) parentView.findViewById(R.id.SPmunicipalities);
        SPspecliaties       = (Spinner) parentView.findViewById(R.id.SPspecialities);
        SPclinics           = (Spinner) parentView.findViewById(R.id.SPclinics);
        SPservices          = (Spinner) parentView.findViewById(R.id.SPservices);
        SPseguros           = (Spinner) parentView.findViewById(R.id.SPseguro);
        SPschools           = (Spinner) parentView.findViewById(R.id.SPschools);
        TILsearch           = (TextInputLayout) parentView.findViewById(R.id.TILhint);
        BTNsearch           = (Button) parentView.findViewById(R.id.BTNsearch);
        arrayDoctors    = (ArrayList<DoctorsFind>)boostrap.resultSearch.get("doctors");
        bottomSheetDialog.setContentView(parentView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,100,getResources().getDisplayMetrics()));
        bottomSheetDialog.show();
        TILsearch.setHint(boostrap.langStrings.getWhatSearch());
        BTNsearch.setText(boostrap.langStrings.getItSearch());
        BTNsearch.setOnClickListener(BTNsearch_onClic);
        getFilters();
    }

    private View.OnClickListener BTNsearch_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            jsonFilters = new JSONObject();
            try {
                jsonFilters.put("clinica","");
                jsonFilters.put("especialidad","cardiologia");
                jsonFilters.put("servicio","");
                jsonFilters.put("seguro","");
                jsonFilters.put("estado","veracruz");
                jsonFilters.put("municipio","");
                jsonFilters.put("estudio","");
                jsonFilters.put("pprecio","");
                jsonFilters.put("sprecio","");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doSearchWhiteFilters();
            bottomSheetDialog.dismiss();
        }
    };

    public void getFilters (){
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            //loading = ProgressDialog.show(ctx, getString(R.string.status), getString(R.string.connecting));
            BackendThreat threat = new BackendThreat(ctx, new BackendThreat.ObjectTask() {
                @Override
                public Object doInBackground() {
                    final String METHOD = "filterSearch";
                    SoapObject request = new SoapObject(KSOAPHelper.NAMESPACE_AUX, "filterSearch");
                    request.addProperty("status", "1");
                    return KSOAPHelper.getResponceAux(request, METHOD, ctx);
                }
                @Override
                public void onPostExecute(Object response) {
                    //loading.dismiss();
                    if (response != null) {
                        HashMap<String, String> resultCode = KSOAPHelper.parseToHashMap(response);
                        switch (Integer.parseInt(resultCode.get(Constants.Code))) {
                            case 200:
                                arrayFilters = KSOAPHelper.parseToArrayListFilters(response);
                                List<String> spi = new ArrayList<String>();
                                List<String> cli = new ArrayList<String>();
                                List<String> ser = new ArrayList<String>();
                                List<String> sch = new ArrayList<String>();
                                List<String> ins = new ArrayList<String>();
                                //----------- SPECLIAITIES ---------------------------
                                spi.add("Todas las especialidades");
                                for (int i=0; i<arrayFilters.get(0).getSpecialities().size();i++){
                                    spi.add(arrayFilters.get(0).getSpecialities().get(i).get("name"));
                                }
                                ArrayAdapter<String> specialities = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, spi);
                                specialities.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPspecliaties.setAdapter(specialities);
                                //------------ CLINICS ------------------------------------
                                cli.add("Todas las clínicas");
                                for (int i=0; i<arrayFilters.get(0).getClicnics().size();i++){
                                    cli.add(arrayFilters.get(0).getClicnics().get(i).get("name"));
                                }
                                ArrayAdapter<String> clinics = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, cli);
                                clinics.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPclinics.setAdapter(clinics);
                                //------------ SERVICES ------------------------------------
                                ser.add("Todos los servicios");
                                for (int i=0; i<arrayFilters.get(0).getServices().size();i++){
                                    ser.add(arrayFilters.get(0).getServices().get(i).get("name"));
                                }
                                ArrayAdapter<String> services = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, ser);
                                services.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPservices.setAdapter(services);
                                //------------ SCHOOLS ------------------------------------
                                sch.add("Todos los colegios de especialidad");
                                for (int i=0; i<arrayFilters.get(0).getStudies().size();i++){
                                    sch.add(arrayFilters.get(0).getStudies().get(i).get("specialty_school"));
                                }
                                ArrayAdapter<String> schools = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, sch);
                                schools.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPschools.setAdapter(schools);
                                //------------ INSURANCE ------------------------------------
                                ins.add("Todos los servicios");
                                for (int i=0; i<arrayFilters.get(0).getInsurance().size();i++){
                                    ins.add(arrayFilters.get(0).getInsurance().get(i).get("insurance"));
                                }
                                ArrayAdapter<String> insurance = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, ins);
                                insurance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPseguros.setAdapter(insurance);
                                getStates();
                                break;
                            case 400:

                                break;
                        }
                    }
                }
            });
        }
    }

    private void fillRecyclerViewDoctor (final ArrayList<DoctorsFind> doctors){
        for (int i =0; i<doctors.size();i++){
            Log.v("print",""+doctors.get(i).getName());
        }
        if (doctors.size() == 0) { TVnoResponse.setVisibility(View.VISIBLE);    }
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        SwipeRecyclerViewAdapterDoctor adapter = new SwipeRecyclerViewAdapterDoctor(boostrap, doctors, ctx, new SwipeRecyclerViewAdapterDoctor.Event() {
            @Override
            public void onClicViewProfile(DoctorsFind doctorsFind, int i) {
                Bundle b = new Bundle();
                b.putString("position",String.valueOf(i));
                b.putString("idSession",doctorsFind.getId_session());
                boostrap.setFragment(fragments.PROFILEDOCTOR,b);
            }

            @Override
            public void onClicShedule(DoctorsFind doctorsFind, final int index) {
                if (session.profile().get(0) != null){
                    if (arrayDoctor.isEmpty()){
                        if (doctors.get(index).getEnable().equals("1")){
                            if (doctors.get(index).getMyoffices().size()>0){
                                arrayDoctor.add(0,String.valueOf(index));

                                b.putStringArrayList("doctor",arrayDoctor);
                                boostrap.setFragment(fragments.SHEDULEAPPOINMENT,b);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                builder.setMessage("Este médico no tiene consultorios registrados, seleccione otro médico.")
                                        .setTitle("Aviso!")
                                        .setIcon(R.drawable.ic_warning)
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }); builder.show();
                            }
                        } else {
                            if (doctors.get(index).getMyoffices().size()>0){
                                AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                builder.setMessage("Sujeto a disposición, seleccione a un segundo médico como alternativa.")
                                        .setTitle("Aviso!")
                                        .setIcon(R.drawable.ic_warning)
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                arrayDoctor.add(0,String.valueOf(index));
                                                b.putStringArrayList("doctor",arrayDoctor);
                                                dialog.cancel();
                                            }
                                        }); builder.show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                builder.setMessage("Este médico no tiene consultorios registrados, seleccione otro médico.")
                                        .setTitle("Aviso!")
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }); builder.show();
                            }
                        }
                    } else {
                        if (doctors.get(index).getMyoffices().size()>0){
                            if (doctors.get(index).getEnable().equals("1")){

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Este médico tiene horarios disponibles, ¿desea concretar la cita con él y descartar el primero.?")
                                        .setTitle("Aviso!")
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                arrayDoctor.set(0,String.valueOf(index));
                                                b.putStringArrayList("doctor",arrayDoctor);

                                                boostrap.setFragment(fragments.SHEDULEAPPOINMENT,b);
                                                dialog.cancel();
                                            }
                                        })
                                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                boostrap.setFragment(fragments.SHEDULEAPPOINMENT,b);
                                                dialog.cancel();
                                            }
                                        }); builder.show();
                            }
                            else {

                                arrayDoctor.add(1,String.valueOf(index));
                                b.putStringArrayList("doctor",arrayDoctor);
                                boostrap.setFragment(fragments.SHEDULEAPPOINMENT,b);
                            }
                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                            builder.setMessage("Este médico no tiene consultorios registrados, seleccione otro médico.")
                                    .setTitle("Aviso!")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }); builder.show();
                        }
                    }
                } else {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(boostrap);

                    builder.setMessage("¿Desea continuar?")
                            .setTitle("Se requerie iniciar sesión para agendar cita")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                                public void onClick(DialogInterface dialog, int id) {
                                    DialogLogin login = new DialogLogin();
                                    login.show(getChildFragmentManager(), fragments.LOGINDIALOG);

                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            }); builder.show();
                }
            }
        });
        ((SwipeRecyclerViewAdapterDoctor) adapter).setMode(Attributes.Mode.Single);
        myRecycler.setAdapter(adapter);
        seeMapDoctor();
    }

    public void getStates (){
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            //loading = ProgressDialog.show(ctx, getString(R.string.status), getString(R.string.connecting));
            BackendThreat threat = new BackendThreat(ctx, new BackendThreat.ObjectTask() {
                @Override
                public Object doInBackground() {
                    final String METHOD = "getStates";
                    SoapObject request = new SoapObject(KSOAPHelper.NAMESPACE, "getStates");
                    request.addProperty("state", "1");
                    return KSOAPHelper.getResponce(request, METHOD, ctx);
                }
                @Override
                public void onPostExecute(Object response) {
                    //loading.dismiss();
                    if (response != null) {
                        HashMap<String, String> resultCode = KSOAPHelper.parseToHashMap(response);
                        switch (Integer.parseInt(resultCode.get(Constants.Code))) {
                            case 200:
                                ArrayList<HashMap<String, String>> result = KSOAPHelper.parseToArrayList(response);
                                List<String> state = new ArrayList<>();
                                for (int i=0;i<result.size();i++){
                                    HashMap<String,String> states = result.get(i);
                                    state.add(states.get("state"));
                                }
                                ArrayAdapter<String> stts = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, state);
                                stts.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                SPstates.setAdapter(stts);
                                break;
                            case 400:

                                break;
                        }
                    }
                }
            });
        }
    }


    public void initView() {
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_filters,menu);
        menu.getItem(0).setTitle(boostrap.langStrings.getFilters());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.filters:
                FilterSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static String ucFirst(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        } else {
            return  Character.toUpperCase(str.charAt(0)) + str.substring(1, str.length()).toLowerCase();
        }
    }

    @Override
    public void onResumeFragment() {

    }
    private class GetCoordinates extends AsyncTask<String,Void,String>{
        ProgressDialog dialog = new ProgressDialog(ctx);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Por favor espere . . . ");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response = null;
            try {
                String address = strings[0];
                Geocoder http = new Geocoder();
                String url = String.format("http://maps.googleapis.com/maps/api/geocode/json?address=%s",address);
                response = http.getHTTPData(url);

            } catch (Exception e) {  }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            dialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject((s));
                Slat = Double.parseDouble(((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat")+"");
                Slng = Double.parseDouble(((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng")+"");
                Log.v("coordinates",""+Slat+" "+Slng);
                setLocation(Slat,Slng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}


