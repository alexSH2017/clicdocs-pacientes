package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterAdvicesDoctor;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterSpeciality;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.Strings;

import java.util.HashMap;


public class SpecialitiesDoctorActivity extends AppCompatActivity {

    private Toolbar mToolBar;
    public Context ctx;
    private RecyclerView mRecycler;
    private ModelDoctorProfile doctor;
    public HashMap<String,String> langStrings;
    private RequestQueue queueAdvice;
    private SessionManager session;
    private RecyclerViewAdapterSpeciality adapter;
    //private TextView TVlegend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.specialities_doctor_activity);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        //TVlegend            = (TextView) findViewById(R.id.TVlegend);
        mRecycler           = (RecyclerView) findViewById(R.id.mRecycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session             = new SessionManager(ctx);
        Bundle data = getIntent().getExtras();
        if (data != null){
            langStrings     = (HashMap<String,String>) data.getSerializable(Constants.langString);
            setTitle(langStrings.get(Constants.specialities_p));
            doctor       =  (ModelDoctorProfile) data.getSerializable("specialities");
            getSpecialitiesDoctor();
        } else {
            finish();
        }
    }

    //Funciòn getInsurance
    /*
    Carga una lista de las especialidades que tiene un mèdico seleccionado
    */
    private void getSpecialitiesDoctor (){
        if (doctor.getSpeciality().size() > 0){
            adapter = new RecyclerViewAdapterSpeciality(ctx, langStrings, doctor.getSpeciality());
            mRecycler.setAdapter(adapter);
        } else {
            //TVlegend.setVisibility(View.VISIBLE);
            //TVlegend.setText(langStrings.get(Constants.no_specialities_p));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
