package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.AdapterSpecialities;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CatalogSpecialitiesActivity extends AppCompatActivity {
    private Toolbar mToolBar;
    public Context ctx;
    private RecyclerView mRecycler;
    public HashMap<String,String> langStrings;
    private RequestQueue queueSpeciality;
    private AdapterSpecialities adapter;
    private SessionManager session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catalog_specialities_activity);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        mRecycler           = (RecyclerView) findViewById(R.id.mRecycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;

        Bundle data = getIntent().getExtras();
        if (data != null){
            session = new SessionManager(ctx);
            langStrings     = (HashMap<String,String>) data.getSerializable(Constants.langString);
            setTitle(langStrings.get(Constants.selected_option_p));
            getSpecialities();
        } else {
            finish();
        }
    }

    //Funciòn getSpecialities
    /*
    Obtiene el catàlogo de especialidades registrados en la base de datos
    */
    private void getSpecialities(){
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueSpeciality = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_specialities, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonInsuranceAll = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonInsuranceAll.getJSONArray("rows");
                                ArrayList<String> specialities = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    specialities.add(Miscellaneous.ucFirst(item.getString("name")));
                                }

                                adapter = new AdapterSpecialities(ctx, specialities, new AdapterSpecialities.Event() {
                                    @Override
                                    public void onClic(String item) {
                                        Intent data = new Intent();
                                        data.putExtra("item", item);
                                        setResult(RESULT_OK, data);
                                        finish();
                                    }
                                });
                                mRecycler.setAdapter(adapter);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, "");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSpeciality.add(request);
        }  else {

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
