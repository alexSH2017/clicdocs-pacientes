package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterAdvicesDoctor;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.Strings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class AdvicesDoctorActivity  extends AppCompatActivity{

    private Toolbar mToolBar;
    public Context ctx;
    private RecyclerView mRecycler;
    private ModelDoctorProfile doctor;
    public HashMap<String,String> langStrings;
    private RequestQueue queueAdvice;
    private SessionManager session;
    private RecyclerViewAdapterAdvicesDoctor adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advices);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        mRecycler           = (RecyclerView) findViewById(R.id.mRecycler);
            mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session             = new SessionManager(ctx);
        Bundle data = getIntent().getExtras();
            if (data != null){
                langStrings  = (HashMap<String,String>) data.getSerializable(Constants.langString);
                setTitle(langStrings.get(Constants.councils_p));
                doctor       =  (ModelDoctorProfile) data.getSerializable("advices");
                getAdvices();
            } else {
            finish();
            }
}

    //Funciòn getAdvices
    /*
    Carga una lista de los consejos a los que pertenece un mèdico seleccionado
    */
    public void getAdvices() {
        adapter = new RecyclerViewAdapterAdvicesDoctor(ctx, doctor.getAdvices());
        mRecycler.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
