package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class PrivacyCLICDOCSActivity extends AppCompatActivity{

    private Button BTNterms, BTNprivacy;
    private Toolbar mToolBar;
    public Context ctx;
    public HashMap<String,String> langStrings;
    private RequestQueue queueTerms;
    private TextView TVemail;
    private TextView TVglosary;
    private SessionManager session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clicdocs_activity);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        langStrings         = (HashMap<String,String>) getIntent().getExtras().getSerializable(Constants.langString);
        setTitle(langStrings.get(Constants.about_p));
        TVemail             = (TextView) findViewById(R.id.TVemail);
        TVglosary           = (TextView) findViewById(R.id.TVglosary);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session = new SessionManager(ctx);
        BTNterms    = (Button) findViewById(R.id.BTNterms);
        BTNprivacy  = (Button) findViewById(R.id.BTNpolicies);

        BTNterms.setText(langStrings.get(Constants.terms_condi_p));
        BTNprivacy.setText(langStrings.get(Constants.priv_and_poli_p));

        BTNterms.setOnClickListener(BTNterms_onClic);
        BTNprivacy.setOnClickListener(BTNprivacy_onClic);
        TVglosary.setOnClickListener(TVglosary_onClic);
        TVglosary.setText(Html.fromHtml("<u>"+langStrings.get(Constants.glosary_p)+"</u>"));
        TVemail.setText(Html.fromHtml("<u>soporte@clicdocs.com</u>"));
        TVemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] TO = {"soporte@clicdocs.com"};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("sosporte@clicdocs.com:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(PrivacyCLICDOCSActivity.this,
                            "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //Evento para ver tèrminos y condiciones
    /*
    Obtiene los tèrminos y condiciones de CLICDOCS para despues mostrarlos en pantalla
    */
    private View.OnClickListener BTNterms_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(NetworkUtils.haveNetworkConnection(ctx)) {
                final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, langStrings.get(Constants.loading_p));
                loading.show();
                queueTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                StringRequest request = new StringRequest(Request.Method.POST, Constants.get_terms, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        try {
                            Log.v("printLogin",""+response);
                            JSONObject jsonObject = new JSONObject(response);

                            switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                case 200:
                                    JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                    JSONObject terms = jsonResp.getJSONObject(0);
                                    dialogTerms(terms.getString("term").toString());
                                    break;
                                case 403:
                                    Snackbar.make(getWindow().getDecorView().getRootView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                            .show();
                                    break;
                                case 404:
                                    Snackbar.make(getWindow().getDecorView().getRootView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                            .show();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("error", error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("id_user_type", "5");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.key_bundle_id, Constants.bundle_id);
                        headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                        return headers;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queueTerms.add(request);
            }  else {
                Snackbar.make(getWindow().getDecorView().getRootView(), langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    };

    //Funciòn dialogTerms
    /*
    Muestra en pantalla los tèrminos y condiciones de CLICDOCS
    */
    private void dialogTerms(String terms){
        View dialog_terms        = LayoutInflater.from(ctx).inflate(R.layout.dialog_terms, null);
        Button BTNreject         = (Button) dialog_terms.findViewById(R.id.BTNreject);
        Button BTNaccept         = (Button) dialog_terms.findViewById(R.id.BTNaccept);
        TextView TVterms         = (TextView) dialog_terms.findViewById(R.id.TVterms);
        BTNreject.setVisibility(View.INVISIBLE);
        BTNaccept.setText(langStrings.get(Constants.close_p));
        final AlertDialog DLterms   = new AlertDialog.Builder(ctx)
                .setTitle(langStrings.get(Constants.terms_condi_p))
                .setIcon(R.drawable.clic_logo)
                .setView(dialog_terms).show();

        TVterms.setText(Html.fromHtml(terms));

        BTNaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DLterms.dismiss();
            }
        });
    }


    private View.OnClickListener BTNprivacy_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View dialog_privacity    = LayoutInflater.from(ctx).inflate(R.layout.dialog_policies_privacity, null);
            WebView WBprivacity      = (WebView) dialog_privacity.findViewById(R.id.WBprivacity);
            WebSettings webSettings = WBprivacity.getSettings();
            webSettings.setJavaScriptEnabled(true);
            final AlertDialog DLterms   = new AlertDialog.Builder(ctx)
                    .setTitle(langStrings.get(Constants.priv_and_poli_p))
                    .setIcon(R.drawable.clic_logo)
                    .setView(dialog_privacity).show();
            WBprivacity.loadUrl(Constants.URL_policies_privacity);

        }
    };

    //Evento glosario
    /*
    Muestra un glosario de tèrminos utilizados en la aplicacion
    */
    private View.OnClickListener TVglosary_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View dialog_glosary      = LayoutInflater.from(ctx).inflate(R.layout.dialog_glosary_fragment, null);
            final ProgressBar PBloading    = (ProgressBar) dialog_glosary.findViewById(R.id.PBloading);
            final LinearLayout LLglosary   = (LinearLayout) dialog_glosary.findViewById(R.id.LLglosary);
            WebView WVglosary        = (WebView) dialog_glosary.findViewById(R.id.WVglosary);
            final Button BTNclose          = (Button) dialog_glosary.findViewById(R.id.BTNclose);
            BTNclose.setText(langStrings.get(Constants.close_p));

            WebSettings webSettings = WVglosary.getSettings();
            webSettings.setJavaScriptEnabled(true);
            WVglosary.loadUrl(Constants.URL_glosary);
            final AlertDialog DLglosary   = new AlertDialog.Builder(ctx)
                    .setView(dialog_glosary).create();
            DLglosary.requestWindowFeature(Window.FEATURE_NO_TITLE);
            DLglosary.show();

            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    PBloading.setVisibility(View.GONE);
                    LLglosary.setVisibility(View.VISIBLE);
                }
            },2000);


            BTNclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DLglosary.dismiss();
                }
            });
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
