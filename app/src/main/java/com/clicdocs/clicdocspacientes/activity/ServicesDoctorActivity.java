package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterAdvicesDoctor;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterServices;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.Strings;

import java.util.HashMap;


public class ServicesDoctorActivity extends AppCompatActivity {

    private Toolbar mToolBar;
    public Context ctx;
    private RecyclerView mRecycler;
    private ModelDoctorProfile doctor;
    public HashMap<String,String> langStrings;
    private RequestQueue queueAdvice;
    private SessionManager session;
    private RecyclerViewAdapterServices adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_doctor);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);

        mRecycler           = (RecyclerView) findViewById(R.id.mRecycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session             = new SessionManager(ctx);
        Bundle data = getIntent().getExtras();
        if (data != null){
            langStrings     = (HashMap<String,String>) data.getSerializable(Constants.langString);
            setTitle(langStrings.get(Constants.services_p));
            doctor          = (ModelDoctorProfile) data.getSerializable("services");
            getServicesDoctor();
        } else {
            finish();
        }
    }

    //Funciòn getServicesDoctor
    /*
    Carga una lista de los servicios que ofrece un mèdico seleccionado
    */
    private void getServicesDoctor (){
        if (doctor.getServices().size() > 0){
            adapter = new RecyclerViewAdapterServices(ctx, doctor.getServices());
            mRecycler.setAdapter(adapter);
        } else {
            Toast.makeText(this,langStrings.get(Constants.no_services_p),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
