package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelBillData;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apc_g on 09/10/2017.
 */

public class AdminBusinessActivity extends AppCompatActivity {

    private Context ctx;
    private SessionManager session;
    private TextView TVfiscaladdress;
    private TextInputLayout TILrfc, TILreason, TILemail, TILcolony, TILstreet, TILnumext, TILnumint, TILborough, TILcity, TILzipcode, TILstate;
    private EditText ETreasonsocial,ETrfc,ETemail,ETcolony,ETstreet,ETnumext,ETnumint, ETborough,ETcity,ETzipcode;
    private TextView TVmessage;
    private Spinner SPstate;
    private ModelBillData billData;
    private RequestQueue queueSave;
    private Toolbar mToolBar;
    public HashMap<String, String> langStrings;
    private boolean isEdit;
    public Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_business);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session             = new SessionManager(ctx);
        TVfiscaladdress = (TextView) findViewById(R.id.TVfiscaladdress);
        TILrfc             = (TextInputLayout) findViewById(R.id.TILrfc);
        TILreason          = (TextInputLayout) findViewById(R.id.TILreason);
        TILemail           = (TextInputLayout) findViewById(R.id.TILemail);
        TILcolony          = (TextInputLayout) findViewById(R.id.TILcolony);
        TILstreet          = (TextInputLayout) findViewById(R.id.TILstreet);
        TILnumext          = (TextInputLayout) findViewById(R.id.TILnumext);
        TILnumint          = (TextInputLayout) findViewById(R.id.TILnumint);
        TILborough         = (TextInputLayout) findViewById(R.id.TILborough);
        TILcity            = (TextInputLayout) findViewById(R.id.TILcity);
        TILstate           = (TextInputLayout) findViewById(R.id.TILstate);
        TILzipcode         = (TextInputLayout) findViewById(R.id.TILzipcode);

        ETreasonsocial  = (EditText) findViewById(R.id.ETreasonsocial);
        ETrfc           = (EditText) findViewById(R.id.ETrfc);
        ETemail         = (EditText) findViewById(R.id.ETemail);
        ETcolony        = (EditText) findViewById(R.id.ETcolony);
        ETstreet        = (EditText) findViewById(R.id.ETstreet);
        ETnumext        = (EditText) findViewById(R.id.ETnumext);
        ETnumint        = (EditText) findViewById(R.id.ETnumint);
        ETborough       = (EditText) findViewById(R.id.ETborough);
        ETcity          = (EditText) findViewById(R.id.ETcity);
        ETzipcode       = (EditText) findViewById(R.id.ETzipcode);
        SPstate         = (Spinner) findViewById(R.id.SPstate);
        TVmessage       = (TextView) findViewById(R.id.TVmessage);

        Bundle data = getIntent().getExtras();
        if (data != null){
            validator = new Validator();
            langStrings     = (HashMap<String, String>) data.getSerializable(Constants.langString);
            validator.REQUIRED_MESSAGE = langStrings.get(Constants.requier_field_p);
            validator.ONLY_TEXT_MESSAGE = langStrings.get(Constants.only_letters_p);
            validator.ONLY_NUMBER_MESSAGE = langStrings.get(Constants.only_numbers_p);
            validator.EMAIL_MESSAGE = langStrings.get(Constants.invalid_email_p);
            validator.REASON_MESSAGE = langStrings.get(Constants.mess_bussines_p);
            validator.NUMBER_MESSAGE = langStrings.get(Constants.letters_and_num_p);
            validator.GENERAL_MESSAGE = langStrings.get(Constants.letters_o_num_p);
            billData = (ModelBillData) data.getSerializable("bill");
            setTitle(langStrings.get(Constants.billing_p));
            TVfiscaladdress.setText(langStrings.get(Constants.fiscal_address_p).toUpperCase());
            TILreason.setHint(langStrings.get(Constants.rsocial_p));
            TILemail.setHint(langStrings.get(Constants.email_p));
            TILcolony.setHint(langStrings.get(Constants.colony_p));
            TILstreet.setHint(langStrings.get(Constants.street_p));
            TILnumext.setHint(langStrings.get(Constants.num_ext_p));
            TILnumint.setHint(langStrings.get(Constants.num_int_p));
            TILborough.setHint(langStrings.get(Constants.borough_p));
            TILcity.setHint(langStrings.get(Constants.city_p));
            TILzipcode.setHint(langStrings.get(Constants.zipcode_p));
            TVmessage.setText(langStrings.get(Constants.message_bill_p));

            if(billData != null) {
                setTitle(langStrings.get(Constants.edit_billing_p));
                isEdit   = true;
                setBillData();

            } else {
                setTitle(langStrings.get(Constants.add_billing_p));
                isEdit = false;
            }

        } else {
            finish();
        }
    }

    //Funciòn setBillData
    /*
    Obtiene los datos de facturaciòn de un elemento que desea editar y los coloca en su respectivo campo
    */
    private void setBillData (){
        ETrfc.setText(billData.getRfc());
        ETemail.setText(billData.getEmail_business());
        ETreasonsocial.setText(billData.getBusiness_name());
        ETcolony.setText(billData.getColony());
        ETstreet.setText(billData.getCalle());
        ETnumext.setText(billData.getNum_ext());
        ETnumint.setText(billData.getNum_int());
        ETcity.setText(billData.getCity());
        ETborough.setText(billData.getBorough());
        ETzipcode.setText(billData.getZipcode());

        ArrayAdapter adapter_state = ArrayAdapter.createFromResource( ctx, R.array.states , android.R.layout.simple_spinner_item);
        for (int i = 0; i < 32; i++){
            if (billData.getState().equals(adapter_state.getItem(i))){ SPstate.setSelection(i);}

        }
    }

    //Funciòn saveBillData
    /*
    Valida los datos antes de ser mandarlos a guardar o actualizar
    */
    private void saveBillData () {
        InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        //Validator validator = new Validator();
        if (!validator.validate(ETrfc, new String[]{validator.REQUIRED, validator.REASON}) &&
                !validator.validate(ETreasonsocial, new String[]{validator.REQUIRED, validator.REASON}) &&
                !validator.validate(ETemail, new String[]{validator.REQUIRED, validator.EMAIL}) &&
                !validator.validate(ETstreet, new String[]{validator.REQUIRED}) &&
                !validator.validate(ETnumext, new String[]{validator.REQUIRED, validator.NUMBER_HOME}) &&
                !validator.validate(ETcolony, new String[]{validator.REQUIRED}) &&
                !validator.validate(ETborough, new String[]{validator.REQUIRED, validator.ONLY_TEXT}) &&
                !validator.validate(ETcity, new String[]{validator.REQUIRED, validator.ONLY_TEXT}) &&
                !validator.validate(ETzipcode, new String[]{validator.REQUIRED, validator.ONLY_NUMBER})) {
            if (ETzipcode.getText().toString().length() == 5){
                if (billData != null) {
                    if (SPstate.getSelectedItemPosition() != 0){
                        updateBillingData();
                    } else {
                        AlertDialog dialogState = new AlertDialog.Builder(ctx)
                                .setMessage(langStrings.get(Constants.choicestate_p))
                                .setPositiveButton(langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }

                } else {
                    if (SPstate.getSelectedItemPosition() != 0){
                        saveBillingData();
                    } else {
                        AlertDialog dialogState = new AlertDialog.Builder(ctx)
                                .setMessage(langStrings.get(Constants.choicestate_p))
                                .setPositiveButton(langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }

                }
            } else {
                AlertDialog success = new AlertDialog.Builder(ctx)
                        .setMessage(langStrings.get(Constants.required_zipcode_p))
                        .setPositiveButton(langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }

        }
    }

    //Funciòn saveBillingData
    /*
    Manda los parametros al servicio para guardar un nuevo dato de facturaciòn
    */
    private void saveBillingData () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_billing, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonRes = jsonObject.getJSONObject(Constants.response);
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonRes.getString("message"))
                                        .setPositiveButton(langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                finish();
                                            }
                                        }).show();
                                break;
                            case 202:
                                Snackbar.make(getWindow().getDecorView().getRootView(),jsonObject.getString(Constants.response),Snackbar.LENGTH_SHORT).show();
                                ETemail.setError(jsonObject.getString(Constants.response).toString());
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", session.profile().get(0).toString());
                    params.put("rfc", ETrfc.getText().toString());
                    params.put(Constants.name, ETreasonsocial.getText().toString());
                    params.put(Constants.email, ETemail.getText().toString());
                    params.put("street", ETstreet.getText().toString());
                    params.put("outdoor", ETnumext.getText().toString());
                    params.put("interior", ETnumint.getText().toString());
                    params.put("colony", ETcolony.getText().toString());
                    params.put("zipcode", ETzipcode.getText().toString());
                    params.put("city", ETcity.getText().toString());
                    params.put("borough", ETborough.getText().toString());
                    params.put("state", SPstate.getSelectedItem().toString());
                    params.put("country", "México");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Access-Token", session.profile().get(5).toString());
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    //Funciòn saveBillingData
    /*
    Manda los parametros al servicio para actualizar un registro de facturaciòn
    */
    private void updateBillingData () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.update_billing, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseUPDATE",""+response);
                    loading.dismiss();
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonObject.getString("response"))
                                        .setPositiveButton(langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                finish();
                                                //FragmentManager fm = getSupportFragmentManager();
                                                //fm.popBackStack(fragments.BILLINGADMON,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                            case 202:
                                Snackbar.make(getWindow().getDecorView().getRootView(),jsonObject.getString(Constants.response),Snackbar.LENGTH_SHORT).show();
                                ETemail.setError(jsonObject.getString(Constants.response).toString());
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("billing_id", billData.getId_bill());
                    params.put("rfc", ETrfc.getText().toString());
                    params.put(Constants.name, ETreasonsocial.getText().toString());
                    params.put(Constants.email, ETemail.getText().toString());
                    params.put("street", ETstreet.getText().toString());
                    params.put("outdoor", ETnumext.getText().toString());
                    params.put("interior", ETnumint.getText().toString());
                    params.put("colony", ETcolony.getText().toString());
                    params.put("zipcode", ETzipcode.getText().toString());
                    params.put("city", ETcity.getText().toString());
                    params.put("borough", ETborough.getText().toString());
                    params.put("state", SPstate.getSelectedItem().toString());
                    params.put("country", "México");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Access-Token", session.profile().get(5).toString());
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    @Override
    public void finish() {
        Intent res = new Intent();
        setResult(RESULT_OK, res);
        super.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save_bill, menu);
        menu.getItem(0).setTitle(langStrings.get(Constants.add_p));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addBill:
                saveBillData ();
               return true;
            case android.R.id.home:
                finish();
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
