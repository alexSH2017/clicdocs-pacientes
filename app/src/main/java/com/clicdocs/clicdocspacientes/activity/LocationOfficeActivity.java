package com.clicdocs.clicdocspacientes.activity;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Strings;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LocationOfficeActivity extends AppCompatActivity {

    private Marker mMarker;
    private GoogleMap mMap;
    private MapView mapView;
    private Toolbar mToolBar;
    private static final int LOCATION_REQUEST_CODE = 1;
    public Context ctx;
    private LocationManager lm;
    private TextView TVnameOffice, TVaddress, TVborough, TVstate;
    private TextView TVroomT, TVaddressT, TVboroughT, TVstateT;
    private Dates item;
    private HashMap<String,String> langStrings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_office_activity);
        TVnameOffice        = (TextView) findViewById(R.id.TVnameOffice);
        TVaddress           = (TextView) findViewById(R.id.TVaddress);
        TVborough           = (TextView) findViewById(R.id.TVborough);
        TVstate             = (TextView) findViewById(R.id.TVstate);
        TVroomT             = (TextView) findViewById(R.id.TVroomT);
        TVaddressT          = (TextView) findViewById(R.id.TVaddressT);
        TVboroughT          = (TextView) findViewById(R.id.TVboroughT);
        TVstateT            = (TextView) findViewById(R.id.TVstateT);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        mapView             = (MapView) findViewById(R.id.mapLocation);
        mapView.onCreate(savedInstanceState);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        Bundle data = getIntent().getExtras();
        if (data != null){
            langStrings     = (HashMap<String,String>) data.getSerializable(Constants.langString);
            setTitle(langStrings.get(Constants.location_room_p));
            item = (Dates) data.getSerializable("appointment");
            TVroomT.setText(langStrings.get(Constants.consult_p));
            TVaddressT.setText(langStrings.get(Constants.address_p));
            TVboroughT.setText(langStrings.get(Constants.borough_p));
            TVstateT.setText(langStrings.get(Constants.state_p));
            TVnameOffice.setText(item.getName_office());
            TVaddress.setText(item.getAddress());
            TVborough.setText(item.getBorough());
            TVstate.setText(item.getState());
        }
        TrustManagerUtil.restoresHttpsUrlSettingsGoogle();
        seeMapDoctor();
    }

    //Funciòn seeMapDoctor
    /*
    Muestra en el mapa la ubicaciòn de un consultorio seleccionado desde la vista de citas aprovadas
    */
    private void seeMapDoctor (){
        mapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGooglemap) {
                mMap = mGooglemap;
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                ctx, R.raw.style_json));
                mMap.getUiSettings().isZoomControlsEnabled();
                mMap.setMyLocationEnabled(true);

                addMarker(Double.parseDouble(item.getLati()),Double.parseDouble(item.getLngi()),item.getName_office());
            }
        });
    }

    private void addMarker (double lat, double lng, String consultorio){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,15);
        if (mMarker!= null){

        }
        mMarker=mMap.addMarker(new MarkerOptions()
                .position(coordenadas)
                .title(consultorio));
        mMap.animateCamera(ubication);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
