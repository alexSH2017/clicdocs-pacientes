package com.clicdocs.clicdocspacientes.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.AdapterSpecialities;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apc_g on 30/10/2017.
 */

public class StatesDoctorActivity extends AppCompatActivity {

    private Toolbar mToolBar;
    public Context ctx;
    private RecyclerView mRecycler;
    private ModelDoctorProfile doctor;
    public HashMap<String,String> langStrings;
    private RequestQueue queueState;
    private SessionManager session;
    private AdapterSpecialities adapter;
    private TextView TVlegend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catalog_specialities_activity);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        mRecycler           = (RecyclerView) findViewById(R.id.mRecycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ctx = this;
        session = new SessionManager(ctx);
        Bundle data = getIntent().getExtras();
        if (data != null){
            langStrings     = (HashMap<String,String>) data.getSerializable(Constants.langString);
            setTitle(langStrings.get(Constants.selected_option_p));
            getStates();
        } else {
            finish();
        }
    }

    //Funciòn getState
    /*
    Carga una lista de los estados en los que hay mèdicos que atienden con CLICDOCS
    */
    private void getStates (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueState = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_states, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRows = jsonObject.getJSONArray(Constants.response);
                                ArrayList<String> states = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    states.add(item.getString("state"));
                                }

                                adapter = new AdapterSpecialities(ctx, states, new AdapterSpecialities.Event() {
                                    @Override
                                    public void onClic(String item) {
                                        Intent data = new Intent();
                                        data.putExtra("item", item);
                                        setResult(RESULT_OK, data);
                                        finish();
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueState.add(request);
        }  else {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
