package com.clicdocs.clicdocspacientes;


import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MyFireBaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseMsgService";
    private SessionManager session;
    private RequestQueue queueToken;
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        TrustManagerUtil.restoresHttpsUrlSettings();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TAG", "token: "+refreshedToken);
        session = new SessionManager(getApplicationContext());
        if (session.profile().get(0) != null){
            sendRegistrationToServer(refreshedToken,getApplicationContext(), session.profile().get(0).toString());
        }

    }

    public void sendRegistrationToServer (final String token, final Context ctx,final String idSession) {
        session = new SessionManager(ctx);
    Log.v("printtokenSend",token+"    "+idSession);
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueToken = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_token, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        session.setTokenDevice(token);
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printToken",""+response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("session_id", idSession);
                    params.put("device_token", token);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    return headers;
                }
            };
            queueToken.add(request);
        }
    }

}
