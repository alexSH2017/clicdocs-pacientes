package com.clicdocs.clicdocspacientes;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clicdocs.clicdocspacientes.beans.ActionsBeans;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private SessionManager session;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        session = new SessionManager(this);
        TrustManagerUtil.restoresHttpsUrlSettings();
        Log.d("print", "FromData: " + remoteMessage.getData());

        String message = "";
        String title = "Notificaciones CLICDOCS";
        ArrayList<ActionsBeans> actions = new ArrayList<>();


        JSONObject notify = new JSONObject(remoteMessage.getData());
        Log.v("printNotify",""+notify);
        try {
            message = notify.getString("messages");
            if (notify.getString("actions").length() > 2){
                String action = notify.getString("actions");
                JSONArray actionJSON = new JSONArray(action);
                for (int i = 0; i < actionJSON.length(); i++){
                    JSONObject item = actionJSON.getJSONObject(i);
                    actions.add(new ActionsBeans(item.getString("name"),item.getString("action_url"),item.getString("icon")));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }

        if (!message.toLowerCase().equals("e5b9530a33f799ba2148126e3e0f6af3")){
            sendNotification(title,message,actions);
        }else {
            session.deleteFirstDoctor();
            session.secondProfile();
            session.deleteProfile();
            session.deletePanicTelehone();
            session.deleteChat();
        }

    }

    private void sendNotification(final String tittle, final String message_noti, final ArrayList<ActionsBeans> action) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        notificationBuilder.setSmallIcon(R.mipmap.ic_notification);
                notificationBuilder.setContentTitle(tittle)
                .setContentText(message_noti)
                .setVibrate(new long[100])
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(message_noti));
        notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[100]);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (action != null){
            for (int i = 0; i < action.size(); i++){
                if (action.get(i).getIcon().equals("notification_accept")) {
                    Intent intentAccept = new Intent(Intent.ACTION_VIEW, Uri.parse(action.get(i).getLink()));
                    intentAccept.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentAccept.setPackage("com.android.chrome");
                    PendingIntent pendingIntentAccept = PendingIntent.getActivity(this, 0, intentAccept,
                            PendingIntent.FLAG_ONE_SHOT);
                    notificationBuilder.addAction(0, action.get(i).getName(), pendingIntentAccept);
                } else if (action.get(i).getIcon().equals("notification_cancel")) {
                    Intent intentCancel = new Intent(Intent.ACTION_VIEW, Uri.parse(action.get(i).getLink()));
                    intentCancel.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentCancel.setPackage("com.android.chrome");
                    PendingIntent pendingIntentCancel = PendingIntent.getActivity(this, 0, intentCancel,
                            PendingIntent.FLAG_ONE_SHOT);
                    notificationBuilder.addAction(0, action.get(i).getName(), pendingIntentCancel);
                } else if (action.get(i).getIcon().equals("notification_reschedule")) {
                    Intent intentReschedule = new Intent(Intent.ACTION_VIEW, Uri.parse(action.get(i).getLink()));
                    intentReschedule.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentReschedule.setPackage("com.android.chrome");
                    PendingIntent pendingIntentReschedule = PendingIntent.getActivity(this, 0, intentReschedule,
                            PendingIntent.FLAG_ONE_SHOT);
                    notificationBuilder.addAction(0, action.get(i).getName(), pendingIntentReschedule);
                }
            }
        }
        Random r = new Random();
        int randomNo = r.nextInt(1000+1);
        notificationManager.notify(randomNo, notificationBuilder.build());

    }


}