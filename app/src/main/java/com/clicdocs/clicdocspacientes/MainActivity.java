package com.clicdocs.clicdocspacientes;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.activity.PrivacyCLICDOCSActivity;
import com.clicdocs.clicdocspacientes.fragments.AddPillBoxFragment;
import com.clicdocs.clicdocspacientes.fragments.AddSubordinateFragment;
import com.clicdocs.clicdocspacientes.fragments.ChatFragment;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.EditProfileFragment;
import com.clicdocs.clicdocspacientes.fragments.FamilyClicDocsFragment;
import com.clicdocs.clicdocspacientes.fragments.FavoriteDoctorsFragment;
import com.clicdocs.clicdocspacientes.fragments.ForumFragment;
import com.clicdocs.clicdocspacientes.fragments.LinksFragment;
import com.clicdocs.clicdocspacientes.fragments.MapDoctorFragment;
import com.clicdocs.clicdocspacientes.fragments.MapPharmaciesFragment;
import com.clicdocs.clicdocspacientes.fragments.MyDatesFragment;
import com.clicdocs.clicdocspacientes.fragments.OfficesDoctorFragment;
import com.clicdocs.clicdocspacientes.fragments.OfficesWaitList;
import com.clicdocs.clicdocspacientes.fragments.ProfileDoctorFragment;
import com.clicdocs.clicdocspacientes.fragments.RescheduleAppoinmentFragment;
import com.clicdocs.clicdocspacientes.fragments.ResponseSearchPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.SearchFragment;
import com.clicdocs.clicdocspacientes.fragments.SubthemeFragment;
import com.clicdocs.clicdocspacientes.fragments.SubthemesFragment;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogLogoutFragment;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogRatingDoctor;
import com.clicdocs.clicdocspacientes.fragments.LoginFragment;
import com.clicdocs.clicdocspacientes.fragments.RegisterFragment;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.fragments.PillBoxFragment;
import com.clicdocs.clicdocspacientes.utils.CDrawerLayout;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.FontsOverride;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {

    private ActionBarDrawerToggle mToggled;
    public Toolbar TBmain;
    private CDrawerLayout mDrawerLayout;
    private NavigationView NVmenu;
    private FrameLayout FLmain;
    private TabLayout mTabLayout;
    private CoordinatorLayout CLcontainer;
    private Context ctx;
    private Button BTNregister;
    private LinearLayout LLprofile;
    private SessionManager session;
    private Button BTNlogin;
    private TextView tvProfilePatinet;
    private CircleImageView CIVprofile;
    public Menu navigationMenu;
    public Menu navigationProfile;
    public Menu navigationSettings;
    public HashMap<String, Object> resultSearch;
    public HashMap<String, String> langStrings;
    public Validator validator;
    public String WORD_SEARCH = "";
    public String SPECIALITY_SEARCH = "";
    public String STATE_SEARCH = "";
    public JSONObject jsonSearch = new JSONObject();
    public JSONObject jsonPharmacy = new JSONObject();
    public String lati = "";
    public String longi = "";
    public PopupWindow myPopup;
    public BottomSheetDialog bottomSheetDialog;
    public TextView counter;
    public boolean active = false;


    public interface ClicDocs {
        void initTabLayout(TabLayout Tabs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.main_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ctx       = getApplicationContext();
        validator = new Validator();
        if(getIntent().getExtras() != null) {
            langStrings = (HashMap<String, String>) getIntent().getExtras().getSerializable(Constants.langString);
            validator.REQUIRED_MESSAGE = langStrings.get(Constants.requier_field_p);
            validator.ONLY_TEXT_MESSAGE = langStrings.get(Constants.only_letters_p);
            validator.ONLY_NUMBER_MESSAGE = langStrings.get(Constants.only_numbers_p);
            validator.EMAIL_MESSAGE = langStrings.get(Constants.invalid_email_p);
            validator.REASON_MESSAGE = langStrings.get(Constants.mess_bussines_p);
            validator.NUMBER_MESSAGE = langStrings.get(Constants.letters_and_num_p);
            validator.GENERAL_MESSAGE = langStrings.get(Constants.letters_o_num_p);
        }
        TBmain              = (Toolbar) findViewById(R.id.TBmain);
        mDrawerLayout       = (CDrawerLayout) findViewById(R.id.mDrawerLayout);
        NVmenu              = (NavigationView) findViewById(R.id.NVmenu);
        mTabLayout          = (TabLayout) findViewById(R.id.mTabLayout);
        FLmain              = (FrameLayout) findViewById(R.id.FLmain);
        CLcontainer         = (CoordinatorLayout) findViewById(R.id.CLcontainer);
        BTNregister         = (Button) findViewById(R.id.btCreate);
        BTNlogin            = (Button) findViewById(R.id.btLogin);
        session             = new SessionManager(ctx);
        tvProfilePatinet    = (TextView) findViewById(R.id.TVprofilePatient);
        View view           = NVmenu.getHeaderView(0);
        tvProfilePatinet    = (TextView)view.findViewById(R.id.TVprofilePatient);
        CIVprofile          = (CircleImageView) view.findViewById(R.id.CIVprofile);
        LLprofile           = (LinearLayout) view.findViewById(R.id.LLprofile);

        FontsOverride.setDefaultFont(this,"MONOSPACE", "noto_serif/NotoSerif-Regular.ttf");
        navigationMenu = NVmenu.getMenu();
        counter = (TextView) MenuItemCompat.getActionView(navigationMenu.findItem(R.id.NVchat));
        counter.setVisibility(View.GONE);

        navigationMenu.getItem(0).setTitle(langStrings.get(Constants.search_p));
        navigationMenu.getItem(1).setTitle(langStrings.get(Constants.my_appointment_p));
        navigationMenu.getItem(2).setTitle(langStrings.get(Constants.waiting_list_p));
        navigationMenu.getItem(3).setTitle("Chat");
        navigationMenu.getItem(4).setTitle(langStrings.get(Constants.profile_p));
        navigationProfile = navigationMenu.getItem(4).getSubMenu();
        navigationProfile.getItem(0).setTitle(langStrings.get(Constants.clinic_history_p));
        navigationProfile.getItem(1).setTitle(langStrings.get(Constants.my_doctor_p));
        navigationProfile.getItem(2).setTitle(langStrings.get(Constants.pillbox_p));
        navigationProfile.getItem(3).setTitle(langStrings.get(Constants.familiy_clicdocs_p));
        navigationMenu.getItem(5).setTitle(langStrings.get(Constants.more_p));
        navigationSettings = navigationMenu.getItem(5).getSubMenu();
        navigationSettings.getItem(0).setTitle(Miscellaneous.ucFirst(langStrings.get(Constants.settings_p)));
        navigationSettings.getItem(1).setTitle(langStrings.get(Constants.forums_p));
        navigationSettings.getItem(2).setTitle(langStrings.get(Constants.links_p));
        navigationSettings.getItem(3).setTitle(langStrings.get(Constants.about_p));
        navigationSettings.getItem(4).setTitle(langStrings.get(Constants.logout_p));


        BTNlogin.setText(langStrings.get(Constants.login_p));
        BTNregister.setText(langStrings.get(Constants.create_account_p));
        if (session.profile().get(0) != null){
            setNameProfile (session.profile().get(2).toString());
            setImageProfile(session.profile().get(4).toString());
            showItem();
            LLprofile.setVisibility(View.VISIBLE);
            setFragment(fragments.SEARCH,null);
            BTNregister.setVisibility(View.GONE);
            BTNlogin.setVisibility(View.GONE);
        } else {
            hideItem();
            setFragment(fragments.SEARCH,null);
            LLprofile.setVisibility(View.INVISIBLE);
            tvProfilePatinet.setText("");
            BTNlogin.setVisibility(View.VISIBLE);
            BTNregister.setVisibility(View.VISIBLE);
        }

        setSupportActionBar(TBmain);
        DrawerLayout.LayoutParams CLparams = (DrawerLayout.LayoutParams) CLcontainer.getLayoutParams();
        if(CLparams.getMarginStart() == (int)getResources().getDimension(R.dimen.drawermenu)) {
            mDrawerLayout.setLocked(true);
            mDrawerLayout.setDrawerShadow(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shadow), GravityCompat.START);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, NVmenu);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        }
        if(!mDrawerLayout.isLocked()) {
            mToggled = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    if (session.profile().get(0) != null){
                        if(NetworkUtils.haveNetworkConnection(ctx)) {
                            RequestQueue queueUnRead = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                            StringRequest request = new StringRequest(Request.Method.POST, Constants.unread_chat, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.v("printUnread",""+response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                                case 200:
                                                    if (Integer.parseInt(jsonObject.get(Constants.response).toString()) > 0) {
                                                        counter.setBackgroundResource(R.drawable.unread_messages);
                                                        counter.setTextSize(15);
                                                        counter.setTextColor(ctx.getResources().getColor(R.color.textButtom));
                                                        if (Integer.parseInt(jsonObject.get(Constants.response).toString()) > 99) {
                                                            counter.setText("99+");
                                                            counter.setPadding(8, 22, 10, 6);
                                                        }
                                                        else {
                                                            if (Integer.parseInt(jsonObject.get(Constants.response).toString()) > 9) {
                                                                counter.setPadding(8, 22, 10, 6);
                                                            } else {
                                                                counter.setPadding(14, 22, 14, 0);
                                                            }
                                                            counter.setText(jsonObject.get(Constants.response).toString());
                                                        }
                                                        counter.setVisibility(View.VISIBLE);
                                                    }else {
                                                        counter.setVisibility(View.GONE);
                                                    }
                                                    break;
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.v("error", error.toString());
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        HashMap<String, String> params = new HashMap<>();
                                        params.put("session_id", session.profile().get(0).toString());
                                        params.put("type", "5");
                                        return params;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<>();
                                        headers.put(Constants.key_access_token, session.profile().get(5).toString());
                                        headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                        headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                        return headers;
                                    }
                                };
                                request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                queueUnRead.add(request);
                            }  else {

                            }
                    }
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);

                }
            };
            mDrawerLayout.addDrawerListener(mToggled);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    mToggled.syncState();
                }
            });
        }
        NVmenu.setNavigationItemSelectedListener(NVmenu_onClick);

        BTNregister.setOnClickListener(BTNregister_onClick);

        BTNlogin.setOnClickListener(BTNlogin_onClick);
        LLprofile.setOnClickListener(LLeditProfile_onClic);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.clicdocs.clicdocspacientes",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private View.OnClickListener BTNregister_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setFragment(fragments.REGISTER,null);
            if(!mDrawerLayout.isLocked()) mDrawerLayout.closeDrawer(NVmenu);
        }
    };

    private View.OnClickListener BTNlogin_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setFragment(fragments.LOGIN,null);
            if(!mDrawerLayout.isLocked()) mDrawerLayout.closeDrawer(NVmenu);
        }
    };

    //Funciòn setFragment
    /*
    Va mostrando los frsgmentos que el usuario va solicitando
    */
    public void setFragment(String fragment, Bundle extras) {
        mTabLayout.setVisibility(View.GONE);
        Fragment current = getSupportFragmentManager().findFragmentById(R.id.FLmain);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        String tokenFragment = "";
        switch (fragment) {
            case fragments.SEARCH:
                if (!(current instanceof SearchFragment)){
                    transaction.replace(R.id.FLmain,new SearchFragment (), fragments.SEARCH);
                    tokenFragment = fragments.SEARCH;
                }
                break;
            case fragments.HISTORYCLINIC:
                if (!(current instanceof ClinicHistoryPagerFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain,new ClinicHistoryPagerFragment(), fragments.HISTORYCLINIC);
                    tokenFragment = fragments.HISTORYCLINIC;
                }
                break;
            case fragments.FORUM:
                if (!(current instanceof ForumFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain,new ForumFragment(), fragments.FORUM);
                    tokenFragment = fragments.FORUM;
                }
                break;
            case fragments.LINKS:
                if (!(current instanceof LinksFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain,new LinksFragment(), fragments.LINKS);
                    tokenFragment = fragments.LINKS;
                }
                break;
            case fragments.OFFICES:
                if (!(current instanceof OfficesDoctorFragment)){
                    OfficesDoctorFragment offices = new OfficesDoctorFragment();
                    if (extras != null)offices.setArguments(extras);
                    transaction.replace(R.id.FLmain, offices, fragments.OFFICES);
                    tokenFragment = fragments.OFFICES;
                }
                break;
            case fragments.REGISTER:
                if (!(current instanceof RegisterFragment)){
                    transaction.replace(R.id.FLmain, new RegisterFragment(), fragments.REGISTER);
                    tokenFragment = fragments.REGISTER;
                }
                break;
            case fragments.FAVORITEDOCOTORS:
                if (!(current instanceof FavoriteDoctorsFragment)){
                    transaction.replace(R.id.FLmain, new FavoriteDoctorsFragment(), fragments.FAVORITEDOCOTORS);
                    tokenFragment = fragments.FAVORITEDOCOTORS;
                }
                break;
            case fragments.LOGIN:
                if (!(current instanceof LoginFragment)){
                    transaction.replace(R.id.FLmain, new LoginFragment(), fragments.LOGIN);
                    tokenFragment = fragments.LOGIN;
                }
                break;
            case fragments.RESCHEDULE:
                if (!(current instanceof RescheduleAppoinmentFragment)){
                    RescheduleAppoinmentFragment rescheduleAppointment=new RescheduleAppoinmentFragment();
                    rescheduleAppointment.setArguments(extras);
                    transaction.replace(R.id.FLmain, rescheduleAppointment, fragments.RESCHEDULE);
                    tokenFragment = fragments.RESCHEDULE;
                }
                break;
            case fragments.RATING:
                if (!(current instanceof DialogRatingDoctor)){
                    transaction.replace(R.id.FLmain, new DialogRatingDoctor(), fragments.RATING);
                    tokenFragment = fragments.RATING;
                }
                break;
            case fragments.SUBTHEMES:
                if (!(current instanceof SubthemesFragment)){
                    SubthemesFragment subthemes=new SubthemesFragment();
                    subthemes.setArguments(extras);
                    transaction.replace(R.id.FLmain, subthemes, fragments.SUBTHEMES);
                    tokenFragment = fragments.SUBTHEMES;
                }
                break;
            case fragments.MYDATES:
                if (!(current instanceof MyDatesFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new MyDatesFragment(), fragments.MYDATES);
                    tokenFragment = fragments.MYDATES;
                }
                break;
            case fragments.PILLBOX:
                if (!(current instanceof PillBoxFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new PillBoxFragment(), fragments.PILLBOX);
                    tokenFragment = fragments.PILLBOX;
                }
                break;
            case fragments.FAMILY:
                if (!(current instanceof FamilyClicDocsFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new FamilyClicDocsFragment(), fragments.FAMILY);
                    tokenFragment = fragments.FAMILY;
                }
                break;
            case fragments.SUBTHEME:
                if (!(current instanceof SubthemeFragment)){
                    SubthemeFragment subtheme = new SubthemeFragment();
                    subtheme.setArguments(extras);
                    transaction.replace(R.id.FLmain, subtheme, fragments.SUBTHEME);
                    tokenFragment = fragments.SUBTHEME;
                }
                break;
            case fragments.LOGOUT:
                if (!(current instanceof DialogLogoutFragment)){
                    transaction.replace(R.id.FLmain, new DialogLogoutFragment(), fragments.LOGOUT);
                    tokenFragment = fragments.LOGOUT;
                }
                break;
            case fragments.PROFILEDOCTOR:
                if (!(current instanceof ProfileDoctorFragment)){
                    ProfileDoctorFragment profile = new ProfileDoctorFragment();
                    profile.setArguments(extras);
                    transaction.replace(R.id.FLmain, profile, fragments.PROFILEDOCTOR);
                    tokenFragment = fragments.PROFILEDOCTOR;
                }
                break;

            case fragments.EDITPROFILE:
                if (!(current instanceof EditProfileFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new EditProfileFragment(), fragments.EDITPROFILE);
                    tokenFragment = fragments.EDITPROFILE;
                }
                break;
            case fragments.ADDPILL:
                if (!(current instanceof AddPillBoxFragment)){
                    transaction.replace(R.id.FLmain, new AddPillBoxFragment(), fragments.ADDPILL);
                    tokenFragment = fragments.ADDPILL;
                }
                break;
            case fragments.MAPDOCTORS:
                if (!(current instanceof MapDoctorFragment)){
                    MapDoctorFragment mapDoctor = new MapDoctorFragment();
                    mapDoctor.setArguments(extras);
                    transaction.replace(R.id.FLmain, mapDoctor, fragments.MAPDOCTORS);
                    tokenFragment = fragments.MAPDOCTORS;
                }
                break;
            case fragments.MAPPHARMACIES:
                if (!(current instanceof MapPharmaciesFragment)){
                    MapPharmaciesFragment mapPharmacy = new MapPharmaciesFragment();
                    mapPharmacy.setArguments(extras);
                    transaction.replace(R.id.FLmain, mapPharmacy, fragments.MAPPHARMACIES);
                    tokenFragment = fragments.MAPPHARMACIES;
                }
                break;
            case fragments.OFFICEWAITLIST:
                if (!(current instanceof OfficesWaitList)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new OfficesWaitList(), fragments.OFFICEWAITLIST);
                    tokenFragment = fragments.OFFICEWAITLIST;
                }
                break;
            case fragments.SETTINGS:
                if (myPopup != null){
                    session.deleteFirstDoctor();
                    myPopup.dismiss();
                }
                Intent config = new Intent(ctx, ConfigActivity.class);
                config.putExtra(Constants.langString, langStrings);
                startActivity(config);
                break;
            case fragments.RESPONSE:
                if (!(current instanceof ResponseSearchPagerFragment)){
                    transaction.replace(R.id.FLmain, new ResponseSearchPagerFragment(), fragments.RESPONSE);
                    tokenFragment = fragments.RESPONSE;
                }
                break;
            case fragments.CHAT:
                if (!(current instanceof ChatFragment)){
                    transaction.replace(R.id.FLmain, new ChatFragment(), fragments.CHANGEPASS);
                    tokenFragment = fragments.CHAT;
                }
                break;
            case fragments.ADDSUBORDINATE:
                if (!(current instanceof AddSubordinateFragment)){
                    transaction.replace(R.id.FLmain,new AddSubordinateFragment(), fragments.ADDSUBORDINATE);
                    tokenFragment = fragments.ADDSUBORDINATE;
                }
                break;

            default:
                if (!(current instanceof SearchFragment)){
                    if (myPopup != null){
                        session.deleteFirstDoctor();
                        myPopup.dismiss();
                    }
                    transaction.replace(R.id.FLmain, new SearchFragment(), fragments.SEARCH);
                    tokenFragment = fragments.SEARCH;
                }
                break;
        }

         if(!tokenFragment.equals(fragments.SEARCH)) {
            int count = manager.getBackStackEntryCount();
            if(count > 0) {
                int index = count - 1;
                String tag = manager.getBackStackEntryAt(index).getName();
                if(!tag.equals(tokenFragment)) {
                    transaction.addToBackStack(tokenFragment);
                }
            } else {
                transaction.addToBackStack(tokenFragment);
            }
        } else {
            cleanFragments();
        }
        transaction.commit();

    }

    //Funciòn cleandFrsgments
    /*
    Limpia todos los fragmentos que estan en la pila
    */
    public void cleanFragments() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        session.deleteFirstDoctor();
    }

    public void hasTabLayout(ClicDocs callback) {
        callback.initTabLayout(mTabLayout);
    }

    public void setActionBar(Toolbar TBmain, HashMap<String, String> options) {
        setSupportActionBar(TBmain);
        if(options != null) {
            for (Map.Entry<String, String> entry : options.entrySet()) {
                switch (entry.getKey()) {
                    case Constants.TBtitle:
                        setTitle(options.get(Constants.TBtitle));
                        break;
                    case Constants.TBhasBack:
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        break;
                }
            }
        }
    }

    private NavigationView.OnNavigationItemSelectedListener NVmenu_onClick = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.NVsearch:
                    setFragment(fragments.SEARCH, null);
                    break;
                case R.id.NVmydates:
                    setFragment(fragments.MYDATES, null);
                    break;
                case R.id.NVofficewait:
                    setFragment(fragments.OFFICEWAITLIST, null);
                    break;
                case R.id.NVhistoryclinic:
                    setFragment(fragments.HISTORYCLINIC, null);
                    break;
                case R.id.NVpills:
                    setFragment(fragments.PILLBOX, null);
                    break;
                case R.id.NVchat:
                    setFragment(fragments.CHAT, null);
                    break;
                case R.id.NVforos:
                    setFragment(fragments.FORUM, null);
                    break;
                case R.id.NVlinks:
                    setFragment(fragments.LINKS, null);
                    break;
                case R.id.NVfavorite:
                    setFragment(fragments.FAVORITEDOCOTORS, null);
                    break;
                case R.id.NVfamily:
                    setFragment(fragments.FAMILY, null);
                    break;
                case R.id.NVsetting:
                    setFragment(fragments.SETTINGS, null);
                    break;
                case R.id.NVacerca:
                    Intent clicdocs = new Intent(ctx, PrivacyCLICDOCSActivity.class);
                    clicdocs.putExtra(Constants.langString, langStrings);
                    startActivity(clicdocs);
                    break;
                case R.id.NVlogout:
                    DialogLogoutFragment logoutFragment = new DialogLogoutFragment();
                    logoutFragment.show(getSupportFragmentManager(),fragments.LOGOUT);
                    break;
                default:
                    Intent intento = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intento);
                    finish();
                    break;
            }
            if(!mDrawerLayout.isLocked()) mDrawerLayout.closeDrawer(NVmenu);
            return true;
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(!mDrawerLayout.isLocked()) mToggled.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!mDrawerLayout.isLocked()) {
            if (mToggled.onOptionsItemSelected(item)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener LLeditProfile_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setFragment(fragments.EDITPROFILE,null);
            if(!mDrawerLayout.isLocked()) mDrawerLayout.closeDrawer(NVmenu);
        }
    };

    public void setNameProfile (final String name){
        tvProfilePatinet.setText(name);
    }

    public void hideElements (){
        BTNregister.setVisibility(View.VISIBLE);
        BTNlogin.setVisibility(View.VISIBLE);
        tvProfilePatinet.setVisibility(View.INVISIBLE);
        LLprofile.setVisibility(View.INVISIBLE);
    }

    public void Login (){
        BTNregister.setVisibility(View.GONE);
        BTNlogin.setVisibility(View.GONE);
        tvProfilePatinet.setVisibility(View.VISIBLE);
        LLprofile.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        return super.onCreateOptionsMenu(menu);
    }

    public void showItem (){
        navigationMenu.getItem(1).setVisible(true);
        navigationMenu.getItem(2).setVisible(true);
        navigationMenu.getItem(3).setVisible(true);
        navigationMenu.getItem(4).setVisible(true);
        navigationProfile.getItem(0).setVisible(true);
        navigationProfile.getItem(1).setVisible(true);
        navigationProfile.getItem(2).setVisible(true);
        navigationProfile.getItem(3).setVisible(true);
        navigationSettings.getItem(0).setVisible(true);
        navigationSettings.getItem(4).setVisible(true);
    }

    public void hideItem (){
        navigationMenu.getItem(1).setVisible(false);
        navigationMenu.getItem(2).setVisible(false);
        navigationMenu.getItem(3).setVisible(false);
        navigationMenu.getItem(4).setVisible(false);
        navigationProfile.getItem(0).setVisible(false);
        navigationProfile.getItem(1).setVisible(false);
        navigationProfile.getItem(2).setVisible(false);
        navigationProfile.getItem(3).setVisible(false);
        navigationSettings.getItem(0).setVisible(false);
        navigationSettings.getItem(4).setVisible(false);
    }
    public void setImageProfile (final String url){

        if (!url.isEmpty()){
            Picasso.with(getApplicationContext()).
                    load(url).
                    resize(90,90).
                    centerCrop().
                    noFade().
                    into(CIVprofile);
        } else {
            Picasso.with(getApplicationContext()).
                    load(R.drawable.avatar_patient).
                    resize(90,90).
                    centerCrop().
                    noFade().
                    into(CIVprofile);
        }
    }

    //Funciòn ShowDoctorSelect
    /*
    Muestra una vista de un mèdico seleccionado, esta funcion de ejecuta cuando el usuario selecciona
     a un mèdico no afialido, y tiene la acciòn de poder remover al mèdico.
    */
    public void ShowDoctorSelected (){
        bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
        View parentView     = this.getLayoutInflater().inflate(R.layout.doctor_dialog_popup,null);
        TextView TVname     = (TextView) parentView.findViewById(R.id.TVname) ;
        TextView TVoffice   = (TextView) parentView.findViewById(R.id.TVnameOffice);
        TextView TVdateSelect = (TextView) parentView.findViewById(R.id.TVdateSelect);
        TextView TVhourSelect = (TextView) parentView.findViewById(R.id.TVhourSelect);
        Button BTNremove    = (Button) parentView.findViewById(R.id.BTNremove);
        bottomSheetDialog.setContentView(parentView);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,100,MainActivity.this.getResources().getDisplayMetrics()));
        bottomSheetDialog.show();
        TVname.setText(session.getFdoctor().get(1).toString());
        TVdateSelect.setText(session.getFdoctor().get(2).toString());
        TVhourSelect.setText(session.getFdoctor().get(3).toString());
        TVoffice.setText(session.getFdoctor().get(7).toString());
        BTNremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPopup.dismiss();
                session.deleteFirstDoctor();
                bottomSheetDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }
}
