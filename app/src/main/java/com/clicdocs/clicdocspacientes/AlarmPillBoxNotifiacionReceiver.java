package com.clicdocs.clicdocspacientes;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clicdocs.clicdocspacientes.database.DBhelper;
import com.clicdocs.clicdocspacientes.fragments.PillBoxFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;

public class AlarmPillBoxNotifiacionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int number = 0;
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        int _id = Integer.parseInt(intent.getStringExtra("idPill"));
        String medicine =intent.getStringExtra("medicine");
        String dose = intent.getStringExtra("dose");
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setWhen(System.currentTimeMillis());
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.mipmap.ic_notification);
            builder.setColor(context.getResources().getColor(R.color.blue));
        } else {
            builder.setSmallIcon(R.mipmap.ic_notification);
        }
        builder.setContentTitle(medicine);
        builder.setContentText(dose);
        builder.setContentInfo("info");
        builder.setSound(alarmSound);
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        DBhelper dBhelper = new DBhelper(context);
        SQLiteDatabase bd = dBhelper.getWritableDatabase();
        Cursor fila = bd.rawQuery("select id_pillbox,number_take from CD_pillbox_table where id_pillbox='"+_id+"'", null);
        if (fila.moveToFirst()) {
            String id = fila.getString(0);
            number = Integer.parseInt(fila.getString(1));
        }
        bd.close();

        DBhelper objBD = new DBhelper(context);
        SQLiteDatabase sql = objBD.getWritableDatabase();
        ContentValues updateRecord = new ContentValues();
        updateRecord.put("number_take", (number-1));
        int cant = sql.update("CD_pillbox_table", updateRecord, "id_pillbox='"+_id+"'", null);
        sql.close();
        if ((number-1) == 0){
            DBhelper dBhelperDelete = new DBhelper(context);
            SQLiteDatabase bdDelte = dBhelperDelete.getWritableDatabase();
            bdDelte.delete("CD_pillbox_table", "id_pillbox="+_id, null);
            bdDelte.close();
            Intent intento = new Intent(context, AlarmPillBoxNotifiacionReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(context, _id, intento, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.cancel(sender);
        }
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(_id,builder.build());
    }

}


