package com.clicdocs.clicdocspacientes.beans;


import java.io.Serializable;

public class SubthemesBeans implements Serializable {

    private String forum_id;
    private String topic_id;
    private String patient_id;
    private String subtheme;
    private String full_name;
    private String nickname;
    private String replies;
    private String views;
    private String timestamp;
    private String content;
    private String picture;

    public SubthemesBeans(String forum_id, String topic_id, String patient_id, String subtheme, String full_name, String nickname, String replies, String views, String timestamp, String content, String picture) {
        this.forum_id = forum_id;
        this.topic_id = topic_id;
        this.patient_id = patient_id;
        this.subtheme   = subtheme;
        this.full_name = full_name;
        this.nickname = nickname;
        this.replies = replies;
        this.views = views;
        this.timestamp = timestamp;
        this.content = content;
        this.picture = picture;
    }

    public String getForum_id() {
        return forum_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public String getSubtheme() {
        return subtheme;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getReplies() {
        return replies;
    }

    public String getViews() {
        return views;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getContent() {
        return content;
    }

    public String getPicture() {
        return picture;
    }
}
