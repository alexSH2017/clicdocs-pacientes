package com.clicdocs.clicdocspacientes.beans;


import java.io.Serializable;

/**
 * Created by Vicente on 30/07/2017.
 */

public class ModelBillData implements Serializable{

    private String id_bill, email_business, rfc,business_name, calle, num_ext, num_int, colony, zipcode,city, borough, state;

    public ModelBillData(String id_bill, String email_business, String rfc, String business_name, String calle, String num_ext, String num_int, String colony, String zipcode, String city, String borough, String state) {
        this.id_bill = id_bill;
        this.rfc = rfc;
        this.business_name = business_name;
        this.calle = calle;
        this.num_ext = num_ext;
        this.num_int = num_int;
        this.colony = colony;
        this.zipcode = zipcode;
        this.city = city;
        this.borough = borough;
        this.state = state;
        this.email_business = email_business;
    }

    public String getId_bill() {
        return id_bill;
    }

    public void setId_bill(String id_bill) {
        this.id_bill = id_bill;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNum_ext() {
        return num_ext;
    }

    public void setNum_ext(String num_ext) {
        this.num_ext = num_ext;
    }

    public String getNum_int() {
        return num_int;
    }

    public void setNum_int(String num_int) {
        this.num_int = num_int;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail_business() {
        return email_business;
    }

    public void setEmail_business(String email_business) {
        this.email_business = email_business;
    }
}

