package com.clicdocs.clicdocspacientes.beans;

import java.util.ArrayList;
import java.util.HashMap;


public class ModelFilters {

    private ArrayList<HashMap<String, String>> clicnics;
    private ArrayList<HashMap<String, String>> specialities;
    private ArrayList<HashMap<String, String>> services;
    private ArrayList<HashMap<String, String>> insurance;
    private ArrayList<HashMap<String, String>> states;
    private ArrayList<HashMap<String, String>> municipalties;
    private ArrayList<HashMap<String, String>> studies;

    public ArrayList<HashMap<String, String>> getClicnics() {
        return clicnics;
    }

    public void setClicnics(ArrayList<HashMap<String, String>> clicnics) {
        this.clicnics = clicnics;
    }

    public ArrayList<HashMap<String, String>> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<HashMap<String, String>> specialities) {
        this.specialities = specialities;
    }

    public ArrayList<HashMap<String, String>> getServices() {
        return services;
    }

    public void setServices(ArrayList<HashMap<String, String>> services) {
        this.services = services;
    }

    public ArrayList<HashMap<String, String>> getInsurance() {
        return insurance;
    }

    public void setInsurance(ArrayList<HashMap<String, String>> insurance) {
        this.insurance = insurance;
    }

    public ArrayList<HashMap<String, String>> getStates() {
        return states;
    }

    public void setStates(ArrayList<HashMap<String, String>> states) {
        this.states = states;
    }


    public void setMunicipalties(ArrayList<HashMap<String, String>> municipalties) {
        this.municipalties = municipalties;
    }

    public ArrayList<HashMap<String, String>> getStudies() {
        return studies;
    }

    public void setStudies(ArrayList<HashMap<String, String>> studies) {
        this.studies = studies;
    }
}
