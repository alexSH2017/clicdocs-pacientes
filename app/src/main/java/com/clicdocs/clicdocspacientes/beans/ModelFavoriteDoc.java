package com.clicdocs.clicdocspacientes.beans;


import java.util.ArrayList;

public class ModelFavoriteDoc {

    private String idSession,name, picture, id_favorite;
    private ArrayList<String> speciliaty;

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getId_favorite() {
        return id_favorite;
    }

    public void setId_favorite(String id_favorite) {
        this.id_favorite = id_favorite;
    }

    public ArrayList<String> getSpeciliaty() {
        return speciliaty;
    }

    public void setSpeciliaty(ArrayList<String> speciliaty) {
        this.speciliaty = speciliaty;
    }
}
