package com.clicdocs.clicdocspacientes.beans;


public class ModelInformationRoom {

    private String label, information;

    public ModelInformationRoom(String label, String information) {
        this.label = label;
        this.information = information;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }
}
