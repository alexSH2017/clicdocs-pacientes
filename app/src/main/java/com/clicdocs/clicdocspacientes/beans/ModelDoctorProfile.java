package com.clicdocs.clicdocspacientes.beans;


import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.ProfileDoctor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ModelDoctorProfile implements Serializable{

    private String name, picture, enable, ranking, favorite, f_amount, s_amount, major, university, professional_id, graduation;
    private ArrayList<HashMap<String,String>> speciality;
    private ArrayList<HashMap<String,String>> services;
    private ArrayList<String> advices;
    private ArrayList<String> insurance;
    private ArrayList<Offices> myoffices;
    public Offices off= new Offices();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getRanking() {
        return ranking;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getF_amount() {
        return f_amount;
    }

    public void setF_amount(String f_amount) {
        this.f_amount = f_amount;
    }

    public String getS_amount() {
        return s_amount;
    }

    public void setS_amount(String s_amount) {
        this.s_amount = s_amount;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getProfessional_id() {
        return professional_id;
    }

    public void setProfessional_id(String professional_id) {
        this.professional_id = professional_id;
    }

    public String getGraduation() {
        return graduation;
    }

    public void setGraduation(String graduation) {
        this.graduation = graduation;
    }

    public ArrayList<HashMap<String, String>> getSpeciality() {
        return speciality;
    }

    public ArrayList<String> getInsurance() {
        return insurance;
    }

    public void setInsurance(ArrayList<String> insurance) {
        this.insurance = insurance;
    }

    public void setSpeciality(ArrayList<HashMap<String, String>> speciality) {
        this.speciality = speciality;
    }

    public ArrayList<HashMap<String, String>> getServices() {
        return services;
    }

    public void setServices(ArrayList<HashMap<String, String>> services) {
        this.services = services;
    }

    public ArrayList<Offices> getMyoffices() {
        return myoffices;
    }

    public void setMyoffices(ArrayList<Offices> myoffices) {
        this.myoffices = myoffices;
    }

    public ArrayList<String> getAdvices() {
        return advices;
    }

    public void setAdvices(ArrayList<String> advices) {
        this.advices = advices;
    }

    public class Offices implements Serializable {
        private String office_id, name_office,full_address,state,borough, city,latitude,longitude;
        private ArrayList<String> open_time;
        private ArrayList<String> locked_time;

        public String getOffice_id() {
            return office_id;
        }

        public void setOffice_id(String office_id) {
            this.office_id = office_id;
        }

        public String getName_office() {
            return name_office;
        }

        public void setName_office(String name_office) {
            this.name_office = name_office;
        }

        public String getFull_address() {
            return full_address;
        }

        public void setFull_address(String full_address) {
            this.full_address = full_address;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getBorough() {
            return borough;
        }

        public void setBorough(String borough) {
            this.borough = borough;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public ArrayList<String> getOpen_time() {
            return open_time;
        }

        public void setOpen_time(ArrayList<String> open_time) {
            this.open_time = open_time;
        }

        public ArrayList<String> getLocked_time() {
            return locked_time;
        }

        public void setLocked_time(ArrayList<String> locked_time) {
            this.locked_time = locked_time;
        }
    }

}
