package com.clicdocs.clicdocspacientes.beans;


import java.util.ArrayList;

public class ModelOffices {

    private String office_id, office, full_address, clinic, country, state, borough, city, street, num_ext, num_int, colony, zipcode, latitude, longitude, first_amount, later_amount;
    private ArrayList<String> open_time;
    private ArrayList<String> locked_time;
    private ArrayList<String> pay_methods;

    public String getOffice_id() {
        return office_id;
    }

    public void setOffice_id(String office_id) {
        this.office_id = office_id;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNum_ext() {
        return num_ext;
    }

    public void setNum_ext(String num_ext) {
        this.num_ext = num_ext;
    }

    public String getNum_int() {
        return num_int;
    }

    public void setNum_int(String num_int) {
        this.num_int = num_int;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFirst_amount() {
        return first_amount;
    }

    public void setFirst_amount(String first_amount) {
        this.first_amount = first_amount;
    }

    public String getLater_amount() {
        return later_amount;
    }

    public void setLater_amount(String later_amount) {
        this.later_amount = later_amount;
    }

    public ArrayList<String> getOpen_time() {
        return open_time;
    }

    public void setOpen_time(ArrayList<String> open_time) {
        this.open_time = open_time;
    }

    public ArrayList<String> getLocked_time() {
        return locked_time;
    }

    public void setLocked_time(ArrayList<String> locked_time) {
        this.locked_time = locked_time;
    }

    public ArrayList<String> getPay_methods() {
        return pay_methods;
    }

    public void setPay_methods(ArrayList<String> pay_methods) {
        this.pay_methods = pay_methods;
    }
}
