package com.clicdocs.clicdocspacientes.beans;

import java.io.Serializable;


public class KeyPairsBean implements Serializable {

    private int key;
    private String value;
    public KeyPairsBean(int key, String value) {
        this.key   = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }



    @Override
    public String toString() {
        return value;
    }

}
