package com.clicdocs.clicdocspacientes.beans;


public class allinsuranceBean {
    private String id_insurance, insurance;
    public allinsuranceBean(String id_insurance, String insurance){
        this.id_insurance = id_insurance;
        this.insurance    = insurance;
    }

    public String getId_insurance() {
        return id_insurance;
    }

    public String getInsurance() {
        return insurance;
    }

    @Override
    public String toString() {
        return insurance;
    }
}
