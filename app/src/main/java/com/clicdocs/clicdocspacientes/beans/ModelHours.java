package com.clicdocs.clicdocspacientes.beans;


public class ModelHours {
    private int idHour;
    private String hour;
    private int flag;

    public ModelHours(int idHour, String hour, int flag) {
        this.idHour = idHour;
        this.hour = hour;
        this.flag = flag;
    }

    public int getIdHour() {
        return idHour;
    }

    public void setIdHour(int idHour) {
        this.idHour = idHour;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
