package com.clicdocs.clicdocspacientes.beans;


public class insuranceBean {
    private String id_insurance, insurance;
    public insuranceBean(String id_insurance, String insurance){
        this.id_insurance           = id_insurance;
        this.insurance              = insurance;
    }

    public String getId_insurance() {
        return id_insurance;
    }

    public String getInsurance() {
        return insurance;
    }
}
