package com.clicdocs.clicdocspacientes.beans;

/**
 * Created by apc_g on 10/11/2017.
 */

public class ActionsBeans {

    private String name, link, icon;

    public ActionsBeans(String name, String link, String icon) {
        this.name = name;
        this.link = link;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public String getIcon() {
        return icon;
    }
}
