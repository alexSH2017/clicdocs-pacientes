package com.clicdocs.clicdocspacientes.beans;


public class ModelAppoinmentWait {

    private String idFsession, idFoffice, fName, fDate, fHour, fPicture, idSsession, idSoffice, sName, sDate, sHour, sPicture;

    public ModelAppoinmentWait(String idFsession, String idFoffice, String fName, String fDate, String fHour, String fPicture, String idSsession, String idSoffice, String sName, String sDate, String sHour, String sPicture) {
        this.idFsession = idFsession;
        this.idFoffice = idFoffice;
        this.fName = fName;
        this.fDate = fDate;
        this.fHour = fHour;
        this.fPicture = fPicture;
        this.idSsession = idSsession;
        this.idSoffice = idSoffice;
        this.sName = sName;
        this.sDate = sDate;
        this.sHour = sHour;
        this.sPicture = sPicture;
    }

    public String getIdFsession() {
        return idFsession;
    }

    public void setIdFsession(String idFsession) {
        this.idFsession = idFsession;
    }

    public String getIdFoffice() {
        return idFoffice;
    }

    public void setIdFoffice(String idFoffice) {
        this.idFoffice = idFoffice;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getfDate() {
        return fDate;
    }

    public void setfDate(String fDate) {
        this.fDate = fDate;
    }

    public String getfHour() {
        return fHour;
    }

    public void setfHour(String fHour) {
        this.fHour = fHour;
    }

    public String getfPicture() {
        return fPicture;
    }

    public void setfPicture(String fPicture) {
        this.fPicture = fPicture;
    }

    public String getIdSsession() {
        return idSsession;
    }

    public void setIdSsession(String idSsession) {
        this.idSsession = idSsession;
    }

    public String getIdSoffice() {
        return idSoffice;
    }

    public void setIdSoffice(String idSoffice) {
        this.idSoffice = idSoffice;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsHour() {
        return sHour;
    }

    public void setsHour(String sHour) {
        this.sHour = sHour;
    }

    public String getsPicture() {
        return sPicture;
    }

    public void setsPicture(String sPicture) {
        this.sPicture = sPicture;
    }
}
