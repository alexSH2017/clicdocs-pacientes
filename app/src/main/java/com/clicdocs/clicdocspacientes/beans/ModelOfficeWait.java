package com.clicdocs.clicdocspacientes.beans;


import java.util.ArrayList;
import java.util.HashMap;

public class ModelOfficeWait {

    private String picture, id_waiting_list, doctor_name, appoinment_id, date, hour, office_name, street, outnumber,interiornumber,colony;

    private ArrayList<HashMap<String,String>> hours_released;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getId_waiting_list() {
        return id_waiting_list;
    }

    public void setId_waiting_list(String id_waiting_list) {
        this.id_waiting_list = id_waiting_list;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getAppoinment_id() {
        return appoinment_id;
    }

    public void setAppoinment_id(String appoinment_id) {
        this.appoinment_id = appoinment_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getOffice_name() {
        return office_name;
    }

    public void setOffice_name(String office_name) {
        this.office_name = office_name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getOutnumber() {
        return outnumber;
    }

    public void setOutnumber(String outnumber) {
        this.outnumber = outnumber;
    }

    public String getInteriornumber() {
        return interiornumber;
    }

    public void setInteriornumber(String interiornumber) {
        this.interiornumber = interiornumber;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public ArrayList<HashMap<String, String>> getHours_released() {
        return hours_released;
    }

    public void setHours_released(ArrayList<HashMap<String, String>> hours_released) {
        this.hours_released = hours_released;
    }
}
