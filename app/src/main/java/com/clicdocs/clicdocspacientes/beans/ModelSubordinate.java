package com.clicdocs.clicdocspacientes.beans;

public class ModelSubordinate  {
    public String id_sesion, picture, name, fname, mname;
    public boolean isSelected;

    public ModelSubordinate(String id_sesion, String picture, String name, boolean isSelected) {
        this.id_sesion = id_sesion;
        this.picture = picture;
        this.name = name;
        this.isSelected = isSelected;
    }

    public String getId_sesion() {
        return id_sesion;
    }

    public String getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getFname() {
        return fname;
    }

    public String getMname() {
        return mname;
    }

    public boolean isSelected() {
        return isSelected;
    }
}

