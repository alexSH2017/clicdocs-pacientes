package com.clicdocs.clicdocspacientes.beans;


import java.util.ArrayList;
import java.util.HashMap;

public class ModelIdentification {

    private String maritalStatus, origin, residence, ocupation, religion, scholarship;
    private ArrayList<HashMap<String, String>> address;

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public ArrayList<HashMap<String, String>> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<HashMap<String, String>> address) {
        this.address = address;
    }
}
