package com.clicdocs.clicdocspacientes.beans;

public class specialityBean {
    private String id_medic_speciality, name;
    public specialityBean(String id_medic_speciality, String name) {
        this.id_medic_speciality = id_medic_speciality;
        this.name                = name;
    }

    public String getId_medic_speciality() {
        return id_medic_speciality;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}

