package com.clicdocs.clicdocspacientes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.FakeX509TrustManager;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    private Context ctx;
    private SessionManager session;
    private RequestQueue queueLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx         = this;
        //Fabric.with(this, new Crashlytics());
        session = new SessionManager(ctx);
        //FakeX509TrustManager.allowAllSSL();
        getStrings(session.getLanguageID());
    }

    //Funciòn getString
    /*
    Obtiene un diccionario de strings para el cambio de idioma,
     se manda un paràmetro (1 = spanish, 2 = english) para obtener los string en su respectivo idioma.
    */
    public void getStrings (final String lan) {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueLanguage = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            //queueLanguage = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_language, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.v("printResponse",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray resp = jsonObject.getJSONArray(Constants.response);
                                ArrayList<LinkedHashMap<String, String>> langString = new ArrayList<>();
                                LinkedHashMap<String, String> lang = new LinkedHashMap<>();
                                for (int i = 0; i < resp.length(); i++) {
                                    Log.v(resp.getJSONArray(i).get(0).toString(), resp.getJSONArray(i).get(1).toString());
                                    lang.put(resp.getJSONArray(i).get(0).toString(),resp.getJSONArray(i).get(1).toString());
                                }

                                langString.add(lang);
                                Intent start = new Intent(SplashActivity.this, MainActivity.class);
                                start.putExtra(Constants.langString, langString.get(0));
                                startActivity(start);
                                finish();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.id_language, lan);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueLanguage.add(request);
        }  else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage("Error de conexión, verifique la conexión a Internet.")
                    .setTitle("CLICDOCS")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });
            builder.show();
        }

    }

}
