package com.clicdocs.clicdocspacientes.utils;


public class Doctors {

    String id_profile;
    String nameDoctor;
    String last_Father_Doctor;
    String last_Mother_Doctor;
    String specialities;
    String enabled;
    String image;

    public Doctors(String id_profile, String nameDoctor, String last_Father_Doctor, String last_Mother_Doctor, String specialities, String enabled, String image) {
        this.id_profile = id_profile;
        this.nameDoctor = nameDoctor;
        this.last_Father_Doctor = last_Father_Doctor;
        this.last_Mother_Doctor = last_Mother_Doctor;
        this.specialities = specialities;
        this.enabled = enabled;
        this.image = image;
    }

    public String getId_profile() {
        return id_profile;
    }

    public void setId_profile(String id_profile) {
        this.id_profile = id_profile;
    }

    public String getNameDoctor() {
        return nameDoctor;
    }

    public void setNameDoctor(String nameDoctor) {
        this.nameDoctor = nameDoctor;
    }

    public String getLast_Father_Doctor() {
        return last_Father_Doctor;
    }

    public void setLast_Father_Doctor(String last_Father_Doctor) {
        this.last_Father_Doctor = last_Father_Doctor;
    }

    public String getLast_Mother_Doctor() {
        return last_Mother_Doctor;
    }

    public void setLast_Mother_Doctor(String last_Mother_Doctor) {
        this.last_Mother_Doctor = last_Mother_Doctor;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

