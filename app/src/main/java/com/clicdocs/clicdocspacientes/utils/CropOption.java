package com.clicdocs.clicdocspacientes.utils;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by Vicente on 26/07/2017.
 */

public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
