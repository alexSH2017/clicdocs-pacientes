package com.clicdocs.clicdocspacientes.utils;


import java.io.Serializable;

public class Pillbox implements Serializable{

    private String idPill, medicine_name, dose, measurement, frequency, duration, note, enable, status, origin;

    public Pillbox(String idPill, String medicine_name, String dose, String measurement, String frequency, String duration, String note, String enable, String status,String origin) {
        this.idPill = idPill;
        this.medicine_name = medicine_name;
        this.dose = dose;
        this.measurement = measurement;
        this.frequency = frequency;
        this.duration = duration;
        this.note = note;
        this.enable = enable;
        this.status = status;
        this.origin = origin;
    }

    public String getIdPill() {
        return idPill;
    }

    public String getMedicine_name() {
        return medicine_name;
    }

    public String getDose() {
        return dose;
    }

    public String getMeasurement() {
        return measurement;
    }

    public String getFrequency() {
        return frequency;
    }

    public String getDuration() {
        return duration;
    }

    public String getNote() {
        return note;
    }

    public String getEnable() {
        return enable;
    }

    public String getStatus() {
        return status;
    }

    public String getOrigin() {
        return origin;
    }
}
