package com.clicdocs.clicdocspacientes.utils;


import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SessionManager {
    private final String APP_PREF               = "com.clicdocs.patients";
    private final String LANGUAGE               = "language";
    private final String SESSION_S              = "sesion_s";
    private final String PROFILE_S              = "profile_s";
    private final String NAME_S                 = "name_s";
    private final String PASS_S                 = "pass_s";
    private final String PICTURE_S              = "picture";
    private final String PANIC_TELEPHONE        = "panic_telephone";
    private final String USERNAME               = "username";
    private final String SERVER                 = "server";
    private final String DEVICE_TOKEN           = "token_device";

    //private final String pic = " https://scontent.xx.fbcdn.net/v/t1.0-1/c1.0.480.480/p480x480/15578821_10207654963508691_7520571533853241687_n.jpg?oh=ccc2c7128a4ec6ba4619aec66118103d&oe=5A1645A6";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context ctx;


    public SessionManager(Context ctx) {
        this.ctx = ctx;
    }

    public void setLogin(String idSession, String idProfile, String name, String pass, String photo, String token) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(Constants.idsession, idSession);
        editor.putString(Constants.idprofile, idProfile);
        editor.putString(Constants.name, name);
        editor.putString(Constants.password, pass);
        editor.putString(Constants.photo, photo);
        editor.putString("token", token);
        editor.commit();
    }

    public void secondSession(String idSession, String idProfile, String name, String pass, String photo, String token) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(SESSION_S, idSession);
        editor.putString(PROFILE_S, idProfile);
        editor.putString(NAME_S, name);
        editor.putString(PASS_S, pass);
        editor.putString(PICTURE_S, photo);
        editor.putString("token_s", token);
        editor.commit();
    }

    public void firstDoctor(String idSession, String name, String date, String hour, String idOffice, String enable, String picture, String address) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("f_session", idSession);
        editor.putString("f_name", name);
        editor.putString("f_date", date);
        editor.putString("f_hour", hour);
        editor.putString("f_idOffice", idOffice);
        editor.putString("f_enable", enable);
        editor.putString("f_picture", picture);
        editor.putString("f_address", address);
        editor.commit();
    }

    public void deleteFirstDoctor (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove("f_session");
        editor.remove("f_name");
        editor.remove("f_date");
        editor.remove("f_hour");
        editor.remove("f_idOffice");
        editor.remove("f_enable");
        editor.remove("f_picture");
        editor.remove("f_address");
        editor.apply();
    }

    public ArrayList getFdoctor () {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        ArrayList<String> list = new ArrayList<String>();
        list.add(preferences.getString("f_session", null));  //0
        list.add(preferences.getString("f_name", null));      //1
        list.add(preferences.getString("f_date", null));     //2
        list.add(preferences.getString("f_hour", null));     //3
        list.add(preferences.getString("f_idOffice", null)); //4
        list.add(preferences.getString("f_enable", null));   //5
        list.add(preferences.getString("f_picture", null));   //6
        list.add(preferences.getString("f_address", null));   //7
        return list;
    }

    public void setSearch(String string, String state) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("string", string);
        editor.putString("state", state);
        editor.commit();
    }

    public HashMap<String, String> getSearch() {
        preferences  = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        String string = preferences.getString("string", null);
        String state  = preferences.getString("state", null);
        if(string != null && state != null) {
            HashMap<String, String> data = new HashMap<>();
            data.put("string", string);
            data.put("state", state);
            return data;
        } else {
            return null;
        }
    }

    public void setPanicTelephone (String telephone, String origen){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(PANIC_TELEPHONE, telephone);
        editor.putString("origen", origen);
        editor.commit();
    }

    public ArrayList<String> getPanicTelephone() {
        ArrayList<String> list = new ArrayList<String>();
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        list.add(preferences.getString(PANIC_TELEPHONE, null));  //0
        list.add(preferences.getString("origen", "0"));      //1
        return list;
    }

    public void deletePanicTelehone (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(PANIC_TELEPHONE);
        editor.apply();
    }

    // ===================  DATA CHAT ==========================
    public void setChat (String username, String server){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(USERNAME, username);
        editor.putString(SERVER, server);
        editor.commit();
    }

    public ArrayList<String> getChat() {
        ArrayList<String> list = new ArrayList<String>();
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        list.add(preferences.getString(USERNAME, null));  //0
        list.add(preferences.getString(SERVER, null));      //1
        return list;
    }

    public void deleteChat (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(USERNAME);
        editor.remove(SERVER);
        editor.apply();
    }
    // =========================================================

    public String isLogged() {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        String id = preferences.getString(Constants.idsession, null);
        return id;
    }

    public ArrayList profile () {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        ArrayList<String> list = new ArrayList<String>();
        list.add(preferences.getString(Constants.idsession, null));  //0
        list.add(preferences.getString(Constants.idprofile, null));  //1
        list.add(preferences.getString(Constants.name, null));       //2
        list.add(preferences.getString(Constants.password, null));   //3
        list.add(preferences.getString(Constants.photo, null));      //4
        list.add(preferences.getString("token", null));              //5
        return list;
    }

    public ArrayList secondProfile () {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        ArrayList<String> list = new ArrayList<String>();
        list.add(preferences.getString(SESSION_S, null));  //0
        list.add(preferences.getString(PROFILE_S, null));  //1
        list.add(preferences.getString(NAME_S, null));     //2
        list.add(preferences.getString(PASS_S, null));     //3
        list.add(preferences.getString(PICTURE_S, null));  //4
        list.add(preferences.getString("token_s", null));  //5
        return list;
    }

    public void deleteProfile (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(Constants.idsession);
        editor.remove(Constants.idprofile);
        editor.remove(Constants.password);
        editor.remove(Constants.name);
        editor.remove(Constants.photo);
        editor.remove("token");
        editor.apply();
    }

    public void deleteSecondProfile (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(SESSION_S);
        editor.remove(PROFILE_S);
        editor.remove(NAME_S);
        editor.remove(PASS_S);
        editor.remove(PICTURE_S);
        editor.remove("token_s");
        editor.apply();
    }

    public void setLanguageID(String ID) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(LANGUAGE, ID);
        editor.commit();
    }

    public String getLanguageID() {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        return preferences.getString(LANGUAGE, "1");
    }

    //----------------DEVICE TOKEN ------------------
    public void setTokenDevice(String ID) {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(DEVICE_TOKEN, ID);
        editor.commit();
    }

    public String getTokenDevice() {
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        return preferences.getString(DEVICE_TOKEN, null);
    }

    public void deleteTokenDevice (){
        preferences = ctx.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(DEVICE_TOKEN);
        editor.apply();
    }


}
