package com.clicdocs.clicdocspacientes.utils;


public class Links {

    String idLink;
    String imagen;
    String title;
    String description;
    String link;

    public Links(String idLink, String imagen, String title, String description, String link) {
        this.idLink = idLink;
        this.imagen = imagen;
        this.title = title;
        this.description = description;
        this.link = link;
    }

    public String getIdLink() {
        return idLink;
    }

    public void setIdLink(String idLink) {
        this.idLink = idLink;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
