package com.clicdocs.clicdocspacientes.utils;


public class Forums {

    String idTheme;
    String imagen;
    String theme;


    public Forums(String idTheme, String imagen, String theme) {
        this.idTheme = idTheme;
        this.imagen = imagen;
        this.theme = theme;
    }

    public String getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(String idTheme) {
        this.idTheme = idTheme;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
