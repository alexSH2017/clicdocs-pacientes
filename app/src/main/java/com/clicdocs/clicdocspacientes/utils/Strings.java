package com.clicdocs.clicdocspacientes.utils;


import java.io.Serializable;

public class Strings implements Serializable{
    private String Itsearch,ITclinicHistory,ITmyAppoinments,Itpillbox,Itforums,ITlistInterest,Itlogout,addsubthem,filters,
                   shedule,save,TscheduleAppoinment,TprofileDoctor,Trooms,Tdetails,Tservices,Tspecialties,Tscholarship,
                   TsubThemes,Tpost,TrecoveryPass,name,fname,mname,nickname,phone,cellphone,gender,birthday,email,password,
                   confipass,language,legenSearch,dateappoinment,hourappoinment,passcurrent,newpass,maritalstatus,origin,
                   live,colony,street,numext,numintoptional,zipcode,ocupation,religion,editprofile,personalphoto,personaldata,contact,
                   changepass,settings,phoneoptional,enterlocation,favoritedoctors,login,forgotpass,newaccount,createaccount,
                   recover,cancel,legendrecovery,accept,identificationfile,heredofamily,patholofical,nopathological,identification,
                   waitappoinment,processappoinment,approvedappoinment,finalizedappoinment,rejectappoinment,popularsubthemes,
                   othressubthemes,opinion,pharmacies,results,searchrecords,status,affiliate,noaffiliate,notice,legendnoaffiliate,
                   pleasewait,legendaffiliate,firstmedic,secondmedic,legendnooffices,legendnohours,legenbusyhouraffiliate,selecthour,
                   equalshours,completfirstmedic,loadin,concreatappoinment,noconnection,noofficehours,busyhournoaffliate,selectphoto,
                   selected,photosuccessful,passnomatch,eightcharacters,incorrectpass,requiredadult,requieredtencharacters,selectgender,
                   perfilsuccessful,changepasssuccessful,selectcivilstatus,successfulidentification,diabetes,hepatopatia,asma,endocrinas,
                   hipertension,nefropatia,cancer,cardiopatia,mentales,alergicas,negados,othres,diseases,successfulfamily,past,diseasescurrent,
                   surgical,transfusion,allergies,traumatics,hospital,adiccion,cleanliness,shower,toothbrush,room,vices,smooking,yearsmooking,
                   beer,feeding,foodday,qualityfeed,sports,immunizations,nofound,finalize,reschedule,approved,completed,mymedicine,takecompleted,
                   shotsremaining,startshots,dose,frequency,duration,medicinename,note,erroduration,selectfrequency,newmedicine,layoffmedicine,
                   deletemedicine,nextshots,legendlogout,billdata, filedrequiered, selectoption, legendslogan, legenddologing, connecting, onlyletters,
                   onlynumbers, invalidemail, reasonsoacil, onlylettersnumbers,vrequired, vtext, vnumber, vemail,academictraining,college,
                   professionalid,yearsexperience,dategraduation,education,undefined, medicationsus, recipe, firstdate,subsequent, doctor,
                   notdefinedspecialities,specialityin,forumtopic, title, publication,description, newsubtopic, addpill, medicinebydoctor,
                   medicinesucceful, editmedicine, serandloc, studies,academic, country, municipality,state,reason, fiscal, numint,savechanges, changelan,
                   spanish,english,nosubtheme, nocomment, male, female,accountsuccessful, emailopassincorrect, recoverycomplete,emailnoexist,
                   identificationsuccessful, datanoupdate,nogetagend,diseasesexit, docontinue, addfavorite, deletefavorite, addfavoritesuccessful,
                   deletefavoritesuccessful, requiredlogin,errorlenguage,add, family, legendchangepass,change,realeaseaccount,release,switched,
                   notswitched,couldgetlist, process, newaccountfamily, requiredmainaccount, father, mother, both, any, notupdateinformation,
                   informationupdate, deworming, single, married, divorced,widow, cancelappoinment, docancelappoinment, reject,dorescheduleappoinment,
                   rescheduleappoinment, capsule, tablet,mililiter, ampoule,shot, completeage, pending,photoerror, daily, thirdday, irregular, map,
                   onceday, twoday, threeday, urban, rural, allservices, latrine, good, regular, bad, wait, approveappoinment, mnameoptional;

    public String getItsearch() {
        return Itsearch;
    }

    public void setItsearch(String itsearch) {
        Itsearch = itsearch;
    }

    public String getITclinicHistory() {
        return ITclinicHistory;
    }

    public void setITclinicHistory(String ITclinicHistory) {
        this.ITclinicHistory = ITclinicHistory;
    }

    public String getITmyAppoinments() {
        return ITmyAppoinments;
    }

    public void setITmyAppoinments(String ITmyAppoinments) {
        this.ITmyAppoinments = ITmyAppoinments;
    }

    public String getItpillbox() {
        return Itpillbox;
    }

    public void setItpillbox(String itpillbox) {
        Itpillbox = itpillbox;
    }

    public String getItforums() {
        return Itforums;
    }

    public void setItforums(String itforums) {
        Itforums = itforums;
    }

    public String getITlistInterest() {
        return ITlistInterest;
    }

    public void setITlistInterest(String ITlistInterest) {
        this.ITlistInterest = ITlistInterest;
    }

    public String getItlogout() {
        return Itlogout;
    }

    public void setItlogout(String itlogout) {
        Itlogout = itlogout;
    }

    public String getAddsubthem() {
        return addsubthem;
    }

    public void setAddsubthem(String addsubthem) {
        this.addsubthem = addsubthem;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public String getShedule() {
        return shedule;
    }

    public void setShedule(String shedule) {
        this.shedule = shedule;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getTscheduleAppoinment() {
        return TscheduleAppoinment;
    }

    public void setTscheduleAppoinment(String tscheduleAppoinment) {
        TscheduleAppoinment = tscheduleAppoinment;
    }

    public String getTprofileDoctor() {
        return TprofileDoctor;
    }

    public void setTprofileDoctor(String tprofileDoctor) {
        TprofileDoctor = tprofileDoctor;
    }

    public String getTrooms() {
        return Trooms;
    }

    public void setTrooms(String trooms) {
        Trooms = trooms;
    }

    public String getTdetails() {
        return Tdetails;
    }

    public void setTdetails(String tdetails) {
        Tdetails = tdetails;
    }

    public String getTservices() {
        return Tservices;
    }

    public void setTservices(String tservices) {
        Tservices = tservices;
    }

    public String getTspecialties() {
        return Tspecialties;
    }

    public void setTspecialties(String tspecialties) {
        Tspecialties = tspecialties;
    }

    public String getTscholarship() {
        return Tscholarship;
    }

    public void setTscholarship(String tscholarship) {
        Tscholarship = tscholarship;
    }

    public String getTsubThemes() {
        return TsubThemes;
    }

    public void setTsubThemes(String tsubThemes) {
        TsubThemes = tsubThemes;
    }

    public String getTpost() {
        return Tpost;
    }

    public void setTpost(String tpost) {
        Tpost = tpost;
    }

    public String getTrecoveryPass() {
        return TrecoveryPass;
    }

    public void setTrecoveryPass(String trecoveryPass) {
        TrecoveryPass = trecoveryPass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfipass() {
        return confipass;
    }

    public void setConfipass(String confipass) {
        this.confipass = confipass;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLegenSearch() {
        return legenSearch;
    }

    public void setLegenSearch(String legenSearch) {
        this.legenSearch = legenSearch;
    }

    public String getDateappoinment() {
        return dateappoinment;
    }

    public void setDateappoinment(String dateappoinment) {
        this.dateappoinment = dateappoinment;
    }

    public String getHourappoinment() {
        return hourappoinment;
    }

    public void setHourappoinment(String hourappoinment) {
        this.hourappoinment = hourappoinment;
    }

    public String getPasscurrent() {
        return passcurrent;
    }

    public void setPasscurrent(String passcurrent) {
        this.passcurrent = passcurrent;
    }

    public String getNewpass() {
        return newpass;
    }

    public void setNewpass(String newpass) {
        this.newpass = newpass;
    }

    public String getMaritalstatus() {
        return maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        this.maritalstatus = maritalstatus;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumext() {
        return numext;
    }

    public void setNumext(String numext) {
        this.numext = numext;
    }

    public String getNumintoptional() {
        return numintoptional;
    }

    public void setNumintoptional(String numintoptional) {
        this.numintoptional = numintoptional;
    }

    public String getFiscal() {
        return fiscal;
    }

    public void setFiscal(String fiscal) {
        this.fiscal = fiscal;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEditprofile() {
        return editprofile;
    }

    public void setEditprofile(String editprofile) {
        this.editprofile = editprofile;
    }

    public String getPersonalphoto() {
        return personalphoto;
    }

    public void setPersonalphoto(String personalphoto) {
        this.personalphoto = personalphoto;
    }

    public String getPersonaldata() {
        return personaldata;
    }

    public void setPersonaldata(String personaldata) {
        this.personaldata = personaldata;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getChangepass() {
        return changepass;
    }

    public void setChangepass(String changepass) {
        this.changepass = changepass;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public String getPhoneoptional() {
        return phoneoptional;
    }

    public void setPhoneoptional(String phoneoptional) {
        this.phoneoptional = phoneoptional;
    }

    public String getEnterlocation() {
        return enterlocation;
    }

    public void setEnterlocation(String enterlocation) {
        this.enterlocation = enterlocation;
    }

    public String getFavoritedoctors() {
        return favoritedoctors;
    }

    public void setFavoritedoctors(String favoritedoctors) {
        this.favoritedoctors = favoritedoctors;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getForgotpass() {
        return forgotpass;
    }

    public void setForgotpass(String forgotpass) {
        this.forgotpass = forgotpass;
    }

    public String getNewaccount() {
        return newaccount;
    }

    public void setNewaccount(String newaccount) {
        this.newaccount = newaccount;
    }

    public String getCreateaccount() {
        return createaccount;
    }

    public void setCreateaccount(String createaccount) {
        this.createaccount = createaccount;
    }

    public String getRecover() {
        return recover;
    }

    public void setRecover(String recover) {
        this.recover = recover;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getLegendrecovery() {
        return legendrecovery;
    }

    public void setLegendrecovery(String legendrecovery) {
        this.legendrecovery = legendrecovery;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getIdentificationfile() {
        return identificationfile;
    }

    public void setIdentificationfile(String identificationfile) {
        this.identificationfile = identificationfile;
    }

    public String getHeredofamily() {
        return heredofamily;
    }

    public void setHeredofamily(String heredofamily) {
        this.heredofamily = heredofamily;
    }

    public String getPatholofical() {
        return patholofical;
    }

    public void setPatholofical(String patholofical) {
        this.patholofical = patholofical;
    }

    public String getNopathological() {
        return nopathological;
    }

    public void setNopathological(String nopathological) {
        this.nopathological = nopathological;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getWaitappoinment() {
        return waitappoinment;
    }

    public void setWaitappoinment(String waitappoinment) {
        this.waitappoinment = waitappoinment;
    }

    public String getProcessappoinment() {
        return processappoinment;
    }

    public void setProcessappoinment(String processappoinment) {
        this.processappoinment = processappoinment;
    }

    public String getApprovedappoinment() {
        return approvedappoinment;
    }

    public void setApprovedappoinment(String approvedappoinment) {
        this.approvedappoinment = approvedappoinment;
    }

    public String getFinalizedappoinment() {
        return finalizedappoinment;
    }

    public void setFinalizedappoinment(String finalizedappoinment) {
        this.finalizedappoinment = finalizedappoinment;
    }

    public String getRejectappoinment() {
        return rejectappoinment;
    }

    public void setRejectappoinment(String rejectappoinment) {
        this.rejectappoinment = rejectappoinment;
    }

    public String getPopularsubthemes() {
        return popularsubthemes;
    }

    public void setPopularsubthemes(String popularsubthemes) {
        this.popularsubthemes = popularsubthemes;
    }

    public String getOthressubthemes() {
        return othressubthemes;
    }

    public void setOthressubthemes(String othressubthemes) {
        this.othressubthemes = othressubthemes;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public String getPharmacies() {
        return pharmacies;
    }

    public void setPharmacies(String pharmacies) {
        this.pharmacies = pharmacies;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getSearchrecords() {
        return searchrecords;
    }

    public void setSearchrecords(String searchrecords) {
        this.searchrecords = searchrecords;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAffiliate() {
        return affiliate;
    }

    public void setAffiliate(String affiliate) {
        this.affiliate = affiliate;
    }

    public String getNoaffiliate() {
        return noaffiliate;
    }

    public void setNoaffiliate(String noaffiliate) {
        this.noaffiliate = noaffiliate;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getLegendnoaffiliate() {
        return legendnoaffiliate;
    }

    public void setLegendnoaffiliate(String legendnoaffiliate) {
        this.legendnoaffiliate = legendnoaffiliate;
    }

    public String getPleasewait() {
        return pleasewait;
    }

    public void setPleasewait(String pleasewait) {
        this.pleasewait = pleasewait;
    }

    public String getLegendaffiliate() {
        return legendaffiliate;
    }

    public void setLegendaffiliate(String legendaffiliate) {
        this.legendaffiliate = legendaffiliate;
    }

    public String getFirstmedic() {
        return firstmedic;
    }

    public void setFirstmedic(String firstmedic) {
        this.firstmedic = firstmedic;
    }

    public String getSecondmedic() {
        return secondmedic;
    }

    public void setSecondmedic(String secondmedic) {
        this.secondmedic = secondmedic;
    }

    public String getLegendnooffices() {
        return legendnooffices;
    }

    public void setLegendnooffices(String legendnooffices) {
        this.legendnooffices = legendnooffices;
    }

    public String getLegendnohours() {
        return legendnohours;
    }

    public void setLegendnohours(String legendnohours) {
        this.legendnohours = legendnohours;
    }

    public String getLegenbusyhouraffiliate() {
        return legenbusyhouraffiliate;
    }

    public void setLegenbusyhouraffiliate(String legenbusyhouraffiliate) {
        this.legenbusyhouraffiliate = legenbusyhouraffiliate;
    }

    public String getSelecthour() {
        return selecthour;
    }

    public void setSelecthour(String selecthour) {
        this.selecthour = selecthour;
    }

    public String getEqualshours() {
        return equalshours;
    }

    public void setEqualshours(String equalshours) {
        this.equalshours = equalshours;
    }

    public String getCompletfirstmedic() {
        return completfirstmedic;
    }

    public void setCompletfirstmedic(String completfirstmedic) {
        this.completfirstmedic = completfirstmedic;
    }

    public String getLoadin() {
        return loadin;
    }

    public void setLoadin(String loadin) {
        this.loadin = loadin;
    }

    public String getConcreatappoinment() {
        return concreatappoinment;
    }

    public void setConcreatappoinment(String concreatappoinment) {
        this.concreatappoinment = concreatappoinment;
    }

    public String getNoconnection() {
        return noconnection;
    }

    public void setNoconnection(String noconnection) {
        this.noconnection = noconnection;
    }

    public String getNoofficehours() {
        return noofficehours;
    }

    public void setNoofficehours(String noofficehours) {
        this.noofficehours = noofficehours;
    }

    public String getBusyhournoaffliate() {
        return busyhournoaffliate;
    }

    public void setBusyhournoaffliate(String busyhournoaffliate) {
        this.busyhournoaffliate = busyhournoaffliate;
    }

    public String getSelectphoto() {
        return selectphoto;
    }

    public void setSelectphoto(String selectphoto) {
        this.selectphoto = selectphoto;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getPhotosuccessful() {
        return photosuccessful;
    }

    public void setPhotosuccessful(String photosuccessful) {
        this.photosuccessful = photosuccessful;
    }

    public String getPassnomatch() {
        return passnomatch;
    }

    public void setPassnomatch(String passnomatch) {
        this.passnomatch = passnomatch;
    }

    public String getEightcharacters() {
        return eightcharacters;
    }

    public void setEightcharacters(String eightcharacters) {
        this.eightcharacters = eightcharacters;
    }

    public String getIncorrectpass() {
        return incorrectpass;
    }

    public void setIncorrectpass(String incorrectpass) {
        this.incorrectpass = incorrectpass;
    }

    public String getRequiredadult() {
        return requiredadult;
    }

    public void setRequiredadult(String requiredadult) {
        this.requiredadult = requiredadult;
    }

    public String getRequieredtencharacters() {
        return requieredtencharacters;
    }

    public void setRequieredtencharacters(String requieredtencharacters) {
        this.requieredtencharacters = requieredtencharacters;
    }

    public String getSelectgender() {
        return selectgender;
    }

    public void setSelectgender(String selectgender) {
        this.selectgender = selectgender;
    }

    public String getPerfilsuccessful() {
        return perfilsuccessful;
    }

    public void setPerfilsuccessful(String perfilsuccessful) {
        this.perfilsuccessful = perfilsuccessful;
    }

    public String getChangepasssuccessful() {
        return changepasssuccessful;
    }

    public void setChangepasssuccessful(String changepasssuccessful) {
        this.changepasssuccessful = changepasssuccessful;
    }

    public String getSelectcivilstatus() {
        return selectcivilstatus;
    }

    public void setSelectcivilstatus(String selectcivilstatus) {
        this.selectcivilstatus = selectcivilstatus;
    }

    public String getSuccessfulidentification() {
        return successfulidentification;
    }

    public void setSuccessfulidentification(String successfulidentification) {
        this.successfulidentification = successfulidentification;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public String getHepatopatia() {
        return hepatopatia;
    }

    public void setHepatopatia(String hepatopatia) {
        this.hepatopatia = hepatopatia;
    }

    public String getAsma() {
        return asma;
    }

    public void setAsma(String asma) {
        this.asma = asma;
    }

    public String getEndocrinas() {
        return endocrinas;
    }

    public void setEndocrinas(String endocrinas) {
        this.endocrinas = endocrinas;
    }

    public String getHipertension() {
        return hipertension;
    }

    public void setHipertension(String hipertension) {
        this.hipertension = hipertension;
    }

    public String getNefropatia() {
        return nefropatia;
    }

    public void setNefropatia(String nefropatia) {
        this.nefropatia = nefropatia;
    }

    public String getCancer() {
        return cancer;
    }

    public void setCancer(String cancer) {
        this.cancer = cancer;
    }

    public String getCardiopatia() {
        return cardiopatia;
    }

    public void setCardiopatia(String cardiopatia) {
        this.cardiopatia = cardiopatia;
    }

    public String getMentales() {
        return mentales;
    }

    public void setMentales(String mentales) {
        this.mentales = mentales;
    }

    public String getAlergicas() {
        return alergicas;
    }

    public void setAlergicas(String alergicas) {
        this.alergicas = alergicas;
    }

    public String getNegados() {
        return negados;
    }

    public void setNegados(String negados) {
        this.negados = negados;
    }

    public String getOthres() {
        return othres;
    }

    public void setOthres(String othres) {
        this.othres = othres;
    }

    public String getDiseases() {
        return diseases;
    }

    public void setDiseases(String diseases) {
        this.diseases = diseases;
    }

    public String getSuccessfulfamily() {
        return successfulfamily;
    }

    public void setSuccessfulfamily(String successfulfamily) {
        this.successfulfamily = successfulfamily;
    }

    public String getPast() {
        return past;
    }

    public void setPast(String past) {
        this.past = past;
    }

    public String getDiseasescurrent() {
        return diseasescurrent;
    }

    public void setDiseasescurrent(String diseasescurrent) {
        this.diseasescurrent = diseasescurrent;
    }

    public String getSurgical() {
        return surgical;
    }

    public void setSurgical(String surgical) {
        this.surgical = surgical;
    }

    public String getTransfusion() {
        return transfusion;
    }

    public void setTransfusion(String transfusion) {
        this.transfusion = transfusion;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getTraumatics() {
        return traumatics;
    }

    public void setTraumatics(String traumatics) {
        this.traumatics = traumatics;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getAdiccion() {
        return adiccion;
    }

    public void setAdiccion(String adiccion) {
        this.adiccion = adiccion;
    }

    public String getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(String cleanliness) {
        this.cleanliness = cleanliness;
    }

    public String getShower() {
        return shower;
    }

    public void setShower(String shower) {
        this.shower = shower;
    }

    public String getToothbrush() {
        return toothbrush;
    }

    public void setToothbrush(String toothbrush) {
        this.toothbrush = toothbrush;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getVices() {
        return vices;
    }

    public void setVices(String vices) {
        this.vices = vices;
    }

    public String getSmooking() {
        return smooking;
    }

    public void setSmooking(String smooking) {
        this.smooking = smooking;
    }

    public String getYearsmooking() {
        return yearsmooking;
    }

    public void setYearsmooking(String yearsmooking) {
        this.yearsmooking = yearsmooking;
    }

    public String getBeer() {
        return beer;
    }

    public void setBeer(String beer) {
        this.beer = beer;
    }

    public String getFeeding() {
        return feeding;
    }

    public void setFeeding(String feeding) {
        this.feeding = feeding;
    }

    public String getFoodday() {
        return foodday;
    }

    public void setFoodday(String foodday) {
        this.foodday = foodday;
    }

    public String getQualityfeed() {
        return qualityfeed;
    }

    public void setQualityfeed(String qualityfeed) {
        this.qualityfeed = qualityfeed;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getImmunizations() {
        return immunizations;
    }

    public void setImmunizations(String immunizations) {
        this.immunizations = immunizations;
    }

    public String getNofound() {
        return nofound;
    }

    public void setNofound(String nofound) {
        this.nofound = nofound;
    }

    public String getFinalize() {
        return finalize;
    }

    public void setFinalize(String finalize) {
        this.finalize = finalize;
    }

    public String getReschedule() {
        return reschedule;
    }

    public void setReschedule(String reschedule) {
        this.reschedule = reschedule;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getMymedicine() {
        return mymedicine;
    }

    public void setMymedicine(String mymedicine) {
        this.mymedicine = mymedicine;
    }

    public String getTakecompleted() {
        return takecompleted;
    }

    public void setTakecompleted(String takecompleted) {
        this.takecompleted = takecompleted;
    }

    public String getShotsremaining() {
        return shotsremaining;
    }

    public void setShotsremaining(String shotsremaining) {
        this.shotsremaining = shotsremaining;
    }

    public String getStartshots() {
        return startshots;
    }

    public void setStartshots(String startshots) {
        this.startshots = startshots;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMedicinename() {
        return medicinename;
    }

    public void setMedicinename(String medicinename) {
        this.medicinename = medicinename;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getErroduration() {
        return erroduration;
    }

    public void setErroduration(String erroduration) {
        this.erroduration = erroduration;
    }

    public String getSelectfrequency() {
        return selectfrequency;
    }

    public void setSelectfrequency(String selectfrequency) {
        this.selectfrequency = selectfrequency;
    }

    public String getNewmedicine() {
        return newmedicine;
    }

    public void setNewmedicine(String newmedicine) {
        this.newmedicine = newmedicine;
    }

    public String getLayoffmedicine() {
        return layoffmedicine;
    }

    public void setLayoffmedicine(String layoffmedicine) {
        this.layoffmedicine = layoffmedicine;
    }

    public String getDeletemedicine() {
        return deletemedicine;
    }

    public void setDeletemedicine(String deletemedicine) {
        this.deletemedicine = deletemedicine;
    }

    public String getNextshots() {
        return nextshots;
    }

    public void setNextshots(String nextshots) {
        this.nextshots = nextshots;
    }

    public String getLegendlogout() {
        return legendlogout;
    }

    public void setLegendlogout(String legendlogout) {
        this.legendlogout = legendlogout;
    }

    public String getBilldata() {
        return billdata;
    }

    public void setBilldata(String billdata) {
        this.billdata = billdata;
    }

    public String getFiledrequiered() {
        return filedrequiered;
    }

    public void setFiledrequiered(String filedrequiered) {
        this.filedrequiered = filedrequiered;
    }

    public String getSelectoption() {
        return selectoption;
    }

    public void setSelectoption(String selectoption) {
        this.selectoption = selectoption;
    }

    public String getLegendslogan() {
        return legendslogan;
    }

    public void setLegendslogan(String legendslogan) {
        this.legendslogan = legendslogan;
    }

    public String getLegenddologing() {
        return legenddologing;
    }

    public void setLegenddologing(String legenddologing) {
        this.legenddologing = legenddologing;
    }

    public String getConnecting() {
        return connecting;
    }

    public void setConnecting(String connecting) {
        this.connecting = connecting;
    }

    public String getOnlyletters() {
        return onlyletters;
    }

    public void setOnlyletters(String onlyletters) {
        this.onlyletters = onlyletters;
    }

    public String getOnlynumbers() {
        return onlynumbers;
    }

    public void setOnlynumbers(String onlynumbers) {
        this.onlynumbers = onlynumbers;
    }

    public String getInvalidemail() {
        return invalidemail;
    }

    public void setInvalidemail(String invalidemail) {
        this.invalidemail = invalidemail;
    }

    public String getReasonsoacil() {
        return reasonsoacil;
    }

    public void setReasonsoacil(String reasonsoacil) {
        this.reasonsoacil = reasonsoacil;
    }

    public String getOnlylettersnumbers() {
        return onlylettersnumbers;
    }

    public void setOnlylettersnumbers(String onlylettersnumbers) {
        this.onlylettersnumbers = onlylettersnumbers;
    }

    public String getVrequired() {
        return vrequired;
    }

    public void setVrequired(String vrequired) {
        this.vrequired = vrequired;
    }

    public String getVtext() {
        return vtext;
    }

    public void setVtext(String vtext) {
        this.vtext = vtext;
    }

    public String getVnumber() {
        return vnumber;
    }

    public void setVnumber(String vnumber) {
        this.vnumber = vnumber;
    }

    public String getVemail() {
        return vemail;
    }

    public void setVemail(String vemail) {
        this.vemail = vemail;
    }

    public String getAcademictraining() {
        return academictraining;
    }

    public void setAcademictraining(String academictraining) {
        this.academictraining = academictraining;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getProfessionalid() {
        return professionalid;
    }

    public void setProfessionalid(String professionalid) {
        this.professionalid = professionalid;
    }

    public String getYearsexperience() {
        return yearsexperience;
    }

    public void setYearsexperience(String yearsexperience) {
        this.yearsexperience = yearsexperience;
    }

    public String getDategraduation() {
        return dategraduation;
    }

    public void setDategraduation(String dategraduation) {
        this.dategraduation = dategraduation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getUndefined() {
        return undefined;
    }

    public void setUndefined(String undefined) {
        this.undefined = undefined;
    }

    public String getMedicationsus() {
        return medicationsus;
    }

    public void setMedicationsus(String medicationsus) {
        this.medicationsus = medicationsus;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getFirstdate() {
        return firstdate;
    }

    public void setFirstdate(String firstdate) {
        this.firstdate = firstdate;
    }

    public String getSubsequent() {
        return subsequent;
    }

    public void setSubsequent(String subsequent) {
        this.subsequent = subsequent;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getNotdefinedspecialities() {
        return notdefinedspecialities;
    }

    public void setNotdefinedspecialities(String notdefinedspecialities) {
        this.notdefinedspecialities = notdefinedspecialities;
    }

    public String getSpecialityin() {
        return specialityin;
    }

    public void setSpecialityin(String specialityin) {
        this.specialityin = specialityin;
    }

    public String getForumtopic() {
        return forumtopic;
    }

    public void setForumtopic(String forumtopic) {
        this.forumtopic = forumtopic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNewsubtopic() {
        return newsubtopic;
    }

    public void setNewsubtopic(String newsubtopic) {
        this.newsubtopic = newsubtopic;
    }

    public String getAddpill() {
        return addpill;
    }

    public void setAddpill(String addpill) {
        this.addpill = addpill;
    }

    public String getMedicinebydoctor() {
        return medicinebydoctor;
    }

    public void setMedicinebydoctor(String medicinebydoctor) {
        this.medicinebydoctor = medicinebydoctor;
    }

    public String getMedicinesucceful() {
        return medicinesucceful;
    }

    public void setMedicinesucceful(String medicinesucceful) {
        this.medicinesucceful = medicinesucceful;
    }

    public String getEditmedicine() {
        return editmedicine;
    }

    public void setEditmedicine(String editmedicine) {
        this.editmedicine = editmedicine;
    }

    public String getSerandloc() {
        return serandloc;
    }

    public void setSerandloc(String serandloc) {
        this.serandloc = serandloc;
    }

    public String getStudies() {
        return studies;
    }

    public void setStudies(String studies) {
        this.studies = studies;
    }

    public String getAcademic() {
        return academic;
    }

    public void setAcademic(String academic) {
        this.academic = academic;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNumint() {
        return numint;
    }

    public void setNumint(String numint) {
        this.numint = numint;
    }

    public String getSavechanges() {
        return savechanges;
    }

    public void setSavechanges(String savechanges) {
        this.savechanges = savechanges;
    }

    public String getChangelan() {
        return changelan;
    }

    public void setChangelan(String changelan) {
        this.changelan = changelan;
    }

    public String getSpanish() {
        return spanish;
    }

    public void setSpanish(String spanish) {
        this.spanish = spanish;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getNosubtheme() {
        return nosubtheme;
    }

    public void setNosubtheme(String nosubtheme) {
        this.nosubtheme = nosubtheme;
    }

    public String getNocomment() {
        return nocomment;
    }

    public void setNocomment(String nocomment) {
        this.nocomment = nocomment;
    }

    public String getMale() {
        return male;
    }

    public void setMale(String male) {
        this.male = male;
    }

    public String getFemale() {
        return female;
    }

    public void setFemale(String female) {
        this.female = female;
    }

    public String getAccountsuccessful() {
        return accountsuccessful;
    }

    public void setAccountsuccessful(String accountsuccessful) {
        this.accountsuccessful = accountsuccessful;
    }

    public String getEmailopassincorrect() {
        return emailopassincorrect;
    }

    public void setEmailopassincorrect(String emailopassincorrect) {
        this.emailopassincorrect = emailopassincorrect;
    }

    public String getRecoverycomplete() {
        return recoverycomplete;
    }

    public void setRecoverycomplete(String recoverycomplete) {
        this.recoverycomplete = recoverycomplete;
    }

    public String getEmailnoexist() {
        return emailnoexist;
    }

    public void setEmailnoexist(String emailnoexist) {
        this.emailnoexist = emailnoexist;
    }

    public String getIdentificationsuccessful() {
        return identificationsuccessful;
    }

    public void setIdentificationsuccessful(String identificationsuccessful) {
        this.identificationsuccessful = identificationsuccessful;
    }

    public String getDatanoupdate() {
        return datanoupdate;
    }

    public void setDatanoupdate(String datanoupdate) {
        this.datanoupdate = datanoupdate;
    }

    public String getNogetagend() {
        return nogetagend;
    }

    public void setNogetagend(String nogetagend) {
        this.nogetagend = nogetagend;
    }

    public String getDiseasesexit() {
        return diseasesexit;
    }

    public void setDiseasesexit(String diseasesexit) {
        this.diseasesexit = diseasesexit;
    }

    public String getDocontinue() {
        return docontinue;
    }

    public void setDocontinue(String docontinue) {
        this.docontinue = docontinue;
    }

    public String getAddfavorite() {
        return addfavorite;
    }

    public void setAddfavorite(String addfavorite) {
        this.addfavorite = addfavorite;
    }

    public String getDeletefavorite() {
        return deletefavorite;
    }

    public void setDeletefavorite(String deletefavorite) {
        this.deletefavorite = deletefavorite;
    }

    public String getAddfavoritesuccessful() {
        return addfavoritesuccessful;
    }

    public void setAddfavoritesuccessful(String addfavoritesuccessful) {
        this.addfavoritesuccessful = addfavoritesuccessful;
    }

    public String getDeletefavoritesuccessful() {
        return deletefavoritesuccessful;
    }

    public void setDeletefavoritesuccessful(String deletefavoritesuccessful) {
        this.deletefavoritesuccessful = deletefavoritesuccessful;
    }

    public String getRequiredlogin() {
        return requiredlogin;
    }

    public void setRequiredlogin(String requiredlogin) {
        this.requiredlogin = requiredlogin;
    }

    public String getErrorlenguage() {
        return errorlenguage;
    }

    public void setErrorlenguage(String errorlenguage) {
        this.errorlenguage = errorlenguage;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getLegendchangepass() {
        return legendchangepass;
    }

    public void setLegendchangepass(String legendchangepass) {
        this.legendchangepass = legendchangepass;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getRealeaseaccount() {
        return realeaseaccount;
    }

    public void setRealeaseaccount(String realeaseaccount) {
        this.realeaseaccount = realeaseaccount;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getSwitched() {
        return switched;
    }

    public void setSwitched(String switched) {
        this.switched = switched;
    }

    public String getNotswitched() {
        return notswitched;
    }

    public void setNotswitched(String notswitched) {
        this.notswitched = notswitched;
    }

    public String getCouldgetlist() {
        return couldgetlist;
    }

    public void setCouldgetlist(String couldgetlist) {
        this.couldgetlist = couldgetlist;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getNewaccountfamily() {
        return newaccountfamily;
    }

    public void setNewaccountfamily(String newaccountfamily) {
        this.newaccountfamily = newaccountfamily;
    }

    public String getRequiredmainaccount() {
        return requiredmainaccount;
    }

    public void setRequiredmainaccount(String requiredmainaccount) {
        this.requiredmainaccount = requiredmainaccount;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getBoth() {
        return both;
    }

    public void setBoth(String both) {
        this.both = both;
    }

    public String getAny() {
        return any;
    }

    public void setAny(String any) {
        this.any = any;
    }

    public String getNotupdateinformation() {
        return notupdateinformation;
    }

    public void setNotupdateinformation(String notupdateinformation) {
        this.notupdateinformation = notupdateinformation;
    }

    public String getInformationupdate() {
        return informationupdate;
    }

    public void setInformationupdate(String informationupdate) {
        this.informationupdate = informationupdate;
    }

    public String getDeworming() {
        return deworming;
    }

    public void setDeworming(String deworming) {
        this.deworming = deworming;
    }

    public String getSingle() {
        return single;
    }

    public void setSingle(String single) {
        this.single = single;
    }

    public String getMarried() {
        return married;
    }

    public void setMarried(String married) {
        this.married = married;
    }

    public String getDivorced() {
        return divorced;
    }

    public void setDivorced(String divorced) {
        this.divorced = divorced;
    }

    public String getWidow() {
        return widow;
    }

    public void setWidow(String widow) {
        this.widow = widow;
    }

    public String getCancelappoinment() {
        return cancelappoinment;
    }

    public void setCancelappoinment(String cancelappoinment) {
        this.cancelappoinment = cancelappoinment;
    }

    public String getDocancelappoinment() {
        return docancelappoinment;
    }

    public void setDocancelappoinment(String docancelappoinment) {
        this.docancelappoinment = docancelappoinment;
    }

    public String getReject() {
        return reject;
    }

    public void setReject(String reject) {
        this.reject = reject;
    }

    public String getDorescheduleappoinment() {
        return dorescheduleappoinment;
    }

    public void setDorescheduleappoinment(String dorescheduleappoinment) {
        this.dorescheduleappoinment = dorescheduleappoinment;
    }

    public String getRescheduleappoinment() {
        return rescheduleappoinment;
    }

    public void setRescheduleappoinment(String rescheduleappoinment) {
        this.rescheduleappoinment = rescheduleappoinment;
    }

    public String getCapsule() {
        return capsule;
    }

    public void setCapsule(String capsule) {
        this.capsule = capsule;
    }

    public String getTablet() {
        return tablet;
    }

    public void setTablet(String tablet) {
        this.tablet = tablet;
    }

    public String getMililiter() {
        return mililiter;
    }

    public void setMililiter(String mililiter) {
        this.mililiter = mililiter;
    }

    public String getAmpoule() {
        return ampoule;
    }

    public void setAmpoule(String ampoule) {
        this.ampoule = ampoule;
    }

    public String getShot() {
        return shot;
    }

    public void setShot(String shot) {
        this.shot = shot;
    }

    public String getCompleteage() {
        return completeage;
    }

    public void setCompleteage(String completeage) {
        this.completeage = completeage;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getPhotoerror() {
        return photoerror;
    }

    public void setPhotoerror(String photoerror) {
        this.photoerror = photoerror;
    }

    public String getDaily() {
        return daily;
    }

    public void setDaily(String daily) {
        this.daily = daily;
    }

    public String getThirdday() {
        return thirdday;
    }

    public void setThirdday(String thirdday) {
        this.thirdday = thirdday;
    }

    public String getIrregular() {
        return irregular;
    }

    public void setIrregular(String irregular) {
        this.irregular = irregular;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getOnceday() {
        return onceday;
    }

    public void setOnceday(String onceday) {
        this.onceday = onceday;
    }

    public String getTwoday() {
        return twoday;
    }

    public void setTwoday(String twoday) {
        this.twoday = twoday;
    }

    public String getThreeday() {
        return threeday;
    }

    public void setThreeday(String threeday) {
        this.threeday = threeday;
    }

    public String getUrban() {
        return urban;
    }

    public void setUrban(String urban) {
        this.urban = urban;
    }

    public String getRural() {
        return rural;
    }

    public void setRural(String rural) {
        this.rural = rural;
    }

    public String getAllservices() {
        return allservices;
    }

    public void setAllservices(String allservices) {
        this.allservices = allservices;
    }

    public String getLatrine() {
        return latrine;
    }

    public void setLatrine(String latrine) {
        this.latrine = latrine;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getBad() {
        return bad;
    }

    public void setBad(String bad) {
        this.bad = bad;
    }

    public String getWait() {
        return wait;
    }

    public void setWait(String wait) {
        this.wait = wait;
    }

    public String getApproveappoinment() {
        return approveappoinment;
    }

    public void setApproveappoinment(String approveappoinment) {
        this.approveappoinment = approveappoinment;
    }

    public String getMnameoptional() {
        return mnameoptional;
    }

    public void setMnameoptional(String mnameoptional) {
        this.mnameoptional = mnameoptional;
    }
}
