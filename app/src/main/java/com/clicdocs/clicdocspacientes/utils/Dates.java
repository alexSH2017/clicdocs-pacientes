package com.clicdocs.clicdocspacientes.utils;

import java.io.Serializable;

public class Dates implements Serializable{
    private String idAppointment;
    private String idDoctor;
    private String idDoctorOffice;
    private String name_doctor;
    private String name_office;
    private String address;
    private String borough;
    private String state;
    private String lati;
    private String lngi;
    private String date;
    private String hour;
    private String picture;

    public Dates(String idAppointment, String idDoctor, String idDoctorOffice, String name_doctor, String name_office, String address, String borough, String state, String lati, String lngi, String date, String hour, String picture) {
        this.idAppointment = idAppointment;
        this.idDoctor = idDoctor;
        this.idDoctorOffice = idDoctorOffice;
        this.name_doctor = name_doctor;
        this.name_office = name_office;
        this.address = address;
        this.borough = borough;
        this.state = state;
        this.lati = lati;
        this.lngi = lngi;
        this.date = date;
        this.hour = hour;
        this.picture = picture;
    }

    public String getIdAppointment() {
        return idAppointment;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public String getIdDoctorOffice() {
        return idDoctorOffice;
    }

    public String getName_doctor() {
        return name_doctor;
    }

    public String getName_office() {
        return name_office;
    }

    public String getAddress() {
        return address;
    }

    public String getBorough() {
        return borough;
    }

    public String getState() {
        return state;
    }

    public String getLati() {
        return lati;
    }

    public String getLngi() {
        return lngi;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getPicture() {
        return picture;
    }
}


