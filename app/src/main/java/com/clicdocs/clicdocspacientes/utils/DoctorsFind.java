package com.clicdocs.clicdocspacientes.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import co.uk.rushorm.core.RushObject;

public class DoctorsFind implements Serializable {
    private String id_session,id_profile, name,fname,mname,picture,id_professional,raking,studies,enable, latitude, longitude;
    private ArrayList<HashMap<String, String>> specialities;
    private ArrayList<HashMap<String, String>> services;
    private ArrayList<String> open_time;
    private ArrayList<String> locked_time;
    private ArrayList<Offices> myoffices;
    public Offices off= new Offices();

    public String getId_session() {
        return id_session;
    }

    public void setId_session(String id_session) {
        this.id_session = id_session;
    }

    public String getId_profile() {
        return id_profile;
    }

    public void setId_profile(String id_profile) {
        this.id_profile = id_profile;
    }

    public String getName() {
        return name;
    }

    public String getFname() { return fname; }

    public void setFname(String fname) {  this.fname = fname;  }

    public String getMname() { return mname; }

    public void setMname(String mname) {  this.mname = mname;  }

    public String getPicture() {  return picture;  }

    public void setPicture(String picture) {  this.picture = picture;  }

    public String getId_professional() { return id_professional;  }

    public void setId_professional(String id_professional) { this.id_professional = id_professional;   }

    public String getRaking() {  return raking;  }

    public void setRaking(String raking) {  this.raking = raking;  }

    public String getStudies() {
        return studies;
    }

    public void setStudies(String studies) {
        this.studies = studies;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ArrayList<String> getOpen_time() {
        return open_time;
    }

    public void setOpen_time(ArrayList<String> open_time) {
        this.open_time = open_time;
    }

    public ArrayList<String> getLocked_time() {
        return locked_time;
    }

    public void setLocked_time(ArrayList<String> locked_time) {
        this.locked_time = locked_time;
    }

    public ArrayList<HashMap<String, String>> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<HashMap<String, String>> specialities) {
        this.specialities = specialities;
    }

    public ArrayList<HashMap<String, String>> getServices() {
        return services;
    }

    public void setServices(ArrayList<HashMap<String, String>> services) {
        this.services = services;
    }

    public ArrayList<Offices> getMyoffices() {
        return myoffices;
    }

    public void setMyoffices(ArrayList<Offices> myoffices) {
        this.myoffices = myoffices;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class Offices implements Serializable {
        private String id_doctor_office, name,full_address,estado,municipio,latitude,longitude;
        private ArrayList<HashMap<String, String>> clinics;
        private ArrayList<HashMap<String, String>> schedules;

        public String getId_doctor_office() {
            return id_doctor_office;
        }

        public void setId_doctor_office(String id_doctor_office) {
            this.id_doctor_office = id_doctor_office;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFull_address() {  return full_address;   }

        public void setFull_address(String full_address) {  this.full_address = full_address;  }

        public String getEstado() {  return estado;  }

        public void setEstado(String estado) {  this.estado = estado; }

        public String getMunicipio() {  return municipio;  }

        public void setMunicipio(String municipio) {  this.municipio = municipio;  }

        public String getLatitude() { return latitude;  }

        public void setLatitude(String latitude) {  this.latitude = latitude;  }

        public String getLongitude() { return longitude;  }

        public void setLongitude(String longitude) {  this.longitude = longitude;  }

        public ArrayList<HashMap<String, String>> getClinics() {
            return clinics;
        }

        public void setClinics(ArrayList<HashMap<String, String>> clinics) {
            this.clinics = clinics;
        }

        public ArrayList<HashMap<String, String>> getSchedules() {
            return schedules;
        }

        public void setSchedules(ArrayList<HashMap<String, String>> schedules) {
            this.schedules = schedules;
        }
    }

}
