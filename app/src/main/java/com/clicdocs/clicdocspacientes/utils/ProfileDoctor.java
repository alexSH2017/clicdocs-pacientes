package com.clicdocs.clicdocspacientes.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;



public class ProfileDoctor {

    private String idsesion,name,fname,mname,picture, enable;
    private ArrayList<HashMap<String, String>> specialities;
    private ArrayList<HashMap<String, String>> services;
    private ArrayList<Offices> myoffices;
    public Offices off= new Offices();

    public String getIdsesion() {
        return idsesion;
    }

    public void setIdsesion(String idsesion) {
        this.idsesion = idsesion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public ArrayList<HashMap<String, String>> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<HashMap<String, String>> specialities) {
        this.specialities = specialities;
    }

    public ArrayList<HashMap<String, String>> getServices() {
        return services;
    }

    public void setServices(ArrayList<HashMap<String, String>> services) {
        this.services = services;
    }

    public ArrayList<Offices> getMyoffices() {
        return myoffices;
    }

    public void setMyoffices(ArrayList<Offices> myoffices) {
        this.myoffices = myoffices;
    }

    public class Offices {
        private String id_doctor_office, name,full_address,estado,municipio,latitude,longitude;
        private ArrayList<HashMap<String, String>> clinics;
        private ArrayList<HashMap<String, String>> schedules;

        public String getId_doctor_office() {
            return id_doctor_office;
        }

        public void setId_doctor_office(String id_doctor_office) {
            this.id_doctor_office = id_doctor_office;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFull_address() {
            return full_address;
        }

        public void setFull_address(String full_address) {
            this.full_address = full_address;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getMunicipio() {
            return municipio;
        }

        public void setMunicipio(String municipio) {
            this.municipio = municipio;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public ArrayList<HashMap<String, String>> getClinics() {
            return clinics;
        }

        public void setClinics(ArrayList<HashMap<String, String>> clinics) {
            this.clinics = clinics;
        }

        public ArrayList<HashMap<String, String>> getSchedules() {
            return schedules;
        }

        public void setSchedules(ArrayList<HashMap<String, String>> schedules) {
            this.schedules = schedules;
        }
    }
}
