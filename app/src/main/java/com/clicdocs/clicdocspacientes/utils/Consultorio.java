package com.clicdocs.clicdocspacientes.utils;

public class Consultorio {
    String id_consultorio;
    String name;
    private String pictoure;

    public Consultorio(String id_consultorio, String name) {
        this.id_consultorio = id_consultorio;
        this.name = name;
    }

    public String getId_consultorio() {
        return id_consultorio;
    }

    public void setId_consultorio(String id_consultorio) {
        this.id_consultorio = id_consultorio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
