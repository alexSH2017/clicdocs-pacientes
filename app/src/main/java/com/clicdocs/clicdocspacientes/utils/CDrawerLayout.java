package com.clicdocs.clicdocspacientes.utils;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class CDrawerLayout extends DrawerLayout {
    private boolean isLockedMode = false;

    public CDrawerLayout(Context context) {
        super(context);
    }

    public CDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return !isLockedMode && super.onInterceptTouchEvent(ev);
    }

    public void setLocked(boolean lock) {
        isLockedMode = lock;
    }

    public boolean isLocked() {
        return isLockedMode;
    }
}
