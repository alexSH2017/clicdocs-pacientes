package com.clicdocs.clicdocspacientes.utils;


import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;


public class PopUpManager {


    private static int DIALOG_MESSAGE  = R.layout.popup_message;
    private static int DIALOG_LODING   = R.layout.popup_loading;
    private static int DIALOG_PROGRESS = R.layout.popup_progress;
    public interface DialogMessage {
        void OnClickListener(AlertDialog dialog);
    }

    public static AlertDialog showDialogMessage(Context ctx, int message,
                                                int btnOk, final DialogMessage btnOkClick,
                                                int btnFail, final DialogMessage btnFailClick) {

        // LOADING VIEWS
        View view          = LayoutInflater.from(ctx).inflate(DIALOG_MESSAGE, null);
        TextView TVmessage = (TextView) view.findViewById(R.id.TVmessage);
        Button BTNok       = (Button) view.findViewById(R.id.BTNok);
        Button BTNfail     = (Button) view.findViewById(R.id.BTNfail);

        // FILLING VIEWS
        TVmessage.setText(ctx.getString(message));

        final AlertDialog dialog = new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setView(view).create();

        if(btnOk != 0) {
            BTNok.setText(ctx.getString(btnOk));
            BTNok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnOkClick.OnClickListener(dialog);
                }
            });
        }

        if(btnFail != 0) {
            BTNfail.setText(ctx.getString(btnFail));
            BTNfail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnFailClick.OnClickListener(dialog);
                }
            });
        }

        return dialog;
    }

    public static AlertDialog showDialogMessage(Context ctx, String message,
                                                String btnOk, final DialogMessage btnOkClick,
                                                String btnFail, final DialogMessage btnFailClick) {

        // LOADING VIEWS
        View view          = LayoutInflater.from(ctx).inflate(DIALOG_MESSAGE, null);
        TextView TVmessage = (TextView) view.findViewById(R.id.TVmessage);
        Button BTNok       = (Button) view.findViewById(R.id.BTNok);
        Button BTNfail     = (Button) view.findViewById(R.id.BTNfail);

        // FILLING VIEWS
        TVmessage.setText(message);

        final AlertDialog dialog = new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setView(view).create();

        if(btnOk != null) {
            BTNok.setText(btnOk);
            BTNok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnOkClick.OnClickListener(dialog);
                }
            });
        }

        if(btnFail != null) {
            BTNfail.setText(btnFail);
            BTNfail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnFailClick.OnClickListener(dialog);
                }
            });
        }

        return dialog;
    }


    public static AlertDialog showLoadingDialog(Context ctx, String message) {
        View view = LayoutInflater.from(ctx).inflate(DIALOG_LODING, null);
        TextView TVmessage = (TextView) view.findViewById(R.id.TVmessage);
        TVmessage.setText(message);
        return new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setView(view).create();
    }

    public static AlertDialog showProgressDialog(Context ctx, String  message) {
        View view = LayoutInflater.from(ctx).inflate(DIALOG_PROGRESS, null);
        ProgressBar PBloading = (ProgressBar) view.findViewById(R.id.PBloading);
        TextView TVmessage    = (TextView) view.findViewById(R.id.TVmessage);
        TVmessage.setText(message);
        PBloading.setProgress(0);
        return new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setView(view).create();
    }

    public static void setProgress(AlertDialog dialog, int progress) {
        ProgressBar PBloading = (ProgressBar) dialog.getWindow().findViewById(R.id.PBloading);
        PBloading.setProgress(progress);
    }
}
