package com.clicdocs.clicdocspacientes.utils;



public class SectionBeans {

    private String id_section;
    private String name_section;

    public SectionBeans(String id_section, String name_section) {
        this.id_section = id_section;
        this.name_section = name_section;
    }

    public String getName_section() {
        return name_section;
    }

    public void setName_section(String name_section) {
        this.name_section = name_section;
    }

    public String getId_section() {
        return id_section;
    }

    public void setId_section(String id_section) {
        this.id_section = id_section;
    }
}
