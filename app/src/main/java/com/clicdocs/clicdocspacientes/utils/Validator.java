package com.clicdocs.clicdocspacientes.utils;

import android.widget.EditText;

import com.clicdocs.clicdocspacientes.MainActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public final String REQUIRED        = "required";
    public final String ONLY_TEXT       = "text";
    public final String ONLY_NUMBER     = "number";
    public final String EMAIL           = "email";
    public final String REASON          = "razon social";
    public final String NUMBER_HOME     = "numero de casa";
    public final String RFC             = "RFC";
    public final String GENERAL         = "general";

    public  String REQUIRED_MESSAGE       = "Este campo es requerido.";
    public  String ONLY_TEXT_MESSAGE      = "Solo permite letras y espacios.";
    public  String ONLY_NUMBER_MESSAGE    = "Solo permite números.";
    public  String EMAIL_MESSAGE          = "Correo electrónico no valido.";
    public  String REASON_MESSAGE         = "Solo letras, números y &";
    public  String NUMBER_MESSAGE         = "Solo letras y números";
    public  String GENERAL_MESSAGE         = "Solo letras y/o números";

    private final String PATTERN_ONLY_TEXT      = "[A-Za-z ÑñÁáÉéÍíÓóÚú]*";
    private final String PATTERN_ONLY_NUMBER    = "[0-9]*";
    private final String PATTERN_EMAIL          = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private final String PATTERN_REASON         = "[A-Za-z0-9 ÑñÁáÉéÍíÓóÚú&]*";
    private final String PATTERN_NUMBER         = "[0-9A-Za-zÑñÁáÉéÍíÓóÚú]*";
    private final String PATTERN_RFC            = "[0-9A-Za-zÑñÁáÉéÍíÓóÚú&]*";
    private final String PATTERN_GENERAL        = "[0-9 A-Za-zÑñÁáÉéÍíÓóÚú&]*";

    private Pattern pattern;
    private Matcher matcher;

    public boolean validate(EditText etx, String[] RULS) {
        Boolean error = false;

        for (int i = 0; i< RULS.length; i++) {
            switch (RULS[i]) {
                case REQUIRED:
                        if(etx.getText().toString().trim().length() == 0) {
                            etx.setError(REQUIRED_MESSAGE);
                            error = true;
                            return error;
                        }
                    break;
                case ONLY_TEXT:
                        pattern = Pattern.compile(PATTERN_ONLY_TEXT);
                        matcher = pattern.matcher(etx.getText().toString());
                        if(!matcher.matches()) {
                            etx.setError(ONLY_TEXT_MESSAGE);
                            error = true;
                            return error;
                        }
                    break;
                case ONLY_NUMBER:
                    pattern = Pattern.compile(PATTERN_ONLY_NUMBER);
                    matcher = pattern.matcher(etx.getText().toString());
                    if(!matcher.matches()) {
                        etx.setError(ONLY_NUMBER_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
                case EMAIL:
                    pattern = Pattern.compile(PATTERN_EMAIL);
                    matcher = pattern.matcher(etx.getText().toString().toLowerCase());
                    if(!matcher.matches()) {
                        etx.setError(EMAIL_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
                case REASON:
                    pattern = Pattern.compile(PATTERN_REASON);
                    matcher = pattern.matcher(etx.getText().toString().toLowerCase());
                    if(!matcher.matches()) {
                        etx.setError(REASON_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
                case NUMBER_HOME:
                    pattern = Pattern.compile(PATTERN_NUMBER);
                    matcher = pattern.matcher(etx.getText().toString().toLowerCase());
                    if(!matcher.matches()) {
                        etx.setError(NUMBER_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
                case RFC:
                    pattern = Pattern.compile(PATTERN_RFC);
                    matcher = pattern.matcher(etx.getText().toString().toLowerCase());
                    if(!matcher.matches()) {
                        etx.setError(NUMBER_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
                case GENERAL:
                    pattern = Pattern.compile(PATTERN_GENERAL);
                    matcher = pattern.matcher(etx.getText().toString().toLowerCase());
                    if(!matcher.matches()) {
                        etx.setError(GENERAL_MESSAGE);
                        error = true;
                        return error;
                    }
                    break;
            }
        }

        return error;
    }
}
