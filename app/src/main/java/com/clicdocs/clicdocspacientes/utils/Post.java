package com.clicdocs.clicdocspacientes.utils;


public class Post {

    private String name;
    private String nickname;
    private String post;
    private String timestamp;
    private String image;

    public Post(String name, String nickname, String post, String timestamp, String image) {

        this.name = name;
        this.nickname = nickname;
        this.post = post;
        this.timestamp = timestamp;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPost() {
        return post;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getImage() {
        return image;
    }
}

