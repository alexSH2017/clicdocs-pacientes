package com.clicdocs.clicdocspacientes.utils;

import android.content.Context;

import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Miscellaneous {

    public String FATHER        = "Padre";
    public String MOTHER        = "Madre";
    public String BOTH          = "Ambos";
    public String NONE           = "Ninguno";

    public Calendar calendar;
    public SimpleDateFormat ndf = new SimpleDateFormat("MMM dd, yyyy");
    public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static int getGenderInteger(String gender) {
        switch (gender) {
            case "Masculino":
                return 1;
            case "Femenino":
                return 2;
            default:
                return 1;
        }
    }

    public static String setGenderInteger(String gender) {
        switch (gender) {
            case "1":
                return "Masculino";
            case "2":
                return "Femenino";
            default:
                return "Masculino";
        }
    }

    public static String getLanguage(String language) {
        switch (language) {
            case "Español":
                return "spanish";
            case "Inglés":
                return "english";
            default:
                return "spanish";
        }
    }

    public static int getMaritalStatus(String status) {
        switch (status) {
            case "Soltero(a)":
                return 1;
            case "Casado(a)":
                return 2;
            case "Divorciado(a)":
                return 3;
            case "Viudo(a)":
                return 4;
            default:
                return 1;
        }
    }

    public static String setMaritalStatus(String status) {
        switch (status) {
            case "1":
                return "Soltero(a)";
            case "2":
                return "Casado(a)";
            case "3":
                return "Divorciado(a)";
            case "4":
                return "Viudo(a)";
            default:
                return "";
        }
    }

    public static int setHereditary(String hereday) {
        switch (hereday) {
            case "1":
                return 1;
            case "2":
                return 1;
            case "3":
                return 2;
            case "4":
                return 3;
            case "5":
                return 4;
            default:
                return 0;
        }
    }

    public static int getShower(String status) {
        switch (status) {
            case "Diario":
                return 1;
            case "C/3er día":
                return 2;
            case "Irreguar":
                return 3;
            default:
                return 0;
        }
    }

    public static String setShower(String status) {
        switch (status) {
            case "1":
                return "Diario";
            case "2":
                return "C/3er día";
            case "3":
                return "Irreguar";
            default:
                return "";
        }
    }

    public static int getToothBrush(String status) {
        switch (status) {
            case "1 Vez al día":
                return 1;
            case "2 Veces al día":
                return 2;
            case "3 Veces al día":
                return 3;
            default:
                return 0;
        }
    }

    public static String setToothBrush(String status) {
        switch (status) {
            case "1":
                return "1 Vez al día";
            case "2":
                return "2 Veces al día";
            case "3":
                return "3 Veces al día";
            default:
                return "";
        }
    }

    public static int getRoom(String status) {
        switch (status) {
            case "Urbana":
                return 1;
            case "Rural":
                return 2;
            case "Todos los servicios":
                return 3;
            case "Letrina":
                return 4;
            default:
                return 0;
        }
    }

    public static String setRoom(String status) {
        switch (status) {
            case "1":
                return "Urbana";
            case "2":
                return "Rural";
            case "3":
                return "Todos los servicios";
            case "4":
                return "Letrina";
            default:
                return "";
        }
    }

    public static int getFeeding(String status) {
        switch (status) {
            case "Buena":
                return 1;
            case "Regular":
                return 2;
            case "Mala":
                return 3;
            default:
                return 0;
        }
    }

    public static String setFeeding(String status) {
        switch (status) {
            case "1":
                return "Buena";
            case "2":
                return "Regular";
            case "3":
                return "Mala";
            default:
                return "";
        }
    }

    public static int getInmunization(String status) {
        switch (status) {
            case "Completas a edad":
                return 1;
            case "Pendientes":
                return 2;
            default:
                return 0;
        }
    }

    public static String setInmunization(String status) {
        switch (status) {
            case "1":
                return "Completas a edad";
            case "2":
                return "Pendientes";
            default:
                return "";
        }
    }

    public static Integer getFrequency(String frequency) {
        switch (frequency) {
            case "Cada 4 hrs.":
                return 4;
            case "Cada 6 hrs.":
                return 6;
            case "Cada 8 hrs.":
                return 8;
            case "Cada 10 hrs.":
                return 10;
            case "Cada 12 hrs.":
                return 12;
            case "Cada 24 hrs.":
                return 24;
            case "Cada 48 hrs.":
                return 48;
            case "Cada 72 hrs.":
                return 72;
            default:
                return 8;
        }
    }

    public static Integer setFrequency(String frequency) {
        switch (frequency) {
            case "1":
                return 1;
            case "2":
                return 2;
            case "3":
                return 3;
            case "4":
                return 4;
            case "5":
                return 5;
            case "6":
                return 6;
            case "7":
                return 7;
            case "8":
                return 8;
            case "9":
                return 9;
            case "10":
                return 10;
            case "11":
                return 11;
            case "12":
                return 12;
            case "13":
                return 13;
            case "14":
                return 14;
            case "15":
                return 15;
            case "16":
                return 16;
            case "17":
                return 17;
            case "18":
                return 18;
            case "19":
                return 19;
            case "20":
                return 20;
            case "21":
                return 21;
            case "22":
                return 22;
            case "23":
                return 23;
            case "24":
                return 24;
            default:
                return 1;
        }
    }

    public static String getMeasurement (String value){
        switch (value) {
            case "1":
                return "Ampolleta(s)";
            case "2":
                return "Tableta(s)";
            case "3":
                return "Capsulas(s)";
            case "4":
                return "Disparo(s)";
            case "5":
                return "Mililitros(s)";
            case  "6":
                return "Gotas(s)";
            default:
                return "Otro";
        }
    }

    public static String setMeasurement (String value){
        switch (value) {
            case "Cápsula(s)":
                return "1";
            case "Tableta(s)":
                return "2";
            case "Mililitro(s)":
                return "3";
            case "Ampolleta(s)":
                return "4";
            case "Disparo(s)":
                return "5";
            default:
                return "1";
        }
    }

    public static String idioma (String value){
        switch (value) {
            case "0":
                return "1";
            case "1":
                return "2";
            default:
                return "0";
        }
    }

    public static String validate(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        } else {
            return  str;
        }
    }

    public static String ucFirst(String str) {
        if (str.equals("null") || str.isEmpty()) {
            return "";
        } else {
            return  Character.toUpperCase(str.charAt(0)) + str.substring(1, str.length()).toLowerCase();
        }
    }

    public static Integer isInteger (String value){
        try{
            return Integer.parseInt(value);
        } catch (Exception e){
            return  0;
        }
    }

}
