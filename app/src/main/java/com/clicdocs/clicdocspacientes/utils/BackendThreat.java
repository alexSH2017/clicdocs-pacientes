package com.clicdocs.clicdocspacientes.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import org.json.JSONException;
import org.ksoap2.SoapFault;

import java.text.ParseException;

public class BackendThreat {

    public Context ctx;

    public interface ObjectTask {
        Object doInBackground();
        void onPostExecute(Object response) throws JSONException, ParseException;
    }

    public interface BitmapTask {
        Bitmap doInBackground();
        void onPostExecute(Bitmap response);
    }

    public BackendThreat(Context ctx, ObjectTask task) {
        this.ctx = ctx;
        new objectBackground(task).execute(task);
    }

    public BackendThreat(Context ctx, BitmapTask task) {
        this.ctx = ctx;
        new bitmapBackground(task).execute(task);
    }

    private class objectBackground extends AsyncTask<ObjectTask, Void, Object> {
        private ObjectTask post;
        objectBackground(ObjectTask task) { this.post = task; }
        @Override
        protected Object doInBackground(ObjectTask... objectTasks) {
            return objectTasks[0].doInBackground();
        }

        @Override
        protected void onPostExecute(Object response) {
            super.onPostExecute(response);
            try {
                post.onPostExecute(response);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private class bitmapBackground extends AsyncTask<BitmapTask, Void, Bitmap> {
        private BitmapTask post;
        bitmapBackground(BitmapTask task) {
            this.post = task;
        }
        @Override
        protected Bitmap doInBackground(BitmapTask... bitmapTasks) {
            return bitmapTasks[0].doInBackground();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            post.onPostExecute(bitmap);
        }
    }
}
