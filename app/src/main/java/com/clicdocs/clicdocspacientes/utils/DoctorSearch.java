package com.clicdocs.clicdocspacientes.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


public class DoctorSearch implements Serializable{
    private String id_session, name, picture, enable, friends, f_amount, s_amount;
    private ArrayList<HashMap<String, String>> specialities;
    private ArrayList<HashMap<String, String>> services;
    private ArrayList<HashMap<String, String>> pay_methods;
    private ArrayList<Offices> myoffices;
    public Offices off= new Offices();

    public String getId_session() {
        return id_session;
    }

    public void setId_session(String id_session) {
        this.id_session = id_session;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getF_amount() {
        return f_amount;
    }

    public void setF_amount(String f_amount) {
        this.f_amount = f_amount;
    }

    public String getS_amount() {
        return s_amount;
    }

    public void setS_amount(String s_amount) {
        this.s_amount = s_amount;
    }

    public ArrayList<HashMap<String, String>> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<HashMap<String, String>> specialities) {
        this.specialities = specialities;
    }

    public ArrayList<HashMap<String, String>> getServices() {
        return services;
    }

    public void setServices(ArrayList<HashMap<String, String>> services) {
        this.services = services;
    }

    public ArrayList<HashMap<String, String>> getPay_methods() {
        return pay_methods;
    }

    public void setPay_methods(ArrayList<HashMap<String, String>> pay_methods) {
        this.pay_methods = pay_methods;
    }

    public ArrayList<Offices> getMyoffices() {
        return myoffices;
    }

    public void setMyoffices(ArrayList<Offices> myoffices) {
        this.myoffices = myoffices;
    }

    public class Offices implements Serializable {
        private String office_id, name_office,full_address,street,colony,num_ext,num_int,state,borough, city,latitude,longitude, first_amount, later_amount;
        private ArrayList<String> open_time;
        private ArrayList<String> locked_time;

        public String getOffice_id() {
            return office_id;
        }

        public void setOffice_id(String office_id) {
            this.office_id = office_id;
        }

        public String getName_office() {
            return name_office;
        }

        public void setName_office(String name_office) {
            this.name_office = name_office;
        }

        public String getFull_address() {
            return full_address;
        }

        public void setFull_address(String full_address) {
            this.full_address = full_address;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getColony() {
            return colony;
        }

        public void setColony(String colony) {
            this.colony = colony;
        }

        public String getNum_ext() {
            return num_ext;
        }

        public void setNum_ext(String num_ext) {
            this.num_ext = num_ext;
        }

        public String getNum_int() {
            return num_int;
        }

        public void setNum_int(String num_int) {
            this.num_int = num_int;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getBorough() {
            return borough;
        }

        public void setBorough(String borough) {
            this.borough = borough;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getFirst_amount() {
            return first_amount;
        }

        public void setFirst_amount(String first_amount) {
            this.first_amount = first_amount;
        }

        public String getLater_amount() {
            return later_amount;
        }

        public void setLater_amount(String later_amount) {
            this.later_amount = later_amount;
        }

        public ArrayList<String> getOpen_time() {
            return open_time;
        }

        public void setOpen_time(ArrayList<String> open_time) {
            this.open_time = open_time;
        }

        public ArrayList<String> getLocked_time() {
            return locked_time;
        }

        public void setLocked_time(ArrayList<String> locked_time) {
            this.locked_time = locked_time;
        }
    }

}
