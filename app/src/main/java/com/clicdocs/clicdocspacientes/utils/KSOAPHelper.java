package com.clicdocs.clicdocspacientes.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.clicdocs.clicdocspacientes.beans.ModelFilters;
import com.clicdocs.clicdocspacientes.beans.ModelIdentification;
import com.clicdocs.clicdocspacientes.beans.ModelOfficeWait;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class KSOAPHelper {
   // http://clicdocs.shandweb.com.mx/ws/clicdocs-patients/wsdl
   /* private static final String TAG = "KSOAPHelper";
    private static final String SOAP_ACTION = "http://clicdocs.shandweb.com.mx/CDSoapServer/clicdocs/";
    public static final String NAMESPACE = "http://clicdocs.shandweb.com.mx/CDSoapServer/clicdocs";
    private static final String URL = "http://clicdocs.shandweb.com.mx/CDSoapServer/clicdocs/wsdl";
    //private static final String URL = "http://192.168.1.133/CDSoapServer/clicdocs/wsdl";
    @SuppressLint("LongLogTag")
    public static Object getResponce(SoapObject request, String METHOD, final Context context) {
        final String soap_action =  SOAP_ACTION + METHOD;
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.dotNet = true;
        soapEnvelope.setAddAdornments(false);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.setOutputSoapObject(request);

        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
        httpTransportSE.debug = true;
        try {
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransportSE.call(soap_action, soapEnvelope);
            Log.v(TAG, httpTransportSE.requestDump);
            //Log.v(TAG, httpTransportSE.responseDump);
            if (soapEnvelope.bodyIn instanceof SoapFault) {
                String strFault = ((SoapFault) soapEnvelope.bodyIn).faultstring;
                Log.v(TAG, strFault);
                return null;
            } else {
                return soapEnvelope.bodyIn;
            }

        } catch (Exception e) {
            if (e instanceof SocketException || e instanceof IOException) {
                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                }
            }
            e.printStackTrace();
            return null;
        }
    }*/

    private static final String TAG = "KSOAPHelper";
    private static final String SOAP_ACTION = "http://clicdocs.shandweb.com.mx/ws/clicdocs-patients/";
    public static final String NAMESPACE = "http://clicdocs.shandweb.com.mx/ws/clicdocs-patients/";
    private static final String URL = "http://clicdocs.shandweb.com.mx/ws/clicdocs-patients/wsdl";
    //private static final String URL_AUX = "http://192.168.1.133/ws/clicdocs-patients/wsdl";

    @SuppressLint("LongLogTag")
    public static Object getResponce(SoapObject request, String METHOD, final Context context) {
        final String soap_action =  SOAP_ACTION + METHOD;
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.dotNet = true;
        soapEnvelope.setAddAdornments(false);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.setOutputSoapObject(request);

        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
        httpTransportSE.debug = true;
        try {
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransportSE.call(soap_action, soapEnvelope);
            Log.v(TAG, httpTransportSE.requestDump);
            //Log.v(TAG, httpTransportSE.responseDump);
            if (soapEnvelope.bodyIn instanceof SoapFault) {
                String strFault = ((SoapFault) soapEnvelope.bodyIn).faultstring;
                Log.v(TAG, strFault);
                return null;
            } else {
                return soapEnvelope.bodyIn;
            }

        } catch (Exception e) {
            if (e instanceof SocketException || e instanceof IOException) {
                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                }
            }
            e.printStackTrace();
            return null;
        }
    }

    public static HashMap<String, String> parseToHashMap(Object response) {
        KvmSerializable ks = (KvmSerializable) response;
        HashMap<String, String> data = new HashMap<>();
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            PropertyInfo info = new PropertyInfo();
            if(ks.getProperty(i) instanceof SoapObject) {
                SoapObject object = (SoapObject) ks.getProperty(i);
                for (int j = 0; j < object.getPropertyCount(); j++) {
                    object.getPropertyInfo(j, null, info);
                    data.put(info.getName(), info.getValue().toString());
                }
            } else {
                ks.getPropertyInfo(i, null, info);
                if(info.getValue() != null) {
                    data.put(info.getName(), info.getValue().toString());
                }
            }
        }
        return data;
    }
    public static ArrayList<HashMap<String, String>> parseToArrayList(Object response) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                Vector resp = (Vector) ((SoapObject) ks.getProperty(i)).getProperty("response");
                for(int j = 0; j < resp.size(); j++) {
                    SoapObject so = (SoapObject) resp.elementAt(j);
                    data.add(parseToHashMap(so));
                }
            }
        }
        return data;
    }

    //----------------------------------------------------------------------------------------------

    /*public static ArrayList<ModelOfficeWait> parseToArrayListOfficeWait(Object response) {
        ArrayList<ModelOfficeWait> data = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                Vector item = (Vector) ((SoapObject) ks.getProperty(i)).getProperty("response");
                for (int it = 0; it < item.size(); it++){
                    SoapObject pro = (SoapObject) item.elementAt(it);
                    Log.v("printOffice",pro.getName());
                    ModelOfficeWait mow = new ModelOfficeWait();
                    mow.setIdWaitList(pro.getPrimitivePropertyAsString("id_waiting_list"));
                    mow.setIdOffice(pro.getPrimitivePropertyAsString("id_doctor_office"));
                    mow.setNameOffice(pro.getPrimitivePropertyAsString("name"));

                    //========= ADDRESS OFFICES ==============
                    ArrayList<HashMap<String, String>> addO = new ArrayList<>();
                    Vector address = (Vector) pro.getProperty("address");
                    for(int x = 0; x < address.size(); x++) {
                        SoapObject oadd = (SoapObject) address.elementAt(x);
                        HashMap<String, String> addressO = parseToHashMap(oadd);
                        addO.add(addressO);
                    }
                    mow.setOfficesAddress(addO);
                    data.add(mow);
                    //========= DATA DOCTOR ==============
                    ArrayList<HashMap<String, String>> doc = new ArrayList<>();
                    Vector doctors = (Vector) pro.getProperty("doctor");
                    for(int x = 0; x < doctors.size(); x++) {
                        SoapObject odoc = (SoapObject) doctors.elementAt(x);
                        HashMap<String, String> doctor = parseToHashMap(odoc);
                        doc.add(doctor);
                    }
                    mow.setDoctor(doc);
                    data.add(mow);

                    //========= APPOINMENT ==============
                    ArrayList<HashMap<String, String>> appo = new ArrayList<>();
                    Vector appoiment = (Vector) pro.getProperty("appointment");
                    for(int x = 0; x < appoiment.size(); x++) {
                        SoapObject oappo = (SoapObject) appoiment.elementAt(x);
                        HashMap<String, String> appoin = parseToHashMap(oappo);
                        appo.add(appoin);
                    }
                    mow.setAppoinment(appo);
                    data.add(mow);
                }
            }
        }
        return data;
    }*/

    //===============================================================
    public static  HashMap<String, Object> parseToArrayList2(Object response) {
        HashMap<String, Object> hashMap = new HashMap<>();
        ArrayList<DoctorsFind> doct = new ArrayList<>();
        ArrayList<PharmacyFind> phar = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        Log.v("printKS",ks.getPropertyCount()+"");
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                SoapObject resp = (SoapObject) ((SoapObject) ks.getProperty(i)).getProperty("response");
                    if(resp.getProperty("doctors") instanceof Vector) {
                        Vector item = (Vector) resp.getProperty("doctors");
                        for (int it = 0; it < item.size(); it++){
                            SoapObject doc = (SoapObject) item.elementAt(it);
                            DoctorsFind my = new DoctorsFind();
                            my.setId_session(doc.getPrimitivePropertyAsString("id_provider_session"));
                            my.setName(doc.getPrimitivePropertyAsString("name"));
                            my.setFname(doc.getPrimitivePropertyAsString("fathers_last_name"));
                            my.setMname(doc.getPrimitivePropertyAsString("mothers_last_name"));
                            my.setPicture(doc.getPrimitivePropertyAsString("picture"));
                            my.setRaking(doc.getPrimitivePropertyAsString("ranking"));
                            my.setEnable(doc.getPrimitivePropertyAsString("enabled"));
                            //========= SPECIALITIES ==============
                            ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                            Vector specs = (Vector) doc.getProperty("specialities");
                            for(int x = 0; x < specs.size(); x++) {
                                SoapObject ospec = (SoapObject) specs.elementAt(x);
                                HashMap<String, String> speciality = parseToHashMap(ospec);
                                sp.add(speciality);
                            }
                            my.setSpecialities(sp);
                            //=========== SERVICES ================
                            ArrayList<HashMap<String, String>> services = new ArrayList<>();
                            Vector service = (Vector) doc.getProperty("services");
                            for(int x = 0; x < service.size(); x++) {
                                SoapObject oservi = (SoapObject) service.elementAt(x);
                                HashMap<String, String> servi = parseToHashMap(oservi);
                                services.add(servi);
                            }
                            my.setServices(services);
                            //============= OFFICES ==========================
                            ArrayList<DoctorsFind.Offices> offices = new ArrayList<>();
                            Vector office = (Vector) doc.getProperty("offices");
                            for(int x = 0; x < office.size(); x++) {
                                SoapObject oOffi = (SoapObject) office.elementAt(x);
                                DoctorsFind itemDoctor = new DoctorsFind();
                                DoctorsFind.Offices myOffices = itemDoctor.off;
                                myOffices.setId_doctor_office(oOffi.getPrimitivePropertyAsString("id_doctor_office"));
                                myOffices.setName(oOffi.getPrimitivePropertyAsString("name"));
                                myOffices.setFull_address(oOffi.getPrimitivePropertyAsString("full_address"));
                                myOffices.setEstado(oOffi.getPrimitivePropertyAsString("estado"));
                                myOffices.setMunicipio(oOffi.getPrimitivePropertyAsString("municipio"));
                                myOffices.setLatitude(oOffi.getPrimitivePropertyAsString("latitude"));
                                myOffices.setLongitude(oOffi.getPrimitivePropertyAsString("longitude"));
                                ArrayList<HashMap<String, String>>  sc = new ArrayList<>();
                                Vector schedules = (Vector) oOffi.getProperty("schedules");
                                for(int y = 0; y < schedules.size(); y++) {
                                    SoapObject oschedule = (SoapObject) schedules.elementAt(y);
                                    HashMap<String, String> schedule = parseToHashMap(oschedule);
                                    sc.add(schedule);
                                }
                                myOffices.setSchedules(sc);
                                offices.add(myOffices);
                            }
                            my.setMyoffices(offices);
                            doct.add(my);
                        }
                }
                if(resp.getProperty("pharmacies") instanceof Vector) {
                    Vector itemPhar = (Vector) resp.getProperty("pharmacies");
                    Log.v("printPharmacy",itemPhar.size()+"");
                    for (int it = 0; it < itemPhar.size(); it++) {
                        SoapObject pharm = (SoapObject) itemPhar.elementAt(it);
                        PharmacyFind pharmacyFind = new PharmacyFind();
                        pharmacyFind.setPharmacy_id(pharm.getPrimitivePropertyAsString("id_pharmacy"));
                        pharmacyFind.setPharmacy_name(pharm.getPrimitivePropertyAsString("name"));
                        pharmacyFind.setStreet(pharm.getPrimitivePropertyAsString("full_address"));
                        pharmacyFind.setState(pharm.getPrimitivePropertyAsString("estado"));
                        pharmacyFind.setBorough(pharm.getPrimitivePropertyAsString("municipio"));
                        pharmacyFind.setLatitude(pharm.getPrimitivePropertyAsString("latitude"));
                        pharmacyFind.setLongitude(pharm.getPrimitivePropertyAsString("longitude"));
                        phar.add(pharmacyFind);
                    }
                }
            }
        }
        hashMap.put("doctors",doct);
        hashMap.put("pharmacies",phar);

        return hashMap;
    }

    public static ArrayList<ProfileDoctor> parseToArrayListProfile(Object response) {
        ArrayList<ProfileDoctor> data = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                Vector item = (Vector) ((SoapObject) ks.getProperty(i)).getProperty("response");
                for (int it = 0; it < item.size(); it++){
                    SoapObject pro = (SoapObject) item.elementAt(it);
                    ProfileDoctor pd = new ProfileDoctor();
                    pd.setIdsesion(pro.getPrimitivePropertyAsString("id_provider_session"));
                    pd.setName(pro.getPrimitivePropertyAsString("name"));
                    pd.setFname(pro.getPrimitivePropertyAsString("fathers_last_name"));
                    pd.setMname(pro.getPrimitivePropertyAsString("mothers_last_name"));
                    pd.setPicture(pro.getPrimitivePropertyAsString("picture"));
                    pd.setEnable(pro.getPrimitivePropertyAsString("enabled"));
                    //========= SPECIALITIES ==============
                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                    Vector specialities = (Vector) pro.getProperty("specialities");
                    for(int x = 0; x < specialities.size(); x++) {
                        SoapObject osp = (SoapObject) specialities.elementAt(x);
                        HashMap<String, String> speciality = parseToHashMap(osp);
                        sp.add(speciality);
                    }
                    pd.setSpecialities(sp);
                    data.add(pd);
                    //========= SERVICES ==============
                    ArrayList<HashMap<String, String>> sv = new ArrayList<>();
                    Vector services = (Vector) pro.getProperty("services");
                    for(int x = 0; x < services.size(); x++) {
                        SoapObject osv = (SoapObject) services.elementAt(x);
                        HashMap<String, String> service = parseToHashMap(osv);
                        sv.add(service);
                    }
                    pd.setServices(sv);
                    data.add(pd);
                    //========= OFFICES ==============
                    ArrayList<ProfileDoctor.Offices> offices = new ArrayList<>();
                    Vector office = (Vector) pro.getProperty("offices");
                    for(int x = 0; x < office.size(); x++) {
                        SoapObject oOffi = (SoapObject) office.elementAt(x);
                        ProfileDoctor itemOffice = new ProfileDoctor();
                        ProfileDoctor.Offices myOffices = itemOffice.off;
                        myOffices.setId_doctor_office(oOffi.getPrimitivePropertyAsString("id_doctor_office"));
                        myOffices.setName(oOffi.getPrimitivePropertyAsString("name"));
                        myOffices.setFull_address(oOffi.getPrimitivePropertyAsString("full_address"));
                        myOffices.setEstado(oOffi.getPrimitivePropertyAsString("estado"));
                        myOffices.setMunicipio(oOffi.getPrimitivePropertyAsString("municipio"));
                        myOffices.setLatitude(oOffi.getPrimitivePropertyAsString("latitude"));
                        myOffices.setLongitude(oOffi.getPrimitivePropertyAsString("longitude"));
                        ArrayList<HashMap<String, String>>  sc = new ArrayList<>();
                        Vector schedules = (Vector) oOffi.getProperty("schedules");
                        for(int y = 0; y < schedules.size(); y++) {
                            SoapObject oschedule = (SoapObject) schedules.elementAt(y);
                            HashMap<String, String> schedule = parseToHashMap(oschedule);
                            sc.add(schedule);
                        }
                        myOffices.setSchedules(sc);
                        offices.add(myOffices);
                    }
                    pd.setMyoffices(offices);
                    data.add(pd);
                }
            }
        }
        return data;
    }

    //-----------------------------------------------------------------------------------------------
    public static  ArrayList<ModelFilters> parseToArrayListFilters(Object response) {

        ArrayList<ModelFilters> modFil = new ArrayList<>();
        ModelFilters mf = new ModelFilters();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                SoapObject resp = (SoapObject) ((SoapObject) ks.getProperty(i)).getProperty("response");
                if(resp.getProperty("specialities") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("specialities");
                    //========= SPECIALITIES ==============
                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                    for(int x = 0; x < item.size(); x++) {
                        SoapObject ospec = (SoapObject) item.elementAt(x);
                        HashMap<String, String> speciality = parseToHashMap(ospec);
                        sp.add(speciality);
                    }
                    mf.setSpecialities(sp);
                    modFil.add(mf);
                }
                if(resp.getProperty("clinics") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("clinics");
                    //========= CLICNICS ==============
                    ArrayList<HashMap<String, String>> cn = new ArrayList<>();
                    for(int x = 0; x < item.size(); x++) {
                        SoapObject oclic = (SoapObject) item.elementAt(x);
                        HashMap<String, String> clinic = parseToHashMap(oclic);
                        cn.add(clinic);
                    }
                    mf.setClicnics(cn);
                    modFil.add(mf);
                }
                if(resp.getProperty("services") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("services");
                    //========= SERVICES ==============
                    ArrayList<HashMap<String, String>> sv = new ArrayList<>();
                    for(int x = 0; x < item.size(); x++) {
                        SoapObject oserv = (SoapObject) item.elementAt(x);
                        HashMap<String, String> servi = parseToHashMap(oserv);
                        sv.add(servi);
                    }
                    mf.setServices(sv);
                    modFil.add(mf);
                }
                if(resp.getProperty("specialty_school") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("specialty_school");
                    //========= SPECIALTY SCHOOL ==============
                    ArrayList<HashMap<String, String>> ss = new ArrayList<>();
                    for(int x = 0; x < item.size(); x++) {
                        SoapObject oschool = (SoapObject) item.elementAt(x);
                        HashMap<String, String> servi = parseToHashMap(oschool);
                        ss.add(servi);
                    }
                    mf.setStudies(ss);
                    modFil.add(mf);
                }
                if(resp.getProperty("insurance") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("insurance");
                    //========= insurance ==============
                    ArrayList<HashMap<String, String>> se = new ArrayList<>();
                    for(int x = 0; x < item.size(); x++) {
                        SoapObject oschool = (SoapObject) item.elementAt(x);
                        HashMap<String, String> insurance = parseToHashMap(oschool);
                        se.add(insurance);
                    }
                    mf.setInsurance(se);
                    modFil.add(mf);
                }
            }
        }
        return modFil;
    }

    //------------------------------------------------------------------------------------------
    public static ArrayList<ModelIdentification> parseToArrayListIdentification(Object response) {
        ArrayList<ModelIdentification> data = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                Vector item = (Vector) ((SoapObject) ks.getProperty(i)).getProperty("response");
                for (int it = 0; it < item.size(); it++){
                    SoapObject pro = (SoapObject) item.elementAt(it);
                    ModelIdentification mi = new ModelIdentification();
                    mi.setMaritalStatus(pro.getPrimitivePropertyAsString("marital_status"));
                    mi.setOrigin(pro.getPrimitivePropertyAsString("origin"));
                    mi.setResidence(pro.getPrimitivePropertyAsString("residence"));
                    mi.setReligion(pro.getPrimitivePropertyAsString("religion"));
                    mi.setOcupation(pro.getPrimitivePropertyAsString("occupation"));
                    mi.setScholarship(pro.getPrimitivePropertyAsString("scholarship"));

                    //========= ADDRESS ==============
                    ArrayList<HashMap<String, String>> ad = new ArrayList<>();
                    Vector address = (Vector) pro.getProperty("address");
                    for(int x = 0; x < address.size(); x++) {
                        SoapObject oad = (SoapObject) address.elementAt(x);
                        HashMap<String, String> speciality = parseToHashMap(oad);
                        ad.add(speciality);
                    }
                    mi.setAddress(ad);
                    data.add(mi);
                }
            }
        }
        return data;
    }

    //===============================================================
    public static  ArrayList<String> parseToArrayListMunicipalities(Object response) {
        ArrayList<String> municipalities = new ArrayList<>();
        SoapObject ks = (SoapObject) response;
        for(int i = 0; i < ks.getPropertyCount(); i++) {
            if(ks.getProperty(i) instanceof SoapObject) {
                SoapObject resp = (SoapObject) ((SoapObject) ks.getProperty(i)).getProperty("response");
                if(resp.getProperty("municipalities") instanceof Vector) {
                    Vector item = (Vector) resp.getProperty("municipalities");
                    for (int it = 0; it < item.size(); it++){
                        SoapObject muni = (SoapObject) item.elementAt(it);
                        municipalities.add(muni.getPrimitivePropertyAsString("municipality"));
                    }
                }
            }
        }
        return municipalities;
    }
}
