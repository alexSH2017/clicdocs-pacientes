package com.clicdocs.clicdocspacientes.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RegisterFragment extends Fragment {
    private EditText ETname, ETplname, ETmlname, ETphone, ETbirthdate, ETemail, ETpass, ETpassConfi, ETcellphone;
    private TextView TVname, TVflname, TVmlname, TVphone, TVcellphone, TVgender, TVbirthdate, TVemail, TVpass, TVconfipass, TVterms;
    private ImageButton IMBback;
    private Spinner SPgender;
    private CheckBox CBaccept;
    private Button BTNregister;
    private MainActivity boostrap;
    private ImageView IVbackground;
    private AlertDialog DLsetgender, DLnoconnection;
    private ProgressDialog loading;
    private Calendar myCalendar;
    private Context ctx;
    private RequestQueue queueRegisters, queueTerms;
    private View view;
    private String birthday = "";
    private Calendar calendar;
    private SimpleDateFormat formatDate = new SimpleDateFormat("MMM dd, yyyy");
    private ArrayList<KeyPairsBean> gender = new ArrayList<>();
    private SessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view    = inflater.inflate(R.layout.register_fragment, container, false);
        BTNregister  = (Button) view.findViewById(R.id.BTNregister);
        IMBback      = (ImageButton) view.findViewById(R.id.IMBback);
        ETname       = (EditText) view.findViewById(R.id.ETname);
        ETplname     = (EditText) view.findViewById(R.id.ETplname);
        ETmlname     = (EditText) view.findViewById(R.id.ETmlname);
        ETphone      = (EditText) view.findViewById(R.id.ETphone);
        ETcellphone  = (EditText) view.findViewById(R.id.ETcellphone);
        ETbirthdate  = (EditText) view.findViewById(R.id.ETbirthdate);
        SPgender     = (Spinner) view.findViewById(R.id.SPgender);
        ETemail      = (EditText) view.findViewById(R.id.ETemail);
        ETpass       = (EditText) view.findViewById(R.id.ETpass);
        ETpassConfi  = (EditText) view.findViewById(R.id.ETpassConfi);
        CBaccept     = (CheckBox) view.findViewById(R.id.CBacceptTerms);

        TVname       = (TextView) view.findViewById(R.id.TVname);
        TVflname     = (TextView) view.findViewById(R.id.TVflname);
        TVmlname     = (TextView) view.findViewById(R.id.TVmlname);
        TVphone      = (TextView) view.findViewById(R.id.TVphone);
        TVcellphone  = (TextView) view.findViewById(R.id.TVcellphone);
        TVgender     = (TextView) view.findViewById(R.id.TVgender);
        TVbirthdate  = (TextView) view.findViewById(R.id.TVbirthdate);
        TVemail      = (TextView) view.findViewById(R.id.TVemail);
        TVpass       = (TextView) view.findViewById(R.id.TVpass);
        TVconfipass  = (TextView) view.findViewById(R.id.TVconfipass);
        TVterms      = (TextView) view.findViewById(R.id.TVterms);

        IVbackground = (ImageView) view.findViewById(R.id.IVbackground);
        boostrap     = (MainActivity) getActivity();
        ctx          = getContext();
        boostrap.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        gender.add(new KeyPairsBean(0,boostrap.langStrings.get(Constants.male_p)));
        gender.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.female_p)));

        TVterms.setText(Html.fromHtml("<u>"+boostrap.langStrings.get(Constants.terms_condi_p)+"</u>"));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().hide();
        prepareActionBar();
        session      = new SessionManager(ctx);
        ArrayAdapter<KeyPairsBean> genderA = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, gender);
        SPgender.setAdapter(genderA);
        ETname.setHint(boostrap.langStrings.get(Constants.name_p));
        ETplname.setHint(boostrap.langStrings.get(Constants.fname_p));
        ETmlname.setHint(boostrap.langStrings.get(Constants.mname_p));
        ETphone.setHint(boostrap.langStrings.get(Constants.phone_option_p));
        ETcellphone.setHint(boostrap.langStrings.get(Constants.cellphone_p));
        ETbirthdate.setHint(boostrap.langStrings.get(Constants.birthday_p));
        ETemail.setHint(boostrap.langStrings.get(Constants.email_p));
        ETpass.setHint(boostrap.langStrings.get(Constants.password_p));
        ETpassConfi.setHint(boostrap.langStrings.get(Constants.confi_pass_p));

        TVname.setText(boostrap.langStrings.get(Constants.name_p));
        TVflname.setText(boostrap.langStrings.get(Constants.fname_p));
        TVmlname.setText(boostrap.langStrings.get(Constants.mname_p));
        TVphone.setText(boostrap.langStrings.get(Constants.phone_p));
        TVcellphone.setText(boostrap.langStrings.get(Constants.cellphone_p));
        TVgender.setText(boostrap.langStrings.get(Constants.gender_p));
        TVbirthdate.setText(boostrap.langStrings.get(Constants.birthday_p));
        TVemail.setText(boostrap.langStrings.get(Constants.email_p));
        TVpass.setText(boostrap.langStrings.get(Constants.password_p));
        TVconfipass.setText(boostrap.langStrings.get(Constants.confi_pass_p));
        CBaccept.setText(boostrap.langStrings.get(Constants.accept_p));

        BTNregister.setText(boostrap.langStrings.get(Constants.create_account_p));
        prepareActionBar();
        myCalendar = Calendar.getInstance();
        calendar = Calendar.getInstance();
        ETbirthdate.setOnClickListener(ETbirthday_onClick);
        BTNregister.setOnClickListener(BTNregister_onClick);
        IMBback.setOnClickListener(IMBback_onClic);
        TVterms.setOnClickListener(TVterms_onClic);
    }

    // ============== BUTTONS EVENTS =========================
    private View.OnClickListener IMBback_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm = boostrap.getSupportFragmentManager();
            fm.popBackStack(fragments.REGISTER,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    };

    //Evento al dar click el textView TVterms
    /*
    Obtiene los tèrminos y condiciones de la aplicaciòn mandado como paràmetro el tipo de usuario (5 identifica que es para paciente)
    */
    private View.OnClickListener TVterms_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(NetworkUtils.haveNetworkConnection(ctx)) {
                final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                loading.show();
                queueTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                StringRequest request = new StringRequest(Request.Method.POST, Constants.get_terms, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.v("printLogin",""+response);
                            switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                case 200:
                                    JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                    JSONObject terms = jsonResp.getJSONObject(0);
                                    dialogTerms(terms.getString("term").toString());
                                    break;
                                case 403:
                                    Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                            .show();
                                    break;
                                case 404:
                                    Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                            .show();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("error", error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("id_user_type", "5");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.key_bundle_id, Constants.bundle_id);
                        headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                        return headers;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queueTerms.add(request);
            }  else {
                Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    };

    //Evento al dar click al botòn de registrar
    /*
    Valida primero los paràmetros antes de mandar al servicio de registrar al paciente
    */

    private View.OnClickListener BTNregister_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            //Validator validator = new Validator();
            if(!boostrap.validator.validate(ETname, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_TEXT}) &&
                    !boostrap.validator.validate(ETplname, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_TEXT}) &&
                    !boostrap.validator.validate(ETcellphone, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_NUMBER}) &&
                    !boostrap.validator.validate(ETbirthdate, new String[] {boostrap.validator.REQUIRED}) &&
                    !boostrap.validator.validate(ETemail, new String[] {boostrap.validator.REQUIRED, boostrap.validator.EMAIL}) &&
                    !boostrap.validator.validate(ETpass, new String[] {boostrap.validator.REQUIRED})) {
                    if (ETphone.getText().toString().length() !=0) {
                        if (!boostrap.validator.validate(ETphone, new String[] {boostrap.validator.ONLY_NUMBER})){
                            if (ETphone.getText().toString().length() == 10){
                                if (ETcellphone.length() == 10){
                                    String pass = ETpass.getText().toString();
                                    String passConfi = ETpassConfi.getText().toString();
                                    if (pass.length() >7) {
                                        if (passConfi.length() > 7){
                                            if (pass.equals(passConfi)){
                                                if (getAge() > 17){
                                                    if (CBaccept.isChecked()){
                                                        doRegisterRequest();
                                                    } else {
                                                        Snackbar.make(view,boostrap.langStrings.get(Constants.requier_accept_terms_p),Snackbar.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    AlertDialog error = new AlertDialog.Builder(ctx)
                                                            .setMessage(boostrap.langStrings.get(Constants.requier_age_legal_p))
                                                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    dialogInterface.dismiss();
                                                                }
                                                            }).show();
                                                }

                                            } else {
                                                AlertDialog errorConfir = new AlertDialog.Builder(ctx)
                                                        .setTitle("Error")
                                                        .setMessage(boostrap.langStrings.get(Constants.no_match_password_p))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        }).show();
                                            }
                                        } else ETpassConfi.setError(boostrap.langStrings.get(Constants.min_eight_characters_p));
                                    } else ETpass.setError(boostrap.langStrings.get(Constants.min_eight_characters_p));

                                } else { ETcellphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));  }
                            } else { ETphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p)); }
                        }
                    } else {
                        if (ETcellphone.length() == 10){
                            String pass = ETpass.getText().toString();
                            String passConfi = ETpassConfi.getText().toString();
                            if (pass.length() > 7){
                                if (passConfi.length() > 7) {
                                    if (pass.equals(passConfi)){
                                        if (getAge() > 17){
                                            if (CBaccept.isChecked()){
                                                doRegisterRequest();
                                            } else {
                                                Snackbar.make(view,boostrap.langStrings.get(Constants.requier_accept_terms_p),Snackbar.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            AlertDialog error = new AlertDialog.Builder(ctx)
                                                    .setMessage(boostrap.langStrings.get(Constants.requier_age_legal_p))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            ETbirthdate.setError("");
                                        }
                                    } else {
                                        AlertDialog errorConfir = new AlertDialog.Builder(ctx)
                                                .setTitle("Error")
                                                .setMessage(boostrap.langStrings.get(Constants.no_match_password_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();
                                    }
                                } else ETpassConfi.setError(boostrap.langStrings.get(Constants.min_eight_characters_p));

                            } else ETpass.setError(boostrap.langStrings.get(Constants.min_eight_characters_p));

                        } else { ETcellphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));  }
                    }
            }
        }
    };

    private View.OnClickListener ETbirthday_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(ctx, CPbirthday_onClick, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    //Funciòn dialogTerms
    /*
    Esta funciòn se ejecuta cuando el usuario quiere ver los tèrminos y condiciones.
    */
    private void dialogTerms(String terms){
        View dialog_terms        = LayoutInflater.from(ctx).inflate(R.layout.dialog_terms, null);
        Button BTNreject         = (Button) dialog_terms.findViewById(R.id.BTNreject);
        Button BTNclose         = (Button) dialog_terms.findViewById(R.id.BTNaccept);
        TextView TVterms         = (TextView) dialog_terms.findViewById(R.id.TVterms);
        final AlertDialog DLterms   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.terms_condi_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_terms).show();

        TVterms.setText(Html.fromHtml(terms));
        BTNclose.setText(boostrap.langStrings.get(Constants.close_p));
        BTNreject.setVisibility(View.INVISIBLE);

        BTNclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DLterms.dismiss();
            }
        });

    }

    private DatePickerDialog.OnDateSetListener CPbirthday_onClick = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDatePicked();
        }
    };

    // ============== METHODS ======================================

    private void setDatePicked() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        sdf.setTimeZone(myCalendar.getTimeZone());
        birthday = sdf.format(myCalendar.getTime());
        Date strDate = null;
        try {
            strDate = sdf.parse(birthday);
            calendar.setTime(strDate);
            ETbirthdate.setText(formatDate.format(calendar.getTime()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Integer getAge (){
        Date fechaNac=null;
        try {
            fechaNac = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar fechaActual = Calendar.getInstance();
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.setTime(fechaNac);

        int year = fechaActual.get(Calendar.YEAR)- fechaNacimiento.get(Calendar.YEAR);
        int mes =fechaActual.get(Calendar.MONTH)- fechaNacimiento.get(Calendar.MONTH);
        int dia = fechaActual.get(Calendar.DATE)- fechaNacimiento.get(Calendar.DATE);
        if(mes<0 || (mes==0 && dia<0)){
            year--;
        }
        return year;
    }

    //Funciòn doRegisterRequest
    /*
    Esta funciòm se ejecuta cuando se termina de validar los parametros que se van a mandar para el registro
     y los paràmetros que se mandan son:
     nombre, apellido paterno, apellido materno(opcional),fecha de naciemiento, gènero, telèfono (opcional),
     celular, correo electrònico, contraseña.
    */
    private void doRegisterRequest(){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueRegisters = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject user = jsonObject.getJSONObject("response");
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(user.getString("message"))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                ETname.setText("");
                                                ETplname.setText("");
                                                ETmlname.setText("");
                                                ETphone.setText("");
                                                ETcellphone.setText("");
                                                ETbirthdate.setText("");
                                                ETemail.setText("");
                                                ETpass.setText("");
                                                ETpassConfi.setText("");
                                                FragmentManager fm = boostrap.getSupportFragmentManager();
                                                fm.popBackStack(fragments.REGISTER,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                boostrap.setFragment(fragments.LOGIN, null);
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                hideError ();
                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 400:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("name", ETname.getText().toString());
                    params.put("fname", ETplname.getText().toString());
                    params.put("mname", ETmlname.getText().toString());
                    params.put("birthday", birthday);
                    params.put("gender", String.valueOf((SPgender.getSelectedItemPosition())+1));
                    params.put("phone", ETphone.getText().toString());
                    params.put("cellphone", ETcellphone.getText().toString());
                    params.put("email", ETemail.getText().toString());
                    params.put("password", ETpass.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueRegisters.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    // ============== SPECIALS ==============================
    private void prepareActionBar() {
        HashMap<String, String> options = new HashMap<>();
        options.put(Constants.TBtitle, "");
        options.put(Constants.TBhasBack, Constants.TBhasBack);
        boostrap.setActionBar(boostrap.TBmain, options);
    }

    private void hideError (){
        ETname.setError(null);
        ETmlname.setError(null);
        ETmlname.setError(null);
        ETbirthdate.setError(null);
        ETphone.setError(null);
        ETpass.setError(null);
        ETpassConfi.setError(null);
        ETcellphone.setError(null);
    }
}

