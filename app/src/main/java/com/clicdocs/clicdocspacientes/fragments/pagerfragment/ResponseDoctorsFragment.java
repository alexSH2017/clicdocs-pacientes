package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterDoctors;
import com.clicdocs.clicdocspacientes.fragments.ResponseSearchPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.MyCurrentListener;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;


public class ResponseDoctorsFragment extends Fragment implements ResponseSearchPagerFragment.FragmentLifecycle {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RecyclerView myRecycler;
    private LinearLayout PBloadMore;
    private View view;
    private Bundle b = new Bundle();
    private ArrayList<DoctorSearch> arrayDoctors = new ArrayList<>();
    private TextView TVnoResponse;
    private EditText ETsearch;
    private Button BTNsearch;
    private Switch SWlocation;
    private Spinner SPspecliaties, SPborough, SPstates, SPadvices, SPservices, SPclinic, SPseguros, SPpay_method;
    //private JSONObject jsonFilters;
    private  BottomSheetDialog bottomSheetDialog;
    private TextView TVpincial, TVpfinal;
    private CrystalRangeSeekbar seekbar;
    private final int WR_PERMS = 3453;
    private TextView TVdate;
    private TextView TVless, TVmore;
    RecyclerViewAdapterDoctors adapter;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat ndf = new SimpleDateFormat("dd MMM, yyyy");
    private Calendar myCalendar, calendar, currentCalendar;
    private String DATE_CURRENT = "";
    private String lati;
    private String longi;
    private final static int LOCATION = 312;
    //==========================================================
    LocationManager locationManager;
    AlertDialog alert = null;
    private HashMap<String, Object> hashMap = new HashMap<>();
    private RequestQueue queueSearch, queueState, queueBorough, queueAdvices, queueSpecilities, queueInsurance, queueClinic, queueServices;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view            = inflater.inflate(R.layout.response_doctors_fragment,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        b               = savedInstanceState;
        myRecycler      = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        PBloadMore      = (LinearLayout) view.findViewById(R.id.PBloadMore);
        TVnoResponse    = (TextView) view.findViewById(R.id.TVnoResponse);
        session         = new SessionManager(ctx);
        TVdate          = (TextView) view.findViewById(R.id.TVdate);
        TVless          = (TextView) view.findViewById(R.id.TVless);
        TVmore          = (TextView) view.findViewById(R.id.TVmore);
        TVless.setText("<");
        arrayDoctors    = (ArrayList<DoctorSearch>)boostrap.resultSearch.get("doctors");
        myCalendar      = Calendar.getInstance();
        calendar        = Calendar.getInstance();
        currentCalendar = Calendar.getInstance();
        PBloadMore.setVisibility(View.GONE);
        fillRecyclerViewDoctor(arrayDoctors);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(ctx,Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[] {Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, WR_PERMS);
        }
        TVdate.setText(ndf.format(myCalendar.getTime()));
        DATE_CURRENT = sdf.format(myCalendar.getTime()).toString();
        TVdate.setOnClickListener(TVdate_onClic);
        TVless.setOnClickListener(TVless_onClic);
        TVmore.setOnClickListener(TVmore_onClic);
    }

    private View.OnClickListener TVless_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                Date strDate = sdf.parse(DATE_CURRENT);
                calendar.setTime(strDate);
                calendar.add(Calendar.DAY_OF_MONTH,-1);
                Date currentDate = sdf.parse(String.valueOf(sdf.format(currentCalendar.getTime())));
                Date newDate = sdf.parse(String.valueOf(sdf.format(calendar.getTime())));
                if (newDate.before(currentDate)){
                    TVless.setEnabled(false);
                } else {
                    DATE_CURRENT = sdf.format(calendar.getTime()).toString();
                    Log.v("printDateCurrent",DATE_CURRENT);
                    TVdate.setText(ndf.format(calendar.getTime()));
                    searchByDate(calendar.get(Calendar.DAY_OF_WEEK));

                }
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener TVmore_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                TVless.setEnabled(true);
                Date strDate = sdf.parse(DATE_CURRENT);
                calendar.setTime(strDate);
                calendar.add(Calendar.DAY_OF_MONTH,+1);
                DATE_CURRENT = sdf.format(calendar.getTime()).toString();
                Log.v("printDateCurrent",DATE_CURRENT);
                TVdate.setText(ndf.format(calendar.getTime()));
                searchByDate(calendar.get(Calendar.DAY_OF_WEEK));
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private View.OnClickListener TVdate_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Calendar fecha = new GregorianCalendar();
            int año = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH);
            int dia = fecha.get(Calendar.DAY_OF_MONTH);
            Date d=null;
            try {
                d = sdf.parse(año+"-"+(mes+1)+"-"+dia);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            DatePickerDialog dpd= new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR,year);
                    myCalendar.set(Calendar.MONTH,monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                    try {
                        setDatePicked();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            dpd.getDatePicker().setMinDate(d.getTime());
            dpd.show();
        }
    };

    private void setDatePicked() throws JSONException, ParseException {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        sdf.setTimeZone(myCalendar.getTimeZone());

        Date currentDate = sdf.parse(String.valueOf(sdf.format(currentCalendar.getTime())));
        Date newDate = sdf.parse(String.valueOf(sdf.format(myCalendar.getTime())));
        if (newDate.equals(currentDate)) TVless.setEnabled(false);
         else TVless.setEnabled(true);
        DATE_CURRENT = sdf.format(myCalendar.getTime()).toString();
        TVdate.setText(ndf.format(myCalendar.getTime()));
        searchByDate(myCalendar.get(Calendar.DAY_OF_WEEK));
    }

    public void doSearchWhiteFilters () throws JSONException {
        TVnoResponse.setVisibility(View.GONE);
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearch",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                boostrap.WORD_SEARCH = ETsearch.getText().toString().trim();
                                JSONObject jsonDoctor = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonDoctor.getJSONArray("rows");
                                ArrayList<DoctorSearch> doct = new ArrayList<>();
                                for (int i = 0; i<jsonRows.length(); i++){
                                    DoctorSearch my = new DoctorSearch();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    my.setId_session(item.getString("doctor_id"));
                                    my.setName(item.getString("doctor_full_name"));
                                    my.setPicture(item.getString("picture"));
                                    my.setEnable(item.getString("enabled"));
                                    my.setFriends(item.getString("fb_friends"));
                                    my.setF_amount(item.getString("first_amount"));
                                    my.setS_amount(item.getString("later_amount"));
                                    Log.v("printCosto",item.getString("first_amount")+"   "+item.getString("later_amount"));

                                    //---------  SPECIALITIES  ------------------------------
                                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                    Log.v("printSpecialities",item.get("specialities").toString());
                                    if (!String.valueOf(item.get("specialities")).equals("")) {
                                        JSONArray jsonSpe = new JSONArray((String)item.get("specialities"));

                                        for (int k = 0; k < jsonSpe.length(); k++){
                                            JSONObject itemSpecialities = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> speciality = new HashMap<>();
                                            speciality.put("specialty_id",itemSpecialities.getString("id"));
                                            speciality.put("specialty",itemSpecialities.getString("speciality"));
                                            speciality.put("university",itemSpecialities.getString("university"));
                                            sp.add(speciality);
                                        }
                                    }
                                    my.setSpecialities(sp);

                                    //-------------- OFFICE  -------------------------------------------
                                    ArrayList<DoctorSearch.Offices> offices = new ArrayList<>();

                                    if (String.valueOf(item.get("consulting_rooms")).length() > 4) {
                                        JSONArray jsonConsulting = item.getJSONArray("consulting_rooms");
                                        for (int z = 0; z<jsonConsulting.length(); z++){
                                            JSONObject itemOffice = jsonConsulting.getJSONObject(z);
                                            //Log.v("printNameOffice",itemOffice.getString("office"));
                                            DoctorSearch itemDoctor = new DoctorSearch();
                                            DoctorSearch.Offices myOffices = itemDoctor.off;
                                            myOffices.setOffice_id(itemOffice.getString("id"));
                                            myOffices.setName_office(itemOffice.getString("office"));
                                            myOffices.setFull_address(itemOffice.getString("full_address"));
                                            myOffices.setStreet(itemOffice.getString("street"));
                                            myOffices.setColony(itemOffice.getString("colony"));
                                            myOffices.setNum_ext(itemOffice.getString("outdoor_number"));
                                            myOffices.setNum_int(itemOffice.getString("interior_number"));
                                            myOffices.setState(itemOffice.getString("state"));
                                            myOffices.setBorough(itemOffice.getString("borough"));
                                            myOffices.setCity(itemOffice.getString("city"));
                                            myOffices.setLatitude(itemOffice.getString("latitude"));
                                            myOffices.setLongitude(itemOffice.getString("longitude"));
                                            myOffices.setFirst_amount(item.getString("first_amount"));
                                            myOffices.setLater_amount(item.getString("later_amount"));
                                            //---------- HOURS --------------------------------------
                                            ArrayList<String> open_times = new ArrayList<String>();
                                            ArrayList<String> locked_times = new ArrayList<>();
                                            JSONObject jsonTime = itemOffice.getJSONObject("times");
                                            if (String .valueOf(jsonTime.get(DATE_CURRENT)).length() > 4){
                                                JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                                //Log.v("printJsonDate",""+jsonDate);
                                                JSONArray jsonOpen = jsonDate.getJSONArray("open_times");
                                                JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                                for(int l = 0; l < jsonOpen.length(); l++) {
                                                    open_times.add(jsonOpen.get(l).toString());
                                                }

                                                for(int m = 0; m < jsonLocked.length(); m++) {
                                                    locked_times.add(jsonLocked.get(m).toString());
                                                }
                                            }
                                            myOffices.setLocked_time(locked_times);
                                            myOffices.setOpen_time(open_times);
                                            offices.add(myOffices);
                                        }
                                    }

                                    my.setMyoffices(offices);
                                    doct.add(my);
                                }
                                hashMap.put("doctors",doct);
                                hashMap.put("pharmacies",boostrap.resultSearch.get("pharmacies"));
                                boostrap.resultSearch = hashMap;
                                adapter.updateByDate(doct,DATE_CURRENT);
                                break;
                            case 204:
                                TVnoResponse.setVisibility(View.VISIBLE);
                                ArrayList<DoctorSearch> docto = new ArrayList<>();
                                hashMap.put("doctors",docto);
                                hashMap.put("pharmacies",boostrap.resultSearch.get("pharmacies"));
                                boostrap.resultSearch = hashMap;
                                TVnoResponse.setVisibility(View.VISIBLE);
                                TVnoResponse.setText(boostrap.langStrings.get(Constants.no_result_p));
                                adapter.clear();
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    Log.v("printPArams",boostrap.jsonSearch.toString());
                    params.put("fields", boostrap.jsonSearch.toString());
                    params.put("date", DATE_CURRENT);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);

        } else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }


    private void FilterSearch () throws JSONException {
        bottomSheetDialog = new BottomSheetDialog(ctx);
        View parentView         = boostrap.getLayoutInflater().inflate(R.layout.filters_dialog,null);
        ETsearch                = (EditText) parentView.findViewById(R.id.ETsearch);
        SWlocation              = (Switch) parentView.findViewById(R.id.SWlocation);
        SPstates                = (Spinner) parentView.findViewById(R.id.SPstates);
        SPborough               = (Spinner) parentView.findViewById(R.id.SPborough);
        SPspecliaties           = (Spinner) parentView.findViewById(R.id.SPspecialities);
        SPadvices               = (Spinner) parentView.findViewById(R.id.SPadvices);
        SPclinic                = (Spinner) parentView.findViewById(R.id.SPclinic);
        SPservices              = (Spinner) parentView.findViewById(R.id.SPservices);
        SPseguros               = (Spinner) parentView.findViewById(R.id.SPseguro);
        SPpay_method            = (Spinner) parentView.findViewById(R.id.SPpay_method);
        TVpincial               = (TextView) parentView.findViewById(R.id.TVpinicial);
        TVpfinal                = (TextView) parentView.findViewById(R.id.TVpfinal);
        seekbar                 = (CrystalRangeSeekbar) parentView.findViewById(R.id.seekbar);
        BTNsearch               = (Button) parentView.findViewById(R.id.BTNsearch);
        if (boostrap.active)
            SWlocation.setChecked(true);
        else
            SWlocation.setChecked(false);
        TextView TVnext_of_me   = (TextView) parentView.findViewById(R.id.TVnext_of_me);
        TextView TVstate        = (TextView) parentView.findViewById(R.id.TVstate);
        TextView TVdeleg_mun    = (TextView) parentView.findViewById(R.id.TVdeleg_mun);
        TextView TVspeciality   = (TextView) parentView.findViewById(R.id.TVspeciality);
        TextView TVcouncil      = (TextView) parentView.findViewById(R.id.TVcouncil);
        TextView TVclinic       = (TextView) parentView.findViewById(R.id.TVclinic);
        TextView TVinsurance    = (TextView) parentView.findViewById(R.id.TVinsurance);
        TextView TVservices     = (TextView) parentView.findViewById(R.id.TVservices);
        TextView TVmethod_pay   = (TextView) parentView.findViewById(R.id.TVmethod_pay);
        TextView TVrange_price  = (TextView) parentView.findViewById(R.id.TVrange_price);
        TextInputLayout TILhint = (TextInputLayout) parentView.findViewById(R.id.TILhint);

        TILhint.setHint(boostrap.langStrings.get(Constants.name_doctor_p));
        TVstate.setText(boostrap.langStrings.get(Constants.state_p));
        TVdeleg_mun.setText(boostrap.langStrings.get(Constants.deleg_mun_p));
        TVspeciality.setText(boostrap.langStrings.get(Constants.speciality_p));
        TVcouncil.setText(boostrap.langStrings.get(Constants.council_spe_p));
        TVclinic.setText(boostrap.langStrings.get(Constants.clinic_p));
        TVinsurance.setText(boostrap.langStrings.get(Constants.insurance_p));
        TVservices.setText(boostrap.langStrings.get(Constants.services_p));
        TVmethod_pay.setText(boostrap.langStrings.get(Constants.method_pay_p));
        TVrange_price.setText(boostrap.langStrings.get(Constants.price_range_p));

        BTNsearch.setText(boostrap.langStrings.get(Constants.search_p));

        ETsearch.setText(boostrap.langStrings.get(Constants.name_of_doctor_p));
        TVnext_of_me.setText(boostrap.langStrings.get(Constants.next_to_me_p));

        ArrayList<String> method_pay = new ArrayList<>();
        method_pay.add(boostrap.langStrings.get(Constants.selected_option_p));
        method_pay.add("Visa/MasterCard");
        method_pay.add("American Express");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, method_pay);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPpay_method.setAdapter(adapter);

        bottomSheetDialog.setContentView(parentView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,300,getResources().getDisplayMetrics()));
        bottomSheetDialog.show();
        ETsearch.setText(boostrap.WORD_SEARCH);
        BTNsearch.setOnClickListener(BTNsearch_onClic);
        getStates();
        /*locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            SWlocation.setChecked(false);
        } else {
            SWlocation.setChecked(true);
        }*/

        seekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                TVpincial.setText(String.valueOf(minValue));
                TVpfinal.setText(String.valueOf(maxValue));
            }
        });

        SWlocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        SWlocation.setChecked(false);
                        AlertNoOps();
                    } else {
                        getCoordenates();
                    }
                }
                else {
                    boostrap.active = false;
                }
            }
        });
    }

    private void AlertNoOps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(boostrap.langStrings.get(Constants.legend_gps_p))
                .setCancelable(false)
                .setPositiveButton(boostrap.langStrings.get(Constants.yes_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION);
                    }
                })
                .setNegativeButton(boostrap.langStrings.get(Constants.not_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        SWlocation.setChecked(false);
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private boolean getCoordenates() {
        LocationManager locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
        MyCurrentListener locationListener = new MyCurrentListener(new MyCurrentListener.evento() {
            @Override
            public void onComplete(String latitud, String longitud) {
                lati = latitud;
                longi = longitud;
                boostrap.active = true;
                Log.v("printLocation",latitud+"   "+longitud);
            }
        });
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return true;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        return true;
    }

    private View.OnClickListener BTNsearch_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(SWlocation.isChecked()){
                if (getCoordenates()){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (lati != null && longi != null){
                                fillJson();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(boostrap.langStrings.get(Constants.legend_fail_coordenates_p))
                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {dialog.cancel();}
                                        });
                                builder.show();
                            }
                        }
                    },3000);
                }
            } else {
                boostrap.lati = "";
                boostrap.longi = "";
                fillJson();
            }
        }
    };

    private void fillJson (){
        boostrap.jsonSearch = new JSONObject();
        try {
            boostrap.jsonSearch.put("doctor_full_name",ETsearch.getText().toString().trim().replaceAll("\n", " "));
            if (SWlocation.isChecked()){
                boostrap.lati = lati;
                boostrap.longi = longi;
                boostrap.jsonSearch.put("location",lati+","+longi);
            }
            if (SPstates.getSelectedItemPosition() != 0){ boostrap.jsonSearch.put("state",SPstates.getSelectedItem().toString()); }
            if (SPborough.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("borough",SPborough.getSelectedItem().toString()); }
            if (SPspecliaties.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("specialities",SPspecliaties.getSelectedItem().toString()); }
            if (SPadvices.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("advices",SPadvices.getSelectedItem().toString()); }
            if (SPclinic.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("clinic",SPclinic.getSelectedItem().toString()); }
            if (SPservices.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("services",SPservices.getSelectedItem().toString()); }
            if (SPseguros.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("insurances",SPseguros.getSelectedItem().toString()); }
            if (SPpay_method.getSelectedItemPosition() != 0) { boostrap.jsonSearch.put("pay_methods",SPpay_method.getSelectedItem().toString()); }

            boostrap.jsonSearch.put("patient_id",(session.profile().get(0) != null)? session.profile().get(0).toString() : "");
            boostrap.jsonSearch.put("min_amount", TVpincial.getText().toString());
            boostrap.jsonSearch.put("max_amount",TVpfinal.getText().toString());
            boostrap.jsonSearch.put("patient_id",(session.profile().get(0) != null)? session.profile().get(0).toString() : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            doSearchWhiteFilters();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        bottomSheetDialog.dismiss();
    }

    private void fillRecyclerViewDoctor (final ArrayList<DoctorSearch> doctors){
        for (int i =0; i<doctors.size();i++){
        }
        if (doctors.size() == 0) { TVnoResponse.setVisibility(View.VISIBLE); TVnoResponse.setText(boostrap.langStrings.get(Constants.no_result_p));    }
        else { TVnoResponse.setVisibility(View.GONE); }
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        adapter = new RecyclerViewAdapterDoctors(view,doctors, ctx, boostrap, sdf.format(myCalendar.getTime()).toString(), new RecyclerViewAdapterDoctors.Event() {
            @Override
            public void onClicViewProfile(String id) {
                Bundle b = new Bundle();
                b.putString("doctor_id",id);
                boostrap.setFragment(fragments.PROFILEDOCTOR,b);
            }

            @Override
            public void onClicOffice(String idSession, String name, String speciality, String picture, String enable) {
                Log.v("printOnclic",idSession+""+name+""+picture+""+enable);
                Bundle b = new Bundle();
                b.putString("idSession",idSession);
                b.putString("name",name);
                b.putString("speciality",speciality);
                b.putString("picture",picture);
                b.putString("enable",enable);
                b.putInt("card",0);
                boostrap.setFragment(fragments.OFFICES,b);
            }

            @Override
            public void setDate(String date) throws ParseException {
                Date strDate = sdf.parse(date);
                calendar.setTime(strDate);
                DATE_CURRENT = sdf.format(calendar.getTime()).toString();
                TVdate.setText(ndf.format(calendar.getTime()));
            }

            @Override
            public void onPager(final int i) {
                PBloadMore.setVisibility(View.VISIBLE);
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getPageDoctor(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },1000);

            }

            @Override
            public void onClicShare(String pic, String id) {
                FacebookSdk.sdkInitialize(boostrap.getApplicationContext());
                ShareDialog shareDialog = new ShareDialog(boostrap);
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("https://clicdocs.com/patients/medic/"+id))
                        .build();
                Log.v("printContent",content.toString());
                shareDialog.show(content, ShareDialog.Mode.WEB);

            }
        });
        myRecycler.setAdapter(adapter);

        myRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.v("printScroll",""+dy);
            }
        });
    }

    private void getPageDoctor (final int i) throws JSONException {
        TVnoResponse.setVisibility(View.GONE);
        if (!boostrap.lati.isEmpty()){
            boostrap.jsonSearch.put("location",boostrap.lati+","+boostrap.longi);
        }
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearch",""+response);
                    loading.dismiss();
                    PBloadMore.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonDoctor = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonDoctor.getJSONArray("rows");
                                ArrayList<DoctorSearch> doct = new ArrayList<>();
                                for (int i = 0; i<jsonRows.length(); i++){
                                    DoctorSearch my = new DoctorSearch();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    my.setId_session(item.getString("doctor_id"));
                                    my.setName(item.getString("doctor_full_name"));
                                    my.setPicture(item.getString("picture"));
                                    my.setEnable(item.getString("enabled"));
                                    my.setFriends(item.getString("fb_friends"));
                                    my.setF_amount(item.getString("first_amount"));
                                    my.setS_amount(item.getString("later_amount"));

                                    //---------  SPECIALITIES  ------------------------------
                                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                    if (!String.valueOf(item.get("specialities")).equals("")) {
                                        JSONArray jsonSpe = new JSONArray((String)item.get("specialities"));

                                        for (int k = 0; k < jsonSpe.length(); k++){
                                            JSONObject itemSpecialities = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> speciality = new HashMap<>();
                                            speciality.put("specialty_id",itemSpecialities.getString("id"));
                                            speciality.put("specialty",itemSpecialities.getString("speciality"));
                                            speciality.put("university",itemSpecialities.getString("university"));
                                            sp.add(speciality);
                                        }

                                    }
                                    my.setSpecialities(sp);
                                    //-------------- OFFICE  -------------------------------------------
                                    ArrayList<DoctorSearch.Offices> offices = new ArrayList<>();
                                    if (String.valueOf(item.get("consulting_rooms")).length() > 4) {
                                        JSONArray jsonConsulting = item.getJSONArray("consulting_rooms");
                                        for (int z = 0; z<jsonConsulting.length(); z++){
                                            JSONObject itemOffice = jsonConsulting.getJSONObject(z);
                                            //Log.v("printNameOffice",itemOffice.getString("office"));
                                            DoctorSearch itemDoctor = new DoctorSearch();
                                            DoctorSearch.Offices myOffices = itemDoctor.off;
                                            myOffices.setOffice_id(itemOffice.getString("id"));
                                            myOffices.setName_office(itemOffice.getString("office"));
                                            myOffices.setFull_address(itemOffice.getString("full_address"));
                                            myOffices.setStreet(itemOffice.getString("street"));
                                            myOffices.setColony(itemOffice.getString("colony"));
                                            myOffices.setNum_ext(itemOffice.getString("outdoor_number"));
                                            myOffices.setNum_int(itemOffice.getString("interior_number"));
                                            myOffices.setState(itemOffice.getString("state"));
                                            myOffices.setBorough(itemOffice.getString("borough"));
                                            myOffices.setCity(itemOffice.getString("city"));
                                            myOffices.setLatitude(itemOffice.getString("latitude"));
                                            myOffices.setLongitude(itemOffice.getString("longitude"));
                                            myOffices.setFirst_amount(itemOffice.getString("first_amount"));
                                            myOffices.setLater_amount(itemOffice.getString("later_amount"));

                                            //---------- HOURS --------------------------------------
                                            ArrayList<String> open_times = new ArrayList<String>();
                                            ArrayList<String> locked_times = new ArrayList<>();
                                            JSONObject jsonTime = itemOffice.getJSONObject("times");
                                            if (!String .valueOf(jsonTime.get(DATE_CURRENT)).equals("null")){
                                                JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                                //Log.v("printJsonDate",""+jsonDate);
                                                JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                                JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                                for(int l = 0; l < jsonOpen.length(); l++) {
                                                    open_times.add(jsonOpen.get(l).toString());
                                                }

                                                for(int m = 0; m < jsonLocked.length(); m++) {
                                                    locked_times.add(jsonLocked.get(m).toString());
                                                }
                                            }
                                            myOffices.setLocked_time(locked_times);
                                            myOffices.setOpen_time(open_times);
                                            offices.add(myOffices);
                                        }
                                    }
                                    my.setMyoffices(offices);
                                    arrayDoctors.add(my);
                                    doct.add(my);
                                }
                                adapter.add(doct);

                                break;
                            case 204:
                                ArrayList<DoctorSearch> docto = new ArrayList<>();
                                hashMap.put("doctors",docto);
                                hashMap.put("pharmacies",boostrap.resultSearch.get("pharmacies"));
                                boostrap.resultSearch = hashMap;
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", boostrap.jsonSearch.toString());
                    params.put("limit", ""+i);
                    params.put("date", DATE_CURRENT);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);

        } else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    private void searchByDate (final int day) throws JSONException {
        TVnoResponse.setVisibility(View.GONE);
        if (!boostrap.lati.isEmpty()){
            boostrap.jsonSearch.put("location",boostrap.lati+","+boostrap.longi);
        }
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearch",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonDoctor = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonDoctor.getJSONArray("rows");
                                ArrayList<DoctorSearch> doct = new ArrayList<>();
                                for (int i = 0; i<jsonRows.length(); i++){
                                    DoctorSearch my = new DoctorSearch();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    my.setId_session(item.getString("doctor_id"));
                                    my.setName(item.getString("doctor_full_name"));
                                    my.setPicture(item.getString("picture"));
                                    my.setEnable(item.getString("enabled"));
                                    my.setFriends(item.getString("fb_friends"));
                                    my.setF_amount(item.getString("first_amount"));
                                    my.setS_amount(item.getString("later_amount"));

                                    //---------  SPECIALITIES  ------------------------------
                                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                    if (!String.valueOf(item.get("specialities")).equals("")) {
                                        JSONArray jsonSpe = new JSONArray((String)item.get("specialities"));

                                        for (int k = 0; k < jsonSpe.length(); k++){
                                            JSONObject itemSpecialities = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> speciality = new HashMap<>();
                                            speciality.put("specialty_id",itemSpecialities.getString("id"));
                                            speciality.put("specialty",itemSpecialities.getString("speciality"));
                                            speciality.put("university",itemSpecialities.getString("university"));
                                            sp.add(speciality);
                                        }

                                    }
                                    my.setSpecialities(sp);
                                    //-------------- OFFICE  -------------------------------------------
                                    ArrayList<DoctorSearch.Offices> offices = new ArrayList<>();
                                    if (String.valueOf(item.get("consulting_rooms")).length() > 4) {
                                        JSONArray jsonConsulting = item.getJSONArray("consulting_rooms");
                                        for (int z = 0; z<jsonConsulting.length(); z++){
                                            JSONObject itemOffice = jsonConsulting.getJSONObject(z);
                                            //Log.v("printNameOffice",itemOffice.getString("office"));
                                            DoctorSearch itemDoctor = new DoctorSearch();
                                            DoctorSearch.Offices myOffices = itemDoctor.off;
                                            myOffices.setOffice_id(itemOffice.getString("id"));
                                            myOffices.setName_office(itemOffice.getString("office"));
                                            myOffices.setFull_address(itemOffice.getString("full_address"));
                                            myOffices.setStreet(itemOffice.getString("street"));
                                            myOffices.setColony(itemOffice.getString("colony"));
                                            myOffices.setNum_ext(itemOffice.getString("outdoor_number"));
                                            myOffices.setNum_int(itemOffice.getString("interior_number"));
                                            myOffices.setState(itemOffice.getString("state"));
                                            myOffices.setBorough(itemOffice.getString("borough"));
                                            myOffices.setCity(itemOffice.getString("city"));
                                            myOffices.setLatitude(itemOffice.getString("latitude"));
                                            myOffices.setLongitude(itemOffice.getString("longitude"));
                                            myOffices.setFirst_amount(itemOffice.getString("first_amount"));
                                            myOffices.setLater_amount(itemOffice.getString("later_amount"));

                                            //---------- HOURS --------------------------------------
                                            ArrayList<String> open_times = new ArrayList<String>();
                                            ArrayList<String> locked_times = new ArrayList<>();
                                            JSONObject jsonTime = itemOffice.getJSONObject("times");
                                            if (!String .valueOf(jsonTime.get(DATE_CURRENT)).equals("null")){
                                                JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                                //Log.v("printJsonDate",""+jsonDate);
                                                JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                                JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                                for(int l = 0; l < jsonOpen.length(); l++) {
                                                    open_times.add(jsonOpen.get(l).toString());
                                                }

                                                for(int m = 0; m < jsonLocked.length(); m++) {
                                                    locked_times.add(jsonLocked.get(m).toString());
                                                }
                                            }
                                            myOffices.setLocked_time(locked_times);
                                            myOffices.setOpen_time(open_times);
                                            offices.add(myOffices);
                                        }
                                    }
                                    my.setMyoffices(offices);
                                    doct.add(my);
                                }
                                hashMap.put("doctors",doct);
                                hashMap.put("pharmacies",boostrap.resultSearch.get("pharmacies"));
                                boostrap.resultSearch = hashMap;
                                adapter.updateByDate(doct,DATE_CURRENT);
                                break;
                            case 204:
                                ArrayList<DoctorSearch> docto = new ArrayList<>();
                                hashMap.put("doctors",docto);
                                hashMap.put("pharmacies",boostrap.resultSearch.get("pharmacies"));
                                boostrap.resultSearch = hashMap;
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", boostrap.jsonSearch.toString());
                    params.put("filters", "");
                    params.put("date", DATE_CURRENT);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);

        } else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }

    }

    public void initView() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WR_PERMS){
            //goRegisterAppoinment();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_filters,menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.filters_p));
        menu.getItem(1).setTitle(boostrap.langStrings.get(Constants.map_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.filters:
                try {
                    FilterSearch();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.mapDoctors:
                Bundle b = new Bundle();
                b.putSerializable("doctors",arrayDoctors);
                boostrap.setFragment(fragments.MAPDOCTORS,b);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static String ucFirst(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        } else {
            return  Character.toUpperCase(str.charAt(0)) + str.substring(1, str.length()).toLowerCase();
        }
    }

    //==============================  OBTENCION DE FILTROS  ======================================

    private void getStates (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueState = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_states, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRows = jsonObject.getJSONArray(Constants.response);
                                ArrayList<String> states = new ArrayList<>();
                                states.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    states.add(item.getString("state"));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, states);
                                SPstates.setAdapter(adapter);
                                for (int i = 0; i < adapter.getCount(); i++){
                                    if (boostrap.STATE_SEARCH.equals(adapter.getItem(i))){
                                        SPstates.setSelection(i);
                                    }
                                }
                                getAllBorough();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueState.add(request);
        }  else {

        }
    }

    private void getAllBorough (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueBorough = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_borough, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRows = jsonObject.getJSONArray(Constants.response);
                                ArrayList<String> borough = new ArrayList<>();
                                borough.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    borough.add(item.getString("borough"));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, borough);
                                SPborough.setAdapter(adapter);

                                getAllSpecialities();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueBorough.add(request);
        }  else {

        }
    }

    private void getAllSpecialities (){
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queueSpecilities = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_specialities, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printGetSpecialities",""+response);
                    //loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonInsuranceAll = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonInsuranceAll.getJSONArray("rows");
                                ArrayList<String> specialities = new ArrayList<>();
                                specialities.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    specialities.add(Miscellaneous.ucFirst(item.getString("name")));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, specialities);
                                SPspecliaties.setAdapter(adapter);
                                for (int j = 0; j < adapter.getCount(); j++){
                                    if (boostrap.SPECIALITY_SEARCH.equals(adapter.getItem(j))){
                                        SPspecliaties.setSelection(j);
                                    }
                                }
                                getAllAdvices();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, "");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSpecilities.add(request);
        }  else {

        }
    }

    private void getAllAdvices() {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueAdvices = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_advices, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject resp = new JSONObject(response);
                        if(resp.getInt("code") == 200) {
                            JSONArray advices = resp.getJSONArray("response");
                            ArrayList<String> items = new ArrayList<>();
                            if(advices.length() > 0) {
                                items.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < advices.length(); i++) {
                                    JSONObject advice = new JSONObject(advices.getString(i));
                                    items.add( advice.getString("advice_name"));
                                }
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, items);
                            SPadvices.setAdapter(adapter);
                            getAllClinic();
                        } else {

                            Toast.makeText(ctx, resp.getString("response"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("Error", "errorsaso mamalon");
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAdvices.add(request);
        }
    }

    private void getAllInsurances () throws JSONException {
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueInsurance = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_insurance, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printInsurance",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonInsuranceAll = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonInsuranceAll.getJSONArray("rows");
                                ArrayList<String> insurances = new ArrayList<>();
                                insurances.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    insurances.add(item.getString("insurance"));
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, insurances);
                                SPseguros.setAdapter(adapter);
                                getAllServices();
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, "");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueInsurance.add(request);
        }  else {

        }
    }

    private void getAllClinic () throws JSONException {
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueClinic = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_clinic, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printInsurance",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonClinic = jsonObject.getJSONArray("response");
                                ArrayList<String> clinic = new ArrayList<>();
                                clinic.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonClinic.length(); i++) {
                                    JSONObject item = jsonClinic.getJSONObject(i);
                                    clinic.add(item.getString("name"));
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, clinic);
                                SPclinic.setAdapter(adapter);
                                getAllInsurances();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueClinic.add(request);
        }  else {

        }
    }

    private void getAllServices () throws JSONException {
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueServices = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_services, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printInsurance",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonServicesAll = jsonObject.getJSONArray("response");

                                ArrayList<String> services = new ArrayList<>();
                                services.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonServicesAll.length(); i++) {
                                    JSONObject item = jsonServicesAll.getJSONObject(i);
                                    services.add(item.getString("name"));
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, services);
                                SPservices.setAdapter(adapter);
                                break;

                            case 402:
                                ArrayList<String> noFuondServices = new ArrayList<>();
                                noFuondServices.add("Sin servicios");
                                ArrayAdapter<String> adapterNoFund = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, noFuondServices);
                                SPservices.setAdapter(adapterNoFund);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, "");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueServices.add(request);
        }  else {

        }
    }
    //=============================================================================================

    @Override
    public void onResumeFragment() {

    }

}


