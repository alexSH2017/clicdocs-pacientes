package com.clicdocs.clicdocspacientes.fragments;

import android.content.Context;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by apc_g on 29/09/2017.
 */

public class ChatFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private WebView WVchat;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment,container,false);
        boostrap    = (MainActivity) getActivity();
        ctx         = getContext();
        session     = new SessionManager(ctx);
        WVchat      = (WebView) view.findViewById(R.id.WVchat);
        WebSettings webSettings = WVchat.getSettings();
        webSettings.setJavaScriptEnabled(true);
        WVchat.setWebViewClient(new WebViewClient(){
            public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
                try {
                    TrustManagerFactory tmf = TrustManagerUtil.getTrustManagerFactory(ctx, R.raw.keystore, "$h@ndw3b.c0m");
                    for(TrustManager t: tmf.getTrustManagers()){
                        if (t instanceof X509TrustManager) {
                            X509TrustManager trustManager = (X509TrustManager) t;
                            Bundle bundle = SslCertificate.saveState(error.getCertificate());
                            X509Certificate x509Certificate;
                            byte[] bytes = bundle.getByteArray("x509-certificate");
                            if (bytes == null) {
                                x509Certificate = null;
                            } else {
                                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                                Certificate cert = certFactory.generateCertificate(new ByteArrayInputStream(bytes));
                                x509Certificate = (X509Certificate) cert;
                            }
                            X509Certificate[] x509Certificates = new X509Certificate[1];
                            x509Certificates[0] = x509Certificate;
                            trustManager.checkServerTrusted(x509Certificates, "ECDH_RSA");
                        }
                    }
                    handler.proceed();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UnrecoverableKeyException e) {
                    e.printStackTrace();
                } catch (NoSuchProviderException e) {
                    e.printStackTrace();
                }
            }
        });

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WVchat.setWebContentsDebuggingEnabled(true);
        }*/

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle("Chat");
        setHasOptionsMenu(true);

        try {
            /*Log.v("printChat",Constants.URL_CHAT+
                    URLEncoder.encode(session.getChat().get(0).toString(),"UTF-8")+"/"+
                    URLEncoder.encode(session.getChat().get(1).toString(),"UTF-8")+"/"+
                    URLEncoder.encode(session.profile().get(0).toString(),"UTF-8"));*/
            WVchat.loadUrl(Constants.URL_CHAT+
                    URLEncoder.encode(session.getChat().get(0).toString(),"UTF-8")+"/"+
                    URLEncoder.encode(session.getChat().get(1).toString(),"UTF-8")+"/"+
                    URLEncoder.encode(session.profile().get(0).toString(),"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
