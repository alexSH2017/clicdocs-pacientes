package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.ResponseSearchPagerFragment;
import com.clicdocs.clicdocspacientes.utils.DoctorsFind;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;


public class ResponseMapFragment extends Fragment implements ResponseSearchPagerFragment.FragmentLifecycle{

    private MapView mapView;
    private GoogleMap mMap;
    private LocationManager location;
    private Marker mMarker;
    private MainActivity boostrap;
    private Context ctx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.response_map_fragment,container,false);
        boostrap    = (MainActivity) getActivity();
        ctx         = getContext();
        mapView         = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(boostrap.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onResumeFragment() {

    }
}
