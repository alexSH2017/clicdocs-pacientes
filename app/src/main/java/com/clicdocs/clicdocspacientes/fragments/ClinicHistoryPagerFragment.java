package com.clicdocs.clicdocspacientes.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.TabsRecentsAdapter;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ApprovedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.FinishedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.HereditaryFamilyHistoryFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.IdentificationFileFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.NonPathologicalPersonalFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.PersonalPathologicalFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ProcessDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.RejectedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.WaitingDateFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.CustomViewPager;

import org.json.JSONException;


public class ClinicHistoryPagerFragment extends Fragment {
    private MainActivity boostrap;
    private CustomViewPager mViewPager;
    private Context ctx;
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.viewpager_fragment, container, false);
        boostrap   = (MainActivity) getActivity();
        ctx        = getContext();
        mViewPager = (CustomViewPager) view.findViewById(R.id.mViewPager);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        boostrap.getSupportActionBar().show();
    }

    public void initView(){
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                final TabsRecentsAdapter adapter = new TabsRecentsAdapter(getChildFragmentManager());
                adapter.addFragment(new IdentificationFileFragment(), boostrap.langStrings.get(Constants.identification_file_p));
                adapter.addFragment(new HereditaryFamilyHistoryFragment(), boostrap.langStrings.get(Constants.heredo_family_p));
                adapter.addFragment(new PersonalPathologicalFragment(), boostrap.langStrings.get(Constants.pathological_personals_p));
                adapter.addFragment(new NonPathologicalPersonalFragment(), boostrap.langStrings.get(Constants.non_pathological_p));
                mViewPager.setAdapter(adapter);
                Tabs.setupWithViewPager(mViewPager);
                Tabs.setVisibility(View.VISIBLE);
                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
                    @Override
                    public void onPageSelected(int position) {
                        FragmentLifecycle fragmentToShow = (FragmentLifecycle)adapter.getItem(position);
                        fragmentToShow.onResumeFragment();

                    }
                    @Override
                    public void onPageScrollStateChanged(int state) {}
                });


            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.clinic_history_p));
    }

    public interface FragmentLifecycle {
        public void onResumeFragment();
    }
}
