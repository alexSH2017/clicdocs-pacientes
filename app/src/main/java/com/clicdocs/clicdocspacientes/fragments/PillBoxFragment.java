package com.clicdocs.clicdocspacientes.fragments;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.AlarmPillBoxNotifiacionReceiver;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterPillbox;
import com.clicdocs.clicdocspacientes.database.DBhelper;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogViewPill;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.Pillbox;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.ALARM_SERVICE;

public class PillBoxFragment extends Fragment{

    private MainActivity boostrap;
    private Context ctx;
    private DBhelper dBhelper;
    private SQLiteDatabase db;
    private SessionManager session;
    private RecyclerView mRecycler;
    //private TextView TVmymedicine;
    private AlertDialog DLnoconnection;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressDialog loading;
    private View view;
    private RequestQueue queueUpdate, queueDelete;
    private RecyclerViewAdapterPillbox adapter;
    private Paint p = new Paint();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_pillbox,container,false);
        boostrap                =   (MainActivity) getActivity();
        ctx                     =   getContext();
        session                 =   new SessionManager(ctx);
        mRecycler               = (RecyclerView) view.findViewById(R.id.mRecyclerViewPill);
        mSwipeRefreshLayout     = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        dBhelper                = new DBhelper(ctx);
        db                      = dBhelper.getWritableDatabase();
        //TVmymedicine            = (TextView) view.findViewById(R.id.TVmymedicine);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        //TVmymedicine.setText(boostrap.langStrings.getMymedicine());
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getPillBox();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            getPillBox();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getPillBox () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_patient_session",session.profile().get(0).toString());
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            RequestQueue queuePillbox = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonResponse.getJSONArray("rows");

                                final ArrayList<Pillbox> pills =new ArrayList<Pillbox>();
                                for (int i=0;i<jsonRows.length();i++){
                                    JSONObject item = jsonRows.getJSONObject(i);

                                    pills.add(new Pillbox(item.getString("id_pillbox"),
                                            item.getString("medicine_name"),
                                            item.getString("dose"),
                                            item.getString("measurement"),
                                            item.getString("frequency"),
                                            item.getString("duration"),
                                            item.getString("note"),
                                            item.getString("enabled"),
                                            item.getString("status"),
                                            item.getString("origin")));
                                }

                                adapter = new RecyclerViewAdapterPillbox(boostrap, pills, ctx, new RecyclerViewAdapterPillbox.Event() {
                                    @Override
                                    public void onClicView(Pillbox pillbox) {
                                        DialogViewPill viewPill = new DialogViewPill();
                                        Bundle b = new Bundle();
                                        b.putSerializable("pillbox",pillbox);
                                        viewPill.setArguments(b);
                                        viewPill.show(getChildFragmentManager(),fragments.VIEWPILLS);
                                    }

                                    @Override
                                    public void onClicStart(Pillbox pillbox) {
                                        doStartPill(pillbox);
                                    }

                                    @Override
                                    public void onClicStop(final Pillbox pillbox, final int pos) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                        builder.setMessage(boostrap.langStrings.get(Constants.msg_suspend_p))
                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                .setCancelable(false)
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        DBhelper dBhelper = new DBhelper(ctx);
                                                        SQLiteDatabase bd = dBhelper.getWritableDatabase();
                                                        bd.delete("CD_pillbox_table", "id_pillbox="+pillbox.getIdPill(), null);
                                                        bd.close();
                                                        Intent intent = new Intent(ctx, AlarmPillBoxNotifiacionReceiver.class);
                                                        PendingIntent sender = PendingIntent.getBroadcast(ctx, Integer.parseInt(pillbox.getIdPill()), intent, 0);
                                                        AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(ALARM_SERVICE);
                                                        alarmManager.cancel(sender);
                                                        doEditPills (pillbox);
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        adapter.notifyDataSetChanged();
                                                        dialog.cancel();
                                                    }
                                                }); builder.show();
                                    }

                                    @Override
                                    public void onPager(int page) {
                                        try {
                                            pagerPillbox(page);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                initSwipe();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queuePillbox.add(request);
        }  else {

        }
    }

    public void pagerPillbox (final int page) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_patient_session",session.profile().get(0).toString());
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            RequestQueue queuePillbox = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonResponse.getJSONArray("rows");

                                final ArrayList<Pillbox> pills =new ArrayList<Pillbox>();
                                for (int i=0;i<jsonRows.length();i++){
                                    JSONObject item = jsonRows.getJSONObject(i);

                                    pills.add(new Pillbox(item.getString("id_pillbox"),
                                            item.getString("medicine_name"),
                                            item.getString("dose"),
                                            item.getString("measurement"),
                                            item.getString("frequency"),
                                            item.getString("duration"),
                                            item.getString("note"),
                                            item.getString("enabled"),
                                            item.getString("status"),
                                            item.getString("origin")));
                                }

                                adapter.add(pills);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, jsonObject.toString());
                    params.put("limit",""+page);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queuePillbox.add(request);
        }  else {

        }
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT){

                } else {
                    String id_pillbox = adapter.getItem(position).getIdPill();
                    doCancelPill(id_pillbox,position);

                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                    View itemView = viewHolder.itemView;
                    float height  = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width   = height / 3;
                    if(!(dX > 0)){
                        p.setColor(ContextCompat.getColor(ctx, R.color.colorGreen));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.whitedit);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    } else {
                        p.setColor(Color.RED);
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.whitedelete);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(simpleCallback);
        helper.attachToRecyclerView(mRecycler);
    }


    private void doCancelPill (final String idPill, final int pos){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueDelete = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.delete_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                Intent intent = new Intent(ctx, AlarmPillBoxNotifiacionReceiver.class);
                                PendingIntent sender = PendingIntent.getBroadcast(ctx, Integer.parseInt(idPill), intent, 0);
                                AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(ALARM_SERVICE);
                                alarmManager.cancel(sender);
                                adapter.removeItem(pos);
                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("pillbox_id", idPill);
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueDelete.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    private void doEditPills (final Pillbox pill){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUpdate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.update_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.edit_medic_success_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                PillBoxFragment pillBox = (PillBoxFragment) boostrap.getSupportFragmentManager().findFragmentById(R.id.FLmain);
                                                try {
                                                    pillBox.getPillBox();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }).show();
                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    params.put(Constants.name, pill.getMedicine_name());
                    params.put("dose", pill.getDose());
                    params.put("measurement", pill.getMeasurement());
                    params.put("frequency", pill.getFrequency());
                    params.put("duration", pill.getDuration());
                    params.put("note", pill.getNote());
                    params.put("status", pill.getStatus());
                    params.put("enabled", "'");
                    params.put("pillbox_id", pill.getIdPill());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUpdate.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    private void doStartPill (final Pillbox pill){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUpdate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.update_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                startNotification(Integer.parseInt(pill.getIdPill()),pill.getMedicine_name(), pill.getDose(), pill.getMeasurement(),Miscellaneous.setFrequency(pill.getFrequency()),Integer.parseInt(pill.getDuration()),true);
                                getPillBox();
                                break;
                            default:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    params.put(Constants.name, pill.getMedicine_name());
                    params.put("dose", pill.getDose());
                    params.put("measurement", pill.getMeasurement());
                    params.put("frequency", pill.getFrequency());
                    params.put("duration", pill.getDuration());
                    params.put("note", pill.getNote());
                    params.put("status", "1");
                    params.put("enabled", "1");
                    params.put("pillbox_id", pill.getIdPill());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUpdate.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    public void startNotification (final int id, final String medicineName, final String dose, final String measure, final int frequency, final int duration, boolean ban){
        Calendar calendar = Calendar.getInstance();

        if (ban){
            //dBhelper.saveRecordsInto(""+id,""+3,""+calendar.getTime(),db);
            dBhelper.saveRecordsInto(""+id,""+calculateHours(duration,frequency),""+calendar.getTime(),db);
        } else {
            DBhelper objBD = new DBhelper(ctx);
            SQLiteDatabase sql = objBD.getWritableDatabase();
            ContentValues updateRecord = new ContentValues();
            updateRecord.put("number_take", calculateHours(duration,frequency));
            int cant = sql.update("CD_pillbox_table", updateRecord, "id_pillbox='"+id+"'", null);
            sql.close();
        }

        AlarmManager manager = (AlarmManager) ctx.getSystemService(ctx.ALARM_SERVICE);
        Intent myIntent;
        PendingIntent pendingIntent;
        myIntent = new Intent(ctx, AlarmPillBoxNotifiacionReceiver.class);
        myIntent.putExtra("medicine",medicineName);
        myIntent.putExtra("dose",dose+" "+Miscellaneous.getMeasurement(measure));
        myIntent.putExtra("idPill",""+id);
        pendingIntent = PendingIntent.getBroadcast(ctx,id,myIntent,0);
        calendar.setTime(calendar.getTime());
        calendar.add(Calendar.HOUR, frequency);
        calendar.add(Calendar.SECOND,0);
        manager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),1000*60*60*frequency,pendingIntent);

    }

    private Integer calculateHours (int duration, int frequency){
        int numberTake = 0;
        Calendar calendarCurrent = Calendar.getInstance();
        Calendar calendarDuration = Calendar.getInstance();
        calendarDuration.set(Calendar.DAY_OF_MONTH,calendarCurrent.get(Calendar.DAY_OF_MONTH)+duration);
        Date date1 = calendarCurrent.getTime();
        Date date2 = calendarDuration.getTime();
        long diff = Math.abs(date1.getTime() - date2.getTime());
        numberTake =(int) (diff/3600000)/frequency;
        return numberTake;
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.pillbox_p));
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_add_pillbox, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.add_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addPill:
                boostrap.setFragment(fragments.ADDPILL,null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
