package com.clicdocs.clicdocspacientes.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.MyBusinessActivity;
import com.clicdocs.clicdocspacientes.activity.MyInsuranceActivity;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.utils.BackendThreat;
import com.clicdocs.clicdocspacientes.utils.Constants;

import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.ProgressRequestBody;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class EditProfileFragment extends Fragment {

    private final int WR_PERMS = 3453;
    private final int CODE_BUSINESS = 1234;
    private final int CODE_INSURANCE = 4321;
    private EditText ETname, ETflname, ETmlname, ETnickname, ETphone, ETbirthdate, ETcellphone;
    private TextView TVname, TVflname, TVmlname, TVbirthdate, TVgender, TVnickname, TVphone, TVcellphone, TVpicture,TVpersonaldata,TVcontact, TVbusiness, TVmyinsurance;
    private String BIRTHDAY ="";
    private CardView CVbusiness, CVmyInsurances;
    private Spinner SPgender;
    private ProgressDialog loading;
    private AlertDialog DLsetgender, DLnoconnection;
    private SimpleDateFormat formatDate = new SimpleDateFormat("MMM dd, yyyy");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar myCalendar, calendar;
    private Context ctx;
    private MainActivity boostrap;
    private SessionManager session;
    private FloatingActionButton fab;
    private CircleImageView CIVphoto;
    private RequestQueue queueUpdate;
    private View view;
    private String ln = "";
    private static final int MY_INTENT_CLICK=302;
    //---------------------------------------------------
    private Uri mImageCaptureUri;
    private AlertDialog dialog;
    private ArrayList<KeyPairsBean> gender = new ArrayList<>();


    private static final int CROP_FROM_CAMERA = 4;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_profile_fragment,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        ETname          = (EditText) view.findViewById(R.id.ETname);
        ETflname        = (EditText) view.findViewById(R.id.ETplname);
        ETmlname        = (EditText) view.findViewById(R.id.ETmlname);
        ETnickname      = (EditText) view.findViewById(R.id.ETnickname);
        ETphone         = (EditText) view.findViewById(R.id.ETphone);
        ETcellphone     = (EditText) view.findViewById(R.id.ETcellphone);
        ETbirthdate     = (EditText) view.findViewById(R.id.ETbirthdate);
        SPgender        = (Spinner) view.findViewById(R.id.SPgender);
        TVname          = (TextView) view.findViewById(R.id.TVname);
        TVflname        = (TextView) view.findViewById(R.id.TVflname);
        TVmlname        = (TextView) view.findViewById(R.id.TVmlname);
        TVbirthdate     = (TextView) view.findViewById(R.id.TVbirthdate);
        TVgender        = (TextView) view.findViewById(R.id.TVgender);
        TVnickname      = (TextView) view.findViewById(R.id.TVnickname);
        TVphone         = (TextView) view.findViewById(R.id.TVphone);
        TVcellphone     = (TextView) view.findViewById(R.id.TVcellphone);
        TVpicture       = (TextView) view.findViewById(R.id.TVpicture);
        TVpersonaldata  = (TextView) view.findViewById(R.id.TVpersonaldata);
        TVcontact       = (TextView) view.findViewById(R.id.TVcontact);
        TVbusiness      = (TextView) view.findViewById(R.id.TVbusiness);
        TVmyinsurance   = (TextView) view.findViewById(R.id.TVmyinsurance);
        CVbusiness      = (CardView) view.findViewById(R.id.CVbusiness);
        CVmyInsurances  = (CardView) view.findViewById(R.id.CVmyInsurance);
        fab             = (FloatingActionButton) view.findViewById(R.id.fab);
        CIVphoto        = (CircleImageView) view.findViewById(R.id.CIVprofile);
        gender.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.male_p)));
        gender.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.female_p)));
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        ArrayAdapter<KeyPairsBean> genderA = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, gender);
        SPgender.setAdapter(genderA);
        ETname.setHint(boostrap.langStrings.get(Constants.name_p));
        ETflname.setHint(boostrap.langStrings.get(Constants.fname_p));
        ETmlname.setHint(boostrap.langStrings.get(Constants.mname_p));
        ETphone.setHint(boostrap.langStrings.get(Constants.phone_option_p));
        ETnickname.setHint(boostrap.langStrings.get(Constants.nickname_p));
        ETcellphone.setHint(boostrap.langStrings.get(Constants.cellphone_p));
        ETbirthdate.setHint(boostrap.langStrings.get(Constants.birthday_p));

        TVname.setText(boostrap.langStrings.get(Constants.name_p));
        TVflname.setText(boostrap.langStrings.get(Constants.fname_p));
        TVmlname.setText(boostrap.langStrings.get(Constants.mname_p));;
        TVphone.setText(boostrap.langStrings.get(Constants.phone_p));
        TVcellphone.setText(boostrap.langStrings.get(Constants.cellphone_p));
        TVbirthdate.setText(boostrap.langStrings.get(Constants.birthday_p));
        TVgender.setText(boostrap.langStrings.get(Constants.gender_p));
        TVnickname.setText(boostrap.langStrings.get(Constants.nickname_p));
        TVbusiness.setText(boostrap.langStrings.get(Constants.billing_p));
        TVmyinsurance.setText(boostrap.langStrings.get(Constants.my_insurance_p));

        TVpicture.setText(boostrap.langStrings.get(Constants.photo_personal_p));
        TVpersonaldata.setText(boostrap.langStrings.get(Constants.information_personal_p));
        TVcontact.setText(boostrap.langStrings.get(Constants.contact_p));


        myCalendar = Calendar.getInstance();
        calendar = Calendar.getInstance();

        try {
            getProfile();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ETbirthdate.setOnClickListener(ETbirthday_onClick);
        CVbusiness.setOnClickListener(CVbusiness_onClic);
        CVmyInsurances.setOnClickListener(CVmyInsurances_onClic);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(ctx,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, WR_PERMS);
                } else {
                    CropImage.activity().
                            setOutputCompressQuality(50).
                            start(ctx,EditProfileFragment.this);
                }
            }
        });
    }

    private View.OnClickListener CVbusiness_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent business = new Intent(ctx, MyBusinessActivity.class);
            business.putExtra(Constants.langString,boostrap.langStrings);
            startActivity(business);
        }
    };

    private View.OnClickListener CVmyInsurances_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent business = new Intent(ctx, MyInsuranceActivity.class);
            business.putExtra(Constants.langString,boostrap.langStrings);
            startActivity(business);
        }
    };

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null){
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    final Uri selectedImage = result.getUri();
                    final File foto = new File(selectedImage.getPath());
                    final AlertDialog uploadProgress = PopUpManager.showProgressDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                    uploadProgress.show();
                    if(NetworkUtils.haveNetworkConnection(ctx)) {
                        BackendThreat thread = new BackendThreat(ctx, new BackendThreat.ObjectTask() {
                            @Override
                            public Object doInBackground() {
                                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                                builder.addFormDataPart("avatar", foto.getName(), RequestBody.create(MediaType.parse("image/jpg"), foto));
                                builder.addFormDataPart("iduser",session.profile().get(0).toString());
                                builder.addFormDataPart("type","patient");
                                MultipartBody formBody = builder.build();

                                ProgressRequestBody progress = new ProgressRequestBody(formBody, new ProgressRequestBody.Listener() {
                                    @Override
                                    public void onProgress(int progress) {
                                        Log.v("print",String.valueOf(progress));
                                        PopUpManager.setProgress(uploadProgress, progress);

                                    }
                                });

                                Log.v("printToken",session.profile().get(5).toString());
                                okhttp3.Request request = new okhttp3.Request.Builder()
                                        .url(Constants.UPLOADURI)
                                        .header(Constants.key_access_token,session.profile().get(5).toString())
                                        .header(Constants.key_referer,"https://clicdocs.com")
                                        .post(progress)
                                        .build();
                                OkHttpClient client = new OkHttpClient();
                                try {
                                    okhttp3.Response response = client.newCall(request).execute();
                                    return response.body().string();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }

                            @Override
                            public void onPostExecute(Object response) throws JSONException {
                                Log.v("printPicture",response+"");
                                uploadProgress.dismiss();
                                try {
                                    JSONObject json = new JSONObject((String) response);
                                    switch (json.getInt("code")){
                                        case 200:
                                            Snackbar.make(view,boostrap.langStrings.get(Constants.photo_success_p),Snackbar.LENGTH_LONG).show();
                                            String url = json.getString(Constants.response);
                                            session.setLogin(session.profile().get(0).toString(),session.profile().get(1).toString(),session.profile().get(2).toString(),session.profile().get(3).toString(),url,session.profile().get(5).toString());
                                            Picasso.with(ctx).load(url).noFade().resize(150,150).centerCrop().into(CIVphoto);
                                            boostrap.setImageProfile(url);
                                            break;
                                        default:
                                            Snackbar.make(view,boostrap.langStrings.get(Constants.error_photo_p),Snackbar.LENGTH_LONG).show();
                                            break;
                                    }
                                } catch (JSONException e){
                                    final AlertDialog fail = PopUpManager.showDialogMessage(ctx, boostrap.langStrings.get(Constants.noconnection_p), boostrap.langStrings.get(Constants.accept_p), new PopUpManager.DialogMessage() {
                                        @Override
                                        public void OnClickListener(AlertDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    },null ,null);
                                    fail.show();
                                    e.printStackTrace();
                                }
                            }
                        });
                    }  else {
                        AlertDialog errorInternet = new AlertDialog.Builder(ctx)
                                .setMessage(boostrap.langStrings.get(Constants.noconnection_p))
                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    // ============== BUTTONS EVENTS =========================

    private View.OnClickListener ETbirthday_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(ctx, CPbirthday_onClick, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    private DatePickerDialog.OnDateSetListener CPbirthday_onClick = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDatePicked();
        }
    };

    // ============== METHODS ======================================

    private void validateUpdate (){
        InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        //Validator validator = new Validator();
        if(!boostrap.validator.validate(ETname, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_TEXT}) &&
                !boostrap.validator.validate(ETflname, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_TEXT}) &&
                !boostrap.validator.validate(ETbirthdate, new String[] {boostrap.validator.REQUIRED})) {
            if (session.profile().get(0).toString().equals(session.secondProfile().get(0).toString())){
                if (ETphone.getText().toString().length() != 0) {
                    if (!boostrap.validator.validate(ETphone, new String[] {boostrap.validator.ONLY_NUMBER})){
                        if (ETphone.getText().toString().length() == 10){
                            if (ETcellphone.length() == 10){
                                doUpdateProfile();
                            } else  ETcellphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));
                        } else ETphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));
                    }
                } else {
                    if (ETcellphone.length() == 10){
                        doUpdateProfile();
                    } else ETcellphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));
                }
            } else {
                if (ETphone.getText().toString().length() != 0) {
                    if (ETcellphone.getText().length() != 0){
                        if (ETphone.getText().toString().length() == 10){
                            if (ETcellphone.length() == 10){
                                doUpdateProfile();
                            } else  ETcellphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));
                        } else ETphone.setError(boostrap.langStrings.get(Constants.requier_ten_digit_p));
                    } else {
                        doUpdateProfile();
                    }
                } else {
                    doUpdateProfile();
                }
            }
        }
    }

    private void doUpdateProfile (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUpdate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.update_profile, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseU",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObjectProfile = new JSONObject(response);
                        switch (Integer.parseInt(jsonObjectProfile.get(Constants.Code).toString())) {
                            case 200:
                                hideError ();
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.profile_success_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                String name = ETname.getText().toString();
                                String fname = ETflname.getText().toString();
                                String mname = ETmlname.getText().toString();
                                session.setLogin(session.profile().get(0).toString(),session.profile().get(1).toString(),name+" "+fname+" "+mname,session.profile().get(3).toString(),session.profile().get(4).toString(),session.profile().get(5).toString());
                                session.secondSession(session.secondProfile().get(0).toString(),session.secondProfile().get(1).toString(),name+" "+fname+" "+mname,session.secondProfile().get(3).toString(),session.secondProfile().get(4).toString(),session.profile().get(5).toString());
                                boostrap.setNameProfile(name+" "+fname+" "+mname);
                                break;
                            case 404:
                                JSONObject jsonReq = jsonObjectProfile.getJSONObject(Constants.response);
                                Iterator<String> keys = jsonReq.keys();
                                String key = keys.next();
                                AlertDialog error = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonReq.getString(key))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    params.put("name", ETname.getText().toString());
                    params.put("fname", ETflname.getText().toString());
                    params.put("mname", ETmlname.getText().toString());
                    params.put("birthday", BIRTHDAY);
                    params.put("gender", String.valueOf((SPgender.getSelectedItemPosition())+1));
                    params.put("phone", ETphone.getText().toString());
                    params.put("cellphone", ETcellphone.getText().toString());
                    params.put("nickname", ETnickname.getText().toString());
                    params.put("ln", ln);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUpdate.add(request);
        }  else {

        }
    }

    private void getProfile () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUpdate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_profile, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonProfile = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonProfile.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++) {
                                    final JSONObject item = jsonRows.getJSONObject(i);
                                    String name = "", fname = "", mname = "";
                                    name = Miscellaneous.ucFirst(item.getString("name"));
                                    fname = Miscellaneous.ucFirst(item.getString("last_name"));
                                    mname = Miscellaneous.ucFirst(item.getString("middle_name"));
                                    ETname.setText(name);
                                    ETflname.setText(fname);
                                    ETmlname.setText(mname);
                                    BIRTHDAY = item.getString("birthday");
                                    Date strDate = null;
                                    try {
                                        strDate = sdf.parse(BIRTHDAY);
                                        calendar.setTime(strDate);
                                        ETbirthdate.setText(formatDate.format(calendar.getTime()));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    ETnickname.setText(validate(item.getString("nickname")));
                                    ETphone.setText(validate(item.getString("phone")));
                                    ETcellphone.setText(validate(item.getString("cellphone")));
                                    SPgender.setSelection(Integer.parseInt(item.getString("gender"))-1);
                                    ln = item.getString("ln");
                                    Log.v("printFotoget",item.getString("picture"));

                                    if (item.getString("picture").length() > 0) {
                                        CIVphoto.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Picasso.with(ctx).load(item.getString("picture"))
                                                            .resize(160, 160)
                                                            .noFade()
                                                            .centerCrop()
                                                            .into(CIVphoto);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                            }
                                        });
                                        boostrap.setImageProfile(item.getString("picture"));
                                    }
                                    boostrap.setNameProfile(name+" "+fname+" "+mname);
                                }
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUpdate.add(request);
        }  else {

        }
    }

    private void setDatePicked() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        sdf.setTimeZone(myCalendar.getTimeZone());
        BIRTHDAY = sdf.format(myCalendar.getTime());
        Date strDate = null;
        try {
            strDate = sdf.parse(BIRTHDAY);
            calendar.setTime(strDate);
            ETbirthdate.setText(formatDate.format(calendar.getTime()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.edit_profile_p));
        if (!session.profile().get(0).toString().equals(session.secondProfile().get(0).toString())){
            CVmyInsurances.setVisibility(View.GONE);
            CVbusiness.setVisibility(View.GONE);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.save_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                validateUpdate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public Integer getAge (){
        Date fechaNac=null;
        try {
            fechaNac = new SimpleDateFormat("yyyy-MM-dd").parse(BIRTHDAY);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar fechaActual = Calendar.getInstance();
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.setTime(fechaNac);

        int year = fechaActual.get(Calendar.YEAR)- fechaNacimiento.get(Calendar.YEAR);
        int mes =fechaActual.get(Calendar.MONTH)- fechaNacimiento.get(Calendar.MONTH);
        int dia = fechaActual.get(Calendar.DATE)- fechaNacimiento.get(Calendar.DATE);
        if(mes<0 || (mes==0 && dia<0)){
            year--;
        }

        return year;
    }

    private void hideError (){
        ETname.setError(null);
        ETflname.setError(null);
        ETmlname.setError(null);
        ETbirthdate.setError(null);
        ETphone.setError(null);
        ETcellphone.setError(null);
    }

    private String validate (String value){
        String aux = "";
        if (!value.equals("null")) {
            aux = value;
        }
        return aux;
    }

    public static String ucFirst(String str) {
        if (str.equals("null") || str.isEmpty()) {
            return "";
        } else {
            return  Character.toUpperCase(str.charAt(0)) + str.substring(1, str.length()).toLowerCase();
        }
    }
}
