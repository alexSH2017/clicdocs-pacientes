package com.clicdocs.clicdocspacientes.fragments.dialogfragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.ConfigActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DialogChangePassword extends DialogFragment {

    private ConfigActivity boostrap;
    private TextInputLayout TILcurrentPass, TILnewPass, TILconfiPass;
    private EditText ETcurrentPass, ETnewPass, ETconfiPass;
    private Button BTNcancel, BTNchange;
    private SessionManager session;
    private Context ctx;
    private RequestQueue queueChangePass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_changepassword,container,false);
        boostrap            = (ConfigActivity) getActivity();
        TILcurrentPass      = (TextInputLayout) view.findViewById(R.id.TILcurrentPass);
        TILnewPass          = (TextInputLayout) view.findViewById(R.id.TILnewtPass);
        TILconfiPass        = (TextInputLayout) view.findViewById(R.id.TILconfiPass);
        ETcurrentPass       = (EditText) view.findViewById(R.id.ETcurrentPass);
        ETnewPass           = (EditText) view.findViewById(R.id.ETnewPass);
        ETconfiPass         = (EditText) view.findViewById(R.id.ETconfiPass);
        BTNcancel           = (Button) view.findViewById(R.id.BTNcancel);
        BTNchange           = (Button) view.findViewById(R.id.BTNchange);
        ctx                 = getContext();

        getDialog().setTitle(Miscellaneous.ucFirst(boostrap.langStrings.get(Constants.change_pass_p)));

        getDialog().setCancelable(false);
        TILcurrentPass.setHint(boostrap.langStrings.get(Constants.currentpass_p));
        TILnewPass.setHint(boostrap.langStrings.get(Constants.newpass_p));
        TILconfiPass.setHint(boostrap.langStrings.get(Constants.confi_pass_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNchange.setText(boostrap.langStrings.get(Constants.accept_p));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setCancelable(false);
        session = new SessionManager(ctx);
        BTNcancel.setOnClickListener(BTNcancel_onClic);
        BTNchange.setOnClickListener(BTNchange_onClic);
    }

    private View.OnClickListener BTNcancel_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDialog().dismiss();
        }
    };

    private View.OnClickListener BTNchange_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Validator validator = new Validator();
            if (!boostrap.validator.validate(ETcurrentPass, new String[] {boostrap.validator.REQUIRED}) &&
                    !boostrap.validator.validate(ETnewPass, new String[] {boostrap.validator.REQUIRED}) &&
                    !boostrap.validator.validate(ETconfiPass, new String[] {boostrap.validator.REQUIRED})){
                if (ETcurrentPass.getText().toString().equals(session.profile().get(3))){
                    if (ETnewPass.getText().toString().equals(ETconfiPass.getText().toString())){
                        if (ETnewPass.getText().toString().length() > 7 && ETconfiPass.getText().toString().length() > 7){
                            doChangePassword();
                        } else {
                            AlertDialog errorCurrent = new AlertDialog.Builder(ctx)
                                    .setMessage(boostrap.langStrings.get(Constants.legendnewpass_p))
                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).show();
                        }

                    } else {
                        AlertDialog errorCurrent = new AlertDialog.Builder(ctx)
                                .setMessage(boostrap.langStrings.get(Constants.legendconfipass_p))
                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }

                } else {
                    AlertDialog errorCurrent = new AlertDialog.Builder(ctx)
                            .setMessage(boostrap.langStrings.get(Constants.notmachpass_p))
                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
            }
        }
    };

    private void doChangePassword (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueChangePass = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.change_pass, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printUser",response+"");
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonObject.getString("response"))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                session.setLogin(session.profile().get(0).toString(),
                                                        session.profile().get(1).toString(),
                                                        session.profile().get(2).toString(),
                                                        ETnewPass.getText().toString(),
                                                        session.profile().get(4).toString(),
                                                        session.profile().get(5).toString());
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                getDialog().dismiss();

                                break;

                            case 404:
                                JSONObject jsonError = jsonObject.getJSONObject("response");
                                AlertDialog error = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonError.getString("new_password"))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("session_id", session.profile().get(0).toString());
                    params.put("type_user", "patient");
                    params.put("current_password", ETcurrentPass.getText().toString());
                    params.put("new_password", ETnewPass.getText().toString());
                    params.put("confirm_password", ETconfiPass.getText().toString());
                    Log.v("prinParamsPass",params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Access-Token", session.profile().get(5).toString());
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueChangePass.add(request);
        }  else {

        }
    }
}
