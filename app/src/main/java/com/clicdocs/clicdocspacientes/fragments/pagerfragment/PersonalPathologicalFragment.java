package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PersonalPathologicalFragment extends Fragment implements ClinicHistoryPagerFragment.FragmentLifecycle{

    private ProgressDialog loading;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private CardView CVesp;
    private TextView TVenfcurrent, TVquirurjicos, TVtransfusionales, TValergias, TVesp, TVtraumaticos, TVhospital, TVadicciones, TVotros;
    private EditText ETenfActuales,ETquirurjicos,ETtransfusiones, ETesp, ETtraumaticos,EThospital,ETadicciones,ETotros;
    private Spinner SPalergias;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RequestQueue queuePathological,queueSave;
    private View view;
    private ClinicHistoryPagerFragment parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view                    = inflater.inflate(R.layout.pathological_personal_fragment,container,false);
        boostrap                = (MainActivity) getActivity();
        ctx                     = getContext();
        session                 = new SessionManager(ctx);
        parent                  = (ClinicHistoryPagerFragment) getParentFragment();
        mSwipeRefreshLayout     = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue);
        CVesp                   = (CardView) view.findViewById(R.id.CVesp);
        TVenfcurrent            = (TextView) view.findViewById(R.id.TVenfcurrent);
        TVquirurjicos           = (TextView) view.findViewById(R.id.TVquirurjicos);
        TVtransfusionales       = (TextView) view.findViewById(R.id.TVtransfusionales);
        TValergias              = (TextView) view.findViewById(R.id.TValergias);
        TVesp                   = (TextView) view.findViewById(R.id.TVesp);
        TVtraumaticos           = (TextView) view.findViewById(R.id.TVtraumaticos);
        TVhospital              = (TextView) view.findViewById(R.id.TVhospital);
        TVadicciones            = (TextView) view.findViewById(R.id.TVadicciones);
        TVotros                 = (TextView) view.findViewById(R.id.TVotros);

        ETenfActuales           = (EditText) view.findViewById(R.id.ETenfActuales);
        ETquirurjicos           = (EditText) view.findViewById(R.id.ETquirurjicos);
        ETtransfusiones         = (EditText) view.findViewById(R.id.ETtransfusiones);
        SPalergias              = (Spinner) view.findViewById(R.id.SPalergias);
        ETesp                   = (EditText) view.findViewById(R.id.ETesp);
        ETtraumaticos           = (EditText) view.findViewById(R.id.ETtraumaticos);
        EThospital              = (EditText) view.findViewById(R.id.EThospital);
        ETadicciones            = (EditText) view.findViewById(R.id.ETadicciones);
        ETotros                 = (EditText) view.findViewById(R.id.ETotros);
        ArrayList<KeyPairsBean> alerg = new ArrayList<KeyPairsBean>();
        alerg.add(new KeyPairsBean(0, boostrap.langStrings.get(Constants.not_p)));
        alerg.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.yespec_p)));
        ArrayAdapter<KeyPairsBean> alergias = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, alerg);
        SPalergias.setAdapter(alergias);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                getPathological();
            }
        });
        TVenfcurrent.setText(boostrap.langStrings.get(Constants.current_diseases_p));
        TVquirurjicos.setText(boostrap.langStrings.get(Constants.surgical_p));
        TVtransfusionales.setText(boostrap.langStrings.get(Constants.transfusion_p));
        TValergias.setText(boostrap.langStrings.get(Constants.allergies_p));
        TVtraumaticos.setText(boostrap.langStrings.get(Constants.traumatic_p));
        TVhospital.setText(boostrap.langStrings.get(Constants.previous_hospitalizations_p));
        TVadicciones.setText(boostrap.langStrings.get(Constants.addictions_p));
        TVotros.setText(boostrap.langStrings.get(Constants.othres_p));
        TVesp.setText(boostrap.langStrings.get(Constants.specify_p));

        ETenfActuales.setHint(boostrap.langStrings.get(Constants.current_diseases_p));
        ETquirurjicos.setHint(boostrap.langStrings.get(Constants.surgical_p));
        ETtransfusiones.setHint(boostrap.langStrings.get(Constants.transfusion_p));
        ETtraumaticos.setHint(boostrap.langStrings.get(Constants.traumatic_p));
        EThospital.setHint(boostrap.langStrings.get(Constants.previous_hospitalizations_p));
        ETadicciones.setHint(boostrap.langStrings.get(Constants.addictions_p));
        ETotros.setHint(boostrap.langStrings.get(Constants.othres_p));
        ETesp.setHint(boostrap.langStrings.get(Constants.specify_p));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPathological();
            }
        });

        SPalergias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    ETesp.setText("");
                    CVesp.setVisibility(View.GONE);
                } else {
                    ETesp.setText("");
                    CVesp.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    //========================= METODO PARA GUARDAR ANTECEDENTES PERSONALES PATOLÓGICOS==========================
    private void savePathologicalPersonal () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_pathological, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.msg_personal_patologic_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    params.put("enfermedades", ETenfActuales.getText().toString());
                    params.put("quirurgicos", ETquirurjicos.getText().toString());
                    params.put("transfusionales", ETtransfusiones.getText().toString());
                    params.put("alergiasPP", ETesp.getText().toString());
                    params.put("alergiasSelect", String.valueOf(SPalergias.getSelectedItemPosition()+1));
                    params.put("traumaticos", ETtraumaticos.getText().toString());
                    params.put("hospitalizaciones", EThospital.getText().toString());
                    params.put("adicciones", ETadicciones.getText().toString());
                    params.put("otros", ETotros.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    //==========================================================================================================
    private void getPathological (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queuePathological = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_pathological, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printGePatological",""+response);
                    //loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonPathological= jsonObject.getJSONObject("response");

                                ETenfActuales.setText(Miscellaneous.validate(jsonPathological.getString("enfermedades")));
                                ETquirurjicos.setText(Miscellaneous.validate(jsonPathological.getString("quirurgicos")));
                                ETtransfusiones.setText(Miscellaneous.validate(jsonPathological.getString("transfusionales")));
                                SPalergias.setSelection(Integer.parseInt(jsonPathological.getString("alergiasSelect"))-1);
                                if (jsonPathological.getString("alergiasSelect").equals("1")){
                                    CVesp.setVisibility(View.GONE);
                                }else {
                                    CVesp.setVisibility(View.VISIBLE);
                                    ETesp.setText(Miscellaneous.validate(jsonPathological.getString("alergiasPP")));
                                }
                                ETtraumaticos.setText(Miscellaneous.validate(jsonPathological.getString("traumaticos")));
                                EThospital.setText(Miscellaneous.validate(jsonPathological.getString("hospitalizaciones")));
                                ETadicciones.setText(Miscellaneous.validate(jsonPathological.getString("adicciones")));
                                ETotros.setText(Miscellaneous.validate(jsonPathological.getString("otros")));

                                break;
                            case 202:
                                Snackbar.make(parent.getView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queuePathological.add(request);
        }  else {

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.save_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                savePathologicalPersonal();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResumeFragment() {
        if (getContext() != null){ getPathological();
             }
    }
}
