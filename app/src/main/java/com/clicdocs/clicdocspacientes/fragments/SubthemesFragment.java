package com.clicdocs.clicdocspacientes.fragments;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterSubthemes;
import com.clicdocs.clicdocspacientes.beans.SubthemesBeans;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogAddSubthemeFragment;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogLogin;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubthemesFragment  extends Fragment{

    private TextView TVlegend;
    private View view;
    private AlertDialog DLnoconnection;
    private MainActivity boostrap;
    private Context ctx;
    private RecyclerView mRecycler;
    private SessionManager session;
    private ProgressDialog loading;
    private String forum_id = "";
    private SearchView searchView;
    private RequestQueue queueSubtheme, queueSubSearch;
    private RecyclerViewAdapterSubthemes adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_general,container,false);
        boostrap        =   (MainActivity) getActivity();
        ctx             =   getContext();
        session         =   new SessionManager(ctx);
        mRecycler = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(boostrap);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycler.setLayoutManager(llm);
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        Bundle data = getArguments();
        forum_id = data.getString("idTheme");
        view.post(new Runnable() {
            @Override
            public void run() {
                try {
                    GetSubthemes();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetSubthemes () throws JSONException {

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("status","1");
        jsonObject.put("forum_id",forum_id);
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSubtheme = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_subtheme, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonRes = jsonObject.getJSONObject(Constants.response);
                                JSONArray rows = jsonRes.getJSONArray("rows");

                                final ArrayList<SubthemesBeans> subtheme =new ArrayList<SubthemesBeans>();

                                for (int i = 0; i < rows.length(); i++){
                                    JSONObject item = rows.getJSONObject(i);
                                    subtheme.add(new SubthemesBeans(item.getString("forum_id"),
                                                                    item.getString("topic_id"),
                                                                    item.getString("patient_id"),
                                                                    item.getString("subtheme"),
                                                                    item.getString("patient_full_name"),
                                                                    item.getString("nickname"),
                                                                    item.getString("replies"),
                                                                    item.getString("views"),
                                                                    item.getString("timestamp"),
                                                                    item.getString("content"),
                                                                    item.getString("picture")));

                                }
                                adapter= new RecyclerViewAdapterSubthemes(subtheme, ctx, new RecyclerViewAdapterSubthemes.Event() {
                                    @Override
                                    public void onClic(SubthemesBeans subtheme) {
                                        Bundle b = new Bundle();
                                        b.putSerializable("subtheme",subtheme);
                                        boostrap.setFragment(fragments.SUBTHEME,b);
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                TVlegend.setVisibility(View.GONE);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSubtheme.add(request);
        }  else {

        }
    }

    private void searchSubtheme (final String word) {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSubSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_subtheme, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                adapter.remove();
                                final ArrayList<SubthemesBeans> subtheme =new ArrayList<SubthemesBeans>();
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                for (int i = 0; i < jsonResp.length(); i++){
                                    JSONObject item = jsonResp.getJSONObject(i);
                                    subtheme.add(new SubthemesBeans(item.getString("id_forum_theme"),
                                            item.getString("id_forum_subtheme"),
                                            item.getString("id_patient_session"),
                                            item.getString("subtheme"),
                                            item.getString("name"),
                                            item.getString("nickname"),
                                            item.getString("replies"),
                                            item.getString("views"),
                                            item.getString("timestamp"),
                                            item.getString("content"),
                                            item.getString("picture")));

                                }
                                adapter = new RecyclerViewAdapterSubthemes(subtheme, ctx, new RecyclerViewAdapterSubthemes.Event() {
                                    @Override
                                    public void onClic(SubthemesBeans subtheme) {
                                        Bundle b = new Bundle();
                                        b.putSerializable("subtheme",subtheme);
                                        boostrap.setFragment(fragments.SUBTHEME,b);
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("topic_id", forum_id);
                    params.put("word", word);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSubSearch.add(request);
        }  else {

        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.subtheme_p));
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_add_subtheme, menu);
        menu.getItem(1).setTitle(boostrap.langStrings.get(Constants.add_p));
        SearchManager searchManager = (SearchManager) ctx.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint(boostrap.langStrings.get(Constants.search_p)+". . .");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(boostrap.getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchSubtheme(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addSubtheme:
                if (session.isLogged() != null){
                    DialogAddSubthemeFragment addSubtheme = new DialogAddSubthemeFragment();
                    Bundle b = new Bundle();
                    b.putString("idTheme",getArguments().getString("idTheme"));
                    b.putString("idProfile",getArguments().getString("idProfile"));
                    addSubtheme.setArguments(b);
                    addSubtheme.show(getChildFragmentManager(),fragments.ADDSUBTHEME);

                } else {
                    DialogLogin login = new DialogLogin();
                    login.show(getChildFragmentManager(),fragments.LOGINDIALOG);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
