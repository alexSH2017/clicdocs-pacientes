package com.clicdocs.clicdocspacientes.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.DoctorsFind;
import com.clicdocs.clicdocspacientes.utils.Geocoder;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.PharmacyFind;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MapDoctorFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private ImageButton IMBback;
    private MapView mapView;
    private GoogleMap mMap;
    private Marker mMarker;
    private double Slat=0.0,Slng=0.0;
    private ArrayList<DoctorSearch> doctorLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_doctor_fragment,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        IMBback         = (ImageButton) view.findViewById(R.id.IMBback);
        mapView         = (MapView) view.findViewById(R.id.mapView);
        doctorLocation = (ArrayList<DoctorSearch>)getArguments().getSerializable("doctors");
        //doctorLocation    = (ArrayList<DoctorSearch>) boostrap.resultSearch.get("doctors");
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        TrustManagerUtil.restoresHttpsUrlSettingsGoogle();
        seeMapDoctors();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().hide();
        prepareActionBar();
        boostrap.setTitle("");
        IMBback.setOnClickListener(IMBback_onClic);
    }

    private View.OnClickListener IMBback_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm = boostrap.getSupportFragmentManager();
            fm.popBackStack(fragments.MAPDOCTORS,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    };

    private void seeMapDoctors(){
        mapView.onResume();
        try {
            MapsInitializer.initialize(boostrap.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGooglemap) {
                mMap = mGooglemap;
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                double lat=0.0;
                double lng=0.0;
                String office = "", name="";
                Log.v("printDoctorSize",""+doctorLocation.size());
                for (int i = 0; i < doctorLocation.size(); i++){
                    if (doctorLocation.get(i).getMyoffices().size() > 0){
                        if (!doctorLocation.get(i).getMyoffices().get(0).getLatitude().isEmpty() && !doctorLocation.get(i).getMyoffices().get(0).getLatitude().isEmpty()){
                            Log.v("printLatiLongi",i+" "+doctorLocation.get(i).getMyoffices().get(0).getLatitude()+"  "+doctorLocation.get(i).getMyoffices().get(0).getLongitude());
                            lat = Double.parseDouble(doctorLocation.get(i).getMyoffices().get(0).getLatitude());
                            lng = Double.parseDouble(doctorLocation.get(i).getMyoffices().get(0).getLongitude());
                            office = doctorLocation.get(i).getMyoffices().get(0).getName_office();
                            name = Miscellaneous.ucFirst(doctorLocation.get(i).getName());
                            addMarker(lat, lng, name, office);
                        }
                    }
                }
            }
        });
    }


    private void addMarker (double lat, double lng, String pharmacy, String address){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,12);
        if (mMarker!= null){
        }
        mMarker=mMap.addMarker(new MarkerOptions()
                .position(coordenadas)
                .title(pharmacy)
                .snippet(address));
        mMap.animateCamera(ubication);
    }

    private void prepareActionBar() {
        HashMap<String, String> options = new HashMap<>();
        options.put(Constants.TBtitle, "");
        options.put(Constants.TBhasBack, Constants.TBhasBack);
        boostrap.setActionBar(boostrap.TBmain, options);
    }
}
