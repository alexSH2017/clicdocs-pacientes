package com.clicdocs.clicdocspacientes.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterFamilyClicDocs;
import com.clicdocs.clicdocspacientes.beans.ModelSubordinate;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FamilyClicDocsFragment extends Fragment {


    private RecyclerView mRecyvlerView;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private AlertDialog DLnoconnection;
    private RequestQueue queueFamily, queueUnlinked;
    private View view;
    private RecyclerViewAdapterFamilyClicDocs adapter;
    private Paint p = new Paint();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view       = inflater.inflate(R.layout.recycler_general,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        mRecyvlerView   = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyvlerView.setLayoutManager(new LinearLayoutManager(ctx));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        try {
            getFamilySubordinate ();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFamilySubordinate () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("responsible_id",session.secondProfile().get(0).toString());
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueFamily = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_familiy, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResponse = jsonObject.getJSONObject(Constants.response);
                                JSONArray jsonRows = jsonResponse.getJSONArray("rows");
                                ArrayList<ModelSubordinate> subordinates = new ArrayList<>();
                                for (int i = 0; i <jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    subordinates.add(new ModelSubordinate(
                                            item.getString(Constants.patient_id),
                                            item.getString("picture"),
                                            item.getString("name") + " "+
                                            item.getString("last_name")+ " "+
                                            item.getString("middle_name"),
                                            false)
                                    );
                                }
                                adapter = new RecyclerViewAdapterFamilyClicDocs(view,boostrap, ctx, subordinates, new RecyclerViewAdapterFamilyClicDocs.Event() {
                                    @Override
                                    public void onClic(final ModelSubordinate item, final int position) {
                                        if (session.profile().get(0).toString().equals(session.secondProfile().get(0).toString())){
                                            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                            builder.setMessage(boostrap.langStrings.get(Constants.mess_release_account_p))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            enterEmail (item.getId_sesion(), position);
                                                            dialog.cancel();
                                                        }
                                                    })
                                                    .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    }); builder.show();
                                        } else {
                                            Snackbar.make(view,boostrap.langStrings.get(Constants.owner_account_p),Snackbar.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onClicChanceAccount(int position, boolean ban) {
                                        adapter.changeUser(position, ban);
                                    }
                                });

                                mRecyvlerView.setAdapter(adapter);
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.secondProfile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueFamily.add(request);
        }
    }

    private void enterEmail (String idSuburdinate, int pos){
        View dialog_recoverypass    = LayoutInflater.from(ctx).inflate(R.layout.dialog_recoverypass, null);
        Button BTNemail             = (Button) dialog_recoverypass.findViewById(R.id.BTNrecovery);
        Button BTNcancel            = (Button) dialog_recoverypass.findViewById(R.id.BTNcancel);
        TextView TVenterEmail       = (TextView) dialog_recoverypass.findViewById(R.id.TVlegendrecovery);
        EditText ETemail            = (EditText) dialog_recoverypass.findViewById(R.id.ETremail);
        ProgressBar PBloading       = (ProgressBar) dialog_recoverypass.findViewById(R.id.PBloading);
        LinearLayout LLform         = (LinearLayout) dialog_recoverypass.findViewById(R.id.LLform);
        final AlertDialog DLunlinked   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.release_account_p))
                .setCancelable(false)
                .setView(dialog_recoverypass).show();
        TVenterEmail.setText(boostrap.langStrings.get(Constants.legend_release_account_p));
        ETemail.setHint(boostrap.langStrings.get(Constants.email_p));
        BTNemail.setText(boostrap.langStrings.get(Constants.accept_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNemail.setOnClickListener(BTNemail_onClic(ETemail, LLform, PBloading, DLunlinked, idSuburdinate, pos));

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DLunlinked.dismiss();
            }
        });
    }



    private View.OnClickListener BTNemail_onClic(final EditText ETemail, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLunlinked, final String id, final int pos) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final Validator validator = new Validator();
                if(!validator.validate(ETemail, new String [] {validator.REQUIRED, validator.EMAIL})) {
                    if (NetworkUtils.haveNetworkConnection(ctx)) {
                        LLform.setVisibility(View.GONE);
                        PBloading.setVisibility(View.VISIBLE);
                        queueUnlinked = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                        StringRequest request = new StringRequest(Request.Method.POST, Constants.unlinked_patient, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v("printresponseScore", response + "");
                                PBloading.setVisibility(View.GONE);
                                LLform.setVisibility(View.VISIBLE);
                                DLunlinked.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                        case 200:
                                            AlertDialog success = new AlertDialog.Builder(ctx)
                                                    .setMessage(jsonObject.getString(Constants.response))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            adapter.removeItem(pos);
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                        case 404:
                                            AlertDialog error = new AlertDialog.Builder(ctx)
                                                    .setMessage(jsonObject.getString(Constants.response))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                        default:
                                            AlertDialog defaultMessage = new AlertDialog.Builder(ctx)
                                                    .setMessage(jsonObject.getString(Constants.response))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.v("error", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> params = new HashMap<>();
                                params.put("subordinate_id", id);
                                params.put("responsible_id", session.profile().get(0).toString());
                                params.put("email", ETemail.getText().toString().trim());
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put(Constants.key_access_token, session.profile().get(5).toString());
                                headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                return headers;
                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queueUnlinked.add(request);
                    }
                }
            }
        };
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.familiy_clicdocs_p));
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_add_subordinate, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.add_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addSubordinate:
                if (session.profile().get(0).toString().equals(session.secondProfile().get(0).toString())){
                    boostrap.setFragment(fragments.ADDSUBORDINATE,null);
                } else {
                    Snackbar.make(view,boostrap.langStrings.get(Constants.owner_account_p),Snackbar.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }
}
