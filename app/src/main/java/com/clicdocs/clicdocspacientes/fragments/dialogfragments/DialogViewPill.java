package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.fragments.PillBoxFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.Pillbox;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DialogViewPill extends DialogFragment {

    private EditText ETmedicine, ETdose, ETduration;
    private Spinner SPNmeasure, SPNfrequency;
    private TextView TVlegend;
    private MultiAutoCompleteTextView METinstruction;
    private TextView TVmedicine, TVdose, TVmeasurement, TVduration, TVfrequency;
    private Button BTNedit;
    private int aux = 0, auxDuration = 0;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private ProgressDialog loading;
    private Pillbox pill;
    private ArrayList<KeyPairsBean> medicine = new ArrayList<>();
    private View view;
    private RequestQueue queueUpdate, queueConfig;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.dialog_view_pill,container,false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        TVmedicine      = (TextView) view.findViewById(R.id.TVmedicine);
        TVdose          = (TextView) view.findViewById(R.id.TVdose);
        TVmeasurement   = (TextView) view.findViewById(R.id.TVmeasurement);
        TVduration      = (TextView) view.findViewById(R.id.TVduration);
        TVfrequency     = (TextView) view.findViewById(R.id.TVfrequency);
        ETmedicine      = (EditText) view.findViewById(R.id.ETmedicineName);
        ETdose          = (EditText) view.findViewById(R.id.ETdose);
        ETduration      = (EditText) view.findViewById(R.id.ETduration);
        SPNmeasure      = (Spinner) view.findViewById(R.id.SPNmeasure);
        SPNfrequency    = (Spinner) view.findViewById(R.id.SPNfrequency);
        METinstruction  = (MultiAutoCompleteTextView) view.findViewById(R.id.METinstructions);
        TVlegend        = (TextView) view.findViewById(R.id.TVlegend);
        BTNedit         = (Button) view.findViewById(R.id.BTNeditMedicine);
        getPillboxConfig ();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVmedicine.setText(boostrap.langStrings.get(Constants.medicine_p));
        TVdose.setText(boostrap.langStrings.get(Constants.dose_p));
        TVmeasurement.setText(boostrap.langStrings.get(Constants.measure_p));
        TVfrequency.setText(boostrap.langStrings.get(Constants.frequency_p));
        TVduration.setText(boostrap.langStrings.get(Constants.duration_p));
        ETmedicine.setHint(boostrap.langStrings.get(Constants.name_medicine_p));
        ETdose.setHint(boostrap.langStrings.get(Constants.dose_p));
        ETduration.setHint(boostrap.langStrings.get(Constants.duration_p));
        METinstruction.setHint(boostrap.langStrings.get(Constants.note_p));
        BTNedit.setText(boostrap.langStrings.get(Constants.edit_medicine_p));
        pill = (Pillbox) getArguments().getSerializable("pillbox");

        if (pill.getOrigin().equals("1")){
            TVlegend.setVisibility(View.VISIBLE);
            TVlegend.setText(boostrap.langStrings.get(Constants.no_edit_medicine_p));
            ETmedicine.setEnabled(false);
            ETdose.setEnabled(false);
            SPNmeasure.setEnabled(false);
            SPNfrequency.setEnabled(false);
            ETduration.setEnabled(false);
            METinstruction.setEnabled(false);
            BTNedit.setVisibility(View.GONE);
        } else {
            TVlegend.setVisibility(View.GONE);
            BTNedit.setOnClickListener(BTNedit_onClic);
        }
        aux=Miscellaneous.setFrequency(pill.getFrequency());
        auxDuration = Integer.parseInt(pill.getDuration());
        ETmedicine.setText(pill.getMedicine_name());
        ETdose.setText(pill.getDose());
        SPNmeasure.setSelection(Integer.parseInt(pill.getMeasurement())-1);
        SPNfrequency.setSelection(Integer.parseInt(pill.getFrequency())-1);
        ETduration.setText(pill.getDuration());
        METinstruction.setText(pill.getNote());
    }

   private View.OnClickListener BTNedit_onClic = new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           //Validator validator = new Validator();
           if(!boostrap.validator.validate(ETmedicine, new String[] {boostrap.validator.REQUIRED}) &&
                   !boostrap.validator.validate(ETdose, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_NUMBER}) &&
                   !boostrap.validator.validate(ETduration, new String[] {boostrap.validator.REQUIRED})) {
               if(!SPNfrequency.getSelectedItem().toString().equals(Constants.choiceFrequency)) {
                   if (Integer.parseInt(ETduration.getText().toString())>0){

                       if (pill.getStatus().equals("1") && pill.getEnable().equals("0")){ doEditPills();}
                       if (pill.getEnable().equals("1") && (aux != SPNfrequency.getSelectedItemPosition() || auxDuration != Integer.parseInt(ETduration.getText().toString()))) {
                           doEditPills();
                           PillBoxFragment pillBox = (PillBoxFragment) boostrap.getSupportFragmentManager().findFragmentById(R.id.FLmain);
                           pillBox.startNotification(Integer.parseInt(pill.getIdPill()), ETmedicine.getText().toString(), ETdose.getText().toString(), (SPNmeasure.getSelectedItem()).toString(), Miscellaneous.getFrequency(SPNfrequency.getSelectedItem().toString()),Integer.parseInt(ETduration.getText().toString()),false);
                       }
                       if (pill.getEnable().equals("1") && aux == SPNfrequency.getSelectedItemPosition()){
                           doEditPills();
                       }
                   } else {
                       AlertDialog errorFrequency = new AlertDialog.Builder(ctx)
                               .setMessage(boostrap.langStrings.get(Constants.error_duration_p))
                               .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       dialogInterface.dismiss();
                                   }
                               }).show();
                   }

               } else {
                   AlertDialog errorDuration = new AlertDialog.Builder(ctx)
                           .setMessage(boostrap.langStrings.get(Constants.error_frequency_p))
                           .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       dialogInterface.dismiss();
                                   }
                               }).show();
                   }
           }
       }
   };

    private void doEditPills (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUpdate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.update_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.edit_medic_success_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                PillBoxFragment pillBox = (PillBoxFragment) boostrap.getSupportFragmentManager().findFragmentById(R.id.FLmain);
                                                try {
                                                    pillBox.getPillBox();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }).show();
                                getDialog().dismiss();
                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    params.put(Constants.name, ETmedicine.getText().toString());
                    params.put("dose", ETdose.getText().toString());
                    params.put("measurement", String.valueOf(SPNmeasure.getSelectedItemPosition()+1));
                    params.put("frequency", String.valueOf(SPNfrequency.getSelectedItemPosition()+1));
                    params.put("duration", ETduration.getText().toString());
                    params.put("note", METinstruction.getText().toString());
                    params.put("status", pill.getStatus());
                    params.put("enabled", pill.getEnable());
                    params.put("pillbox_id", pill.getIdPill());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUpdate.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    private void getPillboxConfig (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueConfig = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.pillbox_config, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                JSONObject contentFre = jsonResp.getJSONObject(0);
                                JSONArray jsonFre = contentFre.getJSONArray("fre");
                                ArrayList<KeyPairsBean> fre = new ArrayList<>();
                                for (int i = 0; i < jsonFre.length(); i++) {
                                    JSONObject item = jsonFre.getJSONObject(i);
                                    fre.add(new KeyPairsBean(item.getInt("id_pillbox_frequency"),item.getString("name")));
                                }

                                ArrayAdapter<KeyPairsBean> adapter = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, fre);
                                SPNfrequency.setAdapter(adapter);

                                JSONObject contentMea = jsonResp.getJSONObject(1);
                                JSONArray jsonMea = contentMea.getJSONArray("mea");
                                ArrayList<KeyPairsBean> mea = new ArrayList<>();
                                for (int i = 0; i < jsonMea.length(); i++) {
                                    JSONObject item = jsonMea.getJSONObject(i);
                                    mea.add(new KeyPairsBean(item.getInt("id_pillbox_measure"),item.getString("name")));
                                }

                                ArrayAdapter<KeyPairsBean> adapterMea = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, mea);
                                SPNmeasure.setAdapter(adapterMea);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueConfig.add(request);
        }  else {

        }
    }

}
