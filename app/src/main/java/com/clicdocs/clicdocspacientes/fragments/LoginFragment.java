package com.clicdocs.clicdocspacientes.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MyFireBaseInstanceIDService;
import com.clicdocs.clicdocspacientes.SplashActivity;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import okhttp3.OkHttpClient;

public class LoginFragment extends Fragment {
    private Vibrator vib;
    private EditText ETemail, ETpass;
    private SessionManager session;
    private TextView TVregister;
    private Button BTNlogin, BTNloginFB;
    private ImageView IVbackground;
    private MainActivity boostrap;
    private AlertDialog DLnoconnection;
    private TextView TVforgotpass;
    private ProgressDialog loading;
    private Context ctx;
    private CallbackManager mCallbackManager;
    //private LoginButton loginButton;
    private ImageButton IMBback;
    private RequestQueue queueLogin, queueFBLogin, queueRecovery, queueTerms, queueAcceptTerms, queueTelephone;
    private View view;
    Profile profile;
    private boolean flag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view         = inflater.inflate(R.layout.login_fragment, container, false);
        BTNlogin     = (Button) view.findViewById(R.id.BTNlogin);
        BTNloginFB   = (Button) view.findViewById(R.id.BTNloginFB);
        TVregister   = (TextView) view.findViewById(R.id.TVregister);
        IVbackground = (ImageView) view.findViewById(R.id.IVbackground);
        ETemail      = (EditText) view.findViewById(R.id.ETemail);
        ETpass       = (EditText) view.findViewById(R.id.ETpassword);
        TVforgotpass = (TextView) view.findViewById(R.id.TVforgotpass);
        boostrap     = (MainActivity) getActivity();
        ctx          = getContext();
        IMBback      = (ImageButton) view.findViewById(R.id.IMBback);
        vib          = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager=CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().hide();
        BTNlogin.setText(boostrap.langStrings.get(Constants.login_p));
        TVforgotpass.setText(boostrap.langStrings.get(Constants.recovery_pass_p));
        TVregister.setText(boostrap.langStrings.get(Constants.sing_up_p));
        ETemail.setHint(boostrap.langStrings.get(Constants.email_p));
        ETpass.setHint(boostrap.langStrings.get(Constants.password_p));
        session = new SessionManager(ctx);
        TVregister.setOnClickListener(TVregister_onClick);
        BTNlogin.setOnClickListener(BTNlogin_onClick);
        TVforgotpass.setOnClickListener(TVforgotpass_onClick);
        IMBback.setOnClickListener(IMBback_onClic);
        BTNloginFB.setOnClickListener(BTNloginFB_onClick);

        ETpass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //Validator validator = new Validator();
                if(!boostrap.validator.validate(ETemail, new String[] {boostrap.validator.REQUIRED, boostrap.validator.EMAIL}) &&
                        !boostrap.validator.validate(ETpass, new String[] {boostrap.validator.REQUIRED})) {
                    doLogin();
                } else vib.vibrate(120);
                InputMethodManager inputManager = (InputMethodManager) boostrap.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ETpass.getWindowToken(), 0);
                return true;
            }
        });
    }

    private View.OnClickListener IMBback_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm = boostrap.getSupportFragmentManager();
            fm.popBackStack(fragments.LOGIN,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    };

    private View.OnClickListener TVregister_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boostrap.setFragment(fragments.REGISTER, null);
        }
    };

    //Evento al dar click al boton de iniciar sesiòn
    /*
    Se validan los campos de correo electrònico y contraseña que no vallan vacios
    y en caso del campo de correo que sea un formato de correo vàlido
    */
    private View.OnClickListener BTNlogin_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            //Validator validator = new Validator();
            if(!boostrap.validator.validate(ETemail, new String[] {boostrap.validator.REQUIRED, boostrap.validator.EMAIL}) &&
                    !boostrap.validator.validate(ETpass, new String[] {boostrap.validator.REQUIRED})) {
                doLogin();
            } else vib.vibrate(120);
        }
    };

    private View.OnClickListener BTNloginFB_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TrustManagerUtil.restoresHttpsUrlSettings();
            LoginManager.getInstance().logInWithReadPermissions(LoginFragment.this,Arrays.asList("public_profile"));

            LoginManager.getInstance().registerCallback(mCallbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.v("printFBlogin",loginResult.toString());
                            profile = Profile.getCurrentProfile();
                            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    try {
                                        String id = object.optString("id");
                                        String first_name = object.optString("first_name");
                                        String last_name = object.optString("last_name");
                                        String email = object.optString("email");
                                        String birthday = object.optString("birthday");
                                        String gender = object.optString("gender");

                                        JSONObject friends = object.getJSONObject("friends");
                                        JSONArray data = friends.getJSONArray("data");
                                        JSONArray friendsID = new JSONArray();
                                        for (int i = 0; i < data.length(); i++){
                                            JSONObject item = data.getJSONObject(i);
                                            friendsID.put(item.getString("id"));
                                        }

                                        String genderF="";
                                        switch (gender){
                                            case "male":
                                                genderF = "1";
                                                break;
                                            case "female":
                                                genderF = "2";
                                                break;
                                        }
                                        String pictureDataURL =object.getJSONObject("picture").getJSONObject("data").getString("url");;
                                        String [] date =birthday.split("/");
                                        String fecha = date[2]+"-"+date[0]+"-"+date[1];
                                        Log.v("printFoto",pictureDataURL);

                                        //setLoginFacebook(first_name,last_name,email,fecha,genderF,id,pictureDataURL,friendsID);
                                        setLoginFacebook(first_name,last_name,email,fecha,genderF,id,URLEncoder.encode(pictureDataURL,"UTF-8"),friendsID);


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields","id,first_name,last_name,email,birthday,gender,link,picture.width(300).height(300),friends");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(boostrap, "Login Cancel", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Toast.makeText(boostrap, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
        }
    };

    //Funciòn doLogin
    /*
    Esta funciòn se ejecuta cuando despuès de validar los campos de correo y contraseña para ser mandados al servicio
    y si la respuesta fue correcta guarda como datos de sesiòn el nombre, el uri de la foto, id de sesiòn, constraseña,
    tipo de usuario y el token que va ser utilizado para validaciones posteriores en consultas
    */
    private void doLogin(){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueLogin = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            //queueLogin = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, Constants.login, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    Log.v("printLogin",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject user = jsonObject.getJSONObject("response");
                                JSONObject chat = jsonObject.getJSONObject("chat");
                                if (user.getString("cellphone").equals("")) flag = true;
                                else flag = false;
                                session.setChat(chat.getString("username"),chat.getString("server"));
                                session.secondSession(user.getString("patient_id"),"5", user.getString("name")+" "+user.getString("last_name")+" "+user.getString("middle_name"),ETpass.getText().toString(),user.getString("picture"),jsonObject.getString("token"));
                                termsValidate();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            default:
                                Snackbar.make(view, boostrap.langStrings.get(Constants.error_login_p), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.username, ETemail.getText().toString());
                    params.put(Constants.password, ETpass.getText().toString());
                    params.put("type", "patient");
                    Log.v("printParams",""+params);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueLogin.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    //Funciòn setLoginFacebook
    /*
    Recibe los datos obtenidos de facebook y se mandan los paràmetros para loguearse
    */
    private void setLoginFacebook (final String nameF, final String lastnameF, final String emailF, final String birthdayF, final String genderF, final String fbid, final String picture, final JSONArray friendsID){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueFBLogin = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            //queueFBLogin = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, Constants.fblogin, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseF",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject user = jsonObject.getJSONObject("response");
                                JSONObject chat = jsonObject.getJSONObject("chat");
                                if (user.getString("cellphone").equals("")) flag = true;
                                else flag = false;
                                session.setChat(chat.getString("username"),chat.getString("server"));
                                session.secondSession(user.getString("patient_id"),"5", user.getString("name")+" "+user.getString("last_name")+" "+user.getString("middle_name"),"fb",user.getString("picture"),jsonObject.getString("token"));
                                termsValidate();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                AlertDialog error_no_found  = PopUpManager.showDialogMessage(ctx,jsonObject.getString("response"), boostrap.langStrings.get(Constants.accept_p), new PopUpManager.DialogMessage() {
                                    @Override
                                    public void OnClickListener(AlertDialog dialog) {
                                        dialog.dismiss();
                                    }
                                }, null, null);
                                error_no_found.show();
                                break;
                            default:
                                AlertDialog error  = PopUpManager.showDialogMessage(ctx,boostrap.langStrings.get(Constants.error_facebook_account_p), boostrap.langStrings.get(Constants.accept_p), new PopUpManager.DialogMessage() {
                                    @Override
                                    public void OnClickListener(AlertDialog dialog) {
                                        dialog.dismiss();
                                    }
                                }, null, null);
                                error.show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("name", nameF);
                    params.put("lastname", lastnameF);
                    params.put("email", emailF);
                    params.put("birthday", birthdayF);
                    params.put("gender", genderF);
                    params.put("fbid", fbid);
                    params.put("picture", picture);
                    params.put("friends", friendsID.toString());
                    Log.v("printParams",""+params);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueFBLogin.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }
    //Funciòn termsValidate
    /*
    Se valida si el usuario despues que se loguea tiene acaptado los tèrminos y condiciones mas actuales,
    de lo contrario madarà los nuevos tèrminos para que los acepte y si no lo acepta no termina el
    proceso de logueo.
    */
    private void termsValidate (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            //queueTerms = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, Constants.terms_validate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printValidateTerms",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                if (session.getPanicTelephone().get(1).equals("0")){
                                    getTelephoneWS();
                                } else {
                                    if (!flag){
                                        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                        LoginManager.getInstance().logOut();
                                        session.setLogin(session.secondProfile().get(0).toString(),"5", session.secondProfile().get(2).toString(),session.secondProfile().get(3).toString(),session.secondProfile().get(4).toString(),session.secondProfile().get(5).toString());
                                        MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                        token.sendRegistrationToServer(refreshedToken,ctx,session.secondProfile().get(0).toString());
                                        ETemail.setText("");
                                        ETpass.setText("");
                                        boostrap.finish();
                                        Intent main =new Intent(ctx, SplashActivity.class);
                                        startActivity(main);
                                    } else {
                                        registerTelephone(session.secondProfile().get(0).toString());
                                    }

                                }
                                break;
                            case 402:
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                JSONObject terms = jsonResp.getJSONObject(0);
                                dialogTerms(terms.getString("term").toString(),terms.getString("term_id").toString());
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            default:
                                Snackbar.make(view, boostrap.langStrings.get(Constants.error_login_p), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("session_id", session.secondProfile().get(0).toString());
                    params.put("id_user_type", "5");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.secondProfile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueTerms.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    //Funciòn dialogTerms
    /*
    Se muestra en pantalla los nuevos terminos para ser aceptado por el usuario
    */
    private void dialogTerms(String terms, String terms_id){
        View dialog_terms        = LayoutInflater.from(ctx).inflate(R.layout.dialog_terms, null);
        Button BTNreject         = (Button) dialog_terms.findViewById(R.id.BTNreject);
        Button BTNaccept         = (Button) dialog_terms.findViewById(R.id.BTNaccept);
        TextView TVterms         = (TextView) dialog_terms.findViewById(R.id.TVterms);
        LinearLayout LLform      = (LinearLayout) dialog_terms.findViewById(R.id.LLform);
        ProgressBar PBloading    = (ProgressBar) dialog_terms.findViewById(R.id.PBloading);
        BTNaccept.setText(boostrap.langStrings.get(Constants.accept_p));
        BTNreject.setText(boostrap.langStrings.get(Constants.to_refuse_p));
        final AlertDialog DLterms   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.terms_condi_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_terms).show();

        TVterms.setText(Html.fromHtml(terms));

        BTNreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.deleteProfile();
                session.deleteSecondProfile();
                FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                LoginManager.getInstance().logOut();
                DLterms.dismiss();
            }
        });

        BTNaccept.setOnClickListener(BTNaccept_onClick(terms_id, LLform, PBloading, DLterms));

    }

    private View.OnClickListener BTNaccept_onClick(final String id_term,final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLrecovery) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(NetworkUtils.haveNetworkConnection(ctx)) {
                    LLform.setVisibility(View.GONE);
                    PBloading.setVisibility(View.VISIBLE);
                    queueAcceptTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    //queueAcceptTerms = Volley.newRequestQueue(ctx);
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.accep_terms, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("printUser",response+"");
                            PBloading.setVisibility(View.GONE);
                            LLform.setVisibility(View.VISIBLE);
                            DLrecovery.dismiss();
                                //loading.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        if (session.getPanicTelephone().get(1).equals("0")){
                                            getTelephoneWS();
                                        } else {
                                            if (!flag){
                                                FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                                LoginManager.getInstance().logOut();
                                                session.setLogin(session.secondProfile().get(0).toString(),"5", session.secondProfile().get(2).toString(),session.secondProfile().get(3).toString(),session.secondProfile().get(4).toString(),session.secondProfile().get(5).toString());
                                                MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                                token.sendRegistrationToServer(refreshedToken,ctx,session.secondProfile().get(0).toString());
                                                ETemail.setText("");
                                                ETpass.setText("");
                                                boostrap.finish();
                                                Intent main =new Intent(ctx, SplashActivity.class);
                                                startActivity(main);
                                            } else {
                                                registerTelephone(session.secondProfile().get(0).toString());
                                            }
                                        }

                                        break;
                                    case 404:
                                        Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                        break;
                                    default:
                                        Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("session_id", session.secondProfile().get(0).toString());
                            params.put("term_id", id_term);
                            params.put("user_type", "5");
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Constants.key_access_token, session.secondProfile().get(5).toString());
                            headers.put(Constants.key_bundle_id, Constants.bundle_id);
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueAcceptTerms.add(request);
                }  else {

                }
            }
        };
    }

    private void registerTelephone(final String patient_id){
        View dialog_telehone = LayoutInflater.from(ctx).inflate(R.layout.dialog_telephones_numbers, null);
        TextInputLayout TILphone        = (TextInputLayout) dialog_telehone.findViewById(R.id.TILphone);
        TextInputLayout TILcellphone    = (TextInputLayout) dialog_telehone.findViewById(R.id.TILcellphone);
        EditText        ETphone         = (EditText) dialog_telehone.findViewById(R.id.ETphone);
        EditText        ETcellphone     = (EditText) dialog_telehone.findViewById(R.id.ETcellphone);
        Button          BTNsave         = (Button) dialog_telehone.findViewById(R.id.BTNsave);
        ProgressBar     PBloading       = (ProgressBar) dialog_telehone.findViewById(R.id.PBloading);
        LinearLayout    LLshow          = (LinearLayout) dialog_telehone.findViewById(R.id.LLelement);
        TextView        TVlegend        = (TextView) dialog_telehone.findViewById(R.id.TVlegend);

        final AlertDialog DLtelephones = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.complete_registration_p))
                .setCancelable(false)
                .setView(dialog_telehone).show();

        BTNsave.setOnClickListener(BTNsave_onClick(ETphone, ETcellphone, patient_id, LLshow, PBloading, DLtelephones));
    }

    private View.OnClickListener BTNsave_onClick(final EditText phone, final EditText cell, final String id,final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLphone) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Validator validator = new Validator();
                if(!boostrap.validator.validate(phone, new String[] { boostrap.validator.ONLY_NUMBER}) &&
                        !boostrap.validator.validate(cell, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_NUMBER})) {
                    if(NetworkUtils.haveNetworkConnection(ctx)) {
                        LLform.setVisibility(View.GONE);
                        PBloading.setVisibility(View.VISIBLE);
                        RequestQueue queuePhone = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                        //RequestQueue queuePhone = Volley.newRequestQueue(ctx);
                        StringRequest request = new StringRequest(Request.Method.POST, Constants.register_phone, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v("printResponsePhone",response+"");
                                PBloading.setVisibility(View.GONE);
                                LLform.setVisibility(View.VISIBLE);
                                DLphone.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                        case 200:
                                            FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                            LoginManager.getInstance().logOut();
                                            session.setLogin(session.secondProfile().get(0).toString(),"5", session.secondProfile().get(2).toString(),session.secondProfile().get(3).toString(),session.secondProfile().get(4).toString(),session.secondProfile().get(5).toString());
                                            MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                            token.sendRegistrationToServer(refreshedToken,ctx,session.secondProfile().get(0).toString());
                                            ETemail.setText("");
                                            ETpass.setText("");
                                            boostrap.finish();
                                            Intent main =new Intent(ctx, SplashActivity.class);
                                            startActivity(main);

                                            break;
                                        case 404:
                                            Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                            break;
                                        default:
                                            Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                            break;
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.v("error", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> params = new HashMap<>();
                                params.put("phone", phone.getText().toString());
                                params.put("cellphone", cell.getText().toString());
                                params.put("patient_id", id);
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put(Constants.key_access_token, session.secondProfile().get(5).toString());
                                headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                return headers;
                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queuePhone.add(request);
                    }  else {

                    }
                } else vib.vibrate(120);
            }
        };
    }

    //Funciòn getTelephoneWS
    /*
    Obtiene el nùmero de emergencia colocado desde el panel de administrativo, si el usuario ya agregò un nùmero
    ignora la funciòn.
    */
    private void getTelephoneWS (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueTelephone = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            //queueTelephone = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_panic_telephone, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printUser",response+"");
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                session.setPanicTelephone(jsonObject.getString(Constants.response),"0");
                                if (!flag){
                                    FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                    LoginManager.getInstance().logOut();
                                    session.setLogin(session.secondProfile().get(0).toString(),"5", session.secondProfile().get(2).toString(),session.secondProfile().get(3).toString(),session.secondProfile().get(4).toString(),session.secondProfile().get(5).toString());
                                    MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                    token.sendRegistrationToServer(refreshedToken,ctx,session.secondProfile().get(0).toString());
                                    ETemail.setText("");
                                    ETpass.setText("");
                                    boostrap.finish();
                                    Intent main =new Intent(ctx, SplashActivity.class);
                                    startActivity(main);
                                } else {
                                    registerTelephone(session.secondProfile().get(0).toString());
                                }

                                break;
                            case 404:
                                Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.secondProfile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueTelephone.add(request);
        }  else {

        }
    }

    //Funciòn Evento recuperar contraseña
    /*
    Muestra un dialogo para ingresar el correo y recuperar la contraseña
    */
    private View.OnClickListener TVforgotpass_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View dialog_recoverypass = LayoutInflater.from(ctx).inflate(R.layout.dialog_recoverypass, null);
            Button BTNrecovery       = (Button) dialog_recoverypass.findViewById(R.id.BTNrecovery);
            Button BTNcancel         = (Button) dialog_recoverypass.findViewById(R.id.BTNcancel);
            TextView TVlegedrecovery = (TextView) dialog_recoverypass.findViewById(R.id.TVlegendrecovery);
            EditText ETremail        = (EditText) dialog_recoverypass.findViewById(R.id.ETremail);
            ProgressBar PBloading    = (ProgressBar) dialog_recoverypass.findViewById(R.id.PBloading);
            LinearLayout LLform      = (LinearLayout) dialog_recoverypass.findViewById(R.id.LLform);
            final AlertDialog DLrecovery   = new AlertDialog.Builder(ctx)
                    .setTitle(boostrap.langStrings.get(Constants.recover_pass_p))
                    .setCancelable(false)
                    .setView(dialog_recoverypass).show();
            TVlegedrecovery.setText(boostrap.langStrings.get(Constants.legend_recover_pass_p));
            ETremail.setHint(boostrap.langStrings.get(Constants.email_p));
            BTNrecovery.setText(boostrap.langStrings.get(Constants.recover_p));
            BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
            BTNrecovery.setOnClickListener(BTNrecovery_onClick(ETremail, LLform, PBloading, DLrecovery));

            BTNcancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DLrecovery.dismiss();
                }
            });
        }
    };

    private View.OnClickListener BTNrecovery_onClick(final EditText ETremail, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLrecovery) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //final Validator validator = new Validator();
                if(!boostrap.validator.validate(ETremail, new String [] {boostrap.validator.REQUIRED, boostrap.validator.EMAIL})) {
                    if(NetworkUtils.haveNetworkConnection(ctx)) {
                        LLform.setVisibility(View.GONE);
                        PBloading.setVisibility(View.VISIBLE);
                        queueRecovery = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                        //queueRecovery = Volley.newRequestQueue(ctx);
                        StringRequest request = new StringRequest(Request.Method.POST, Constants.recovery_pass, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v("printUser",response+"");
                                PBloading.setVisibility(View.GONE);
                                LLform.setVisibility(View.VISIBLE);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                        case 200:
                                            AlertDialog success = new AlertDialog.Builder(ctx)
                                                    .setMessage(jsonObject.getString("response"))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            DLrecovery.dismiss();
                                                            dialogInterface.dismiss();

                                                        }
                                                    }).show();
                                            break;
                                        case 400:
                                            AlertDialog fail_mail = new AlertDialog.Builder(ctx)
                                                    .setCancelable(false)
                                                    .setMessage(boostrap.langStrings.get(Constants.email_not_exist_p))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                        default:
                                            Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                            break;
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.v("error", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> params = new HashMap<>();
                                params.put(Constants.email, ETremail.getText().toString());
                                params.put("type_user", "patient");
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                return headers;
                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queueRecovery.add(request);
                    }  else {

                    }
                }
            }
        };
    }
}
