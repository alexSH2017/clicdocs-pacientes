package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class NonPathologicalPersonalFragment extends Fragment implements ClinicHistoryPagerFragment.FragmentLifecycle {

    private ProgressDialog loading;
    private TextView TVcleans, TVshower, TVtoohbrush, TVroom, TVvices, TVsmookingday, TVsmookingyear, TVdrink, TVfeeding, TVfeedingday, TVquality, TVsports, TVsport, TVotros, TVinmunizations, TVcuales, TVdreaming;
    private EditText ETcigday,ETcigyear,ETalcoholism, ETfeedingonday,ETsport,ETpendientes,ETdesparasitacion;
    private Spinner SPshower, SPtootbrush, SProom, SPfood;
    private CardView CVpendientes;
    private Spinner SPinmunization;
    private Context ctx;
    private MainActivity boostrap;
    private SessionManager session;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ClinicHistoryPagerFragment parent;

    private ArrayList<KeyPairsBean> food = new ArrayList<>();
    private View view;
    private RequestQueue queueNonPathological,queueSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.no_pathological_personal_fragment,container,false);
        boostrap            = (MainActivity) getActivity();
        ctx                 = getContext();
        session             = new SessionManager(ctx);
        parent              = (ClinicHistoryPagerFragment) getParentFragment();
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        SPshower            = (Spinner) view.findViewById(R.id.SPshower);
        SPtootbrush         = (Spinner) view.findViewById(R.id.SPtootbrush);
        SProom              = (Spinner) view.findViewById(R.id.SProom);
        //ETqualityfood       = (EditText) view.findViewById(R.id.ETqualityFood);
        SPfood              = (Spinner) view.findViewById(R.id.SPfood);
        TVcleans            = (TextView) view.findViewById(R.id.TVclean);
        TVshower            = (TextView) view.findViewById(R.id.TVshower);
        TVtoohbrush         = (TextView) view.findViewById(R.id.TVtoothbrush);
        TVroom              = (TextView) view.findViewById(R.id.TVroom);
        TVvices             = (TextView) view.findViewById(R.id.TVvices);
        TVsmookingday       = (TextView) view.findViewById(R.id.TVsmookingday);
        TVsmookingyear      = (TextView) view.findViewById(R.id.TVsmookingyear);
        TVfeeding           = (TextView) view.findViewById(R.id.TVfeeding);
        TVfeedingday        = (TextView) view.findViewById(R.id.TVfeedingday);
        TVquality           = (TextView) view.findViewById(R.id.TVqualityfood);
        TVsports            = (TextView) view.findViewById(R.id.TVsports);
        TVsport             = (TextView) view.findViewById(R.id.TVsport);
        TVotros             = (TextView) view.findViewById(R.id.TVotros);
        TVinmunizations     = (TextView) view.findViewById(R.id.TVinminization);
        TVcuales            = (TextView) view.findViewById(R.id.TVcuales);
        TVdreaming          = (TextView) view.findViewById(R.id.TVdreaming);
        TVdrink             = (TextView) view.findViewById(R.id.TVdrink);
        ETcigday            = (EditText) view.findViewById(R.id.ETcigday);
        ETcigyear           = (EditText) view.findViewById(R.id.ETcigyear);
        ETalcoholism        = (EditText) view.findViewById(R.id.ETalcoholism);
        ETfeedingonday      = (EditText) view.findViewById(R.id.ETfoodDay);
        ETsport             = (EditText) view.findViewById(R.id.ETsport);
        SPinmunization      = (Spinner) view.findViewById(R.id.SPinmunization);
        ETpendientes        = (EditText) view.findViewById(R.id.ETpendientes);
        CVpendientes        = (CardView) view.findViewById(R.id.CVpendientes);
        ETdesparasitacion   = (EditText) view.findViewById(R.id.ETdeworming);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue);


        ArrayList<KeyPairsBean> shower = new ArrayList<>();
        shower.add(new KeyPairsBean(0, boostrap.langStrings.get(Constants.any)));
        shower.add(new KeyPairsBean(1, boostrap.langStrings.get(Constants.daily)));
        shower.add(new KeyPairsBean(2, boostrap.langStrings.get(Constants.each3days)));
        shower.add(new KeyPairsBean(3, boostrap.langStrings.get(Constants.irregular)));
        ArrayAdapter<KeyPairsBean> Ashower = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, shower);
        SPshower.setAdapter(Ashower);

        ArrayList<KeyPairsBean> tootbrush = new ArrayList<>();
        tootbrush.add(new KeyPairsBean(0, boostrap.langStrings.get(Constants.any)));
        tootbrush.add(new KeyPairsBean(1, boostrap.langStrings.get(Constants.one_day_p)));
        tootbrush.add(new KeyPairsBean(2, boostrap.langStrings.get(Constants.two_day_p)));
        tootbrush.add(new KeyPairsBean(3, boostrap.langStrings.get(Constants.three_day_p)));
        ArrayAdapter<KeyPairsBean> Atootbrush = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, tootbrush);
        SPtootbrush.setAdapter(Atootbrush);

        ArrayList<KeyPairsBean> room = new ArrayList<>();
        room.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.urban_p)));
        room.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.rural_p)));
        room.add(new KeyPairsBean(3,boostrap.langStrings.get(Constants.allservices_p)));
        room.add(new KeyPairsBean(4,boostrap.langStrings.get(Constants.letrine_p)));
        ArrayAdapter<KeyPairsBean> Aroom = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, room);
        SProom.setAdapter(Aroom);

        ArrayList<KeyPairsBean> food = new ArrayList<>();
        food.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.regular_p)));
        food.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.good_p)));
        food.add(new KeyPairsBean(3,boostrap.langStrings.get(Constants.bad_p)));
        ArrayAdapter<KeyPairsBean> Afood = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, food);
        SPfood.setAdapter(Afood);

        ArrayList<KeyPairsBean> inmunization = new ArrayList<>();
        inmunization.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.complete_age_p)));
        inmunization.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.pendings_p)));
        ArrayAdapter<KeyPairsBean> Ainmuni = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, inmunization);
        SPinmunization.setAdapter(Ainmuni);



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                getNoPathological();
            }
        });
        TVcleans.setText(boostrap.langStrings.get(Constants.personal_cleanliness_p));
        TVshower.setText(boostrap.langStrings.get(Constants.bath_p));
        TVtoohbrush.setText(boostrap.langStrings.get(Constants.tooth_washing_p));
        TVroom.setText(boostrap.langStrings.get(Constants.room_p));
        TVvices.setText(boostrap.langStrings.get(Constants.vices_p).toUpperCase());
        TVsmookingday.setText(boostrap.langStrings.get(Constants.smooking_day_p));
        TVsmookingyear.setText(boostrap.langStrings.get(Constants.smooking_year_p));
        TVdrink.setText(boostrap.langStrings.get(Constants.beer_p));
        TVfeeding.setText(boostrap.langStrings.get(Constants.feeding_p));
        TVfeedingday.setText(boostrap.langStrings.get(Constants.foods_day_p));
        TVquality.setText(boostrap.langStrings.get(Constants.food_quality_p));
        TVsport.setText(boostrap.langStrings.get(Constants.sports_p));
        TVsports.setText(boostrap.langStrings.get(Constants.sports_p).toUpperCase());
        TVotros.setText(boostrap.langStrings.get(Constants.othres_p).toUpperCase());
        TVinmunizations.setText(boostrap.langStrings.get(Constants.immunizations_p));
        ETpendientes.setHint(boostrap.langStrings.get(Constants.which_p));
        TVcuales.setText(boostrap.langStrings.get(Constants.which_p));
        TVdreaming.setText(boostrap.langStrings.get(Constants.last_deworming_p));

        ETcigday.setHint(boostrap.langStrings.get(Constants.smooking_day_p));
        ETcigyear.setHint(boostrap.langStrings.get(Constants.smooking_year_p));
        ETalcoholism.setHint(boostrap.langStrings.get(Constants.beer_p));
        ETfeedingonday.setHint(boostrap.langStrings.get(Constants.foods_day_p));
        ETsport.setHint(boostrap.langStrings.get(Constants.sports_p));
        ETdesparasitacion.setHint(boostrap.langStrings.get(Constants.last_deworming_p));


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNoPathological ();
            }
        });

        SPinmunization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    ETpendientes.setText("");
                    CVpendientes.setVisibility(View.GONE);
                } else {
                    ETpendientes.setText("");
                    CVpendientes.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void getNoPathological (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queueNonPathological = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_nonpathological, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    //loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonNonPathological= jsonObject.getJSONArray("response");
                                JSONObject item = jsonNonPathological.getJSONObject(0);

                                SPfood.setSelection(1);
                                SPinmunization.setSelection(1);
                                SPshower.setSelection(Miscellaneous.isInteger(item.getString("bathroom")));
                                SPtootbrush.setSelection(Miscellaneous.isInteger(item.getString("brush_teeth")));
                                SProom.setSelection(Miscellaneous.isInteger(item.getString("room"))-1);
                                ETcigday.setText(Miscellaneous.validate(item.getString("smoking")));
                                ETcigyear.setText(Miscellaneous.validate(item.getString("smoking_time")));
                                ETalcoholism.setText(Miscellaneous.validate(item.getString("alcoholism")));
                                ETfeedingonday.setText(Miscellaneous.validate(item.getString("feeding")));
                                SPfood.setSelection(Miscellaneous.isInteger(item.getString("feeding_quality"))-1);
                                //ETqualityfood.setText(item.getString("feeding_quality"));
                                ETsport.setText(item.getString("sports"));
                                SPinmunization.setSelection(Miscellaneous.isInteger(item.getString("immunizationSelect"))-1);
                                if (Miscellaneous.isInteger(item.getString("immunizationSelect")) == 1) {
                                    ETpendientes.setText("");
                                    CVpendientes.setVisibility(View.GONE);
                                } else {
                                    CVpendientes.setVisibility(View.VISIBLE);
                                    ETpendientes.setText(item.getString("immunization"));
                                }
                                ETdesparasitacion.setText(Miscellaneous.validate(item.getString("last_deworming")));
                                break;
                            case 202:
                                Snackbar.make(parent.getView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueNonPathological.add(request);
        }  else {

        }
    }


    private void validateNopathological (){
        saveNoPathological();
    }

    private void saveNoPathological (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_nonpathological, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.msg_personal_no_patologic_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    params.put("bathroom", String.valueOf(((KeyPairsBean)SPshower.getSelectedItem()).getKey()));
                    params.put("toothbrush", String.valueOf(((KeyPairsBean)SPtootbrush.getSelectedItem()).getKey()));
                    params.put("room", String.valueOf(((KeyPairsBean)SProom.getSelectedItem()).getKey()));
                    params.put("feedingonday", ETfeedingonday.getText().toString());
                    params.put("feedingquality", String.valueOf(SPfood.getSelectedItemPosition()+1));
                    params.put("sport", ETsport.getText().toString());
                    params.put("smookingonday", ETcigday.getText().toString());
                    params.put("smookingonyear", ETcigyear.getText().toString());
                    params.put("alcoholism", ETalcoholism.getText().toString());
                    params.put("deworming", ETdesparasitacion.getText().toString());
                    params.put("immunizationsSelect", String.valueOf(SPinmunization.getSelectedItemPosition()+1));
                    params.put("immunizations", ETpendientes.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.save_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                validateNopathological();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResumeFragment() {
        if (getContext() != null){ //getNoPathological();
             }
    }
}
