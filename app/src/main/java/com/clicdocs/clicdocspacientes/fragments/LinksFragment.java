package com.clicdocs.clicdocspacientes.fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterLinks;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Links;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class LinksFragment extends Fragment {

    private TextView TVlegend;
    private View view;
    private MainActivity boostrap;
    private Context ctx;
    private RecyclerView mRecycler;
    private SessionManager session;
    private RequestQueue queueLink;
    private RecyclerViewAdapterLinks adapter;
    private ProgressBar PBloadMore;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_general,container,false);
        boostrap                =   (MainActivity) getActivity();
        ctx                     =   getContext();
        session                 =   new SessionManager(ctx);
        mRecycler               = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        PBloadMore              = (ProgressBar) view.findViewById(R.id.PBloadMore);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        return view;
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        try {
            GetLinks();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void GetLinks () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueLink = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_link, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonRes = jsonObject.getJSONObject(Constants.response);
                                JSONArray rows = jsonRes.getJSONArray("rows");

                                final ArrayList<Links> links =new ArrayList<Links>();

                                for (int i = 0; i < rows.length(); i++){
                                    JSONObject item = rows.getJSONObject(i);
                                    Log.v("printLink",item.getString("id_link_list")+"  "+ item.getString("picture")+"  "+item.getString("title")+"   "+item.getString("description")+"   "+item.getString("link"));
                                    links.add(new Links(item.getString("id_link_list"), item.getString("picture"),item.getString("title"),item.getString("description"),item.getString("link")));
                                }
                                adapter = new RecyclerViewAdapterLinks(ctx,links, new RecyclerViewAdapterLinks.Event() {
                                    @Override
                                    public void onClic(Links link) {
                                        String urlString=link.getLink().toString();
                                        if (!urlString.isEmpty()){
                                            Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(urlString.toLowerCase()));
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setPackage("com.android.chrome");
                                            try {
                                                ctx.startActivity(intent);
                                            } catch (ActivityNotFoundException ex) {
                                                intent.setPackage(null);
                                                ctx.startActivity(intent);
                                            }
                                        }

                                    }

                                    @Override
                                    public void onMore(int i) throws JSONException {
                                        PBloadMore.setVisibility(View.VISIBLE);
                                        getLoadMore(i);
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                TVlegend.setVisibility(View.GONE);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueLink.add(request);
        }  else {

        }
    }

    private void getLoadMore (final int i) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queueLink = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_link, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    PBloadMore.setVisibility(View.GONE);
                    Log.v("printResponse",""+response);
                    //loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonRes = jsonObject.getJSONObject(Constants.response);
                                JSONArray rows = jsonRes.getJSONArray("rows");
                                final ArrayList<Links> links =new ArrayList<Links>();

                                for (int i = 0; i < rows.length(); i++){
                                    JSONObject item = rows.getJSONObject(i);
                                    Log.v("printLink",item.getString("id_link_list")+"  "+ item.getString("picture")+"  "+item.getString("title")+"   "+item.getString("description")+"   "+item.getString("link"));
                                    links.add(new Links(item.getString("id_link_list"), item.getString("picture"),item.getString("title"),item.getString("description"),item.getString("link")));
                                }
                                adapter.add(links);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    params.put("limit",String.valueOf(i));
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            queueLink.add(request);
        }  else {

        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.links_p));
        setHasOptionsMenu(true);
        boostrap.getSupportActionBar().show();
    }
}
