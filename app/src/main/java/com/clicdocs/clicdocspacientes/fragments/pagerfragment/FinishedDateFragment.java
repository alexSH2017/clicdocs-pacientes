package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.SwipeRecyclerViewFinished;
import com.clicdocs.clicdocspacientes.fragments.MyDatesFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.MetaRequest;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class FinishedDateFragment extends Fragment implements MyDatesFragment.FragmentLifecycle{

    private TextView TVlegend;
    private TextView Tvinformation;
    private View view;
    private AlertDialog DLnoconnection;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RecyclerView mRecycler;
    private Calendar cal;
    private RequestQueue queueAppointment, queueScore;
    private MyDatesFragment parent;
    private SwipeRecyclerViewFinished adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_appointment, container, false);
        parent          = (MyDatesFragment) getParentFragment();
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        mRecycler       = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        Tvinformation = (TextView) view.findViewById(R.id.TVinformation);
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVlegend.setText(boostrap.langStrings.get(Constants.no_result_p));
        Tvinformation.setText(boostrap.langStrings.get(Constants.legend_finished_appo_p));
        cal = Calendar.getInstance();
        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                try {
                    GetDates();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void GetDates () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","4");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseFinished",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                mRecycler.setVisibility(View.VISIBLE);
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                            item.getString("doctor_id"),
                                            item.getString("office_id"),
                                            item.getString("doc_full_name"),
                                            item.getString("consulting_room"),
                                            item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                            item.getString("borough"),
                                            item.getString("state"),
                                            item.getString("latitude"),
                                            item.getString("longitude"),
                                            item.getString("date"),
                                            item.getString("hour"),
                                            item.getString("picture")));
                                }

                                adapter = new SwipeRecyclerViewFinished(boostrap,dates, ctx, new SwipeRecyclerViewFinished.Event() {
                                    @Override
                                    public void onClicLocation(Dates appointment) {

                                    }

                                    @Override
                                    public void onClicSeeRecipe(Dates appoinment) {
                                        downloadPdf(Constants.uri_recipe+appoinment.getIdAppointment()+"/"+session.profile().get(5));
                                    }

                                    @Override
                                    public void onPager(int page) {

                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                break;
                            case 204:
                                TVlegend.setVisibility(View.VISIBLE);
                                mRecycler.setVisibility(View.GONE);
                                break;
                            case 404:
                                TVlegend.setVisibility(View.VISIBLE);
                                mRecycler.setVisibility(View.GONE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new com.android.volley.DefaultRetryPolicy( 0, com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES, com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }

    private void pagerAppointment (final int page) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","4");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseFinished",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                mRecycler.setVisibility(View.VISIBLE);
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                            item.getString("doctor_id"),
                                            item.getString("office_id"),
                                            item.getString("doc_full_name"),
                                            item.getString("consulting_room"),
                                            item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                            item.getString("borough"),
                                            item.getString("state"),
                                            item.getString("latitude"),
                                            item.getString("longitude"),
                                            item.getString("date"),
                                            item.getString("hour"),
                                            item.getString("picture")));
                                }

                                adapter.add(dates);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    params.put("limit",""+page);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new com.android.volley.DefaultRetryPolicy( 0, com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES, com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }

    private void downloadPdf(String uri) {
        final AlertDialog downloading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
        downloading.show();
        Log.v("printURIpres",uri);
        final Uri downloadUri = Uri.parse(uri);
        final RequestQueue queue = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactoryClicDocs(ctx)));
        final MetaRequest headers = new MetaRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response.has("Content-Type")) {
                    try {
                        final String Content = response.getString("Content-Type");
                        if(Content.equals("application/pdf")) {
                            Uri destinationUri = Uri.parse(ctx.getExternalCacheDir() + File.separator + "prescription.pdf");
                            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                                    .setRetryPolicy(new com.thin.downloadmanager.DefaultRetryPolicy())
                                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                                    .setStatusListener(new DownloadStatusListenerV1() {
                                        @Override
                                        public void onDownloadComplete(DownloadRequest downloadRequest) {
                                            downloading.dismiss();
                                            String filepath = downloadRequest.getDestinationURI().getPath();
                                            File file = new File(filepath);
                                            openFile(file);
                                        }

                                        @Override
                                        public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                                            downloading.dismiss();
                                            final AlertDialog faildown = PopUpManager.showDialogMessage(ctx, boostrap.langStrings.get(Constants.not_prescription_p), boostrap.langStrings.get(Constants.accept_p), new PopUpManager.DialogMessage() {
                                                @Override
                                                public void OnClickListener(AlertDialog dialog) {
                                                    dialog.dismiss();
                                                }
                                            }, null, null);
                                            faildown.show();
                                        }

                                        @Override
                                        public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                                            Log.v("downloading", String.valueOf(progress));
                                        }
                                    });
                            ThinDownloadManager manager = new ThinDownloadManager();
                            manager.add(downloadRequest);
                        } else {
                            downloading.dismiss();
                            Toast.makeText(ctx,  boostrap.langStrings.get(Constants.not_prescription_p), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                downloading.dismiss();
                Toast.makeText(ctx,  boostrap.langStrings.get(Constants.not_prescription_p), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(headers);
    }

    private void openFile(File current_file) {
        Log.v("print","print");
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        String ext = extFile(current_file.getName());
        String mimeType = myMime.getMimeTypeFromExtension(ext);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(Build.VERSION.SDK_INT >= 24) {
            newIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            newIntent.setDataAndType(FileProvider.getUriForFile(ctx, ctx.getPackageName()+".provider", current_file),mimeType);
        } else {
            newIntent.setDataAndType(Uri.fromFile(current_file),mimeType);
        }
        try {
            startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }


    }
    // ================ SPECIAL EVENTS =======================
    private String extFile(String name) {
        String [] parts = name.split("\\.");
        String format;
        if(parts.length == 2) {
            format = parts[1];
        } else {
            format = parts[parts.length-1];
        }
        return format.toLowerCase();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onResumeFragment() throws JSONException {
        if (getContext() != null){ GetDates(); }
    }
}
