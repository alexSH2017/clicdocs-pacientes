package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.LocationOfficeActivity;
import com.clicdocs.clicdocspacientes.adapters.SwipeRecyclerViewApproved;
import com.clicdocs.clicdocspacientes.fragments.MyDatesFragment;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class ApprovedDateFragment extends Fragment implements MyDatesFragment.FragmentLifecycle{

    private TextView TVlegend;
    private TextView TVinformation;
    private View view;
    private AlertDialog  DLnoconnection;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RequestQueue queueAppointment, queueCancel;
    private RecyclerView myRecycler;
    private SwipeRecyclerViewApproved adapter;
    private MyDatesFragment parent;
    private final int WR_PERMS = 3453;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_appointment, container, false);
        boostrap        =   (MainActivity) getActivity();
        parent          = (MyDatesFragment) getParentFragment();
        ctx             =   getContext();
        session         =   new SessionManager(ctx);
        myRecycler= (RecyclerView) view.findViewById(R.id.mRecyclerView);
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        TVinformation = (TextView) view.findViewById(R.id.TVinformation);
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVlegend.setText(boostrap.langStrings.get(Constants.no_result_p));
        TVinformation.setText(boostrap.langStrings.get(Constants.legend_approve_appo_p));
        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                try {
                    getAppointmentsApprove();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getAppointmentsApprove ()throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","3");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAprove",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                                        item.getString("doctor_id"),
                                                        item.getString("office_id"),
                                                        item.getString("doc_full_name"),
                                                        item.getString("full_address"),
                                                        item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                                        item.getString("borough"),
                                                        item.getString("state"),
                                                        item.getString("latitude"),
                                                        item.getString("longitude"),
                                                        item.getString("date"),
                                                        item.getString("hour"),
                                                        item.getString("picture")));
                                }

                                adapter = new SwipeRecyclerViewApproved(boostrap, ctx, dates, new SwipeRecyclerViewApproved.Event() {

                                    @Override
                                    public void onClicLocation(Dates item) {
                                        Intent edit = new Intent(ctx, LocationOfficeActivity.class);
                                        edit.putExtra(Constants.langString,boostrap.langStrings);
                                        edit.putExtra("appointment", (Serializable) item);
                                        startActivityForResult(edit, 123);
                                    }

                                    @Override
                                    public void onClicCancelAppoinmnet(final Dates appoinment, final int position) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                        builder.setMessage(boostrap.langStrings.get(Constants.message_cancel_appo_p))
                                                .setTitle(boostrap.langStrings.get(Constants.cancel_appo_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        doCancelAppoinment(appoinment.getIdAppointment(), position);
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }); builder.show();
                                    }

                                    @Override
                                    public void onClicRescheduleAppoinmnet(Dates appoinment) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("idAppoinment",appoinment.getIdAppointment());
                                        bundle.putString("idDoctorOffice", appoinment.getIdDoctorOffice());
                                        bundle.putString("idSession",appoinment.getIdDoctor());
                                        bundle.putString("name_doctor",appoinment.getName_doctor());
                                        bundle.putString("picture",appoinment.getPicture());
                                        boostrap.setFragment(fragments.RESCHEDULE,bundle);
                                    }

                                    @Override
                                    public void onClicCalendar(Dates item) {
                                        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                                                && ContextCompat.checkSelfPermission(ctx,Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED){
                                            requestPermissions(new String[] {Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, WR_PERMS);
                                        } else {
                                            final AlertDialog loadingCalendar = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                                            loadingCalendar.show();
                                            if (saveAppoinmentCalendar(item.getHour(),item.getDate(),item.getName_doctor(),item.getName_office())){
                                                loadingCalendar.dismiss();
                                                AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                                                builder.setMessage(boostrap.langStrings.get(Constants.add_event_calendar_p))
                                                        .setTitle("CLICDOCS")
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builder.show();
                                            }
                                        }

                                    }

                                    @Override
                                    public void onPager(int page) {

                                    }
                                });
                                myRecycler.setAdapter(adapter);
                                break;
                            case 204:
                                TVlegend.setVisibility(View.VISIBLE);
                                myRecycler.setVisibility(View.GONE);
                                break;
                            case 404:
                                TVlegend.setVisibility(View.VISIBLE);
                                myRecycler.setVisibility(View.GONE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Access-Token", session.profile().get(5).toString());
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }

    private void pagerAppointment (final int page)throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","3");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAprove",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                            item.getString("doctor_id"),
                                            item.getString("office_id"),
                                            item.getString("doc_full_name"),
                                            item.getString("full_address"),
                                            item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                            item.getString("borough"),
                                            item.getString("state"),
                                            item.getString("latitude"),
                                            item.getString("longitude"),
                                            item.getString("date"),
                                            item.getString("hour"),
                                            item.getString("picture")));
                                }
                                adapter.add(dates);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    params.put("limit",""+page);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Access-Token", session.profile().get(5).toString());
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }

    public void doCancelAppoinment (final String appoinment_id, final int position){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueCancel = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.cancel_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseCancel",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonObject.getString("response"))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                adapter.removeItem(position);
                                break;
                            default:

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("id_appointment", appoinment_id);
                    params.put("cause", "");
                    params.put("cancel_by", "patient");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueCancel.add(request);
        }  else {

        }
    }

    private boolean saveAppoinmentCalendar(String hourR, String fecha, String name, String address) {
        boolean ban = false;
        Log.v("printCalendar", hourR + "  " + fecha + "   " + name + "   " + address);
        if (true) {
            long calID = 2;
            Log.v("printCalID",""+calID);
            long startMillis = 0;
            long endMillis = 0;
            String [] separedDate = fecha.split("-");
            String [] separedHour = hourR.split(":");
            int year = Integer.parseInt(separedDate[0]);
            int month = Integer.parseInt(separedDate[1]);
            int day = Integer.parseInt(separedDate[2]);
            int hour = Integer.parseInt(separedHour[0]);
            int minute = Integer.parseInt(separedHour[1]);
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(year, (month-1), day, hour, minute);
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(year, (month-1), day, hour, minute+5);
            endMillis = endTime.getTimeInMillis();

            ContentResolver cr = boostrap.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.TITLE, "Cita al médico");
            values.put(CalendarContract.Events.DESCRIPTION, "Cita con el médico "+name+", a la(s) "+hourR+"hr(s), ubicado en "+address);
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Mexico_City");

            if (ActivityCompat.checkSelfPermission(boostrap, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(boostrap,"Se requiere de permisos",Toast.LENGTH_SHORT).show();
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            long eventID = Long.parseLong(uri.getLastPathSegment());
            Log.v("printEventIDCalendar",""+eventID);

            ContentValues reminders = new ContentValues();
            reminders.put(CalendarContract.Reminders.EVENT_ID, eventID);
            reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            reminders.put(CalendarContract.Reminders.MINUTES, 120);
            Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);
            ban = true;
        }

        Log.v("printFlag",String.valueOf(ban));
        return ban;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WR_PERMS){

        }
    }

    @Override
    public void onResumeFragment() throws JSONException {
        if (getContext() != null){ getAppointmentsApprove(); }
    }
}
