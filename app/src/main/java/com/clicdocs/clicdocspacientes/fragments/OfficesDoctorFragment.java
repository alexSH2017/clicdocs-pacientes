package com.clicdocs.clicdocspacientes.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterMoreOffice;
import com.clicdocs.clicdocspacientes.beans.ModelOffices;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OfficesDoctorFragment extends Fragment {

    private MainActivity boostrap;
    private RecyclerView mRecycler;
    private TextView TVdate;
    private Context ctx;
    private Calendar myCalendar;
    private SessionManager session;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat ndf = new SimpleDateFormat("dd MMM, yyyy");
    private RecyclerViewAdapterMoreOffice adapter;
    private int position, card = 0;
    private String idsession, name, specialities, picture, enable;
    private RequestQueue queueOffices;
    private View view;
    private String DATE_CURRENT = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.more_offices_doctor,container,false);
        boostrap    = (MainActivity) getActivity();
        ctx         = getContext();
        session     = new SessionManager(ctx);
        TVdate      = (TextView) view.findViewById(R.id.TVdate);
        mRecycler   = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        myCalendar  = Calendar.getInstance();
        TVdate.setText(sdf.format(myCalendar.getTime()).toString());
        Bundle data = getArguments();
        if(data != null) {
            idsession            = data.getString("idSession");
            name                 = data.getString("name");
            specialities         = data.getString("speciality");
            picture              = data.getString("picture");
            enable               = data.getString("enable");
            card                 = data.getInt("card");
        }
        getMoreOffices(sdf.format(myCalendar.getTime()));
        TVdate.setOnClickListener(TVdate_onClic);
    }

    private void getMoreOffices(final String date){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueOffices = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_offices, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRes = jsonObject.getJSONArray(Constants.response);
                                ArrayList<ModelOffices> offices = new ArrayList<>();

                                for (int i = 0; i < jsonRes.length(); i++){
                                    JSONObject item = jsonRes.getJSONObject(i);
                                    ModelOffices off = new ModelOffices();
                                    off.setOffice_id(item.getString("id"));
                                    off.setOffice(item.getString("office"));
                                    off.setFull_address(item.getString("full_address"));
                                    off.setClinic(item.getString("clinic"));
                                    off.setCountry(item.getString("country"));
                                    off.setState(item.getString("state"));
                                    off.setBorough(item.getString("borough"));
                                    off.setCity(item.getString("city"));
                                    off.setStreet(item.getString("street"));
                                    off.setNum_ext(item.getString("outdoor_number"));
                                    off.setNum_int(item.getString("interior_number"));
                                    off.setColony(item.getString("colony"));
                                    off.setZipcode(item.getString("zipcode"));
                                    off.setLatitude(item.getString("latitude"));
                                    off.setLongitude(item.getString("longitude"));
                                    off.setFirst_amount(item.getString("first_amount"));
                                    off.setLater_amount(item.getString("later_amount"));

                                    ArrayList<String> open_times = new ArrayList<String>();
                                    ArrayList<String> locked_times = new ArrayList<>();
                                    JSONObject jsonTime = item.getJSONObject("times");

                                    if (!String .valueOf(jsonTime.get(sdf.format(myCalendar.getTime()).toString())).equals("null")){
                                        JSONObject jsonDate = jsonTime.getJSONObject(sdf.format(myCalendar.getTime()).toString());
                                        JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                        JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                        for(int l = 0; l < jsonOpen.length(); l++) {
                                            open_times.add(jsonOpen.get(l).toString());
                                        }

                                        off.setOpen_time(open_times);
                                        for(int m = 0; m < jsonLocked.length(); m++) {
                                            locked_times.add(jsonLocked.get(m).toString());
                                        }
                                        off.setLocked_time(locked_times);
                                    }
                                    ArrayList<String> methods = new ArrayList<>();
                                    JSONObject jsonMethods = item.getJSONObject("pay_methods");
                                    methods.add(jsonMethods.getString("credit_card"));
                                    methods.add(jsonMethods.getString("debit_card"));
                                    methods.add(jsonMethods.getString("american_express"));
                                    off.setPay_methods(methods);

                                    offices.add(off);
                                }

                                if (card != 1){
                                    offices.remove(0);
                                }

                                adapter = new RecyclerViewAdapterMoreOffice(view, offices, ctx, boostrap, idsession, name, specialities, picture, enable, myCalendar.get(Calendar.DAY_OF_WEEK), sdf.format(myCalendar.getTime()).toString(), card, new RecyclerViewAdapterMoreOffice.Event() {
                                    @Override
                                    public void setDate(String date) throws ParseException {
                                        Date strDate = sdf.parse(date);
                                        myCalendar.setTime(strDate);
                                        DATE_CURRENT = sdf.format(myCalendar.getTime()).toString();
                                        TVdate.setText(ndf.format(myCalendar.getTime()));
                                    }
                                });
                                mRecycler.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.doctor_id, idsession);
                    params.put("date", date);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueOffices.add(request);
        }  else {

        }
    }

    private View.OnClickListener TVdate_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new DatePickerDialog(ctx, CPbirthday_onClick, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    private DatePickerDialog.OnDateSetListener CPbirthday_onClick = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDatePicked();
        }
    };

    private void setDatePicked() {
        sdf.setTimeZone(myCalendar.getTimeZone());
        TVdate.setText(ndf.format(myCalendar.getTime()));
        adapter.clear();
        getMoreOffices(sdf.format(myCalendar.getTime()).toString());
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.more_offices_p));
        setHasOptionsMenu(true);
    }
}
