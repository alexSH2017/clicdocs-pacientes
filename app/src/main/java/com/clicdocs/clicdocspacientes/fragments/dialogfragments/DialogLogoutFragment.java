package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DialogLogoutFragment extends DialogFragment {

    private TextView BTNlogout;
    private TextView BTNcancel;
    private MainActivity boostrap;
    private SessionManager session;
    private Context ctx;
    private TextView TVlegendlogout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       =   inflater.inflate(R.layout.dialog_logoutfragment, container, false);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        TVlegendlogout  = (TextView) view.findViewById(R.id.TVlegenlogout);
        BTNlogout       = (TextView) view.findViewById(R.id.BTNlogout);
        BTNcancel       = (TextView) view.findViewById(R.id.BTNcancel);
        getDialog().setTitle(boostrap.langStrings.get(Constants.logout_p));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        session = new SessionManager(ctx);
        getDialog().setCancelable(false);
        TVlegendlogout.setText(boostrap.langStrings.get(Constants.legend_logout_p));
        BTNlogout.setText(boostrap.langStrings.get(Constants.accept_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNlogout.setOnClickListener(BTNlogout_onClic);
        BTNcancel.setOnClickListener(BTNcancel_onClic);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private View.OnClickListener BTNlogout_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(NetworkUtils.haveNetworkConnection(ctx)) {
                /*session.deleteProfile();
                session.deleteChat();
                session.deleteTokenDevice();
                FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                LoginManager.getInstance().logOut();
                boostrap.hideElements ();
                boostrap.hideItem();
                DialogLogoutFragment.this.dismiss();
                boostrap.cleanFragments();
                boostrap.setFragment(fragments.LOGIN, null);*/
                setLogout();
            } else {
                AlertDialog success = new AlertDialog.Builder(ctx)
                        .setMessage(boostrap.langStrings.get(Constants.noconnection_p))
                        .setTitle(boostrap.langStrings.get(Constants.error_conecction_p))
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        }
    };

    private View.OnClickListener BTNcancel_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogLogoutFragment.this.dismiss();
        }
    };

    private void setLogout () {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            RequestQueue queue = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.delete_logout, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseLogout",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                session.deleteProfile();
                                session.deleteChat();
                                session.deleteTokenDevice();
                                FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                LoginManager.getInstance().logOut();
                                boostrap.hideElements ();
                                boostrap.hideItem();
                                DialogLogoutFragment.this.dismiss();
                                boostrap.cleanFragments();
                                boostrap.setFragment(fragments.LOGIN, null);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("device_token", session.getTokenDevice());
                    params.put("access_token", session.profile().get(5).toString());
                    Log.v("printParamsLogout",params.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}