package com.clicdocs.clicdocspacientes.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.TabsRecentsAdapter;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ResponseDoctorsFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ResponseMapFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ResponsePharmaciesFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.CustomViewPager;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;

public class ResponseSearchPagerFragment extends Fragment {

    private MainActivity boostrap;
    private CustomViewPager mViewPager;
    private Context ctx;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.viewpager_fragment, container, false);
        boostrap   = (MainActivity) getActivity();
        ctx        = getContext();
        mViewPager = (CustomViewPager) view.findViewById(R.id.mViewPager);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();


        boostrap.getSupportActionBar().show();
    }

    public void initView(){
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                final TabsRecentsAdapter adapter = new TabsRecentsAdapter(getChildFragmentManager());
                adapter.addFragment(new ResponseDoctorsFragment(), boostrap.langStrings.get(Constants.doctor_p));
                adapter.addFragment(new ResponsePharmaciesFragment(), boostrap.langStrings.get(Constants.pharmacies_p));
                mViewPager.setAdapter(adapter);
                Tabs.setupWithViewPager(mViewPager);
                Tabs.setVisibility(View.VISIBLE);
                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
                    @Override
                    public void onPageSelected(int position) {
                        FragmentLifecycle fragmentToShow = (FragmentLifecycle)adapter.getItem(position);
                        fragmentToShow.onResumeFragment();
                    }
                    @Override
                    public void onPageScrollStateChanged(int state) {}
                });


            }
        });

        boostrap.setTitle(Miscellaneous.ucFirst(boostrap.langStrings.get(Constants.results_p)));
    }

    public interface FragmentLifecycle {
        public void onResumeFragment();
    }
}
