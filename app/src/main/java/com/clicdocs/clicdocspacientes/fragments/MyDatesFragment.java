package com.clicdocs.clicdocspacientes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.TabLayout;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;

import com.clicdocs.clicdocspacientes.adapters.TabsRecentsAdapter;

import com.clicdocs.clicdocspacientes.fragments.pagerfragment.AppointmentToApproveFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ApprovedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.CancelAppointmentFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.FinishedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.NoAssistingFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.ProcessDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.RejectedDateFragment;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.WaitingDateFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.CustomViewPager;

import org.json.JSONException;

public class MyDatesFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private CustomViewPager mViewPager;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.viewpager_fragment, container, false);
        boostrap   = (MainActivity) getActivity();
        ctx        = getContext();
        mViewPager = (CustomViewPager) view.findViewById(R.id.mViewPager);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        setHasOptionsMenu(true);
        boostrap.getSupportActionBar().show();
    }

    public void initView(){
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                final TabsRecentsAdapter adapter = new TabsRecentsAdapter(getChildFragmentManager());
                adapter.addFragment(new AppointmentToApproveFragment(), boostrap.langStrings.get(Constants.pending_approval_p));
                adapter.addFragment(new WaitingDateFragment(), boostrap.langStrings.get(Constants.waiting_appointmnet_p));
                adapter.addFragment(new ProcessDateFragment(), boostrap.langStrings.get(Constants.process_appointment_p));
                adapter.addFragment(new ApprovedDateFragment(), boostrap.langStrings.get(Constants.approve_appointment_p));
                adapter.addFragment(new FinishedDateFragment(), boostrap.langStrings.get(Constants.finish_appointment_p));
                adapter.addFragment(new RejectedDateFragment(), boostrap.langStrings.get(Constants.reject_appointment_p));
                adapter.addFragment(new CancelAppointmentFragment(), boostrap.langStrings.get(Constants.cancel_appointment_p));
                adapter.addFragment(new NoAssistingFragment(), boostrap.langStrings.get(Constants.no_assiting_appointment_p));
                mViewPager.setAdapter(adapter);
                Tabs.setupWithViewPager(mViewPager);
                Tabs.setVisibility(View.VISIBLE);
                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

                    @Override
                    public void onPageSelected(int position) {
                        FragmentLifecycle fragmentToShow = (FragmentLifecycle) adapter.getItem(position);
                        try {
                            fragmentToShow.onResumeFragment();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) { }
                });
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.my_appointment_p));
    }

    public interface FragmentLifecycle {
        public void onResumeFragment() throws JSONException;
    }
}
