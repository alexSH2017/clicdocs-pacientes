package com.clicdocs.clicdocspacientes.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterPost;
import com.clicdocs.clicdocspacientes.beans.SubthemesBeans;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogLogin;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.Post;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubthemeFragment extends Fragment{

    private TextView TVlegend;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private TextView TVname, TVtimestamp, TVcomment;
    private CircleImageView CIVphoto;
    private ImageButton IBsend;
    private EditText ETpost;
    private View view;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecycler;
    private ProgressDialog loading;
    private AlertDialog DLnoconnection;
    final ArrayList<Post> posts =new ArrayList<Post>();
    private SubthemesBeans adapterS;
    RecyclerViewAdapterPost adapter = new RecyclerViewAdapterPost(posts,ctx);
    private RequestQueue queuePost, queueAddPost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_post,container,false);
        boostrap               =   (MainActivity) getActivity();
        ctx                    =   getContext();
        session                =   new SessionManager(ctx);
        CIVphoto               = (CircleImageView) view.findViewById(R.id.CIVprofile);
        TVname                 = (TextView) view.findViewById(R.id.TVname);
        TVtimestamp            = (TextView) view.findViewById(R.id.TVtimestamp);
        TVcomment              = (TextView) view.findViewById(R.id.TVcomment);
        ETpost                 =   (EditText) view.findViewById(R.id.ETpost);
        IBsend                 =   (ImageButton) view.findViewById(R.id.IBsend);
        mRecycler              = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mSwipeRefreshLayout    = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue, R.color.yellow);
        mRecycler.setNestedScrollingEnabled(false);
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle data = getArguments();
        setHasOptionsMenu(true);
        if(data != null) {
            adapterS = (SubthemesBeans) data.getSerializable("subtheme");
            boostrap.setTitle(adapterS.getSubtheme());
            if (!adapterS.getPicture().isEmpty()){
                Picasso.with(ctx).load(adapterS.getPicture())
                        .noFade()
                        .resize(80, 80)
                        .into(CIVphoto);
            }
            //TVname.setText((!adapterS.getNickname().isEmpty())?adapterS.getNickname() : adapterS.getFull_name());
            TVname.setText(adapterS.getNickname());
            TVtimestamp.setText(adapterS.getTimestamp());
            TVcomment.setText(adapterS.getContent());
            try {
                getPost();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ETpost.setHint(boostrap.langStrings.get(Constants.legend_post_p));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RecyclerViewAdapterPost post = new RecyclerViewAdapterPost(posts,ctx);
                post.updateData(posts);
                try {
                    getPost();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        ETpost.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                Validator validator = new Validator();
                if(!validator.validate(ETpost, new String[] {validator.REQUIRED})){
                    addPost();
                }
                InputMethodManager inputManager = (InputMethodManager)boostrap.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ETpost.getWindowToken(), 0);
                return true;
            }
        });
        IBsend.setOnClickListener(IBsend_onClic);
    }

    private View.OnClickListener IBsend_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Validator validator = new Validator();
            if(!validator.validate(ETpost, new String[] {validator.REQUIRED})){
                InputMethodManager inputManager = (InputMethodManager)boostrap.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ETpost.getWindowToken(), 0);
                addPost();
            }
        }
    };

    private void addPost (){
        Validator validator = new Validator();
        if (session.isLogged() != null){
            if(!validator.validate(ETpost, new String[] {validator.REQUIRED})) {
                if(NetworkUtils.haveNetworkConnection(ctx)) {
                    final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                    loading.show();
                    queueAddPost = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.register_post, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            loading.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.v("printLogin",""+response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        //adapter = new RecyclerViewAdapterPost(null,ctx);
                                        //adapter.addItem(new Post("Alejandro Isaias","",ETpost.getText().toString(),"",session.profile().get(4).toString()));
                                        ETpost.setText("");
                                        adapter.clear();
                                        getPost();
                                        break;
                                    case 403:
                                        Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                .show();
                                        break;
                                    case 404:
                                        Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                .show();
                                        break;
                                    default:
                                        Snackbar.make(view, "Error al iniciar sesión", Snackbar.LENGTH_LONG)
                                                .show();
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("topic_id", adapterS.getTopic_id());
                            params.put("patient_id", session.profile().get(0).toString());
                            params.put("post", ETpost.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                            headers.put(Constants.key_access_token, session.profile().get(5).toString());
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueAddPost.add(request);
                }  else {
                    Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        } else {
            DialogLogin login = new DialogLogin();
            login.show(getChildFragmentManager(),fragments.LOGINDIALOG);
        }
    }

    private void getPost () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("status","1");
        jsonObject.put("topic_id",adapterS.getTopic_id());

        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queuePost = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_post, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonRes = jsonObject.getJSONObject(Constants.response);
                                JSONArray rows = jsonRes.getJSONArray("rows");
                                mRecycler.setLayoutManager(new LinearLayoutManager(ctx));

                                for (int i=0;i<rows.length();i++){
                                    JSONObject item = rows.getJSONObject(i);
                                    posts.add(new Post(item.getString("patient_full_name"),
                                                       item.getString("nickname"),
                                                       item.getString("post"),
                                                       item.getString("timestamp"),
                                                       item.getString("picture")));
                                }
                                adapter = new RecyclerViewAdapterPost(posts,ctx);
                                mRecycler.setAdapter(adapter);

                                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                                    @Override
                                    public void onItemRangeInserted(int positionStart, int itemCount) {
                                        super.onItemRangeInserted(positionStart, itemCount);
                                        setScroll();
                                    }
                                });
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queuePost.add(request);
        }  else {

        }
    }


    private void setScroll(){
        mRecycler.scrollToPosition(adapter.getItemCount()-1);
    }
}
