package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class IdentificationFileFragment extends Fragment implements ClinicHistoryPagerFragment.FragmentLifecycle{

    private ProgressDialog loading;
    private EditText  ETorigin, ETlive, ETcolony, ETstreet, ETnumExt, ETnumInt, ETzipCode, ETocupation, ETreligion, ETschool;
    private TextView TVidentification, TVcivilstatus, TVorigin, TVlive, TVcolony, TVaddress, TVstreet, TVnumext, TVnumint, TVzipcode, TVoccupation, TVreligion, TVscholarship;
    private SessionManager session;
    private Spinner SPmarital;
    private MainActivity boostrap;
    private Context ctx;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RequestQueue queueIdentification, queueSave;
    private View view;
    private ClinicHistoryPagerFragment parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.identification_file_fragment,container,false);
        ctx = getContext();
        boostrap  = (MainActivity) getActivity();
        session = new SessionManager(ctx);
        parent  = (ClinicHistoryPagerFragment) getParentFragment();
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue);
        SPmarital       = (Spinner) view.findViewById(R.id.SPmarital);
        ETorigin        = (EditText) view.findViewById(R.id.ETorigin);
        ETlive          = (EditText) view.findViewById(R.id.ETlive);
        ETcolony        = (EditText) view.findViewById(R.id.ETcolony);
        ETstreet        = (EditText) view.findViewById(R.id.ETstreet);
        ETnumExt        = (EditText) view.findViewById(R.id.ETnumExt);
        ETnumInt        = (EditText) view.findViewById(R.id.ETnumInt);
        ETzipCode       = (EditText) view.findViewById(R.id.ETzipCode);
        ETocupation     = (EditText) view.findViewById(R.id.ETocupation);
        ETreligion      = (EditText) view.findViewById(R.id.ETreligion);
        ETschool        = (EditText) view.findViewById(R.id.ETschool);
        TVidentification= (TextView) view.findViewById(R.id.TVidentification);
        TVaddress       = (TextView) view.findViewById(R.id.TVaddress);
        TVcivilstatus   = (TextView) view.findViewById(R.id.TVcivilstatus);
        TVorigin        = (TextView) view.findViewById(R.id.TVorigin);
        TVlive          = (TextView) view.findViewById(R.id.TVlive);
        TVcolony        = (TextView) view.findViewById(R.id.TVcolony);
        TVstreet        = (TextView) view.findViewById(R.id.TVstreet);
        TVnumext        = (TextView) view.findViewById(R.id.TVnumext);
        TVnumint        = (TextView) view.findViewById(R.id.TVnumint);
        TVzipcode       = (TextView) view.findViewById(R.id.TVzipcode);
        TVoccupation    = (TextView) view.findViewById(R.id.TVoccupation);
        TVreligion      = (TextView) view.findViewById(R.id.TVreligion);
        TVscholarship   = (TextView) view.findViewById(R.id.TVscholarship);
        ArrayList<KeyPairsBean> marital = new ArrayList<KeyPairsBean>();
        marital.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.married_p)));
        marital.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.single_p)));
        marital.add(new KeyPairsBean(3,boostrap.langStrings.get(Constants.divorced_p)));
        marital.add(new KeyPairsBean(4,boostrap.langStrings.get(Constants.widower_p)));
        ArrayAdapter<KeyPairsBean> hereditary = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, marital);
        SPmarital.setAdapter(hereditary);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                getIdentificationFile();
            }
        });
        TVidentification.setText(boostrap.langStrings.get(Constants.identification_file_p));
        TVaddress.setText(boostrap.langStrings.get("direction").toUpperCase());
        TVcivilstatus.setText(boostrap.langStrings.get(Constants.civil_status_p));
        TVorigin.setText(boostrap.langStrings.get(Constants.origin_p));
        TVlive.setText(boostrap.langStrings.get(Constants.live_p));
        //TVorigin.setText(boostrap.langStrings.getOrigin());
        //TVlive.setText(boostrap.langStrings.getLive());
        TVcolony.setText(boostrap.langStrings.get(Constants.colony_p));
        TVstreet.setText(boostrap.langStrings.get(Constants.street_p));
        TVnumext.setText(boostrap.langStrings.get(Constants.num_ext_p));
        TVnumint.setText(boostrap.langStrings.get(Constants.num_int_p));
        TVzipcode.setText(boostrap.langStrings.get(Constants.zipcode_p));
        TVoccupation.setText(boostrap.langStrings.get(Constants.ocupation_p));
        TVreligion.setText(boostrap.langStrings.get(Constants.religion_p));
        TVscholarship.setText(boostrap.langStrings.get(Constants.scholarship_p));

        ETorigin.setHint(boostrap.langStrings.get(Constants.origin_p));
        ETlive.setHint(boostrap.langStrings.get(Constants.live_p));
        ETcolony.setHint(boostrap.langStrings.get(Constants.colony_p));
        ETstreet.setHint(boostrap.langStrings.get(Constants.street_p));
        ETnumExt.setHint(boostrap.langStrings.get(Constants.num_ext_p));
        ETnumInt.setHint(boostrap.langStrings.get(Constants.num_int_p));
        ETzipCode.setHint(boostrap.langStrings.get(Constants.zipcode_p));
        ETocupation.setHint(boostrap.langStrings.get(Constants.ocupation_p));
        ETreligion.setHint(boostrap.langStrings.get(Constants.religion_p));
        ETschool.setHint(boostrap.langStrings.get(Constants.scholarship_p));

        setHasOptionsMenu(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getIdentificationFile();
            }
        });

    }

    private void validateIdentificationfile (){
        doRegisterIdentificationFile();
        /*Validator validator = new Validator();
        if(!validator.validate(ETorigin, new String[] {validator.REQUIRED,validator.ONLY_TEXT}) &&
                !validator.validate(ETlive, new String[] {validator.REQUIRED,validator.ONLY_TEXT}) &&
                !validator.validate(ETcolony, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETstreet, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETnumExt, new String[] {validator.REQUIRED, validator.NUMBER_HOME}) &&
                !validator.validate(ETzipCode, new String[] {validator.REQUIRED, validator.ONLY_NUMBER}) &&
                !validator.validate(ETocupation, new String[] {validator.REQUIRED, validator.ONLY_TEXT}) &&
                !validator.validate(ETreligion, new String[] {validator.REQUIRED, validator.ONLY_TEXT}) &&
                !validator.validate(ETschool, new String[] {validator.REQUIRED})) {
            doRegisterIdentificationFile();
            }*/
    }

    private void doRegisterIdentificationFile (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_identification, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printIdentification",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.msg_identification_file_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;

                            case 400:
                                JSONObject jsonResp = jsonObject.getJSONObject(Constants.response);
                                Iterator<String> keys = jsonResp.keys();
                                boolean flag = true;

                                while (flag){
                                    if (keys.hasNext()){
                                        String key = keys.next();
                                        if (key.equals("occupation")){
                                            ETocupation.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("origin")) {
                                            ETorigin.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("residence")){
                                            ETlive.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("street")){
                                            ETstreet.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("colony")){
                                            ETcolony.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("outdoor_number")){
                                            ETnumExt.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("zipcode")){
                                            ETzipCode.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("interior_number")){
                                            ETnumInt.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("religion")){
                                            ETreligion.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        } else if (key.equals("scholarship")){
                                            ETschool.setError(boostrap.langStrings.get(Constants.requier_field_p));
                                        }
                                    } else {
                                        flag = false;
                                    }

                                }
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    params.put("marital_status", String.valueOf(SPmarital.getSelectedItemPosition()+1));
                    params.put("occupation", ETocupation.getText().toString());
                    params.put("origin", ETorigin.getText().toString());
                    params.put("residence", ETlive.getText().toString());
                    params.put("religion", ETreligion.getText().toString());
                    params.put("scholarship", ETschool.getText().toString());
                    params.put("street", ETstreet.getText().toString());
                    params.put("colony", ETcolony.getText().toString());
                    params.put("outdoor_number", ETnumExt.getText().toString());
                    params.put("interior_number", ETnumInt.getText().toString());
                    params.put("zipcode", ETzipCode.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.key_access_token,session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    //=========== Obtencion de ficha de identificación =============================
    private void getIdentificationFile () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueIdentification = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_identification, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonIdentification= jsonObject.getJSONArray("response");
                                JSONObject item = jsonIdentification.getJSONObject(0);
                                JSONArray jsonAddress = item.getJSONArray("address");
                                JSONObject itemAddress = jsonAddress.getJSONObject(0);
                                SPmarital.setSelection(Miscellaneous.isInteger(item.getString("marital_status"))-1);
                                ETorigin.setText(Miscellaneous.validate(item.getString("origin")));
                                ETlive.setText(Miscellaneous.validate(item.getString("residence")));
                                ETocupation.setText(Miscellaneous.validate(item.getString("occupation")));
                                ETschool.setText(Miscellaneous.validate(item.getString("scholarship")));
                                ETreligion.setText(Miscellaneous.validate(item.getString("religion")));
                                ETcolony.setText(Miscellaneous.validate(itemAddress.getString("colony")));
                                ETnumExt.setText(Miscellaneous.validate(itemAddress.getString("outdoor_number")));
                                ETnumInt.setText(Miscellaneous.validate(itemAddress.getString("interior_number")));
                                ETstreet.setText(Miscellaneous.validate(itemAddress.getString("street")));
                                ETzipCode.setText(Miscellaneous.validate(itemAddress.getString("zipcode")));

                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.key_access_token,session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueIdentification.add(request);
        }  else {

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.save_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                validateIdentificationfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResumeFragment() {
        if (getContext() != null){ getIdentificationFile(); }
    }
}
