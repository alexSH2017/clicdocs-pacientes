package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.fragments.ClinicHistoryPagerFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HereditaryFamilyHistoryFragment extends Fragment implements ClinicHistoryPagerFragment.FragmentLifecycle {

    private MainActivity boostrap;
    private Context ctx;
    private ProgressDialog loading;
    private SessionManager session;
    private TextView TVdiseases, TVdiabetes, TVhepatopatia, TVasma, TVendocrinas, TVhipertension, TVnefropatia, TVcancer, TVcardiopatia, TVmentales, TValergicas, TVintneg, TVotros;
    private EditText ETintNeg, ETotros;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Spinner SPdiabetes, SPhepatopatia, SPasma, SPendocrinas, SPhipertension, SPnefropatia, SPcancer, SPcardiopatia, SPmentales, SPalergicas;
    private RequestQueue queueSave,queueHereditary;
    private View view;
    private ClinicHistoryPagerFragment parent;
    public static int MILISEGUNDOS_ESPERA = 5000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.hereditary_family_fragment,container,false);
        boostrap = (MainActivity) getActivity();
        ctx = getContext();
        session = new SessionManager(ctx);
        parent  = (ClinicHistoryPagerFragment) getParentFragment();
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue);
        TVdiseases          = (TextView) view.findViewById(R.id.TVdiseases);
        TVdiabetes          = (TextView) view.findViewById(R.id.TVdiabetes);
        TVhepatopatia       = (TextView) view.findViewById(R.id.TVhepatopatia);
        TVasma              = (TextView) view.findViewById(R.id.TVasma);
        TVendocrinas        = (TextView) view.findViewById(R.id.TVendocrinas);
        TVhipertension      = (TextView) view.findViewById(R.id.TVhipertension);
        TVnefropatia        = (TextView) view.findViewById(R.id.TVnefropatia);
        TVcancer            = (TextView) view.findViewById(R.id.TVcancer);
        TVcardiopatia       = (TextView) view.findViewById(R.id.TVcardiopatia);
        TVmentales          = (TextView) view.findViewById(R.id.TVmentales);
        TValergicas         = (TextView) view.findViewById(R.id.TValergicas);
        TValergicas         = (TextView) view.findViewById(R.id.TValergicas);
        TVintneg            = (TextView) view.findViewById(R.id.TVintneg);
        TVotros             = (TextView) view.findViewById(R.id.TVotros);

        SPdiabetes          = (Spinner) view.findViewById(R.id.SPdiabetes);
        SPhepatopatia       = (Spinner) view.findViewById(R.id.SPhepatopatia);
        SPasma              = (Spinner) view.findViewById(R.id.SPasma);
        SPnefropatia        = (Spinner) view.findViewById(R.id.SPnefropatia);
        SPendocrinas        = (Spinner) view.findViewById(R.id.SPendocrinas);
        SPhipertension      = (Spinner) view.findViewById(R.id.SPhipertension);
        SPnefropatia        = (Spinner) view.findViewById(R.id.SPnefropatia);
        SPcancer            = (Spinner) view.findViewById(R.id.SPcancer);
        SPcardiopatia       = (Spinner) view.findViewById(R.id.SPcardiopatia);
        SPmentales          = (Spinner) view.findViewById(R.id.SPmentales);
        SPalergicas         = (Spinner) view.findViewById(R.id.SPalergicas);
        ETintNeg            = (EditText) view.findViewById(R.id.ETintNeg);
        ETotros             = (EditText) view.findViewById(R.id.ETotros);

        ArrayList<KeyPairsBean> adapter = new ArrayList<>();
        adapter.add(new KeyPairsBean(0,boostrap.langStrings.get(Constants.any)));
        adapter.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.father_p)));
        adapter.add(new KeyPairsBean(2,boostrap.langStrings.get(Constants.mother_p)));
        adapter.add(new KeyPairsBean(3,boostrap.langStrings.get(Constants.gfather_father_p)));
        adapter.add(new KeyPairsBean(4,boostrap.langStrings.get(Constants.gmother_father_p)));
        adapter.add(new KeyPairsBean(5,boostrap.langStrings.get(Constants.gfather_mother_p)));
        adapter.add(new KeyPairsBean(6,boostrap.langStrings.get(Constants.gmother_mother_p)));
        ArrayAdapter<KeyPairsBean> hereditary = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, adapter);
        SPdiabetes.setAdapter(hereditary);
        SPhepatopatia.setAdapter(hereditary);
        SPasma.setAdapter(hereditary);
        SPnefropatia.setAdapter(hereditary);
        SPendocrinas.setAdapter(hereditary);
        SPhipertension.setAdapter(hereditary);
        SPnefropatia.setAdapter(hereditary);
        SPcancer.setAdapter(hereditary);
        SPcardiopatia.setAdapter(hereditary);
        SPmentales.setAdapter(hereditary);
        SPalergicas.setAdapter(hereditary);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                getHereditaryFamily();
            }
        });

        TVdiseases.setText(boostrap.langStrings.get(Constants.suffering_p));
        TVdiabetes.setText(boostrap.langStrings.get(Constants.diabetes_p));
        TVhepatopatia.setText(boostrap.langStrings.get(Constants.liver_disease_p));
        TVasma.setText(boostrap.langStrings.get(Constants.asthma_p));
        TVendocrinas.setText(boostrap.langStrings.get(Constants.endocrine_p));
        TVhipertension.setText(boostrap.langStrings.get(Constants.hypertension_p));
        TVnefropatia.setText(boostrap.langStrings.get(Constants.nephropathy_p));
        TVcancer.setText(boostrap.langStrings.get(Constants.cancer_p));
        TVcardiopatia.setText(boostrap.langStrings.get(Constants.heart_diasease_p));
        TVmentales.setText(boostrap.langStrings.get(Constants.mental_disease_p));
        TValergicas.setText(boostrap.langStrings.get(Constants.allergic_p));
        TVintneg.setText(boostrap.langStrings.get(Constants.asked_denied_p));
        TVotros.setText(boostrap.langStrings.get(Constants.othres_p));

        ETintNeg.setHint(boostrap.langStrings.get(Constants.asked_denied_p));
        ETotros.setHint(boostrap.langStrings.get(Constants.othres_p));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHereditaryFamily();
            }
        });
    }

    //========================= METODO PARA GUARDAR ANTECEDENTES HEREDO FAMILIARES==========================
    /*private void ValidateHereditaryFamily (){
        Validator validator = new Validator();
        if(!validator.validate(ETdiabetes, new String[] {validator.REQUIRED}) &&
                !validator.validate(EThepatopatia, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETasma, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETendocrinas, new String[] {validator.REQUIRED}) &&
                !validator.validate(EThipertension, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETnefropatia, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETcancer, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETcardiopatia, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETmentales, new String[] {validator.REQUIRED}) &&
                !validator.validate(ETalergicas, new String[] {validator.REQUIRED})) {
            setHereditaryFamily();
        }
    }*/

    private void setHereditaryFamily (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSave = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_hereditary, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.msg_hereditary_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    params.put("diabetes", String.valueOf(SPdiabetes.getSelectedItemPosition()));
                    params.put("hepatopatia", String.valueOf(SPhepatopatia.getSelectedItemPosition()));
                    params.put("asma", String.valueOf(SPasma.getSelectedItemPosition()));
                    params.put("endocrinas", String.valueOf(SPendocrinas.getSelectedItemPosition()));
                    params.put("hipertension", String.valueOf(SPhipertension.getSelectedItemPosition()));
                    params.put("nefropatia", String.valueOf(SPnefropatia.getSelectedItemPosition()));
                    params.put("cancer", String.valueOf(SPcancer.getSelectedItemPosition()));
                    params.put("cardiopatia", String.valueOf(SPcardiopatia.getSelectedItemPosition()));
                    params.put("mentales", String.valueOf(SPmentales.getSelectedItemPosition()));
                    params.put("alergicas", String.valueOf(SPalergicas.getSelectedItemPosition()));
                    params.put("negados", ETintNeg.getText().toString());
                    params.put("otros", ETotros.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueSave.add(request);
        }  else {

        }
    }

    private void getHereditaryFamily () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queueHereditary = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_hereditary, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    //loading.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject json = jsonObject.getJSONObject("response");
                                SPdiabetes.setSelection(Miscellaneous.isInteger(json.getString("diabetes")));
                                SPhepatopatia.setSelection(Miscellaneous.isInteger(json.getString("hepatopatia")));
                                SPasma.setSelection(Miscellaneous.isInteger(json.getString("asma")));
                                SPendocrinas.setSelection(Miscellaneous.isInteger(json.getString("endocrinas")));
                                SPhipertension.setSelection(Miscellaneous.isInteger(json.getString("hipertension")));
                                SPnefropatia.setSelection(Miscellaneous.isInteger(json.getString("nefropatia")));
                                SPcancer.setSelection(Miscellaneous.isInteger(json.getString("cancer")));
                                SPcardiopatia.setSelection(Miscellaneous.isInteger(json.getString("cardiopatia")));
                                SPmentales.setSelection(Miscellaneous.isInteger(json.getString("mentales")));
                                //SPalergicas.setSelection(1);
                                SPalergicas.setSelection(Miscellaneous.isInteger(json.getString("alergias")));

                                ETintNeg.setText(Miscellaneous.validate(json.getString("negados")));
                                ETotros.setText(Miscellaneous.validate(json.getString("otros")));

                                break;
                            case 202:
                                Snackbar.make(parent.getView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 400:
                                Snackbar.make(parent.getView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(parent.getView(), jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("iduser", session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueHereditary.add(request);
        }  else {

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.save_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                setHereditaryFamily ();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResumeFragment() {
        if (getContext() != null){ getHereditaryFamily(); }

    }
}
