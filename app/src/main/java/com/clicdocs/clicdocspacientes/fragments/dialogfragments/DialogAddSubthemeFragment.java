package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.fragments.SubthemesFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DialogAddSubthemeFragment extends DialogFragment {

    private EditText ETtopic;
    private MultiAutoCompleteTextView METdescription;
    private TextView TVtheme, TVtitle, TVpublication;
    private Button BTNaddSubtheme;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private AlertDialog DLnoconnection;
    private ProgressDialog loading;
    private RequestQueue queueAddSubtheme;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_addsubtheme,container,false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        boostrap        = (MainActivity)  getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        TVtheme         = (TextView) view.findViewById(R.id.TVtheme);
        TVtitle         = (TextView) view.findViewById(R.id.TVtitle);
        TVpublication   = (TextView) view.findViewById(R.id.TVpublication);
        ETtopic         = (EditText) view.findViewById(R.id.ETtopic);
        BTNaddSubtheme  = (Button) view.findViewById(R.id.btnAddSubtheme);
        METdescription  = (MultiAutoCompleteTextView) view.findViewById(R.id.METdescription);
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVtheme.setText(boostrap.langStrings.get(Constants.add_subtheme_p));
        TVtitle.setText(boostrap.langStrings.get(Constants.title_p));
        TVpublication.setText(boostrap.langStrings.get(Constants.publication_p));
        METdescription.setHint(boostrap.langStrings.get(Constants.description_p));
        BTNaddSubtheme.setText(boostrap.langStrings.get(Constants.add_subtheme_p));
        ETtopic.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                //Validator validator = new Validator();
                if(!boostrap.validator.validate(ETtopic, new String[] {boostrap.validator.REQUIRED}) &&
                        !boostrap.validator.validate(METdescription, new String[] {boostrap.validator.REQUIRED})){
                    sendSubtheme();
                }
                InputMethodManager inputManager = (InputMethodManager)boostrap.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ETtopic.getWindowToken(), 0);
                return true;
            }
        });
        BTNaddSubtheme.setOnClickListener(BTNaddSubtheme_onClic);
    }

    private View.OnClickListener BTNaddSubtheme_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Validator validator = new Validator();
            if(!boostrap.validator.validate(ETtopic, new String[] {boostrap.validator.REQUIRED}) &&
                    !boostrap.validator.validate(METdescription, new String[] {boostrap.validator.REQUIRED})){
               sendSubtheme();
            }
        }
    };

    private void sendSubtheme (){
        //Validator validator = new Validator();
        if (session.isLogged() != null){
            if(!boostrap.validator.validate(ETtopic, new String[] {boostrap.validator.REQUIRED})) {
                if (NetworkUtils.haveNetworkConnection(ctx)) {
                    final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                    loading.show();
                    queueAddSubtheme = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.add_subtheme, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            loading.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.v("printLogin", "" + response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        DialogAddSubthemeFragment.this.dismiss();

                                        AlertDialog newsubtopic = new AlertDialog.Builder(ctx)
                                                .setMessage(boostrap.langStrings.get(Constants.new_subtheme_add_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        SubthemesFragment sub = (SubthemesFragment) boostrap.getSupportFragmentManager().findFragmentById(R.id.FLmain);
                                                        try {
                                                            sub.GetSubthemes();
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();
                                        break;
                                    case 403:
                                        Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                .show();
                                        break;
                                    case 404:
                                        Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                .show();
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("forum_id", getArguments().getString("idTheme"));
                            params.put("patient_id", session.profile().get(0).toString());
                            params.put("subtheme", ETtopic.getText().toString());
                            params.put("content", METdescription.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                            headers.put(Constants.key_access_token, session.profile().get(5).toString());
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueAddSubtheme.add(request);
                } else {
                    Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        }
    }
}
