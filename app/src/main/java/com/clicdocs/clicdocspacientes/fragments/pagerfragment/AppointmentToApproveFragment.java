package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.LocationOfficeActivity;
import com.clicdocs.clicdocspacientes.adapters.RecyclerAppointmentToBeApprove;
import com.clicdocs.clicdocspacientes.fragments.MyDatesFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apc_g on 14/11/2017.
 */

public class AppointmentToApproveFragment extends Fragment implements MyDatesFragment.FragmentLifecycle{

    private TextView TVlegend, TVinformation;
    private View view;
    private MainActivity boostrap;
    private Context ctx;
    private RecyclerAppointmentToBeApprove adapter;
    private SessionManager session;
    private RequestQueue queueAppointment;
    private RecyclerView myRecycler;

    private MyDatesFragment parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_appointment, container, false);
        boostrap        =  (MainActivity) getActivity();
        parent          =  (MyDatesFragment) getParentFragment();
        ctx             =  getContext();
        session         =  new SessionManager(ctx);
        TVlegend        = (TextView) view.findViewById(R.id.TVlegend);
        TVinformation   = (TextView) view.findViewById(R.id.TVinformation);
        myRecycler= (RecyclerView) view.findViewById(R.id.mRecyclerView);
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));

        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVinformation.setText(boostrap.langStrings.get(Constants.legend_to_approve_appo_p));
        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                try {
                    getAppointmentsApprove();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getAppointmentsApprove ()throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAprove",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                myRecycler.setVisibility(View.VISIBLE);
                                TVlegend.setVisibility(View.GONE);
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                            item.getString("doctor_id"),
                                            item.getString("office_id"),
                                            item.getString("doc_full_name"),
                                            item.getString("consulting_room"),
                                            item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                            item.getString("borough"),
                                            item.getString("state"),
                                            item.getString("latitude"),
                                            item.getString("longitude"),
                                            item.getString("date"),
                                            item.getString("hour"),
                                            item.getString("picture")));
                                }
                                adapter = new RecyclerAppointmentToBeApprove(boostrap,dates, ctx, new RecyclerAppointmentToBeApprove.Event() {
                                    @Override
                                    public void onClicLocation(Dates item) {
                                        Intent edit = new Intent(ctx, LocationOfficeActivity.class);
                                        edit.putExtra("strings",boostrap.langStrings);
                                        edit.putExtra("appointment", (Serializable) item);
                                        startActivityForResult(edit, 123);
                                    }

                                    @Override
                                    public void onPager(int page) {

                                    }
                                });
                                myRecycler.setAdapter(adapter);
                                break;
                            case 204:
                                myRecycler.setVisibility(View.GONE);
                                TVlegend.setText(boostrap.langStrings.get(Constants.no_result_p));
                                TVlegend.setVisibility(View.VISIBLE);
                                break;
                            case 404:
                                myRecycler.setVisibility(View.GONE);
                                TVlegend.setText(boostrap.langStrings.get(Constants.no_result_p));
                                TVlegend.setVisibility(View.VISIBLE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }

    private void pagerAppointment (final int page) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status","1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAprove",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                final ArrayList<Dates> dates =new ArrayList<Dates>();
                                JSONObject jsonApprove = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonApprove.getJSONArray("rows");
                                for (int i = 0; i<jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    dates.add(new Dates(item.getString("appointment_id"),
                                            item.getString("doctor_id"),
                                            item.getString("office_id"),
                                            item.getString("doc_full_name"),
                                            item.getString("consulting_room"),
                                            item.getString("street")+" #"+item.getString("outdoor_number")+", "+item.getString("colony"),
                                            item.getString("borough"),
                                            item.getString("state"),
                                            item.getString("latitude"),
                                            item.getString("longitude"),
                                            item.getString("date"),
                                            item.getString("hour"),
                                            item.getString("picture")));
                                }
                                adapter.add(dates);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    params.put("limit", ""+page);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }


    @Override
    public void onResumeFragment() throws JSONException {
        if (getContext() != null){ getAppointmentsApprove(); }
    }
}
