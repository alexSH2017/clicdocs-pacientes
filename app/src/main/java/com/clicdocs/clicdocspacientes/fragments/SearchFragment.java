package com.clicdocs.clicdocspacientes.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.CatalogSpecialitiesActivity;
import com.clicdocs.clicdocspacientes.activity.StatesDoctorActivity;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogLogin;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.MyCurrentListener;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PharmacyFind;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;

import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


public class SearchFragment extends Fragment {

    private final int WR_PERMS = 3453;
    private final int REQUEST_PERMISSION_CALL = 12312;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private Button BTNsearch;
    private LinearLayout LLregis_log;
    private EditText ETsearch, ETspeciality, ETstate;
    private TextView TVpillbox, TVfavoritedoctors, TVsos, TVslogan, TVcreateAccount, TVlogin;
    private Switch SWlocation;
    private ImageView IMGfamily,IBpillbox, IBfavorite, IBsos;
    private TextInputLayout TILhint;
    private RequestQueue queueSearch, queueWaitingRate, queueScore;
    private View view_parent;
    private Calendar myCalendar;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private HashMap<String, Object> hashMap = new HashMap<>();
    private final static int SELECT_SPECIALITY = 321;
    private final static int SELECT_STATE = 123;
    private String lati;
    private String longi;
    //==========================================================
    LocationManager locationManager;
    AlertDialog alert = null;
    private final static int LOCATION = 312;
    AlertDialog loadingSearch;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view_parent         = inflater.inflate(R.layout.search_fragment, container, false);
        boostrap            = (MainActivity) getActivity();
        ctx                 = getContext();
        session             = new SessionManager(ctx);
        TVpillbox           = (TextView) view_parent.findViewById(R.id.TVpillbox);
        TVslogan            = (TextView) view_parent.findViewById(R.id.TVlegendslogan);
        TVfavoritedoctors   = (TextView) view_parent.findViewById(R.id.TVfavoritedoctors);
        TILhint             = (TextInputLayout) view_parent.findViewById(R.id.TILhint);
        ETsearch            = (EditText) view_parent.findViewById(R.id.ETsearch);
        ETstate             = (EditText) view_parent.findViewById(R.id.ETstate);
        ETspeciality        = (EditText) view_parent.findViewById(R.id.ETspeciality);
        SWlocation          = (Switch) view_parent.findViewById(R.id.SWlocation);
        BTNsearch           = (Button) view_parent.findViewById(R.id.BTNsearch);
        IBpillbox           = (ImageView) view_parent.findViewById(R.id.IBpillbox);
        IBfavorite          = (ImageView) view_parent.findViewById(R.id.IBfavorite);
        IBsos               = (ImageView) view_parent.findViewById(R.id.IBsos);
        TVcreateAccount     = (TextView) view_parent.findViewById(R.id.TVcreateAccount);
        LLregis_log         = (LinearLayout) view_parent.findViewById(R.id.LLregis_log);
        TVlogin             = (TextView) view_parent.findViewById(R.id.TVlogin);
        IMGfamily           = (ImageView) view_parent.findViewById(R.id.IMGfamily);
        myCalendar          = Calendar.getInstance();
        //BTNsearch.setEnabled(false);
        boostrap.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        boostrap.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ETsearch.clearFocus();
        loadingSearch = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));

        return view_parent;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

        TVslogan.setText(boostrap.langStrings.get(Constants.legend_admon_p));
        //TVlegendSearch.setHint(boostrap.langStrings.getLegenSearch());
        BTNsearch.setText(boostrap.langStrings.get(Constants.search_p));
        ETsearch.setHint(boostrap.langStrings.get(Constants.med_and_phar_p));
        SWlocation.setText(boostrap.langStrings.get(Constants.next_to_me_p));
        ETspeciality.setHint(boostrap.langStrings.get(Constants.speciality_p));
        ETstate.setHint(boostrap.langStrings.get(Constants.state_p));
        TVpillbox.setText(boostrap.langStrings.get(Constants.pillbox_p));
        TVfavoritedoctors.setText(boostrap.langStrings.get(Constants.my_doctor_p));
        TVlogin.setText(boostrap.langStrings.get(Constants.s_login_p));
        TVcreateAccount.setText(boostrap.langStrings.get(Constants.sing_up_p));
        BTNsearch.setOnClickListener(BTNsearch_onClic);
        IBpillbox.setOnClickListener(IBpillbox_onClic);
        IBfavorite.setOnClickListener(IBfavorite_onClic);
        IBsos.setOnClickListener(IBsos_onClic);
        TVcreateAccount.setOnClickListener(TVcreateAccount_onClic);
        TVlogin.setOnClickListener(TVlogin_onClic);
        ETspeciality.setOnClickListener(ETspeciality_onClic);
        ETstate.setOnClickListener(ETstate_onClic);

        ETsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                InputMethodManager inputManager = (InputMethodManager) boostrap.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ETsearch.getWindowToken(), 0);
                return true;
            }
        });

        SWlocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        SWlocation.setChecked(false);
                        AlertNoOps();
                    } else {
                        getCoordenates();
                    }
                }
            }
        });

        Log.v("Active",String.valueOf(boostrap.active));
        if (boostrap.active){
            SWlocation.setChecked(true);
        }
        else {
            SWlocation.setChecked(false);
        }
    }

    private void AlertNoOps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(boostrap.langStrings.get(Constants.legend_gps_p))
                .setCancelable(false)
                .setPositiveButton(boostrap.langStrings.get(Constants.yes_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION);
                    }
                })
                .setNegativeButton(boostrap.langStrings.get(Constants.not_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        SWlocation.setChecked(false);
                    }
                });
        alert = builder.create();
        alert.show();

    }

    private View.OnClickListener ETspeciality_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent speciality = new Intent(ctx, CatalogSpecialitiesActivity.class);
            speciality.putExtra(Constants.langString, boostrap.langStrings);
            startActivityForResult(speciality, SELECT_SPECIALITY);
        }
    };

    private View.OnClickListener ETstate_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent state = new Intent(ctx, StatesDoctorActivity.class);
            state.putExtra(Constants.langString, boostrap.langStrings);
            startActivityForResult(state, SELECT_STATE);
        }
    };

    private View.OnClickListener BTNsearch_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, WR_PERMS);
            } else {
                try {

                    if (SWlocation.isChecked()){
                        if (getCoordenates()){
                            loadingSearch.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (lati != null && longi != null){
                                            doSearch();
                                        } else {
                                            loadingSearch.dismiss();
                                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                            builder.setMessage(boostrap.langStrings.get(Constants.legend_fail_coordenates_p))
                                                    .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            builder.show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },3000);
                        }
                    } else {
                        loadingSearch.show();
                        lati = "";
                        longi = "";
                        doSearch();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private View.OnClickListener IBpillbox_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (session.profile().get(0) != null) {
                boostrap.setFragment(fragments.PILLBOX, null);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(boostrap.langStrings.get(Constants.requier_login_p))
                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DialogLogin login = new DialogLogin();
                                login.show(getChildFragmentManager(), fragments.LOGINDIALOG);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        }
    };

    private View.OnClickListener IBfavorite_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (session.profile().get(0) != null) {
                boostrap.setFragment(fragments.FAVORITEDOCOTORS, null);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(boostrap.langStrings.get(Constants.requier_login_p))
                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DialogLogin login = new DialogLogin();
                                login.show(getChildFragmentManager(), fragments.LOGINDIALOG);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        }
    };

    private View.OnClickListener IBsos_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

                View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_call_emergency_number, null);
                LinearLayout LLform = (LinearLayout) view.findViewById(R.id.LLform);
                final TextView TVemergencyNumber = (TextView) view.findViewById(R.id.TVemergencyNumber);
                Button BTNcancel = (Button) view.findViewById(R.id.BTNcancel);
                Button BTNcall = (Button) view.findViewById(R.id.BTNcall);
                BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
                BTNcall.setText(boostrap.langStrings.get(Constants.call_p));

                TVemergencyNumber.setText(session.getPanicTelephone().get(0));
                final AlertDialog DLcallEmergencyNumber = new AlertDialog.Builder(ctx)
                        .setTitle(boostrap.langStrings.get(Constants.call_emergency_p))
                        .setView(view).show();

                BTNcancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DLcallEmergencyNumber.dismiss();
                    }
                });

                BTNcall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + TVemergencyNumber.getText()));
                            startActivity(intent);
                        }
                    }
                });
        }
    };

    private View.OnClickListener TVcreateAccount_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boostrap.setFragment(fragments.REGISTER, null);
        }
    };

    private View.OnClickListener TVlogin_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boostrap.setFragment(fragments.LOGIN, null);
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.v("printRequest", "" + requestCode);
        if (requestCode == WR_PERMS) {
            try {
                doSearch();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void doSearch() throws JSONException {

        boostrap.SPECIALITY_SEARCH = ETspeciality.getText().toString();
        boostrap.STATE_SEARCH = ETstate.getText().toString();
        //boostrap.STATE_SEARCH = (SPstates.getSelectedItemPosition() != 0)? SPstates.getSelectedItem().toString() : "";
        boostrap.jsonSearch = new JSONObject();
        boostrap.jsonSearch.put("doctor_full_name", ETsearch.getText().toString().replaceAll("\n", " "));
        if (ETspeciality.getText().toString().length() > 0) {
            boostrap.jsonSearch.put("specialities", boostrap.SPECIALITY_SEARCH);
        }
        if (ETstate.getText().toString().length() > 0){
            boostrap.jsonSearch.put("state", boostrap.STATE_SEARCH);
        }
        if (SWlocation.isChecked()){
            boostrap.lati = lati;
            boostrap.longi = longi;
            boostrap.jsonSearch.put("location",lati+","+longi);
        }
        if (session.profile().get(0) != null){
            boostrap.jsonSearch.put("patient_id", session.profile().get(0).toString());
        }

        Log.v("printJson",boostrap.jsonSearch.toString());

        if (NetworkUtils.haveNetworkConnection(ctx)) {
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearch", "" + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                boostrap.WORD_SEARCH = ETsearch.getText().toString().trim();
                                JSONObject jsonDoctor = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonDoctor.getJSONArray("rows");
                                ArrayList<DoctorSearch> doct = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    DoctorSearch my = new DoctorSearch();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    Log.v("NameSearch", item.getString("doctor_full_name"));
                                    my.setId_session(item.getString("doctor_id"));
                                    my.setName(item.getString("doctor_full_name"));
                                    my.setPicture(item.getString("picture"));
                                    my.setEnable(item.getString("enabled"));
                                    my.setFriends(item.getString("fb_friends"));
                                    my.setF_amount(item.getString("first_amount"));
                                    my.setS_amount(item.getString("later_amount"));

                                    //---------  SPECIALITIES  ------------------------------
                                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                    if (!String.valueOf(item.get("specialities")).equals("")) {
                                        JSONArray jsonSpe = new JSONArray((String) item.get("specialities"));

                                        for (int k = 0; k < jsonSpe.length(); k++) {
                                            JSONObject itemSpecialities = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> speciality = new HashMap<>();
                                            speciality.put("specialty_id", itemSpecialities.getString("id"));
                                            speciality.put("specialty", itemSpecialities.getString("speciality"));
                                            speciality.put("university", itemSpecialities.getString("university"));
                                            sp.add(speciality);
                                        }

                                    }
                                    my.setSpecialities(sp);
                                    //-------------- OFFICE  -------------------------------------------
                                    ArrayList<DoctorSearch.Offices> offices = new ArrayList<>();
                                    if (String.valueOf(item.get("consulting_rooms")).length() > 4) {

                                        JSONArray jsonConsulting = item.getJSONArray("consulting_rooms");

                                        for (int z = 0; z < jsonConsulting.length(); z++) {
                                            JSONObject itemOffice = jsonConsulting.getJSONObject(z);

                                            DoctorSearch itemDoctor = new DoctorSearch();
                                            DoctorSearch.Offices myOffices = itemDoctor.off;
                                            myOffices.setOffice_id(itemOffice.getString("id"));
                                            myOffices.setName_office(itemOffice.getString("office"));
                                            myOffices.setFull_address(itemOffice.getString("full_address"));
                                            myOffices.setState(itemOffice.getString("state"));
                                            myOffices.setBorough(itemOffice.getString("borough"));
                                            myOffices.setStreet(itemOffice.getString("street"));
                                            myOffices.setColony(itemOffice.getString("colony"));
                                            myOffices.setNum_ext(itemOffice.getString("outdoor_number"));
                                            myOffices.setNum_int(itemOffice.getString("interior_number"));
                                            myOffices.setCity(itemOffice.getString("city"));
                                            myOffices.setLatitude(itemOffice.getString("latitude"));
                                            myOffices.setLongitude(itemOffice.getString("longitude"));
                                            myOffices.setFirst_amount(itemOffice.getString("first_amount"));
                                            myOffices.setLater_amount(itemOffice.getString("later_amount"));

                                            //---------- HOURS --------------------------------------
                                            ArrayList<String> open_times = new ArrayList<String>();
                                            ArrayList<String> locked_times = new ArrayList<>();
                                            JSONObject jsonTime = itemOffice.getJSONObject("times");
                                            if (String.valueOf(jsonTime.get(sdf.format(myCalendar.getTime()).toString())).length() > 4) {
                                                JSONObject jsonDate = jsonTime.getJSONObject(sdf.format(myCalendar.getTime()).toString());
                                                JSONArray jsonOpen = jsonDate.getJSONArray("open_times");
                                                JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                                for (int l = 0; l < jsonOpen.length(); l++) {
                                                    open_times.add(jsonOpen.get(l).toString());
                                                }

                                                for (int m = 0; m < jsonLocked.length(); m++) {
                                                    locked_times.add(jsonLocked.get(m).toString());
                                                }
                                            }
                                            myOffices.setLocked_time(locked_times);
                                            myOffices.setOpen_time(open_times);
                                            offices.add(myOffices);
                                        }
                                    }
                                    my.setMyoffices(offices);
                                    doct.add(my);
                                }
                                hashMap.put("doctors", doct);
                                doSearchPharmacies();
                                break;
                            case 204:
                                ArrayList<DoctorSearch> docto = new ArrayList<>();
                                hashMap.put("doctors", docto);
                                doSearchPharmacies();
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                    loadingSearch.dismiss();
                    Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, boostrap.jsonSearch.toString());
                    params.put(Constants.filters, "");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    Log.v("printHeader",""+headers);
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1));
            queueSearch.add(request);

        } else {
            Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void doSearchPharmacies() throws JSONException {
        boostrap.jsonPharmacy.put("pharmacy", ETsearch.getText().toString());
        if (ETstate.getText().toString().length() > 0){
            boostrap.jsonPharmacy.put("state", boostrap.STATE_SEARCH);
        }
        if (SWlocation.isChecked()){
            boostrap.jsonPharmacy.put("location",lati+","+longi);
        }

        if (NetworkUtils.haveNetworkConnection(ctx)) {
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_pharmacies, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearchPharmacies", "" + response);
                    loadingSearch.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonPharmacies = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonPharmacies.getJSONArray("rows");
                                ArrayList<PharmacyFind> phar = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    PharmacyFind ph = new PharmacyFind();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    ph.setPharmacy_id(item.getString("pharmacy_id"));
                                    ph.setPharmacy_name(item.getString("pharmacy"));
                                    ph.setNetwork(item.getString("network"));
                                    ph.setFull_address(item.getString("full_address"));
                                    ph.setReference(item.getString("reference"));
                                    ph.setPhone(item.getString("phone"));
                                    ph.setPicture(item.getString("picture"));
                                    ph.setStreet(item.getString("street"));
                                    ph.setOutdoor_number(item.getString("outdoor_number"));
                                    ph.setInterior_number(item.getString("interior_number"));
                                    ph.setColony(item.getString("colony"));
                                    ph.setZipcode(item.getString("zipcode"));
                                    ph.setCity(item.getString("city"));
                                    ph.setBorough(item.getString("borough"));
                                    ph.setState(item.getString("state"));
                                    ph.setCountry(item.getString("country"));
                                    ph.setLatitude(item.getString("latitude"));
                                    ph.setLongitude(item.getString("longitude"));

                                    phar.add(ph);
                                }

                                hashMap.put("pharmacies", phar);
                                boostrap.resultSearch = hashMap;
                                boostrap.setFragment(fragments.RESPONSE, null);
                                break;
                            case 204:
                                ArrayList<PharmacyFind> pharma = new ArrayList<>();
                                hashMap.put("pharmacies", pharma);
                                boostrap.resultSearch = hashMap;
                                boostrap.setFragment(fragments.RESPONSE, null);
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                    loadingSearch.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", boostrap.jsonPharmacy.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    Log.v("printHeader",""+headers);
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);
        } else {
            Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void initView() {
        decorate();
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.search_p));
        setHasOptionsMenu(true);

        if (session.profile().get(0) != null) {
            LLregis_log.setVisibility(View.GONE);
        } else {
            LLregis_log.setVisibility(View.VISIBLE);
        }

        if (session.profile().get(0) != null) {
            getWaitingRate();
        }
        if (session.getPanicTelephone().get(0) == null){
                getTelephoneWS();
        }
    }

    private void getWaitingRate() {
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueWaitingRate = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.set_waitings_rates, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printWaitingRate", "" + response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonResponse = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonResponse.length(); i++) {
                                    JSONObject item = jsonResponse.getJSONObject(i);
                                    setQualification(item.getString("prefix"), item.getString("picture"), item.getString("doctor_full_name"), item.getString("id_provider_session"), item.getString("id_appointment"));
                                }

                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                    loading.dismiss();
                    Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    Log.v("printHeader",""+headers);
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1));
            queueWaitingRate.add(request);

        } else {
            Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void setQualification(final String prefix, final String picture, final String name, final String doctor_id, final String appointment_id) {
        View dialog_rate = LayoutInflater.from(ctx).inflate(R.layout.dialog_ratingdoctor, null);
        TextView TVname_doctor = (TextView) dialog_rate.findViewById(R.id.TVname);
        CircleImageView CIVpicture = (CircleImageView) dialog_rate.findViewById(R.id.CIVprofile);
        final RatingBar RBscore = (RatingBar) dialog_rate.findViewById(R.id.RBscore);
        Button BTNqualify = (Button) dialog_rate.findViewById(R.id.BTNqualify);
        final LinearLayout LLform = (LinearLayout) dialog_rate.findViewById(R.id.LLform);
        final ProgressBar PBloading = (ProgressBar) dialog_rate.findViewById(R.id.PBloading);
        BTNqualify.setText(boostrap.langStrings.get(Constants.rate_p));
        TVname_doctor.setText(prefix + " " + name);
        if (!picture.isEmpty()) {
            Picasso.with(ctx).load(picture)
                    .resize(130, 130)
                    .centerCrop()
                    .noFade()
                    .into(CIVpicture);
        } else {
            Picasso.with(ctx).load(R.drawable.blank)
                    .resize(130, 130)
                    .noFade()
                    .centerCrop()
                    .into(CIVpicture);
        }
        final AlertDialog DLrate = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.score_medic_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_rate).show();

        RBscore.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating < 2) {
                    RBscore.setRating(1);
                }
            }
        });

        BTNqualify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setScore(appointment_id, doctor_id, (int) RBscore.getRating(), LLform, PBloading, DLrate);
                Log.v("printRating", RBscore.getNumStars() + "   " + (int) RBscore.getRating());
            }
        });
    }

    public void setScore(final String appoinment_id, final String doctor_id, final int score, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLrate) {

        Log.v("printSendScore", score + "");
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            LLform.setVisibility(View.GONE);
            PBloading.setVisibility(View.VISIBLE);
            //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
            //loading.show();
            queueScore = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.set_score_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printresponseScore", response + "");
                    PBloading.setVisibility(View.GONE);
                    LLform.setVisibility(View.VISIBLE);
                    DLrate.dismiss();
                    //loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                Snackbar.make(view_parent, jsonObject.getString(Constants.response), Snackbar.LENGTH_SHORT).show();
                                break;
                            case 404:
                                Snackbar.make(view_parent, jsonObject.getString("response").toString(), Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                Snackbar.make(view_parent, jsonObject.getString("response").toString(), Snackbar.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("session_id", session.profile().get(0).toString());
                    params.put("appointment_id", appoinment_id);
                    params.put("qualified_id", doctor_id);
                    params.put("ranking", score + "");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    Log.v("printHeaders",""+headers);
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueScore.add(request);
        } else {

        }
    }

    private void getTelephoneWS (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            RequestQueue queueTelephone = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_panic_telephone, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printUser",response+"");
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                session.setPanicTelephone(jsonObject.getString(Constants.response),"0");
                                break;
                            case 404:
                                Snackbar.make(view_parent,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                Snackbar.make(view_parent,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueTelephone.add(request);
        }  else {

        }
    }

    private void decorate(){
        Picasso.with(ctx).load(R.drawable.family_login).noFade().into(IMGfamily);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("printResultCode", "" + resultCode + "    requestCode " + requestCode);
        if (requestCode == SELECT_SPECIALITY) {
            if (resultCode == RESULT_OK) {
                Bundle result = data.getExtras();
                if (result != null) {
                    ETspeciality.setText(result.getString("item"));
                }
            }
        }

        if (requestCode == SELECT_STATE) {
            if (resultCode == RESULT_OK) {
                Bundle result = data.getExtras();
                if (result != null) {
                    ETstate.setText(result.getString("item"));
                }
            }
        }

        if (requestCode == LOCATION) {
            locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                SWlocation.setChecked(false);
            }
        }
    }

    private boolean getCoordenates() {
        LocationManager locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
        MyCurrentListener locationListener = new MyCurrentListener(new MyCurrentListener.evento() {
            @Override
            public void onComplete(String latitud, String longitud) {
                lati = latitud;
                longi = longitud;
                boostrap.active = true;
                Log.v("printLocation",latitud+"   "+longitud);
            }
        });
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return true;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (boostrap != null){
            if (boostrap.active){
                SWlocation.setChecked(true);
            }
            else {
                SWlocation.setChecked(false);
            }
        }
    }
}
