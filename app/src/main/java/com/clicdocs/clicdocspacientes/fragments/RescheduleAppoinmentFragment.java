package com.clicdocs.clicdocspacientes.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterHourByOffice;
import com.clicdocs.clicdocspacientes.beans.ModelOffices;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class RescheduleAppoinmentFragment extends Fragment {

    private int idDoctor, idAppoinment;
    private MainActivity boostrap;
    private Context ctx;
    private RecyclerView mRecyclerSchedule;
    private TextView TVdate, TVname, TVoffice, TVlabel;
    private CircleImageView CIVprofile;
    private Button BTNnextAvailable;
    private Calendar myCalendar, myCal;
    private SessionManager session;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private Date date = new Date();
    private TextView TVnohours;
    private HashMap<Integer,String> days = new HashMap<Integer, String>();
    private RecyclerViewAdapterHourByOffice adapter;
    private String idDoctorOffice;
    private View view;
    private RequestQueue queueReschedule, queueHours;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.adapter_reschedule_appoinment,container,false);
        boostrap            = (MainActivity) getActivity();
        ctx                 = getContext();
        session             = new SessionManager(ctx);
        myCalendar  = Calendar.getInstance();
        TVname              = (TextView) view.findViewById(R.id.TVname);
        TVnohours           = (TextView) view.findViewById(R.id.TVnohour);
        TVoffice            = (TextView) view.findViewById(R.id.TVoffice);
        TVlabel             = (TextView) view.findViewById(R.id.TVlabel);
        CIVprofile          = (CircleImageView) view.findViewById(R.id.CIVprofile);
        TVdate      = (TextView) view.findViewById(R.id.TVdate);
        BTNnextAvailable    = (Button) view.findViewById(R.id.BTNnextEnable);
        mRecyclerSchedule   = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyclerSchedule.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));

        Bundle data = getArguments();
        if (data != null) {
            idAppoinment     = Integer.parseInt(data.getString("idAppoinment"));
            idDoctor         = Integer.parseInt(data.getString("idSession"));
            idDoctorOffice   = data.getString("idDoctorOffice");
            TVname.setText(data.getString("name_doctor"));
            if (!data.getString("picture").isEmpty()){
                Picasso.with(ctx).load(data.getString("picture"))
                        .resize(65, 65)
                        .noFade()
                        .centerCrop()
                        .into(CIVprofile);
            }
            getScheduleOffice(sdf.format(myCalendar.getTime()));

        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        TVdate.setText(sdf.format(myCalendar.getTime()).toString());
        TVdate.setOnClickListener(TVdate_onClic);
        BTNnextAvailable.setOnClickListener(BTNnextAvailable_onClic);

    }

    private View.OnClickListener TVdate_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar fecha = new GregorianCalendar();
            int año = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH);
            int dia = fecha.get(Calendar.DAY_OF_MONTH);
            Date d=null;
            try {
                d = sdf.parse(año+"-"+(mes+1)+"-"+dia);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            DatePickerDialog dpd= new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR,year);
                    myCalendar.set(Calendar.MONTH,monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                    setDatePicked();
                }
            }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            dpd.getDatePicker().setMinDate(d.getTime());
            dpd.show();
        }
    };

    private void setDatePicked(){
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        sdf.setTimeZone(myCalendar.getTimeZone());
        TVdate.setText(sdf.format(myCalendar.getTime()));
        getScheduleOffice(TVdate.getText().toString());

    }

    private View.OnClickListener BTNnextAvailable_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                getNextDateEnable();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    private void getScheduleOffice (final String fecha){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueHours = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.offices_time, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseReschedule",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVnohours.setVisibility(View.GONE);
                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                JSONArray jsonOpen = jsonResponse.getJSONArray("open_times");
                                JSONArray jsonLocked = jsonResponse.getJSONArray("locked_times");

                                ArrayList<String> open_times = new ArrayList<>();
                                for (int i = 0; i<jsonOpen.length(); i++){
                                    open_times.add(jsonOpen.get(i).toString());
                                }
                                ArrayList<String> locked_time = new ArrayList<>();
                                for (int j = 0; j<jsonLocked.length(); j++){
                                    locked_time.add(jsonLocked.get(j).toString());
                                }

                                adapter = new RecyclerViewAdapterHourByOffice(open_times, locked_time, ctx, new RecyclerViewAdapterHourByOffice.Event(){

                                    @Override
                                    public void onClic(String hour) {
                                        //rescheduleAppoinment(TVdate.getText().toString(),hour);
                                        //setRescheduleAppoinment(TVdate.getText().toString(),hour);
                                        View dialog_reschedule   = LayoutInflater.from(ctx).inflate(R.layout.dialog_reason_patient, null);
                                        Button BTNaccept         = (Button) dialog_reschedule.findViewById(R.id.BTNaccept);
                                        Button BTNcancel         = (Button) dialog_reschedule.findViewById(R.id.BTNcancel);
                                        TextView TVreason        = (TextView) dialog_reschedule.findViewById(R.id.TVreason);
                                        EditText ETreason        = (EditText) dialog_reschedule.findViewById(R.id.ETreason);
                                        ProgressBar PBloading    = (ProgressBar) dialog_reschedule.findViewById(R.id.PBloading);
                                        LinearLayout LLform      = (LinearLayout) dialog_reschedule.findViewById(R.id.LLform);

                                        final AlertDialog DLreason   = new AlertDialog.Builder(ctx)
                                                .setTitle("Regendar cita")
                                                .setCancelable(false)
                                                .setView(dialog_reschedule).show();
                                        TVreason.setText("Motivo del reagendado");
                                        ETreason.setHint("Motivo");
                                        BTNaccept.setText(boostrap.langStrings.get(Constants.accept_p));
                                        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
                                        BTNaccept.setOnClickListener(BTNaccept_onClick(dialog_reschedule, ETreason, LLform, PBloading, DLreason, TVdate.getText().toString(), hour));

                                        BTNcancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                DLreason.dismiss();
                                            }
                                        });
                                    }
                                });
                                mRecyclerSchedule.setAdapter(adapter);
                                break;
                            case 400:
                                TVnohours.setVisibility(View.VISIBLE);
                                mRecyclerSchedule.setAdapter(null);
                                mRecyclerSchedule.setVisibility(View.GONE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("office_id", idDoctorOffice);
                    params.put("date", fecha);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueHours.add(request);
        }  else {

        }
    }

    private View.OnClickListener BTNaccept_onClick(final View v, final EditText ETreason, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLreason, final String newDate, final String newHour) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.v("printNewAppoinment", idAppoinment+" "+ newDate+" "+newHour+" "+session.profile().get(5));
                //final Validator validator = new Validator();
                if(!boostrap.validator.validate(ETreason, new String [] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_TEXT})) {
                    if(NetworkUtils.haveNetworkConnection(ctx)) {
                        //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
                        //loading.show();
                        LLform.setVisibility(View.GONE);
                        PBloading.setVisibility(View.VISIBLE);
                        queueReschedule = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                        StringRequest request = new StringRequest(Request.Method.POST, Constants.reschedule_appoinment, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //loading.dismiss();
                                Log.v("printReschedule",""+response);
                                DLreason.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                        case 200:
                                            JSONObject message = jsonObject.getJSONObject(Constants.response);
                                            AlertDialog success = new AlertDialog.Builder(ctx)
                                                    .setMessage(message.getString("message"))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            FragmentManager fm = boostrap.getSupportFragmentManager();
                                                            fm.popBackStack(fragments.RESCHEDULE,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                        case 400:
                                            AlertDialog error = new AlertDialog.Builder(ctx)
                                                    .setMessage(jsonObject.getString(Constants.response))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                            break;
                                        case 403:
                                            Snackbar.make(v, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                    .show();
                                            break;
                                        case 404:
                                            Snackbar.make(v, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                                    .show();
                                            break;
                                        default:
                                            Snackbar.make(v, "Error al iniciar sesión", Snackbar.LENGTH_LONG)
                                                    .show();
                                            break;
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.v("error", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> params = new HashMap<>();
                                params.put("id_appointment", String.valueOf(idAppoinment));
                                params.put("date", newDate);
                                params.put("time", newHour);
                                params.put("cause", ETreason.getText().toString());
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put("X-Access-token", session.profile().get(5).toString());
                                headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                                headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                return headers;
                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queueReschedule.add(request);
                    }  else {
                        Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
            }
        };
    }

    public void getNextDateEnable () throws ParseException, JSONException {
        final Calendar cale;
        cale = Calendar.getInstance();
        RequestQueue queueNextEnable;
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            queueNextEnable = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_next_enable, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printNextEnable",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                JSONObject jsonToday = jsonResponse.getJSONObject("today");
                                Log.v("printNextDate",jsonToday.getString("date"));
                                Date strDate = sdf.parse(jsonToday.getString("date"));
                                cale.setTime(strDate);
                                TVdate.setText(jsonToday.getString("date"));
                                nextAvailable(jsonToday.getString("date"));
                                break;
                            default:
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonObject.getString(Constants.response))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                mRecyclerSchedule.setAdapter(null);
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("date", sdf.format(myCalendar.getTime()));
                    params.put("office_id", idDoctorOffice);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueNextEnable.add(request);
        } else {

        }
    }

    private void nextAvailable (final String DATE_CURRENT){
        RequestQueue queueOffices;
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueOffices = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_offices, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVnohours.setVisibility(View.GONE);
                                mRecyclerSchedule.setVisibility(View.VISIBLE);
                                JSONArray jsonRes = jsonObject.getJSONArray(Constants.response);
                                ArrayList<ModelOffices> offices = new ArrayList<>();

                                for (int i = 0; i < jsonRes.length(); i++){
                                    JSONObject item = jsonRes.getJSONObject(i);
                                    ModelOffices off = new ModelOffices();
                                    off.setOffice_id(item.getString("id"));
                                    off.setOffice(item.getString("office"));
                                    off.setCountry(item.getString("country"));
                                    off.setState(item.getString("state"));
                                    off.setBorough(item.getString("borough"));
                                    off.setCity(item.getString("city"));
                                    off.setStreet(item.getString("street"));
                                    off.setNum_ext(item.getString("outdoor_number"));
                                    off.setNum_int(item.getString("interior_number"));
                                    off.setColony(item.getString("colony"));
                                    off.setZipcode(item.getString("zipcode"));
                                    off.setLatitude(item.getString("latitude"));
                                    off.setLongitude(item.getString("longitude"));

                                    ArrayList<String> open_times = new ArrayList<String>();
                                    ArrayList<String> locked_times = new ArrayList<>();
                                    JSONObject jsonTime = item.getJSONObject("times");

                                    if (!DATE_CURRENT.equals("null")){
                                        JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                        JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                        JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                        for(int l = 0; l < jsonOpen.length(); l++) {
                                            open_times.add(jsonOpen.get(l).toString());
                                        }

                                        off.setOpen_time(open_times);
                                        for(int m = 0; m < jsonLocked.length(); m++) {
                                            locked_times.add(jsonLocked.get(m).toString());
                                        }
                                        off.setLocked_time(locked_times);
                                    }
                                    offices.add(off);
                                }
                                for (int i = 0; i < offices.size(); i++){
                                    if (offices.get(i).getOffice_id().equals(String.valueOf(idDoctorOffice))){
                                       Log.v("print",""+offices.get(i).getOpen_time()+"   "+offices.get(i).getLocked_time());
                                       adapter = new RecyclerViewAdapterHourByOffice(offices.get(i).getOpen_time(), offices.get(i).getLocked_time(), ctx, new RecyclerViewAdapterHourByOffice.Event() {
                                           @Override
                                           public void onClic(String hour) {
                                               View dialog_reschedule   = LayoutInflater.from(ctx).inflate(R.layout.dialog_reason_patient, null);
                                               Button BTNaccept         = (Button) dialog_reschedule.findViewById(R.id.BTNaccept);
                                               Button BTNcancel         = (Button) dialog_reschedule.findViewById(R.id.BTNcancel);
                                               TextView TVreason        = (TextView) dialog_reschedule.findViewById(R.id.TVreason);
                                               EditText ETreason        = (EditText) dialog_reschedule.findViewById(R.id.ETreason);
                                               ProgressBar PBloading    = (ProgressBar) dialog_reschedule.findViewById(R.id.PBloading);
                                               LinearLayout LLform      = (LinearLayout) dialog_reschedule.findViewById(R.id.LLform);

                                               final AlertDialog DLreason   = new AlertDialog.Builder(ctx)
                                                       .setTitle(boostrap.langStrings.get(Constants.titlereschedule_p))
                                                       .setCancelable(false)
                                                       .setView(dialog_reschedule).show();
                                               TVreason.setText(boostrap.langStrings.get(Constants.reason_reschedule_p));
                                               ETreason.setHint(boostrap.langStrings.get(Constants.reason_reschedule_p));
                                               BTNaccept.setText(boostrap.langStrings.get(Constants.accept_p));
                                               BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
                                               BTNaccept.setOnClickListener(BTNaccept_onClick(dialog_reschedule,ETreason, LLform, PBloading, DLreason,DATE_CURRENT, hour));

                                               BTNcancel.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View view) {
                                                       DLreason.dismiss();
                                                   }
                                               });
                                           }
                                       });
                                        mRecyclerSchedule.setAdapter(adapter);
                                        //adapter.update(offices.get(i).getOpen_time(),offices.get(i).getLocked_time(),ctx);
                                    }
                                }
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.doctor_id, String.valueOf(idDoctor));
                    params.put("date", DATE_CURRENT);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueOffices.add(request);
        }  else {

        }

    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.reschedule_p));
        setHasOptionsMenu(true);
    }
}
