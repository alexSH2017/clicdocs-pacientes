package com.clicdocs.clicdocspacientes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewFavoriteDoctors;
import com.clicdocs.clicdocspacientes.beans.ModelFavoriteDoc;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FavoriteDoctorsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    //private SwipeRefreshLayout swipe;
    private TextView TVlegend;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private AlertDialog  DLnoconnection;
    private View view;
    private RequestQueue queueFavorite;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view       = inflater.inflate(R.layout.recycler_general,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        mRecyclerView   = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        TVlegend        = (TextView) view.findViewById(R.id.TVlegend);
        //swipe           = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        //swipe.setColorSchemeResources(R.color.green, R.color.blue, R.color.yellow);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        getFavoriteDoctors ();
        /*swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavoriteDoctors();
            }
        });*/
    }

    private void getFavoriteDoctors (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueFavorite = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_favorite, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                final ArrayList<ModelFavoriteDoc> favorites =new ArrayList<ModelFavoriteDoc>();
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                ArrayList<ModelFavoriteDoc> arrayDoctor = new ArrayList<>();

                                for (int i = 0; i < jsonResp.length(); i++){
                                    JSONObject item = jsonResp.getJSONObject(i);
                                    ModelFavoriteDoc model = new ModelFavoriteDoc();

                                    model.setIdSession(item.getString("doctor_id"));
                                    model.setName(item.getString("doctor_full_name"));
                                    model.setPicture(item.getString("picture"));
                                    model.setId_favorite(item.getString("id_my_favorites_doctors"));
                                    ArrayList<String> spec = new ArrayList<>();

                                    if (item.getString("specialities").length() > 4){
                                        JSONArray jsonSpe = new JSONArray((String) item.getString("specialities"));
                                        for (int j = 0; j < jsonSpe.length(); j++){
                                            JSONObject itemSpe = jsonSpe.getJSONObject(j);
                                            spec.add(itemSpe.getString("speciality"));
                                        }
                                        model.setSpeciliaty(spec);
                                    }
                                    arrayDoctor.add(model);
                                }

                                RecyclerViewFavoriteDoctors adapter = new RecyclerViewFavoriteDoctors(boostrap, ctx, arrayDoctor, new RecyclerViewFavoriteDoctors.Event() {
                                    @Override
                                    public void onClic(ModelFavoriteDoc fav) {
                                        Bundle b = new Bundle();
                                        b.putString("doctor_id",fav.getIdSession());
                                        boostrap.setFragment(fragments.PROFILEDOCTOR,b);
                                    }
                                });
                                mRecyclerView.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueFavorite.add(request);
        }  else {

        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.my_doctor_p));
        setHasOptionsMenu(true);
    }
}
