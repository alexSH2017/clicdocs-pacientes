package com.clicdocs.clicdocspacientes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterOfficeWait;

import com.clicdocs.clicdocspacientes.beans.ModelOfficeWait;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class OfficesWaitList extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RecyclerView mRecycler;
    private RequestQueue queueWaitOffice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_general,container,false);
        boostrap    = (MainActivity) getActivity();
        ctx         = getContext();
        session     = new SessionManager(ctx);
        mRecycler   = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        try {
            getOfficeWait ();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getOfficeWait () throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("patient_id",session.profile().get(0).toString());
        jsonObject.put("status", "1");
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");

        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueWaitOffice = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_wait_office, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseWaitList",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                final ArrayList<ModelOfficeWait> modelOffceWait =new ArrayList<ModelOfficeWait>();
                                JSONObject jsonOffice = jsonObject.getJSONObject(Constants.response);
                                JSONArray jsonRows = jsonOffice.getJSONArray("rows");
                                ArrayList<ModelOfficeWait> arrayOfficeWait = new ArrayList<>();

                                for (int i = 0; i < jsonRows.length(); i++){
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    ModelOfficeWait modOffice = new ModelOfficeWait();
                                    modOffice.setPicture(item.getString("picture"));
                                    modOffice.setId_waiting_list(item.getString("waiting_list_id"));
                                    modOffice.setDoctor_name(item.getString("doctor_full_name"));
                                    modOffice.setAppoinment_id(item.getString("appointment_id"));
                                    modOffice.setDate(item.getString("date"));
                                    modOffice.setHour(item.getString("hour"));
                                    modOffice.setOffice_name(item.getString("consulting_room"));
                                    modOffice.setStreet(item.getString("full_address"));
                                    modOffice.setOutnumber(item.getString("outdoor_number"));
                                    modOffice.setInteriornumber(item.getString("interior_number"));
                                    modOffice.setColony(item.getString("colony"));
                                    if (item.getJSONArray("hours_released").length() > 0){
                                        ArrayList<HashMap<String, String>> hours_released = new ArrayList<>();
                                        JSONArray jsonSpe = item.getJSONArray("hours_released");

                                        for (int k = 0; k < jsonSpe.length(); k++) {
                                            JSONObject itemHours = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> hour_map = new HashMap<>();
                                            hour_map.put("id_hour_released", itemHours.getString("id_hour_released"));
                                            hour_map.put("date", itemHours.getString("date"));
                                            hour_map.put("hour", itemHours.getString("hour"));
                                            hours_released.add(hour_map);
                                        }
                                        modOffice.setHours_released(hours_released);
                                    }

                                    arrayOfficeWait.add(modOffice);
                                }
                                RecyclerViewAdapterOfficeWait adapter = new RecyclerViewAdapterOfficeWait(arrayOfficeWait,boostrap,ctx);
                                mRecycler.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", jsonArray.toString());
                    params.put("filters", jsonObject.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueWaitOffice.add(request);
        }  else {

        }
    }

    private void initView (){
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.setTitle(boostrap.langStrings.get(Constants.waiting_list_p));
        setHasOptionsMenu(true);
        boostrap.getSupportActionBar().show();
    }

}
