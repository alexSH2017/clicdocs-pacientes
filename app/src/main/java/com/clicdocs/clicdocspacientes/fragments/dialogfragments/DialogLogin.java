package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.MyFireBaseInstanceIDService;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class DialogLogin extends DialogFragment {

    private Vibrator vib;
    private EditText ETemail, ETpass;
    private SessionManager session;
    private Button BTNlogin;
    private ImageView IVbackground;
    private MainActivity boostrap;
    private AlertDialog DLnoconnection;
    private TextView TVregister;
    private ProgressDialog loading;
    private Context ctx;
    private CallbackManager mCallbackManager;
    private LoginButton loginButton;
    Profile profile;
    private RequestQueue queueFBLogin, queueLogin, queueTerms, queueTelephone, queueAcceptTerms;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_login_new,container,false);
        BTNlogin     = (Button) view.findViewById(R.id.BTNlogin);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //IVbackground = (ImageView) view.findViewById(R.id.IVbackground);
        ETemail      = (EditText) view.findViewById(R.id.ETemail);
        ETpass       = (EditText) view.findViewById(R.id.ETpassword);
        boostrap     = (MainActivity) getActivity();
        ctx          = getContext();
        //TVregister   = (TextView) view.findViewById(R.id.TVregister);
        vib          = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        boostrap.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        loginButton = (LoginButton) view.findViewById(R.id.btnFacebook);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        loginButton.setFragment(this);
        loginButton.registerCallback(mCallbackManager,mCallback);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager=CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BTNlogin.setText(boostrap.langStrings.get(Constants.login_p));
        ETemail.setHint(boostrap.langStrings.get(Constants.email_p));
        ETpass.setHint(boostrap.langStrings.get(Constants.password_p));
        session = new SessionManager(ctx);
        BTNlogin.setOnClickListener(BTNlogin_onClick);
    }


    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            profile = Profile.getCurrentProfile();
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    try {
                        String id = object.optString("id");
                        String first_name = object.optString("first_name");
                        String last_name = object.optString("last_name");
                        String email = object.optString("email");
                        String birthday = object.optString("birthday");
                        String gender = object.optString("gender");

                        JSONObject friends = object.getJSONObject("friends");
                        JSONArray data = friends.getJSONArray("data");
                        JSONArray friendsID = new JSONArray();
                        for (int i = 0; i < data.length(); i++){
                            JSONObject item = data.getJSONObject(i);
                            Log.v("printID",item.getString("id"));
                            friendsID.put(item.getString("id"));
                        }

                        String genderF="";
                        switch (gender){
                            case "male":
                                genderF = "1";
                                break;
                            case "female":
                                genderF = "2";
                                break;
                        }
                        String pictureDataURL =object.getJSONObject("picture").getJSONObject("data").getString("url");;
                        String [] date =birthday.split("/");
                        String fecha = date[2]+"-"+date[0]+"-"+date[1];
                        Log.v("printfoto",pictureDataURL);

                        setLoginFacebook(first_name,last_name,email,fecha,genderF,id, URLEncoder.encode(pictureDataURL,"UTF-8"),friendsID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields","id,first_name,last_name,email,birthday,gender,link,picture.width(400).height(400),friends");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() { }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(boostrap, error.getMessage(), Toast.LENGTH_LONG).show();
        }
    };

    private void setLoginFacebook (final String nameF, final String lastnameF, final String emailF, final String birthdayF, final String genderF, final String fbid, final String picture, final JSONArray friendsID){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueFBLogin = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.fblogin, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseF",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject user = jsonObject.getJSONObject("response");
                                JSONObject chat = jsonObject.getJSONObject("chat");
                                session.setChat(chat.getString("username"),chat.getString("server"));
                                session.setLogin(user.getString("patient_id"),"5", Miscellaneous.ucFirst(user.getString("name"))+" "+Miscellaneous.ucFirst(user.getString("last_name"))+" "+Miscellaneous.ucFirst(user.getString("middle_name")),"fb",user.getString("picture"),jsonObject.getString("token"));
                                session.secondSession(user.getString("patient_id"),"5", Miscellaneous.ucFirst(user.getString("name"))+" "+Miscellaneous.ucFirst(user.getString("last_name"))+" "+Miscellaneous.ucFirst(user.getString("middle_name")),"fb",user.getString("picture"),jsonObject.getString("token"));

                                termsValidate();
                                /*MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                token.sendRegistrationToServer(refreshedToken,ctx,user.getString("patient_id"));
                                boostrap.finish();
                                Intent main =new Intent(ctx, SplashActivity.class);
                                startActivity(main);*/
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            default:
                                Snackbar.make(view, "La cuenta de Facebook no está asociada a una cuenta de correo electrónico, favor de verificar.", Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("name", nameF);
                    params.put("lastname", lastnameF);
                    params.put("email", emailF);
                    params.put("birthday", birthdayF);
                    params.put("gender", genderF);
                    params.put("fbid", fbid);
                    params.put("picture", picture);
                    params.put("friends", friendsID.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueFBLogin.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void doLogin(){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueLogin = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.login, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printLogin",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject user = jsonObject.getJSONObject("response");
                                JSONObject chat = jsonObject.getJSONObject("chat");
                                session.setChat(chat.getString("username"),chat.getString("server"));
                                session.setLogin(user.getString("patient_id"),"5", Miscellaneous.ucFirst(user.getString("name"))+" "+Miscellaneous.ucFirst(user.getString("last_name"))+" "+Miscellaneous.ucFirst(user.getString("middle_name")),ETpass.getText().toString(),user.getString("picture"),jsonObject.getString("token"));
                                session.secondSession(user.getString("patient_id"),"5", Miscellaneous.ucFirst(user.getString("name"))+" "+Miscellaneous.ucFirst(user.getString("last_name"))+" "+Miscellaneous.ucFirst(user.getString("middle_name")),ETpass.getText().toString(),user.getString("picture"),jsonObject.getString("token"));
                                termsValidate();

                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            default:
                                Snackbar.make(view, "Error al iniciar sesión", Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.username, ETemail.getText().toString());
                    params.put(Constants.password, ETpass.getText().toString());
                    params.put("type", "patient");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("X-Bundle-Id", "com.clicdocs.clicdocspacientes");
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueLogin.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void termsValidate (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.terms_validate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printLogin",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                if (session.getPanicTelephone().get(1).equals("0")){
                                    getTelephoneWS();
                                } else {
                                    MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                    token.sendRegistrationToServer(refreshedToken,ctx,session.profile().get(0).toString());
                                    ETemail.setText("");
                                    ETpass.setText("");
                                    boostrap.Login();
                                    boostrap.setNameProfile(session.profile().get(2).toString());
                                    boostrap.setImageProfile(session.profile().get(4).toString());
                                    boostrap.showItem();
                                    DialogLogin.this.dismiss();
                                }
                                break;
                            case 402:
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                JSONObject terms = jsonResp.getJSONObject(0);
                                dialogTerms(terms.getString("term").toString(),terms.getString("term_id").toString());
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            default:
                                Snackbar.make(view, "Error al iniciar sesión", Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("session_id", session.profile().get(0).toString());
                    params.put("id_user_type", "5");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueTerms.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void getTelephoneWS (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueTelephone = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_panic_telephone, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printUser",response+"");
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                session.setPanicTelephone(jsonObject.getString(Constants.response),"0");
                                MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                token.sendRegistrationToServer(refreshedToken,ctx,session.profile().get(0).toString());
                                ETemail.setText("");
                                ETpass.setText("");
                                boostrap.setNameProfile(session.profile().get(2).toString());
                                boostrap.setImageProfile(session.profile().get(4).toString());
                                boostrap.Login();
                                boostrap.showItem();
                                DialogLogin.this.dismiss();
                                break;
                            case 403:

                                break;
                            case 404:
                                Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueTelephone.add(request);
        }  else {

        }
    }

    private void dialogTerms(String terms, String terms_id){
        View dialog_terms        = LayoutInflater.from(ctx).inflate(R.layout.dialog_terms, null);
        Button BTNreject         = (Button) dialog_terms.findViewById(R.id.BTNreject);
        Button BTNaccept         = (Button) dialog_terms.findViewById(R.id.BTNaccept);
        TextView TVterms         = (TextView) dialog_terms.findViewById(R.id.TVterms);
        LinearLayout LLform      = (LinearLayout) dialog_terms.findViewById(R.id.LLform);
        ProgressBar PBloading    = (ProgressBar) dialog_terms.findViewById(R.id.PBloading);
        BTNaccept.setText(boostrap.langStrings.get(Constants.accept_p));
        BTNreject.setText(boostrap.langStrings.get(Constants.to_refuse_p));
        final AlertDialog DLterms   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.terms_condi_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_terms).show();

        TVterms.setText(Html.fromHtml(terms));

        BTNreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.deleteProfile();
                session.deleteSecondProfile();
                DLterms.dismiss();
            }
        });

        BTNaccept.setOnClickListener(BTNaccept_onClick(terms_id, LLform, PBloading, DLterms));

    }

    private View.OnClickListener BTNaccept_onClick(final String id_term,final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLrecovery) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(NetworkUtils.haveNetworkConnection(ctx)) {
                    LLform.setVisibility(View.GONE);
                    PBloading.setVisibility(View.VISIBLE);
                    //final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.getLoadin());
                    //loading.show();
                    queueAcceptTerms = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.accep_terms, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("printUser",response+"");
                            PBloading.setVisibility(View.GONE);
                            LLform.setVisibility(View.VISIBLE);
                            DLrecovery.dismiss();
                            //loading.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        if (session.getPanicTelephone().get(1).equals("0")){
                                            getTelephoneWS();
                                        } else {
                                            MyFireBaseInstanceIDService token = new MyFireBaseInstanceIDService();
                                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                            token.sendRegistrationToServer(refreshedToken,ctx,session.profile().get(0).toString());
                                            ETemail.setText("");
                                            ETpass.setText("");
                                            boostrap.setNameProfile(session.profile().get(2).toString());
                                            boostrap.setImageProfile(session.profile().get(4).toString());
                                            boostrap.Login();
                                            boostrap.showItem();
                                            DialogLogin.this.dismiss();
                                        }

                                        break;
                                    case 404:
                                        Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                        break;
                                    default:
                                        Snackbar.make(view,jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("session_id", session.profile().get(0).toString());
                            params.put("term_id", id_term);
                            params.put("user_type", "5");
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Constants.key_access_token, session.profile().get(5).toString());
                            headers.put(Constants.key_bundle_id, Constants.bundle_id);
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueAcceptTerms.add(request);
                }  else {

                }
            }
        };
    }

    private View.OnClickListener BTNlogin_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Validator validator = new Validator();
            if(!boostrap.validator.validate(ETemail, new String[] {boostrap.validator.REQUIRED, boostrap.validator.EMAIL}) &&
                    !boostrap.validator.validate(ETpass, new String[] {boostrap.validator.REQUIRED})) {
                doLogin();
            } else vib.vibrate(120);
        }
    };

}
