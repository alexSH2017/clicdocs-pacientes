package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.content.Context;
import android.media.Rating;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.SessionManager;


public class DialogRatingDoctor extends DialogFragment {

    private SessionManager session;
    private MainActivity boostrap;
    private Context ctx;
    private RatingBar rtScore;
    private Button btnScore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_ratingdoctor,container,false);
        boostrap        =   (MainActivity) getActivity();
        ctx             =   getContext();
        session         =   new SessionManager(ctx);
        //rtScore         =   (RatingBar) view.findViewById(R.id.ratingDoctor);
        //btnScore        =   (Button) view.findViewById(R.id.btnScore);
        //rtScore.setRating((float)0.5);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnScore.setOnClickListener(btnScore_onClic);
    }

    private View.OnClickListener btnScore_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Toast.makeText(boostrap,""+rtScore.getRating(),Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
