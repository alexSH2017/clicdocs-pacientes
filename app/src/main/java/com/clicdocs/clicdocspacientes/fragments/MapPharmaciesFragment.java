package com.clicdocs.clicdocspacientes.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorsFind;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.PharmacyFind;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class MapPharmaciesFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private ImageButton IMBback;
    private MapView mapView;
    private GoogleMap mMap;
    private Marker mMarker;
    private ArrayList<PharmacyFind> pharmacyLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_pharmacies_fragment,container,false);
        boostrap            = (MainActivity) getActivity();
        ctx                 = getContext();
        IMBback             = (ImageButton) view.findViewById(R.id.IMBback);
        mapView             = (MapView) view.findViewById(R.id.mapView);
        pharmacyLocation = (ArrayList<PharmacyFind>) getArguments().getSerializable("pharmacies");
        //pharmacyLocation    = (ArrayList<PharmacyFind>) boostrap.resultSearch.get("pharmacies");
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        TrustManagerUtil.restoresHttpsUrlSettingsGoogle();
        seeMapPharmacies();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().hide();
        prepareActionBar();
        boostrap.setTitle("");
        IMBback.setOnClickListener(IMBback_onClic);
    }

    private View.OnClickListener IMBback_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm = boostrap.getSupportFragmentManager();
            fm.popBackStack(fragments.MAPPHARMACIES,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    };

    private void seeMapPharmacies(){
        mapView.onResume();
        try {
            MapsInitializer.initialize(boostrap.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGooglemap) {
                mMap = mGooglemap;
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                double lat=0.0;
                double lng=0.0;
                String pharmacy = "", address="";
                Log.v("printPharmaciesSize",pharmacyLocation.size()+"");
                for (int i=0; i<pharmacyLocation.size();i++) {
                    if (pharmacyLocation.get(i).getLatitude() != "") {
                        Log.v("printLatiLongi",i+pharmacyLocation.get(i).getLatitude()+"  "+pharmacyLocation.get(i).getLongitude());
                        lat = Double.parseDouble(pharmacyLocation.get(i).getLatitude());
                        lng = Double.parseDouble(pharmacyLocation.get(i).getLongitude());
                        pharmacy = pharmacyLocation.get(i).getPharmacy_name();
                        address = pharmacyLocation.get(i).getStreet();
                        addMarker(lat, lng, pharmacy, address);
                    }

                }
            }
        });
    }

    private void addMarker (double lat, double lng, String pharmacy, String address){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,12);
        if (mMarker!= null){
        }
        mMarker=mMap.addMarker(new MarkerOptions()
                .position(coordenadas)
                .title(pharmacy)
                .snippet(address));
        mMap.animateCamera(ubication);
    }

    private void prepareActionBar() {
        HashMap<String, String> options = new HashMap<>();
        options.put(Constants.TBtitle, "");
        options.put(Constants.TBhasBack, Constants.TBhasBack);
        boostrap.setActionBar(boostrap.TBmain, options);
    }
}
