package com.clicdocs.clicdocspacientes.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.AdvicesDoctorActivity;
import com.clicdocs.clicdocspacientes.activity.InsurancesDoctorActivity;
import com.clicdocs.clicdocspacientes.activity.ServicesDoctorActivity;
import com.clicdocs.clicdocspacientes.activity.SpecialitiesDoctorActivity;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileDoctorFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private TextView TVname;
    private RatingBar RBscore;
    private CircleImageView CIVpicture;
    private TextView TVmajor, TVuniversity, TVgraduationYear, TVprofessionalID;
    private ImageView IVaddfavorite, IVdeletefavorite;
    private Button BTNoffice, BTNservices, BTNinsurances, BTNspecialities, BTNadvices;
    private String idSession="";
    private Calendar myCalendar;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private MapView mapView;
    private View view;
    private GoogleMap mMap;
    private Marker mMarker;
    private SessionManager session;
    private RequestQueue queueDoctor,queueAddFav,queueDelFav;
    private ModelDoctorProfile mdp;
    private String favorite_id = "";
    private ImageView IVgender;
    private ImageView IVaffiliate;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view             = inflater.inflate(R.layout.profile_doctor_fragment,container,false);
        boostrap         = (MainActivity) getActivity();
        ctx              = getContext();
        IVaddfavorite    = (ImageView) view.findViewById(R.id.IVaddfavorite);
        IVdeletefavorite = (ImageView) view.findViewById(R.id.IVdeletefavorite);
        TVname           = (TextView) view.findViewById(R.id.TVname);
        RBscore          = (RatingBar) view.findViewById(R.id.RBscore);
        TVmajor          = (TextView) view.findViewById(R.id.TVmajor);
        TVuniversity     = (TextView) view.findViewById(R.id.TVuniversity);
        TVgraduationYear = (TextView) view.findViewById(R.id.TVgraduationYear);
        TVprofessionalID = (TextView) view.findViewById(R.id.TVprofessionalID);
        CIVpicture       = (CircleImageView) view.findViewById(R.id.CIVprofile);
        BTNoffice        = (Button) view.findViewById(R.id.BTNoffices);
        BTNservices      = (Button) view.findViewById(R.id.BTNservices);
        BTNinsurances    = (Button) view.findViewById(R.id.BTNinsurances);
        BTNspecialities  = (Button) view.findViewById(R.id.BTNspecialities);
        BTNadvices       = (Button) view.findViewById(R.id.BTNadvices);
        mapView          = (MapView) view.findViewById(R.id.mapDoctor);
        IVgender         = (ImageView) view.findViewById(R.id.IVgender);
        IVaffiliate      = (ImageView) view.findViewById(R.id.IVaffiliate);
        session          = new SessionManager(ctx);
        mapView.onCreate(savedInstanceState);
        idSession = getArguments().getString("doctor_id");
        RBscore.setIsIndicator(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        myCalendar = Calendar.getInstance();
        getProfileDoctor ();
        IVaddfavorite.setOnClickListener(IVaddfavorite_onClic);
        IVdeletefavorite.setOnClickListener(IVdeletefavorite_onClic);
        BTNadvices.setText(boostrap.langStrings.get(Constants.councils_p));
        BTNinsurances.setText(boostrap.langStrings.get(Constants.insurance_p));
        BTNservices.setText(boostrap.langStrings.get(Constants.services_p));
        BTNoffice.setText(boostrap.langStrings.get(Constants.offices_p));
        BTNspecialities.setText(boostrap.langStrings.get(Constants.specilities_p));
        BTNoffice.setOnClickListener(BTNoffice_onClic);
        BTNinsurances.setOnClickListener(BTNinsurances_onClic);
        BTNservices.setOnClickListener(BTNservices_onClic);
        BTNspecialities.setOnClickListener(BTNspecialities_onClic);
        BTNadvices.setOnClickListener(BTNadvices_onClic);
    }


    //============  METHOD ONCLIC LINEARLAYOUT =========================
    private View.OnClickListener BTNoffice_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mdp.getMyoffices() != null){
                if (mdp.getMyoffices().size() > 0) {
                    Bundle b = new Bundle();
                    b.putString("idSession",idSession);
                    b.putString("name",mdp.getName());
                    b.putString("speciality","");
                    b.putString("picture",mdp.getPicture());
                    b.putString("enable",mdp.getEnable());
                    b.putInt("card",1);
                    boostrap.WORD_SEARCH = "prueba";
                    boostrap.setFragment(fragments.OFFICES,b);
                } else {
                    Toast.makeText(boostrap, boostrap.langStrings.get(Constants.no_offices_p), Toast.LENGTH_SHORT).show();
                }
            } else{
                Snackbar.make(view, boostrap.langStrings.get(Constants.no_offices_p), Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener BTNservices_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mdp.getServices() != null){
                if (mdp.getServices().size() > 0){
                    Intent services = new Intent(ctx, ServicesDoctorActivity.class);
                    services.putExtra(Constants.langString,boostrap.langStrings);
                    services.putExtra("services",(Serializable) mdp);
                    startActivityForResult(services, 123);
                } else {
                    Toast.makeText(boostrap, boostrap.langStrings.get(Constants.no_services_p), Toast.LENGTH_SHORT).show();
                }

            } else{
                Snackbar.make(view, boostrap.langStrings.get(Constants.no_services_p), Snackbar.LENGTH_SHORT).show();
            }

        }
    };

    private View.OnClickListener BTNinsurances_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mdp.getInsurance() != null){
                if (mdp.getInsurance().size() > 0){
                    Intent insurance = new Intent(ctx, InsurancesDoctorActivity.class);
                    insurance.putExtra(Constants.langString,boostrap.langStrings);
                    insurance.putExtra("insurances",(Serializable) mdp);
                    startActivityForResult(insurance, 123);
                } else {
                    Toast.makeText(boostrap, boostrap.langStrings.get(Constants.no_insurances_p), Toast.LENGTH_SHORT).show();
                }
            } else{
                Snackbar.make(view, boostrap.langStrings.get(Constants.no_insurances_p), Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener BTNadvices_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mdp.getAdvices() != null){
                if (mdp.getAdvices().size() > 0){
                    Intent advices = new Intent(ctx, AdvicesDoctorActivity.class);
                    advices.putExtra(Constants.langString,boostrap.langStrings);
                    advices.putExtra("advices",(Serializable) mdp);
                    startActivityForResult(advices, 123);
                } else {
                    Toast.makeText(boostrap, boostrap.langStrings.get(Constants.no_councils_p), Toast.LENGTH_SHORT).show();
                }

            } else{
                Snackbar.make(view, boostrap.langStrings.get(Constants.no_councils_p), Snackbar.LENGTH_SHORT).show();
            }

        }
    };

    private View.OnClickListener BTNspecialities_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mdp.getSpeciality() != null){
                if (mdp.getSpeciality().size() > 0){
                    Intent speciality = new Intent(ctx, SpecialitiesDoctorActivity.class);
                    speciality.putExtra(Constants.langString,boostrap.langStrings);
                    speciality.putExtra("specialities",(Serializable) mdp);
                    startActivityForResult(speciality, 123);
                } else {
                    Toast.makeText(boostrap, boostrap.langStrings.get(Constants.no_specialities_p), Toast.LENGTH_SHORT).show();
                }

            } else{
                Snackbar.make(view, boostrap.langStrings.get(Constants.no_specialities_p), Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener IVaddfavorite_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
            builder.setMessage(boostrap.langStrings.get(Constants.add_my_doctors_p))
                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                        public void onClick(DialogInterface dialog, int id) {
                            if (session.profile().get(0) != null){
                                setFavoriteDoctor();
                            } else {
                                Toast.makeText(ctx,boostrap.langStrings.get(Constants.is_required_login_p),Toast.LENGTH_SHORT).show();
                            }
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }); builder.show();
        }
    };

    private View.OnClickListener IVdeletefavorite_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
            builder.setMessage(boostrap.langStrings.get(Constants.remove_my_doctors_p))
                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteFavoriteDoctor();
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }); builder.show();
        }
    };

    //==============================================
    private void seeMapDoctor (){
        mapView.onResume();
        try {
            MapsInitializer.initialize(boostrap.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGooglemap) {
                mMap = mGooglemap;
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                ctx, R.raw.style_json));


                mMap.getUiSettings().isZoomControlsEnabled();
                mMap.setMyLocationEnabled(true);
                double lat=0.0;
                double lng=0.0;
                String office = "";
                Log.v("printSize",""+mdp.getMyoffices().size());

                if (mdp.getMyoffices().size() > 0){
                    for (int i = 0; i < mdp.getMyoffices().size(); i++){
                        if (!mdp.getMyoffices().get(i).getLatitude().isEmpty() && !mdp.getMyoffices().get(i).getLongitude().isEmpty()){
                            lat = Double.parseDouble(mdp.getMyoffices().get(i).getLatitude());
                            lng = Double.parseDouble(mdp.getMyoffices().get(i).getLongitude());
                            office = mdp.getMyoffices().get(i).getName_office();
                            addMarker(lat,lng,office);
                        }
                    }

                }

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {

                    }
                });
            }
        });
    }

    private void addMarker (double lat, double lng, String consultorio){
        LatLng coordenadas = new LatLng(lat,lng);
        CameraUpdate ubication = CameraUpdateFactory.newLatLngZoom(coordenadas,15);
        if (mMarker!= null){

        }
        mMarker=mMap.addMarker(new MarkerOptions()
        .position(coordenadas)
        .title(consultorio));
        mMap.animateCamera(ubication);
    }

    private void getProfileDoctor () {
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueDoctor = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_doctor_profile, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e("printProfile",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResp = jsonObject.getJSONObject(Constants.response);

                                if (jsonResp.getInt("gender") == 1){
                                    IVgender.setBackgroundResource(R.drawable.icon_male);
                                } else {
                                    IVgender.setBackgroundResource(R.drawable.icon_famale);
                                }
                                mdp = new ModelDoctorProfile();

                                mdp.setName(jsonResp.getString("doctor_full_name"));
                                mdp.setPicture(jsonResp.getString("picture"));
                                mdp.setEnable(jsonResp.getString("enabled"));
                                mdp.setRanking(jsonResp.getString("ranking"));
                                mdp.setFavorite(jsonResp.getString("favorite"));
                                mdp.setF_amount(jsonResp.getString("first_amount"));
                                mdp.setS_amount(jsonResp.getString("later_amount"));
                                mdp.setMajor(jsonResp.getString("carrer"));
                                mdp.setUniversity(jsonResp.getString("university"));
                                mdp.setProfessional_id(jsonResp.getString("lic_professional_id"));
                                mdp.setGraduation(jsonResp.getString("graduation_year"));
                                ArrayList<ModelDoctorProfile.Offices> offices = new ArrayList<>();
                                if (!String.valueOf(jsonResp.getString("offices")).equals("null")){
                                    JSONArray jsonOffices = jsonResp.getJSONArray("offices");
                                    for (int i = 0; i < jsonOffices.length(); i++){
                                        JSONObject itemOffice = jsonOffices.getJSONObject(i);
                                        ModelDoctorProfile itemDoctor = new ModelDoctorProfile();
                                        ModelDoctorProfile.Offices myOffices = itemDoctor.off;
                                        myOffices.setOffice_id(itemOffice.getString("id"));
                                        myOffices.setName_office(itemOffice.getString("office"));
                                        myOffices.setFull_address(itemOffice.getString("full_address"));
                                        myOffices.setState(itemOffice.getString("state"));
                                        myOffices.setBorough(itemOffice.getString("borough"));
                                        myOffices.setCity(itemOffice.getString("city"));
                                        myOffices.setLatitude(itemOffice.getString("latitude"));
                                        myOffices.setLongitude(itemOffice.getString("longitude"));

                                        ArrayList<String> open_times = new ArrayList<String>();
                                        ArrayList<String> locked_times = new ArrayList<>();

                                        JSONObject jsonTime = itemOffice.getJSONObject("times");
                                        if (!String .valueOf(jsonTime.get(sdf.format(myCalendar.getTime()).toString())).equals("null")){
                                            JSONObject jsonDate = jsonTime.getJSONObject(sdf.format(myCalendar.getTime()).toString());
                                            JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                            JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                            for(int l = 0; l < jsonOpen.length(); l++) {
                                                open_times.add(jsonOpen.get(l).toString());
                                            }

                                            for(int m = 0; m < jsonLocked.length(); m++) {
                                                locked_times.add(jsonLocked.get(m).toString());
                                            }
                                        }
                                        myOffices.setLocked_time(locked_times);
                                        myOffices.setOpen_time(open_times);
                                        offices.add(myOffices);
                                    }
                                }


                                mdp.setMyoffices(offices);
                                ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                if (String.valueOf(jsonResp.getString("specialities")).length() > 4) {
                                    //JSONObject jsonSpe = new JSONArray((String)jsonResp.getString("specialities"));
                                    JSONObject jsonSpeciality = jsonResp.getJSONObject("specialities");
                                    JSONArray jsonRows = jsonSpeciality.getJSONArray("rows");
                                    for (int k = 0; k < jsonRows.length(); k++){
                                        JSONObject itemSpecialities = jsonRows.getJSONObject(k);
                                        HashMap<String, String> speciality = new HashMap<>();
                                        speciality.put("specialty_id",itemSpecialities.getString("specialty_id"));
                                        speciality.put("specialty",itemSpecialities.getString("specialty"));
                                        speciality.put("university",itemSpecialities.getString("university"));
                                        speciality.put("carrer",itemSpecialities.getString("carrer"));
                                        speciality.put("state",itemSpecialities.getString("state"));
                                        speciality.put("professional_id",itemSpecialities.getString("professional_id"));
                                        sp.add(speciality);
                                    }
                                    mdp.setSpeciality(sp);
                                } else { mdp.setSpeciality(sp); }

                                ArrayList<HashMap<String, String>> services = new ArrayList<>();
                                if (String.valueOf(jsonResp.getString("services")).length() > 4) {
                                    JSONObject jsonServices = jsonResp.getJSONObject("services");
                                    JSONArray jsonRows = jsonServices.getJSONArray("rows");

                                    for (int j = 0; j < jsonRows.length(); j++){
                                        JSONObject itemServices = jsonRows.getJSONObject(j);
                                        HashMap<String, String> servi = new HashMap<>();
                                        servi.put("service_id",itemServices.getString("service_id"));
                                        servi.put("service",itemServices.getString("service"));
                                        servi.put("amount",itemServices.getString("amount"));
                                        services.add(servi);
                                    }
                                }
                                mdp.setServices(services);

                                ArrayList<String> insurances = new ArrayList<>();
                                if (String.valueOf(jsonResp.getString("insurance")).length() > 4) {
                                    JSONArray jsonInsurance = jsonResp.getJSONArray("insurance");

                                    for (int j = 0; j < jsonInsurance.length(); j++){
                                        JSONObject itemServices = jsonInsurance.getJSONObject(j);
                                        insurances.add(itemServices.getString("insurance"));
                                    }
                                }
                                mdp.setInsurance(insurances);

                                ArrayList<String> ad = new ArrayList<>();
                                if (String.valueOf(jsonResp.getString("advices")).length() > 4) {
                                    JSONArray jsonAdvices = jsonResp.getJSONArray("advices");
                                    for (int m = 0; m < jsonAdvices.length(); m++){
                                        JSONObject itemAdvices = jsonAdvices.getJSONObject(m);
                                        ad.add(itemAdvices.getString("advice_name"));
                                    }
                                }
                                mdp.setAdvices(ad);

                                if (mdp.getEnable().equals("0")){
                                    IVaffiliate.setVisibility(View.GONE);
                                } else {
                                    Picasso.with(ctx).load(R.drawable.icon_affiliate).noFade().into(IVaffiliate);
                                    IVaffiliate.setVisibility(View.VISIBLE);
                                }

                                if (session.profile().get(0) != null){
                                    if (!mdp.getFavorite().equals("0")){
                                        IVaddfavorite.setVisibility(View.GONE);
                                        IVdeletefavorite.setVisibility(View.VISIBLE);
                                    } else {
                                        IVaddfavorite.setVisibility(View.VISIBLE);
                                        IVdeletefavorite.setVisibility(View.GONE);
                                    }
                                } else {
                                    IVaddfavorite.setVisibility(View.VISIBLE);
                                    IVdeletefavorite.setVisibility(View.GONE);
                                }

                                TVmajor.setText(mdp.getMajor());
                                TVuniversity.setText(mdp.getUniversity());
                                TVprofessionalID.setText(Html.fromHtml("<b>"+boostrap.langStrings.get(Constants.ced_p)+"</b>"+mdp.getProfessional_id()));
                                TVgraduationYear.setText(Html.fromHtml("<b>"+boostrap.langStrings.get(Constants.year_p)+"</b>"+mdp.getGraduation()));

                                TVname.setText(Miscellaneous.validate(mdp.getName()));
                                if (!Miscellaneous.validate(mdp.getPicture()).isEmpty()){
                                    Picasso.with(ctx).load(mdp.getPicture()).resize(CIVpicture.getWidth(),CIVpicture.getHeight()).centerCrop().into(CIVpicture);
                                } else {
                                    Picasso.with(ctx).load(R.drawable.blank).resize(CIVpicture.getWidth(),CIVpicture.getHeight()).centerCrop().into(CIVpicture);
                                }
                                RBscore.setRating(Integer.parseInt(mdp.getRanking()));
                                TrustManagerUtil.restoresHttpsUrlSettingsGoogle();
                                seeMapDoctor ();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.doctor_id, idSession);
                    params.put(Constants.patient_id, String.valueOf((session.profile().get(0) != null)? session.profile().get(0).toString() : ""));

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueDoctor.add(request);
        }  else {

        }

    }

    private void setFavoriteDoctor (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAddFav = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.add_favorite, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printAddFac",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject message = jsonObject.getJSONObject("response");
                                favorite_id = jsonObject.getString(Constants.response);
                                IVdeletefavorite.setVisibility(View.VISIBLE);
                                IVaddfavorite.setVisibility(View.GONE);
                                Snackbar.make(view, message.getString("message"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.doctor_id, idSession);
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAddFav.add(request);
        }  else {

        }
    }

    private void deleteFavoriteDoctor (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueDelFav = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.delete_favorite, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printAddFac",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                IVdeletefavorite.setVisibility(View.GONE);
                                IVaddfavorite.setVisibility(View.VISIBLE);
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("favorite_id", mdp.getFavorite());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueDelFav.add(request);
        }  else {

        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.profile_doctor_p));
        setHasOptionsMenu(true);
    }
}
