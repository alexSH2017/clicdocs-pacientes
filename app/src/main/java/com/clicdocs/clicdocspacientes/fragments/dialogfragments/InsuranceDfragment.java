package com.clicdocs.clicdocspacientes.fragments.dialogfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.activity.MyInsuranceActivity;
import com.clicdocs.clicdocspacientes.beans.allinsuranceBean;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InsuranceDfragment extends DialogFragment {

    private SessionManager session;
    private Button BTNchoice, BTNcancel;
    private Spinner SPinsurance;
    private LinearLayout LLform;
    private ProgressBar PBloading;
    private MyInsuranceActivity boostrap;
    private Context ctx;
    private RequestQueue queueInsurance, queueRegister;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view        = inflater.inflate(R.layout.insurances_dialog, container, false);
        LLform      = (LinearLayout) view.findViewById(R.id.LLform);
        PBloading   = (ProgressBar) view.findViewById(R.id.PBloading);
        BTNchoice   = (Button) view.findViewById(R.id.btnChoice);
        BTNcancel   = (Button) view.findViewById(R.id.btnCancel);
        SPinsurance = (Spinner) view.findViewById(R.id.SPinsurance);
        boostrap    = (MyInsuranceActivity) getActivity();
        ctx         = getContext();
        getDialog().setTitle(boostrap.langStrings.get(Constants.insurance_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNchoice.setText(boostrap.langStrings.get(Constants.choice_p));
        getDialog().setCancelable(false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        session = new SessionManager(ctx);
        BTNcancel.setOnClickListener(BTNcancel_onClick);
        BTNchoice.setOnClickListener(BTNchoice_onClick);
        try {
            getAllInsurances();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener BTNcancel_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDialog().dismiss();
        }
    };

    private View.OnClickListener BTNchoice_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setMyInsurance();
        }
    };

    private void setMyInsurance (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueRegister = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_insurance, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResp = jsonObject.getJSONObject(Constants.response);
                                AlertDialog success = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonResp.getString("message"))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                getDialog().dismiss();
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                break;
                            case 202:
                                Snackbar.make(view,jsonObject.getString(Constants.response),Snackbar.LENGTH_SHORT).show();

                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("id_insurance", ((allinsuranceBean) SPinsurance.getSelectedItem()).getId_insurance());
                    params.put("session_id", session.profile().get(0).toString());
                    params.put("user_type", "patient");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueRegister.add(request);
        }  else {

        }
    }

    private void getAllInsurances () throws JSONException {
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put("*");
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueInsurance = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_all_insurance, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    loading.dismiss();
                    PBloading.setVisibility(View.GONE);
                    LLform.setVisibility(View.VISIBLE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonInsuranceAll = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonInsuranceAll.getJSONArray("rows");
                                ArrayList<allinsuranceBean> insurances;
                                insurances = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    insurances.add(new allinsuranceBean(
                                            item.getString("id_insurance"),
                                            item.getString("insurance")
                                    ));
                                }
                                ArrayAdapter<allinsuranceBean> adapter = new ArrayAdapter<allinsuranceBean>(ctx, android.R.layout.simple_list_item_1, insurances);
                                SPinsurance.setAdapter(adapter);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.fields, jsonArray.toString());
                    params.put(Constants.filters, "");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueInsurance.add(request);
        }  else {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        boostrap.getMyInsurance();
    }
}
