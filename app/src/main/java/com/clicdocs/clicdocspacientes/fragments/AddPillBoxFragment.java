package com.clicdocs.clicdocspacientes.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class AddPillBoxFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private EditText ETmedicineName,ETdose, ETduration;
    private TextInputLayout TILmedicine;
    private TextView TVmedicine, TVdose, TVmeasurement, TVduration, TVfrequency;
    private Spinner SPNmeasure, SPNfrequency;
    private MultiAutoCompleteTextView METinstruction;
    private Button BTNaddPill;
    private ProgressDialog loading;
    private View view;
    private RequestQueue queueConfig;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view                = inflater.inflate(R.layout.add_pillbox_fragment,container,false);
        boostrap            = (MainActivity) getActivity();
        ctx                 = getContext();
        TVmedicine          = (TextView) view.findViewById(R.id.TVmedicine);
        TVdose              = (TextView) view.findViewById(R.id.TVdose);
        TVmeasurement       = (TextView) view.findViewById(R.id.TVmeasurement);
        TVduration          = (TextView) view.findViewById(R.id.TVduration);
        TVfrequency         = (TextView) view.findViewById(R.id.TVfrequency);
        ETmedicineName      = (EditText) view.findViewById(R.id.ETmedicineName);
        ETdose              = (EditText) view.findViewById(R.id.ETdose);
        ETduration          = (EditText) view.findViewById(R.id.ETduration);
        SPNmeasure          = (Spinner) view.findViewById(R.id.SPNmeasure);
        SPNfrequency        = (Spinner) view.findViewById(R.id.SPNfrequency);
        METinstruction      = (MultiAutoCompleteTextView) view.findViewById(R.id.METinstructions);
        BTNaddPill          = (Button) view.findViewById(R.id.BTNaddPills);
        session             = new SessionManager(ctx);
        getPillboxConfig ();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVmedicine.setText(boostrap.langStrings.get(Constants.medicine_p));
        TVdose.setText(boostrap.langStrings.get(Constants.dose_p));
        TVmeasurement.setText(boostrap.langStrings.get(Constants.measure_p));
        TVfrequency.setText(boostrap.langStrings.get(Constants.frequency_p));
        TVduration.setText(boostrap.langStrings.get(Constants.duration_p));

        ETmedicineName.setHint(boostrap.langStrings.get(Constants.name_medicine_p));
        ETdose.setHint(boostrap.langStrings.get(Constants.dose_p));
        ETduration.setHint(boostrap.langStrings.get(Constants.duration_p));
        METinstruction.setHint(boostrap.langStrings.get(Constants.note_p));
        BTNaddPill.setText(boostrap.langStrings.get(Constants.add_medicine_p));
        BTNaddPill.setOnClickListener(BTNaddPill_onClic);
    }

    private View.OnClickListener BTNaddPill_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            //Validator validator = new Validator();
            if(!boostrap.validator.validate(ETmedicineName, new String[] {boostrap.validator.REQUIRED}) &&
                    !boostrap.validator.validate(ETdose, new String[] {boostrap.validator.REQUIRED, boostrap.validator.ONLY_NUMBER}) &&
                    !boostrap.validator.validate(ETduration, new String[] {boostrap.validator.REQUIRED})) {
                if(!SPNfrequency.getSelectedItem().toString().equals(Constants.choiceFrequency)) {
                    if (Integer.parseInt(ETduration.getText().toString())>0){
                        doAddPills();
                    }else {
                        AlertDialog errorFrequency = new AlertDialog.Builder(ctx)
                                .setMessage(boostrap.langStrings.get(Constants.error_duration_p))
                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }

                }else {
                    AlertDialog errorFrequency = new AlertDialog.Builder(ctx)
                            .setMessage(boostrap.langStrings.get(Constants.error_frequency_p))
                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
            }
        }
    };

    private void doAddPills (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            RequestQueue queueAddPillbox = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_pillbox, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject resp = jsonObject.getJSONObject("response");
                                final AlertDialog success  = PopUpManager.showDialogMessage(ctx,resp.get("message").toString(), boostrap.langStrings.get(Constants.accept_p), new PopUpManager.DialogMessage() {
                                    @Override
                                    public void OnClickListener(AlertDialog dialog) {
                                        dialog.dismiss();
                                        FragmentManager fm = boostrap.getSupportFragmentManager();
                                        fm.popBackStack(fragments.ADDPILL,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    }
                                }, null, null);
                                success.show();
                                break;
                            case 202:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Log.v("FREQUENCY",String.valueOf(SPNfrequency.getSelectedItemPosition()+1));
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.patient_id, session.profile().get(0).toString());
                    params.put(Constants.name, ETmedicineName.getText().toString());
                    params.put("dose", ETdose.getText().toString());
                    params.put("measurement", ""+(SPNmeasure.getSelectedItemPosition()+1));
                    params.put("frequency", ""+(SPNfrequency.getSelectedItemPosition()+1));
                    params.put("duration", ETduration.getText().toString());
                    params.put("note", METinstruction.getText().toString());
                    params.put("origin", "2");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAddPillbox.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }
    }

    private void getPillboxConfig (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueConfig = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.pillbox_config, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonResp = jsonObject.getJSONArray(Constants.response);
                                JSONObject contentFre = jsonResp.getJSONObject(0);
                                JSONArray jsonFre = contentFre.getJSONArray("fre");
                                ArrayList<KeyPairsBean> fre = new ArrayList<>();
                                for (int i = 0; i < jsonFre.length(); i++) {
                                    JSONObject item = jsonFre.getJSONObject(i);
                                    fre.add(new KeyPairsBean(item.getInt("id_pillbox_frequency"),item.getString("name")));
                                }

                                ArrayAdapter<KeyPairsBean> adapter = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, fre);
                                SPNfrequency.setAdapter(adapter);

                                JSONObject contentMea = jsonResp.getJSONObject(1);
                                JSONArray jsonMea = contentMea.getJSONArray("mea");
                                ArrayList<KeyPairsBean> mea = new ArrayList<>();
                                for (int i = 0; i < jsonMea.length(); i++) {
                                    JSONObject item = jsonMea.getJSONObject(i);
                                    mea.add(new KeyPairsBean(item.getInt("id_pillbox_measure"),item.getString("name")));
                                }

                                ArrayAdapter<KeyPairsBean> adapterMea = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, mea);
                                SPNmeasure.setAdapter(adapterMea);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english");
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueConfig.add(request);
        }  else {

        }
    }
}
