package com.clicdocs.clicdocspacientes.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.KeyPairsBean;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class AddSubordinateFragment extends Fragment {

    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private EditText ETname, ETfname, ETmname, ETbirthdate;
    private TextView TVname, TVfname, TVmname, TVgender, TVbirtdate;
    private Spinner SPgender;
    private Button BTNadd;
    private Calendar myCalendar;
    private ProgressDialog loading;
    private RequestQueue queueAddSub;
    private View view;
    private ArrayList<KeyPairsBean> gender = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.register_subordinate,container,false);
        boostrap    = (MainActivity) getActivity();
        ctx         = getContext();
        session     = new SessionManager(ctx);
        TVname      = (TextView) view.findViewById(R.id.TVname);
        TVfname     = (TextView) view.findViewById(R.id.TVfname);
        TVmname     = (TextView) view.findViewById(R.id.TVmname);
        TVbirtdate  = (TextView) view.findViewById(R.id.TVbirthdate);
        TVgender    = (TextView) view.findViewById(R.id.TVgender);

        ETname      = (EditText) view.findViewById(R.id.ETname);
        ETfname     = (EditText) view.findViewById(R.id.ETfname);
        ETmname     = (EditText) view.findViewById(R.id.ETmname);
        ETbirthdate  = (EditText) view.findViewById(R.id.ETbirthdate);
        SPgender        = (Spinner) view.findViewById(R.id.SPgender);

        BTNadd      = (Button) view.findViewById(R.id.BTNadd);

        TVname.setText(boostrap.langStrings.get(Constants.name_p));
        TVfname.setText(boostrap.langStrings.get(Constants.fname_p));
        TVmname.setText(boostrap.langStrings.get(Constants.mname_p));
        TVbirtdate.setText(boostrap.langStrings.get(Constants.birthday_p));
        TVgender.setText(boostrap.langStrings.get(Constants.gender_p));

        ETname.setHint(boostrap.langStrings.get(Constants.name_p));
        ETfname.setHint(boostrap.langStrings.get(Constants.fname_p));
        ETmname.setHint(boostrap.langStrings.get(Constants.mname_p));
        ETbirthdate.setHint(boostrap.langStrings.get(Constants.birthday_p));

        BTNadd.setText(boostrap.langStrings.get(Constants.add_familiy_p));

        gender.add(new KeyPairsBean(0,boostrap.langStrings.get(Constants.male_p)));
        gender.add(new KeyPairsBean(1,boostrap.langStrings.get(Constants.female_p)));
        ArrayAdapter<KeyPairsBean> genderA = new ArrayAdapter<KeyPairsBean>(ctx, android.R.layout.simple_list_item_1, gender);
        SPgender.setAdapter(genderA);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        myCalendar = Calendar.getInstance();
        BTNadd.setOnClickListener(BTNadd_onClic);

        ETbirthdate.setOnClickListener(ETbirthday_onClick);
    }

    private View.OnClickListener BTNadd_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            if ( session.profile().get(0).equals(session.secondProfile().get(0)))  {
                if(!boostrap.validator.validate(ETname, new String[] {boostrap.validator.REQUIRED,boostrap.validator.ONLY_TEXT}) &&
                        !boostrap.validator.validate(ETfname, new String[] {boostrap.validator.REQUIRED,boostrap.validator.ONLY_TEXT}) &&
                        !boostrap.validator.validate(ETbirthdate, new String[] {boostrap.validator.REQUIRED})){
                    addSubordinate();
                }
            } else {
                AlertDialog success = new AlertDialog.Builder(ctx)
                        .setMessage(boostrap.langStrings.get(Constants.account_main_p))
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }

        }
    };

    private View.OnClickListener ETbirthday_onClick= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Date date = new Date();
            DatePickerDialog dpd = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR,year);
                    myCalendar.set(Calendar.MONTH,monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                    setDatePicked();
                }
            },myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            dpd.getDatePicker().setMaxDate(date.getTime());
            dpd.show();

        }
    };

    private void setDatePicked() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        sdf.setTimeZone(myCalendar.getTimeZone());
        ETbirthdate.setText(sdf.format(myCalendar.getTime()));
    }

    private void addSubordinate(){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAddSub = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.add_family, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject resp = jsonObject.getJSONObject("response");
                                Snackbar.make(view, resp.getString("message"), Snackbar.LENGTH_SHORT).show();
                                FragmentManager fm = boostrap.getSupportFragmentManager();
                                fm.popBackStack(fragments.ADDSUBORDINATE,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("name", ETname.getText().toString());
                    params.put("fname", ETfname.getText().toString());
                    params.put("mname", ETmname.getText().toString());
                    params.put("birthday", ETbirthdate.getText().toString());
                    params.put("gender",String.valueOf(SPgender.getSelectedItemPosition()+1));
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAddSub.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .setAction(boostrap.langStrings.get(Constants.accept_p), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addSubordinate();
                        }
                    })
                    .show();
        }
    }

    public void initView() {
        boostrap.hasTabLayout(new MainActivity.ClicDocs() {
            @Override
            public void initTabLayout(TabLayout Tabs) {
                Tabs.setVisibility(View.GONE);
            }
        });
        boostrap.getSupportActionBar().show();
        boostrap.setTitle(boostrap.langStrings.get(Constants.add_familiy_p));
        setHasOptionsMenu(true);
    }
}
