package com.clicdocs.clicdocspacientes.fragments.pagerfragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterPharmacy;
import com.clicdocs.clicdocspacientes.fragments.ResponseSearchPagerFragment;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.MyCurrentListener;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PharmacyFind;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.vision.text.Text;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ResponsePharmaciesFragment extends Fragment implements ResponseSearchPagerFragment.FragmentLifecycle {

    private MainActivity boostrap;
    private Context ctx;
    private RecyclerView myRecycler;
    private View view;
    private MapView mapView;
    private RecyclerViewAdapterPharmacy adapter;
    private RequestQueue queueSearchPage;
    private ArrayList<PharmacyFind> arrayPharmacies;
    private TextView TVnoResponse;

    private EditText ETsearch;
    private Button BTNsearch;
    private Spinner SPborough, SPstates;
    private Switch SWlocation;
    private String lati;
    private String longi;
    private final static int LOCATION = 312;
    LocationManager locationManager;
    AlertDialog alert = null;
    private  BottomSheetDialog bottomSheetDialog;
    private RequestQueue queueSearch, queueState, queueBorough;
    private SessionManager session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.response_pharmacies_fragment,container,false);
        boostrap        = (MainActivity) getActivity();
        ctx             = getContext();
        session         = new SessionManager(ctx);
        arrayPharmacies = (ArrayList<PharmacyFind>) boostrap.resultSearch.get("pharmacies");
        //Log.v("printSizePharmacy",arrayPharmacies.size()+"");
        TVnoResponse    = (TextView) view.findViewById(R.id.TVnoResponse);
        myRecycler      = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        if (arrayPharmacies.size() == 0){ TVnoResponse.setVisibility(View.VISIBLE); }
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        fillPharmacies(arrayPharmacies);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void fillPharmacies (ArrayList<PharmacyFind> arg){
        adapter = new RecyclerViewAdapterPharmacy(arg, boostrap.langStrings, ctx, new RecyclerViewAdapterPharmacy.Event() {
            @Override
            public void onPager(int i) throws JSONException {
                getPagerPharmacies(i);
            }
        });
        myRecycler.setAdapter(adapter);
    }

    private void getPagerPharmacies (final int page) throws JSONException {
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearchPage = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_pharmacies, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearchPharmacies",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonPharmacies = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonPharmacies.getJSONArray("rows");
                                ArrayList<PharmacyFind> phar = new ArrayList<>();
                                for (int i = 0; i<jsonRows.length(); i++){
                                    PharmacyFind ph = new PharmacyFind();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    ph.setPharmacy_id(item.getString("pharmacy_id"));
                                    ph.setPharmacy_name(item.getString("pharmacy"));
                                    ph.setNetwork(item.getString("network"));
                                    ph.setFull_address(item.getString("full_address"));
                                    ph.setReference(item.getString("reference"));
                                    ph.setPhone(item.getString("phone"));
                                    ph.setPicture(item.getString("picture"));
                                    ph.setStreet(item.getString("street"));
                                    ph.setOutdoor_number(item.getString("outdoor_number"));
                                    ph.setInterior_number(item.getString("interior_number"));
                                    ph.setColony(item.getString("colony"));
                                    ph.setZipcode(item.getString("zipcode"));
                                    ph.setCity(item.getString("city"));
                                    ph.setBorough(item.getString("borough"));
                                    ph.setState(item.getString("state"));
                                    ph.setCountry(item.getString("country"));
                                    ph.setLatitude(item.getString("latitude"));
                                    ph.setLongitude(item.getString("longitude"));
                                    arrayPharmacies.add(ph);
                                    phar.add(ph);
                                }
                                adapter.add(phar);
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", boostrap.jsonPharmacy.toString());
                    params.put("filters", "");
                    params.put("limit",String.valueOf(page));
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearchPage.add(request);
        } else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void initView() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_filters,menu);
        menu.getItem(0).setTitle(boostrap.langStrings.get(Constants.filters_p));
        menu.getItem(1).setTitle(boostrap.langStrings.get(Constants.map_p));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filters:
                try {
                    FilterSearch();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.mapDoctors:
                Bundle b = new Bundle();
                b.putSerializable("pharmacies",arrayPharmacies);
                boostrap.setFragment(fragments.MAPPHARMACIES, b);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void FilterSearch () throws JSONException {
        bottomSheetDialog = new BottomSheetDialog(ctx);
        View parentView     = boostrap.getLayoutInflater().inflate(R.layout.filters_pharmacies_dialog,null);
        ETsearch            = (EditText) parentView.findViewById(R.id.ETsearch);
        SWlocation          = (Switch) parentView.findViewById(R.id.SWlocation);
        SPstates            = (Spinner) parentView.findViewById(R.id.SPstates);
        SPborough           = (Spinner) parentView.findViewById(R.id.SPborough);
        BTNsearch           = (Button) parentView.findViewById(R.id.BTNsearch);
        if (boostrap.active)
            SWlocation.setChecked(true);
        else
            SWlocation.setChecked(false);
        TextInputLayout TILhint    = (TextInputLayout) parentView.findViewById(R.id.TILhint);
        TextView        TVnext     = (TextView) parentView.findViewById(R.id.TVnext_of_me);
        TextView        TVstateT   = (TextView) parentView.findViewById(R.id.TVstateT);
        TextView        TVboroughT = (TextView) parentView.findViewById(R.id.TVboroughT);
        TILhint.setHint(boostrap.langStrings.get(Constants.name_pharmacy_p));
        TVnext.setText(boostrap.langStrings.get(Constants.next_to_me_p));
        TVstateT.setText(boostrap.langStrings.get(Constants.state_p));
        TVboroughT.setText(boostrap.langStrings.get(Constants.borough_p));
        BTNsearch.setText(boostrap.langStrings.get(Constants.search_p));
        bottomSheetDialog.setContentView(parentView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,300,getResources().getDisplayMetrics()));
        bottomSheetDialog.show();

        ETsearch.setText(boostrap.WORD_SEARCH);
        BTNsearch.setOnClickListener(BTNsearch_onClic);
        getStates();

        /*locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            SWlocation.setChecked(false);
        } else {
            SWlocation.setChecked(true);
        }*/

        SWlocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        SWlocation.setChecked(false);
                        AlertNoOps();
                    } else {
                        getCoordenates();
                    }
                }
                else {
                    boostrap.active = false;
                }
            }
        });
    }

    private void AlertNoOps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(boostrap.langStrings.get(Constants.legend_gps_p))
                .setCancelable(false)
                .setPositiveButton(boostrap.langStrings.get(Constants.yes_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION);
                    }
                })
                .setNegativeButton(boostrap.langStrings.get(Constants.not_p), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        SWlocation.setChecked(false);
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private boolean getCoordenates() {
        LocationManager locationManager = (LocationManager) boostrap.getSystemService(Context.LOCATION_SERVICE);
        MyCurrentListener locationListener = new MyCurrentListener(new MyCurrentListener.evento() {
            @Override
            public void onComplete(String latitud, String longitud) {
                lati = latitud;
                longi = longitud;
                Log.v("printLocation",latitud+"   "+longitud);
            }
        });
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return true;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        return true;
    }

    private View.OnClickListener BTNsearch_onClic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (SWlocation.isChecked()){
                if (getCoordenates()){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (lati != null && longi != null){
                                fillJson();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(boostrap.langStrings.get(Constants.legend_fail_coordenates_p))
                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {dialog.cancel();}
                                        });
                                builder.show();
                            }
                        }
                    },3000);
                }
            } else {
                boostrap.lati = "";
                boostrap.longi = "";
                fillJson();
            }

        }
    };

    private void fillJson (){
        boostrap.jsonPharmacy  = new JSONObject();
        try {

            boostrap.jsonPharmacy.put("pharmacy",ETsearch.getText().toString().trim().replaceAll("\n", " "));

            if (SWlocation.isChecked()){
                boostrap.lati = lati;
                boostrap.longi = longi;
                boostrap.jsonPharmacy.put("location",lati+","+longi);
            }
            if (SPstates.getSelectedItemPosition() != 0){
                boostrap.jsonPharmacy.put("state",SPstates.getSelectedItem().toString());
            }
            if (SPborough.getSelectedItemPosition() != 0) {
                boostrap.jsonPharmacy.put("borough",SPborough.getSelectedItem().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Log.v("printPAramsFar",boostrap.jsonPharmacy.toString());
            doSearchWhiteFilters(boostrap.jsonPharmacy);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        bottomSheetDialog.dismiss();
    }

    private void doSearchWhiteFilters(final JSONObject json) throws JSONException {
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_pharmacies, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearchPharmacies", "" + response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonPharmacies = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonPharmacies.getJSONArray("rows");
                                ArrayList<PharmacyFind> phar = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    PharmacyFind ph = new PharmacyFind();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    ph.setPharmacy_id(item.getString("pharmacy_id"));
                                    ph.setPharmacy_name(item.getString("pharmacy"));
                                    ph.setNetwork(item.getString("network"));
                                    ph.setFull_address(item.getString("full_address"));
                                    ph.setReference(item.getString("reference"));
                                    ph.setPhone(item.getString("phone"));
                                    ph.setPicture(item.getString("picture"));
                                    ph.setStreet(item.getString("street"));
                                    ph.setOutdoor_number(item.getString("outdoor_number"));
                                    ph.setInterior_number(item.getString("interior_number"));
                                    ph.setColony(item.getString("colony"));
                                    ph.setZipcode(item.getString("zipcode"));
                                    ph.setCity(item.getString("city"));
                                    ph.setBorough(item.getString("borough"));
                                    ph.setState(item.getString("state"));
                                    ph.setCountry(item.getString("country"));
                                    ph.setLatitude(item.getString("latitude"));
                                    ph.setLongitude(item.getString("longitude"));
                                    phar.add(ph);
                                }
                                adapter.updateSearch(phar);
                                break;
                            case 204:
                                adapter.clear();
                                break;

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", json.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);
        } else {
        }
    }

    private void getStates (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueState = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_states_pharmacies, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRows = jsonObject.getJSONArray(Constants.response);
                                ArrayList<String> states = new ArrayList<>();
                                states.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    states.add(item.getString("state"));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, states);
                                SPstates.setAdapter(adapter);
                                for (int i = 0; i < adapter.getCount(); i++){
                                    if (boostrap.STATE_SEARCH.equals(adapter.getItem(i))){
                                        SPstates.setSelection(i);
                                    }
                                }
                                getAllBorough();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueState.add(request);
        }  else {

        }
    }

    private void getAllBorough (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            queueBorough = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_borough_pharmacies, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseGetBill",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRows = jsonObject.getJSONArray(Constants.response);
                                ArrayList<String> borough = new ArrayList<>();
                                borough.add(boostrap.langStrings.get(Constants.selected_option_p));
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    borough.add(item.getString("borough"));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, borough);
                                SPborough.setAdapter(adapter);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueBorough.add(request);
        }  else {

        }
    }

    @Override
    public void onResumeFragment() {

    }

}
