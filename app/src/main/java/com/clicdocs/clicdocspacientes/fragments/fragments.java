package com.clicdocs.clicdocspacientes.fragments;

public class fragments {

    public final static String LOGIN                 =   "login_fragment";
    public final static String REGISTER              =   "register_fragment";
    public final static String MYDATES               =   "adapter_mydates";
    public final static String LOGOUT                =   "dialog_logoutfragment";
    public final static String FORUM                 =   "adapter_forolist";
    public final static String LINKS                 =   "adapter_linkslist";
    public final static String RATING                =   "dialog_ratinddoctor";
    public final static String RESCHEDULE            =   "reschedule_appoinment_fragment";
    public final static String SUBTHEMES             =   "subtheme";
    public final static String SUBTHEME              =   "subtheme_view_fragment";
    public final static String PILLBOX               =   "adapter_pillslist";
    public final static String ADDPILL               =   "add_pillbox_fragment";
    public final static String ADDSUBTHEME           =   "dialog_addsubtheme";
    public final static String EDITPROFILE           =   "edit_profile_fragment";
    public final static String HISTORYCLINIC         =   "identification_file";
    public final static String LOGINDIALOG           =   "dialog_login";
    public final static String SEARCH                =   "search_fragment";
    public final static String PROFILEDOCTOR         =   "profile_doctor_fragment";
    public final static String VIEWPILLS             =   "dialog_view_pill";
    public final static String SPECIALITY            =   "speciality_doctor";
    public final static String RESPONSE              =   "response_doctors_fragment";
    public final static String SERVICES              =   "adapter_services";
    public final static String SETTINGS              =   "activity_config";
    public final static String FAVORITEDOCOTORS      =   "adapter_doctorlist";
    public final static String FAMILY                =   "adapter_family";
    public final static String ADDSUBORDINATE        =   "register_subordinate";
    public final static String OFFICES               =   "adapter_consulting_room";
    public final static String OFFICEWAITLIST        =   "adapter_office_in_wait";
    public final static String CHANGEPASS            =   "adapter_office_in_wait";
    public final static String MAPDOCTORS            =   "map_doctor_fragment";
    public final static String MAPPHARMACIES         =   "map_pharmaices_fragment";
    public final static String CHAT                  =   "chat_fragment";
    public final static String INSURANCE             =   "insurances_dialog";
}
