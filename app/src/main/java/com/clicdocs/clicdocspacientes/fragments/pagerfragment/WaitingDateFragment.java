package com.clicdocs.clicdocspacientes.fragments.pagerfragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.adapters.RecyclerViewAdapterWaitAppoinment;
import com.clicdocs.clicdocspacientes.beans.ModelAppoinmentWait;
import com.clicdocs.clicdocspacientes.fragments.MyDatesFragment;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WaitingDateFragment extends Fragment implements MyDatesFragment.FragmentLifecycle{

    private TextView TVlegend;
    private View view;
    private AlertDialog DLnoconnection;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RecyclerView myRecycler;
    //private SwipeRefreshLayout mSwipeRefreshLayout;
    private RequestQueue queueWaitAppoinment;
    private MyDatesFragment parent;
    private TextView TVinformation;
    private RecyclerViewAdapterWaitAppoinment adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recycler_appointment, container, false);
        parent          = (MyDatesFragment) getParentFragment();
        boostrap        =   (MainActivity) getActivity();
        ctx             =   getContext();
        session         =   new SessionManager(ctx);
        TVinformation   = (TextView) view.findViewById(R.id.TVinformation);
        myRecycler      = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        myRecycler.setLayoutManager(new LinearLayoutManager(ctx));
        TVlegend = (TextView) view.findViewById(R.id.TVlegend);
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TVlegend.setText(boostrap.langStrings.get(Constants.no_result_p));
        TVinformation.setText(boostrap.langStrings.get(Constants.legend_waiting_appo_p));
        parent.getView().post(new Runnable() {
            @Override
            public void run() {
                getWaitAppoinments();
            }
        });

    }

    private void getWaitAppoinments (){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueWaitAppoinment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.wait_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    //mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printLogin",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                JSONArray jsonAppoinment = jsonObject.getJSONArray(Constants.response);

                                final ArrayList<ModelAppoinmentWait> dates =new ArrayList<ModelAppoinmentWait>();
                                for (int i=0;i<jsonAppoinment.length();i++){
                                    JSONObject item = jsonAppoinment.getJSONObject(i);

                                    dates.add(new ModelAppoinmentWait(item.getString("id_first_doctor"),item.getString("id_first_doctor_office"),item.getString("name")+" "+item.getString("fathers_last_name")+" "+item.getString("mothers_last_name"),item.getString("first_date"),item.getString("first_time"),item.getString("picture"),item.getString("id_second_doctor"),item.getString("id_second_doctor_office"),item.getString("name_two")+" "+item.getString("fathers_last_name_two")+" "+item.getString("mothers_last_name_two"),item.getString("second_date"),item.getString("second_time"),item.getString("picture_two")));
                                }
                                adapter = new RecyclerViewAdapterWaitAppoinment(boostrap,dates, ctx, new RecyclerViewAdapterWaitAppoinment.Event() {
                                    @Override
                                    public void onClicLocation(ModelAppoinmentWait item) {

                                    }

                                    @Override
                                    public void onPager(int page) {

                                    }
                                });
                                myRecycler.setAdapter(adapter);

                                break;
                            case 204:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                break;
                            case 403:
                                Snackbar.make(view, jsonObject.getString("response"), Snackbar.LENGTH_LONG)
                                        .show();
                                break;
                            case 404:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueWaitAppoinment.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void pagerAppointment (final int page){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueWaitAppoinment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.wait_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    //mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.v("printLogin",""+response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                TVlegend.setVisibility(View.GONE);
                                myRecycler.setVisibility(View.VISIBLE);
                                JSONArray jsonAppoinment = jsonObject.getJSONArray(Constants.response);

                                final ArrayList<ModelAppoinmentWait> dates =new ArrayList<ModelAppoinmentWait>();
                                for (int i=0;i<jsonAppoinment.length();i++){
                                    JSONObject item = jsonAppoinment.getJSONObject(i);

                                    dates.add(new ModelAppoinmentWait(item.getString("id_first_doctor"),item.getString("id_first_doctor_office"),item.getString("name")+" "+item.getString("fathers_last_name")+" "+item.getString("mothers_last_name"),item.getString("first_date"),item.getString("first_time"),item.getString("picture"),item.getString("id_second_doctor"),item.getString("id_second_doctor_office"),item.getString("name_two")+" "+item.getString("fathers_last_name_two")+" "+item.getString("mothers_last_name_two"),item.getString("second_date"),item.getString("second_time"),item.getString("picture_two")));
                                }
                                adapter.add(dates);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("limit", ""+page);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueWaitAppoinment.add(request);
        }  else {
            Snackbar.make(view, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onResumeFragment() {
        if (getContext() != null){ getWaitAppoinments(); }
    }
}
