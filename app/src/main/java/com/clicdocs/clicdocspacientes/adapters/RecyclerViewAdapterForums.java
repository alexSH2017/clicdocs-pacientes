package com.clicdocs.clicdocspacientes.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Doctors;
import com.clicdocs.clicdocspacientes.utils.Forums;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapterForums extends RecyclerView.Adapter<RecyclerViewAdapterForums.ViewHolder> {

    public interface Event {
        void onClic(Forums forums);
    }
    private List<Forums> forumsList;
    private Context ctx;
    private Event evento;

    public RecyclerViewAdapterForums(Context ctx,List<Forums> forumsList, Event evento) {
        this.forumsList = forumsList;
        this.ctx = ctx;
        this.evento=evento;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView TVforum;
        private TextView TVnumber;
        private CircleImageView CIVforum;

        public ViewHolder(View itemView) {
            super(itemView);
            CIVforum = (CircleImageView) itemView.findViewById(R.id.CIVforum);
            TVforum = (TextView) itemView.findViewById(R.id.TVforum);
            //TVnumber = (TextView) itemView.findViewById(R.id.TVnumber);
        }

        public void bind (final Forums forums){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    evento.onClic(forums);
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_forum,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Forums forums=forumsList.get(position);
        holder.bind(forums);

        if (forums.getImagen().length() > 0){
            Picasso.with(ctx).load(forums.getImagen())
                    .centerCrop()
                    .noFade()
                    .resize(80, 80)
                    .into(holder.CIVforum);
        }
        holder.TVforum.setText(Miscellaneous.ucFirst(forums.getTheme()));
        ///holder.TVnumber.setText(forums.getTotalSubtheme());

    }

    @Override
    public int getItemCount() {
        return forumsList.size();
    }


}
