package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelDoctorProfile;
import com.clicdocs.clicdocspacientes.beans.ModelServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecyclerViewAdapterServices extends RecyclerView.Adapter<RecyclerViewAdapterServices.ViewHolder> {

    private Context ctx;
    private ArrayList<HashMap<String,String>> serviceList;

    public RecyclerViewAdapterServices(Context ctx, ArrayList<HashMap<String,String>> serviceList) {
        this.ctx = ctx;
        this.serviceList = serviceList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_services,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HashMap<String,String>  service = serviceList.get(position);
        holder.TVservice.setText(service.get("service"));
        holder.TVamount.setText("$"+service.get("amount"));
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVservice, TVamount;
        public ViewHolder(View itemView) {
            super(itemView);
            TVservice = (TextView) itemView.findViewById(R.id.TVservice);
            TVamount  = (TextView) itemView.findViewById(R.id.TVamount);
        }
    }
}
