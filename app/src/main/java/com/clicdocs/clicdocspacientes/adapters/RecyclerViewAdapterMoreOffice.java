package com.clicdocs.clicdocspacientes.adapters;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelOffices;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public class RecyclerViewAdapterMoreOffice extends RecyclerView.Adapter<RecyclerViewAdapterMoreOffice.ViewHolder> {


    private List<ModelOffices> mOffices;
    private Context ctx;
    private MainActivity boostrap;
    private View view_parent;
    private String idsession, name, specialities, picture, enable;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM, yyyy");
    private Calendar myCalendar, myCal, calAux;
    private String fecha;
    private Date date = new Date();
    private int dayWeek;
    private SessionManager session;
    private RecyclerView mRecyclerWaitList;
    private int valueRB;
    private RequestQueue queueAppointment,queueWaitList, queueSchedule;
    private CircleImageView CIVpicture;
    private Event evento;
    private int card;

    public interface Event {
       void setDate (String date) throws ParseException;
    }


    public RecyclerViewAdapterMoreOffice(View view_parent,List<ModelOffices> mOffices, Context ctx, MainActivity boostrap, String idsession, String name, String specialities, String picture, String enable,int dayWeek, String fecha, int card, Event evento) {
        this.view_parent    = view_parent;
        this.mOffices       = mOffices;
        this.ctx            = ctx;
        this.boostrap       = boostrap;
        this.idsession      = idsession;
        this.name           = name;
        this.specialities   = specialities;
        this.picture        = picture;
        this.enable         = enable;
        this.dayWeek        = dayWeek;
        this.fecha          = fecha;
        this.card           = card;
        this.evento         = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_item_more_offices,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        myCalendar = Calendar.getInstance();
        session = new SessionManager(ctx);
        final ModelOffices offices = mOffices.get(position);
        holder.TVname.setText(name);
        holder.TVspeciality.setText(specialities);
        holder.BTNnextEnable.setText(boostrap.langStrings.get(Constants.next_enable_p));
        holder.BTNwait.setText(boostrap.langStrings.get(Constants.waiting_list_p));
        //holder.TVoffice.setText(offices.getName());
        holder.TVamounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog amount = new AlertDialog.Builder(ctx)
                        .setTitle(boostrap.langStrings.get(Constants.cost_p))
                        .setMessage(boostrap.langStrings.get(Constants.f_appointment_p)+": $ " + offices.getFirst_amount() + "\n" +
                                boostrap.langStrings.get(Constants.s_appointment_p)+": $ " + offices.getLater_amount())
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        });

        if (card == 1){
            if (offices.getPay_methods().get(0).equals("1") || offices.getPay_methods().get(1).equals("1")){
                holder.IVvisa_card.setVisibility(View.VISIBLE);
            } else {
                holder.IVvisa_card.setVisibility(View.INVISIBLE);
            }

            if (offices.getPay_methods().get(2).equals("1")){
                holder.IVamex.setVisibility(View.VISIBLE);
            } else {
                holder.IVamex.setVisibility(View.INVISIBLE);
            }
            holder.TVclinic.setVisibility(View.VISIBLE);
            holder.TVclinic.setText(offices.getClinic());

        } else {
            holder.TVclinic.setVisibility(View.GONE);
            holder.IVvisa_card.setVisibility(View.INVISIBLE);
            holder.IVamex.setVisibility(View.INVISIBLE);
        }

        if (enable.equals("1")) {
            holder.BTNwait.setVisibility(View.VISIBLE);
            holder.BTNnextEnable.setVisibility(View.VISIBLE);
        } else {
            holder.BTNwait.setVisibility(View.INVISIBLE);
            holder.BTNnextEnable.setVisibility(View.VISIBLE);
        }
        holder.TVaddress.setText(offices.getFull_address());

        if (picture.length() > 0) {
            Picasso.with(ctx).load(picture)
                    .resize(60, 60)
                    .centerCrop()
                    .noFade()
                    .into(holder.CIVdoctor);
        } else {
            Picasso.with(ctx).load(R.drawable.blank)
                    .resize(60, 60)
                    .noFade()
                    .centerCrop()
                    .into(holder.CIVdoctor);
        }
        if (offices.getOpen_time() != null){
            holder.LLnoHours.setVisibility(View.GONE);
            holder.mRecyclerHour.setVisibility(View.VISIBLE);
            RecyclerViewAdapterSchedules adapter = new RecyclerViewAdapterSchedules(offices.getOpen_time(), offices.getLocked_time(), ctx, new RecyclerViewAdapterSchedules.Event() {
                @Override
                public void onClic(final String hour, View v) {
                    if (session.profile().get(0) != null){
                        if (boostrap.WORD_SEARCH.length() > 0 && session.getFdoctor().get(0) == null){
                            dialogQuestions(holder.TVaddress.getText().toString(),name,fecha,hour,idsession,offices.getOffice_id(),enable,true);

                        } else {
                            if (enable.equals("1")){
                                if (session.getFdoctor().get(0) == null){
                                    dialogQuestions(holder.TVaddress.getText().toString(),name,fecha,hour,idsession,offices.getOffice_id(),enable,true);

                                } else {
                                    dialogSecondMedic(holder.TVaddress.getText().toString(),name,fecha,hour,idsession,offices.getOffice_id(),enable,true);
                                }

                            } else {
                                if (session.getFdoctor().get(0) != null){
                                    dialogQuestions(holder.TVaddress.getText().toString(),name,fecha,hour,idsession,offices.getOffice_id(),enable,false);

                                } else {
                                    dialogQuestions(holder.TVaddress.getText().toString(),name,fecha,hour,idsession,offices.getOffice_id(),enable,false);
                                }
                            }
                        }

                    } else {
                        AlertDialog success = new AlertDialog.Builder(ctx)
                                .setTitle("CLICDOCS")
                                .setMessage(boostrap.langStrings.get(Constants.legend_log_in_required_p))
                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }
                }
            });
            holder.mRecyclerHour.setAdapter(adapter);
        } else {
            holder.LLnoHours.setVisibility(View.VISIBLE);
            holder.mRecyclerHour.setVisibility(View.GONE);
        }

        holder.BTNwait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.profile().get(0) != null){
                    final View         dialog_wait_list     = LayoutInflater.from(ctx).inflate(R.layout.dialog_wait_list, null);
                    final RadioGroup   RBGwaitlist          = (RadioGroup) dialog_wait_list.findViewById(R.id.RBGwaitlist);
                    final RadioButton  RBmatutino           = (RadioButton) dialog_wait_list.findViewById(R.id.RBmatutino);
                    final RadioButton  RBvespertino         = (RadioButton) dialog_wait_list.findViewById(R.id.RBvespertino);
                    final RadioButton  RBambos              = (RadioButton) dialog_wait_list.findViewById(R.id.RBambos);
                    final CheckBox     CBappoinmentPro      = (CheckBox) dialog_wait_list.findViewById(R.id.CBappoinProvi);
                    final ProgressBar  PBloading            = (ProgressBar) dialog_wait_list.findViewById(R.id.PBloading);
                    final LinearLayout LLform               = (LinearLayout) dialog_wait_list.findViewById(R.id.LLform);
                    final LinearLayout LLappoinProvi        = (LinearLayout) dialog_wait_list.findViewById(R.id.LLappoinmentProvisional);
                    final EditText     ETdatePro            = (EditText) dialog_wait_list.findViewById(R.id.ETdatePro);
                    final TextView     TVhourDisable        = (TextView) dialog_wait_list.findViewById(R.id.TVhourDisable);
                    final Button       BTNacccept           = (Button) dialog_wait_list.findViewById(R.id.BTNaccept);
                    final Button       BTNcancel            = (Button) dialog_wait_list.findViewById(R.id.BTNcancel);
                    final TextView     TVlegendTitle        = (TextView) dialog_wait_list.findViewById(R.id.TVlegend);
                    final TextView     TVdateTitle          = (TextView) dialog_wait_list.findViewById(R.id.TVdateTitle);
                    mRecyclerWaitList                       = (RecyclerView) dialog_wait_list.findViewById(R.id.mRecyclerSchedule);
                    mRecyclerWaitList.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));

                    RBmatutino.setText(boostrap.langStrings.get(Constants.morning_p));
                    RBvespertino.setText(boostrap.langStrings.get(Constants.evening_p));
                    RBambos.setText(boostrap.langStrings.get(Constants.both_p));
                    CBappoinmentPro.setText(boostrap.langStrings.get(Constants.schedule_provisional_p));
                    TVdateTitle.setText(boostrap.langStrings.get(Constants.date_p));
                    ETdatePro.setHint(boostrap.langStrings.get(Constants.date_p));
                    TVlegendTitle.setText(boostrap.langStrings.get(Constants.legend_diloag_waiting_list_p));
                    BTNacccept.setText(boostrap.langStrings.get(Constants.accept_p));
                    BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));


                    LLappoinProvi.setVisibility(View.GONE);
                    TVhourDisable.setVisibility(View.GONE);
                    CBappoinmentPro.setEnabled(false);
                    final AlertDialog DLwaitList  = new AlertDialog.Builder(ctx)
                            .setTitle(boostrap.langStrings.get(Constants.waiting_list_p))
                            .setCancelable(false)
                            .setView(dialog_wait_list).show();

                    BTNcancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DLwaitList.dismiss();
                        }
                    });

                    CBappoinmentPro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b){
                                LLappoinProvi.setVisibility(View.VISIBLE);
                                mRecyclerWaitList.setVisibility(View.VISIBLE);
                                BTNacccept.setVisibility(View.GONE);
                                final Calendar cale;
                                cale = Calendar.getInstance();
                                RequestQueue queueNextEnable;
                                if (NetworkUtils.haveNetworkConnection(ctx)) {
                                    queueNextEnable = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                                    StringRequest request = new StringRequest(Request.Method.POST, Constants.get_next_enable, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.v("printNextEnable", "" + response);
                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                                    case 200:
                                                        JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                                        JSONObject jsonToday = jsonResponse.getJSONObject("today");
                                                        ETdatePro.setText(jsonToday.getString("date"));
                                                        ArrayList<String> open_times = new ArrayList<String>();
                                                        ArrayList<String> locked_times = new ArrayList<>();
                                                        JSONArray jsonOpen = jsonToday.getJSONArray("open_times");
                                                        JSONArray jsonLocked = jsonToday.getJSONArray("locked_times");

                                                        for (int l = 0; l < jsonOpen.length(); l++) {
                                                            open_times.add(jsonOpen.get(l).toString());
                                                        }

                                                        for (int m = 0; m < jsonLocked.length(); m++) {
                                                            locked_times.add(jsonLocked.get(m).toString());
                                                        }

                                                        RecyclerViewAdapterHourByOffice adapterSheadule = new RecyclerViewAdapterHourByOffice(open_times, locked_times, ctx, new RecyclerViewAdapterHourByOffice.Event() {
                                                            @Override
                                                            public void onClic(final String hour) {
                                                                AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                                                builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_be_waiting_list_p))
                                                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                                            public void onClick(DialogInterface dialog, int id) {
                                                                                setMeWaitList(offices.getOffice_id(), idsession, String.valueOf(valueRB), ETdatePro.getText().toString(), hour, LLform, PBloading, DLwaitList);
                                                                                dialog.cancel();
                                                                            }
                                                                        })
                                                                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                                            public void onClick(DialogInterface dialog, int id) {
                                                                                dialog.cancel();
                                                                            }
                                                                        });
                                                                builderAmbos.show();
                                                            }
                                                        });
                                                        mRecyclerWaitList.setAdapter(adapterSheadule);

                                                        break;

                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.v("error", error.toString());
                                        }
                                    }) {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            HashMap<String, String> params = new HashMap<>();
                                            params.put("date", "");
                                            params.put("office_id", offices.getOffice_id());
                                            return params;
                                        }

                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            HashMap<String, String> headers = new HashMap<>();
                                            headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                            headers.put(Constants.key_access_token, session.profile().get(5).toString());
                                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                            return headers;
                                        }
                                    };
                                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queueNextEnable.add(request);
                                } else {

                                }
                            } else {
                                mRecyclerWaitList.setAdapter(null);
                                ETdatePro.setText("");
                                mRecyclerWaitList.setVisibility(View.GONE);
                                LLappoinProvi.setVisibility(View.GONE);
                                BTNacccept.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                    RBGwaitlist.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {

                            if (checkedId == R.id.RBmatutino){
                                valueRB = 1;
                            } else if (checkedId == R.id.RBvespertino) {
                                valueRB = 2;
                            } else {
                                valueRB = 3;
                            }
                            if (checkedId > 0 ){
                                CBappoinmentPro.setEnabled(true);
                            }
                        }
                    });

                    ETdatePro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            Calendar fecha = new GregorianCalendar();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            int año = fecha.get(Calendar.YEAR);
                            int mes = fecha.get(Calendar.MONTH);
                            int dia = fecha.get(Calendar.DAY_OF_MONTH);
                            Date d=null;
                            try {
                                d = sdf.parse(dia+"/"+(mes+1)+"/"+año);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            DatePickerDialog dpd= new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                                    myCalendar.set(Calendar.YEAR,year);
                                    myCalendar.set(Calendar.MONTH,monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                    try {
                                        setDatePicked(view,TVhourDisable,LLform,PBloading,DLwaitList);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                    myCalendar.get(Calendar.DAY_OF_MONTH));
                            dpd.getDatePicker().setMinDate(d.getTime());

                            dpd.show();
                        }
                    });
                    BTNacccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int id = RBGwaitlist.getCheckedRadioButtonId();
                            if (RBmatutino.isChecked() || RBvespertino.isChecked() || RBambos.isChecked()){
                                switch (id){
                                    case R.id.RBmatutino:
                                        AlertDialog.Builder builderMatutino = new AlertDialog.Builder(ctx);
                                        builderMatutino.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        setMeWaitList(offices.getOffice_id(),idsession,"1","","",LLform,PBloading,DLwaitList);
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }); builderMatutino.show();
                                        break;
                                    case R.id.RBvespertino:
                                        AlertDialog.Builder builderVespertino = new AlertDialog.Builder(ctx);
                                        builderVespertino.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        setMeWaitList(offices.getOffice_id(),idsession,"2","","",LLform,PBloading,DLwaitList);
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }); builderVespertino.show();
                                        break;
                                    case R.id.RBambos:
                                        AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                        builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        setMeWaitList(offices.getOffice_id(),idsession,"3","","",LLform,PBloading,DLwaitList);
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }); builderAmbos.show();
                                        break;
                                }
                            } else {
                                Toast.makeText(ctx, boostrap.langStrings.get(Constants.selected_option_p),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                    Toast.makeText(ctx,boostrap.langStrings.get(Constants.is_required_login_p),Toast.LENGTH_SHORT).show();
                }
            }
            private void setDatePicked(View v, TextView tvDisable, LinearLayout LLform, ProgressBar PBloading, AlertDialog DLwait) throws JSONException, ParseException {
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                sdf.setTimeZone(myCalendar.getTimeZone());
                String fecha = sdf.format(myCalendar.getTime());
                ((EditText)v).setText(formatDate(fecha));
                getDoctorSchedulesByOffice(offices.getOffice_id(),sdf.format(myCalendar.getTime()),LLform,PBloading,DLwait, tvDisable);

            }

            private void getDoctorSchedulesByOffice (final String idOffice, final String dateAppo, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLwait, final TextView TVdisable) throws JSONException {
                if (NetworkUtils.haveNetworkConnection(ctx)) {
                    final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                    loading.show();
                    queueSchedule = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.offices_time, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("printResponseReschedule", "" + response);
                            loading.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                        JSONArray jsonOpen = jsonResponse.getJSONArray("open_times");
                                        JSONArray jsonLocked = jsonResponse.getJSONArray("locked_times");

                                        ArrayList<String> open_times = new ArrayList<>();
                                        for (int i = 0; i < jsonOpen.length(); i++) {
                                            open_times.add(jsonOpen.get(i).toString());
                                        }
                                        ArrayList<String> locked_time = new ArrayList<>();
                                        for (int j = 0; j < jsonLocked.length(); j++) {
                                            locked_time.add(jsonLocked.get(j).toString());
                                        }
                                        RecyclerViewAdapterHourByOffice adapterSheadule = new RecyclerViewAdapterHourByOffice(open_times, locked_time, ctx, new RecyclerViewAdapterHourByOffice.Event() {

                                            @Override
                                            public void onClic(final String hour) {
                                                AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                                builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_be_waiting_list_p))
                                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                setMeWaitList(idOffice, idsession, String.valueOf(valueRB), dateAppo, hour, LLform, PBloading, DLwait);
                                                                dialog.cancel();
                                                            }
                                                        })
                                                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builderAmbos.show();
                                            }
                                        });
                                        mRecyclerWaitList.setAdapter(adapterSheadule);
                                        break;
                                    default:
                                        AlertDialog success = new AlertDialog.Builder(ctx)
                                                .setMessage(jsonObject.getString(Constants.response))
                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        mRecyclerWaitList.setAdapter(null);
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();
                                        break;

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("office_id", idOffice);
                            params.put("date", dateAppo);
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Constants.key_access_token, session.profile().get(5).toString());
                            headers.put(Constants.key_bundle_id, Constants.bundle_id);
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueSchedule.add(request);
                } else {

                }
            }
        });

        holder.BTNnextEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cale;
                cale = Calendar.getInstance();
                RequestQueue queueNextEnable;
                if (NetworkUtils.haveNetworkConnection(ctx)) {
                    queueNextEnable = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.get_next_enable, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("printNextEnable",""+response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                    case 200:
                                        JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                        JSONObject jsonToday = jsonResponse.getJSONObject("today");
                                        Log.v("printNextDate",jsonToday.getString("date"));
                                        Date strDate = sdf.parse(jsonToday.getString("date"));
                                        cale.setTime(strDate);
                                        evento.setDate(jsonToday.getString("date"));
                                        searchByDate(jsonToday.getString("date"));
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("date", fecha);
                            params.put("office_id", offices.getOffice_id());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Constants.key_bundle_id, Constants.bundle_id);
                            headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                            return headers;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queueNextEnable.add(request);
                } else {

                }
            }
        });

    }

    //--------------------------------------------------------------------------------------------------
    private void setMeWaitList (final String idOffice, final String doctor_id, final String turno, final String dateW, final String timeW, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLwait){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueWaitList = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_wait_list, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAppoinment",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject message = jsonObject.getJSONObject(Constants.response);
                                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                builder.setMessage(message.getString("message"))
                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }); builder.show();
                                DLwait.dismiss();
                                session.deleteFirstDoctor();
                                break;
                            case 202:
                                AlertDialog.Builder builderError = new AlertDialog.Builder(ctx);
                                builderError.setMessage(jsonObject.getString("response"))
                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                            public void onClick(DialogInterface dialog, int id) {
                                                DLwait.dismiss();
                                                session.deleteFirstDoctor();
                                                dialog.cancel();
                                            }
                                        }); builderError.show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("office_id", idOffice);
                    params.put("doctor_id", doctor_id);
                    params.put("turno", turno);
                    params.put("date", dateW);
                    params.put("time", timeW);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueWaitList.add(request);
        }  else {

        }
    }

    //--------------------------------------------------------------------------------------------------

    //===================== NUEVO DIALOGO PARA PREGUNTAR SI ES PRIMERA O SEGUNDA CITA ===============================
    private void dialogQuestions (final String address,final String name, final String day, final String hour, final String doctor_id, final String office_id, final String enable, final boolean flag){
        final View      dialog_questions        = LayoutInflater.from(ctx).inflate(R.layout.dialog_one_doctor_selected, null);
        TextView        TVmessage               = (TextView)   dialog_questions.findViewById(R.id.TVmessage);
        Button          BTNfirst_op             = (Button)     dialog_questions.findViewById(R.id.BTNf_appo);
        Button          BTNsecond_op            = (Button)     dialog_questions.findViewById(R.id.BTNs_appo);
        Button          BTNcancel               = (Button)     dialog_questions.findViewById(R.id.BTNcancel);

        BTNfirst_op.setText(boostrap.langStrings.get(Constants.yes_first_p));
        BTNsecond_op.setText(boostrap.langStrings.get(Constants.not_sub_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.not_want_appo_p));
        TVmessage.setText(boostrap.langStrings.get(Constants.legend_schedule_one_p)
                + "\n" + name
                + "\n" + boostrap.langStrings.get(Constants.legend_schedule_two_p) + formatDate(day)
                + "\n" + boostrap.langStrings.get(Constants.legend_schedule_three_p) + hour+" Hr(s)");

        final AlertDialog DLoption   = new AlertDialog.Builder(ctx)
                .setTitle("CLICDOCS")
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_questions).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DLoption.dismiss();
            }
        });

        BTNfirst_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                if (flag){
                    data.add(name);
                    data.add(address);
                    data.add(doctor_id);
                    data.add(office_id);
                    data.add(day);
                    data.add(hour);
                    data.add(enable);
                    data.add("1");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    dialogReason(data, DLoption);
                } else {
                    if (enable.equals("0")){
                        if (session.getFdoctor().get(0) != null){
                            data.add(session.getFdoctor().get(1).toString());
                            data.add(session.getFdoctor().get(7).toString());
                            data.add(session.getFdoctor().get(0).toString());
                            data.add(session.getFdoctor().get(4).toString());
                            data.add(session.getFdoctor().get(2).toString());
                            data.add(session.getFdoctor().get(3).toString());
                            data.add(session.getFdoctor().get(5).toString());
                            data.add(session.getFdoctor().get(6).toString());
                            data.add(name);
                            data.add(address);
                            data.add(doctor_id);
                            data.add(office_id);
                            data.add(day);
                            data.add(hour);
                            data.add(enable);
                            data.add("1");
                            dialogReason(data,DLoption);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                            builder.setMessage(boostrap.langStrings.get(Constants.legend_no_affiliate_p))
                                    .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                    .setIcon(R.drawable.ic_warning)
                                    .setCancelable(false)
                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                        public void onClick(DialogInterface dialog, int id) {
                                            session.firstDoctor(doctor_id,name,day,hour,office_id,enable,"1",address);
                                            DLoption.dismiss();
                                            //session.firstDoctor(doctors.getId_session(),doctors.getName(),fecha,hour,doctors.getMyoffices().get(0).getOffice_id(),doctors.getEnable(),doctors.getPicture());
                                            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);
                                            View customView = inflater.inflate(R.layout.popup_window_medic, null);

                                            boostrap.myPopup = new PopupWindow(customView,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT);

                                            CIVpicture = (CircleImageView) customView.findViewById(R.id.CIVdoctorMedic);

                                            Picasso.with(ctx).load(R.drawable.blank).noFade().resize(100, 100).centerCrop().into(CIVpicture);

                                            boostrap.myPopup.showAsDropDown(view_parent, view_parent.getWidth() - 10, 10);

                                            customView.setOnTouchListener(new View.OnTouchListener() {
                                                int myX, myY;
                                                int touchedX, touchedY;
                                                @Override
                                                public boolean onTouch(View view, MotionEvent event) {
                                                    switch (event.getAction()){
                                                        case MotionEvent.ACTION_DOWN:
                                                            myX = (int) event.getX();
                                                            myY = (int) event.getY();
                                                            break;
                                                        case MotionEvent.ACTION_MOVE:
                                                            touchedX = (int) event.getRawX() - myX;
                                                            touchedY = (int) event.getRawY() - myY;

                                                            boostrap.myPopup.update(touchedX,touchedY,-1,-1,true);
                                                            break;
                                                    }
                                                    return false;
                                                }
                                            });

                                            CIVpicture.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    boostrap.ShowDoctorSelected();
                                                }
                                            });
                                            dialog.cancel();
                                        }
                                    }); builder.show();
                        }
                    }

                }
            }
        });

        BTNsecond_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                if (flag){
                    if (session.getFdoctor().get(0) == null){
                        data.add(name);
                        data.add(address);
                        data.add(doctor_id);
                        data.add(office_id);
                        data.add(day);
                        data.add(hour);
                        data.add(enable);
                        data.add("2");
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                        dialogReason(data, DLoption);
                    } else {
                        data.add(session.getFdoctor().get(1).toString());
                        data.add(session.getFdoctor().get(7).toString());
                        data.add(session.getFdoctor().get(0).toString());
                        data.add(session.getFdoctor().get(4).toString());
                        data.add(session.getFdoctor().get(2).toString());
                        data.add(session.getFdoctor().get(3).toString());
                        data.add(session.getFdoctor().get(5).toString());
                        data.add(session.getFdoctor().get(6).toString());
                        data.add(name);
                        data.add(address);
                        data.add(doctor_id);
                        data.add(office_id);
                        data.add(day);
                        data.add(hour);
                        data.add(enable);
                        data.add("2");
                        dialogReason(data,DLoption);
                    }

                } else {
                    if (enable.equals("0")){
                        if (session.getFdoctor().get(0) != null){
                            data.add(session.getFdoctor().get(1).toString());
                            data.add(session.getFdoctor().get(7).toString());
                            data.add(session.getFdoctor().get(0).toString());
                            data.add(session.getFdoctor().get(4).toString());
                            data.add(session.getFdoctor().get(2).toString());
                            data.add(session.getFdoctor().get(3).toString());
                            data.add(session.getFdoctor().get(5).toString());
                            data.add(session.getFdoctor().get(6).toString());
                            data.add(name);
                            data.add(address);
                            data.add(doctor_id);
                            data.add(office_id);
                            data.add(day);
                            data.add(hour);
                            data.add(enable);
                            data.add("2");
                            dialogReason(data,DLoption);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                            builder.setMessage(boostrap.langStrings.get(Constants.legend_no_affiliate_p))
                                    .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                    .setIcon(R.drawable.ic_warning)
                                    .setCancelable(false)
                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                                        public void onClick(DialogInterface dialog, int id) {
                                            session.firstDoctor(doctor_id,name,day,hour,office_id,enable,"2",address);
                                            DLoption.dismiss();
                                            //session.firstDoctor(doctors.getId_session(),doctors.getName(),fecha,hour,doctors.getMyoffices().get(0).getOffice_id(),doctors.getEnable(),doctors.getPicture());
                                            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);
                                            View customView = inflater.inflate(R.layout.popup_window_medic, null);

                                            boostrap.myPopup = new PopupWindow(customView,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT);

                                            CIVpicture = (CircleImageView) customView.findViewById(R.id.CIVdoctorMedic);

                                            Picasso.with(ctx).load(R.drawable.blank).noFade().resize(100, 100).centerCrop().into(CIVpicture);

                                            boostrap.myPopup.showAsDropDown(view_parent, view_parent.getWidth() - 10, 10);

                                            customView.setOnTouchListener(new View.OnTouchListener() {
                                                int myX, myY;
                                                int touchedX, touchedY;
                                                @Override
                                                public boolean onTouch(View view, MotionEvent event) {
                                                    switch (event.getAction()){
                                                        case MotionEvent.ACTION_DOWN:
                                                            myX = (int) event.getX();
                                                            myY = (int) event.getY();
                                                            break;
                                                        case MotionEvent.ACTION_MOVE:
                                                            touchedX = (int) event.getRawX() - myX;
                                                            touchedY = (int) event.getRawY() - myY;

                                                            boostrap.myPopup.update(touchedX,touchedY,-1,-1,true);
                                                            break;
                                                    }
                                                    return false;
                                                }
                                            });

                                            CIVpicture.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    boostrap.ShowDoctorSelected();
                                                }
                                            });
                                            dialog.cancel();
                                        }
                                    }); builder.show();
                        }
                    }
                }

            }
        });
    }
    //=================================================================================================================

    //=========================== DIALOG SEGUNDO MEDICO =====================================================
  //AQUI
    private void dialogSecondMedic (final String address, final String name, final String day, final String hour, final String doctor_id, final String office_id, final String enable, final boolean flag){
        final View      dialog_questions        = LayoutInflater.from(ctx).inflate(R.layout.dialog_one_doctor_selected, null);
        TextView        TVmessage               = (TextView)   dialog_questions.findViewById(R.id.TVmessage);
        Button          BTNfirst_op             = (Button)     dialog_questions.findViewById(R.id.BTNf_appo);
        Button          BTNsecond_op            = (Button)     dialog_questions.findViewById(R.id.BTNs_appo);
        Button          BTNcancel               = (Button)     dialog_questions.findViewById(R.id.BTNcancel);

        BTNfirst_op.setText(boostrap.langStrings.get(Constants.yes_first_p));
        BTNsecond_op.setText(boostrap.langStrings.get(Constants.not_sub_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.not_want_appo_p));

        TVmessage.setText(boostrap.langStrings.get(Constants.legend_schedule_one_p)
                + "\n" + name
                + "\n" + boostrap.langStrings.get(Constants.legend_schedule_two_p) + formatDate(day)
                + "\n" + boostrap.langStrings.get(Constants.legend_schedule_three_p) + hour+" Hr(s)");
        final AlertDialog DLoption   = new AlertDialog.Builder(ctx)
                .setTitle("CLICDOCS")
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_questions).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DLoption.dismiss();
            }
        });

        BTNfirst_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(session.getFdoctor().get(1).toString());
                data.add(session.getFdoctor().get(7).toString());
                data.add(session.getFdoctor().get(0).toString());
                data.add(session.getFdoctor().get(4).toString());
                data.add(session.getFdoctor().get(2).toString());
                data.add(session.getFdoctor().get(3).toString());
                data.add(session.getFdoctor().get(5).toString());
                data.add(session.getFdoctor().get(6).toString());
                data.add(name);
                data.add(address);
                data.add(doctor_id);
                data.add(office_id);
                data.add(day);
                data.add(hour);
                data.add(enable);
                data.add("1");
                dialogChoiceDoctor(data,DLoption);
            }
        });

        BTNsecond_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(session.getFdoctor().get(1).toString());
                data.add(session.getFdoctor().get(7).toString());
                data.add(session.getFdoctor().get(0).toString());
                data.add(session.getFdoctor().get(4).toString());
                data.add(session.getFdoctor().get(2).toString());
                data.add(session.getFdoctor().get(3).toString());
                data.add(session.getFdoctor().get(5).toString());
                data.add(session.getFdoctor().get(6).toString());
                data.add(name);
                data.add(address);
                data.add(doctor_id);
                data.add(office_id);
                data.add(day);
                data.add(hour);
                data.add(enable);
                data.add("2");
                dialogChoiceDoctor(data,DLoption);


            }
        });
    }
    //=======================================================================================================
//==================== DIALOGO PARA PREGUNTAR SI DESEA AGENDAR CON EL AFFILIADO ==================================================
    private void dialogChoiceDoctor(final ArrayList<String> appo, AlertDialog DL){
        View dialog_affiliate           = LayoutInflater.from(ctx).inflate(R.layout.dialog_choice_doctor, null);
        TextView        TVlegend        = (TextView)        dialog_affiliate.findViewById(R.id.TVmessage);
        Button          BTNone          = (Button)     dialog_affiliate.findViewById(R.id.BTNoneDoctor);
        Button          BTNtwo          = (Button)     dialog_affiliate.findViewById(R.id.BTNtwoDoctor);

        BTNone.setText(boostrap.langStrings.get(Constants.if_first_doctor_p));
        BTNtwo.setText(boostrap.langStrings.get(Constants.no_second_option_p));

        DL.dismiss();
        TVlegend.setText(boostrap.langStrings.get(Constants.legend_sec_affiliate_p));
        //TVlegend.setText("Este médico se encuentra afiliado y no es necesario que apruebe la cita, ¿desea que se convierta en su primer opción?");
        final AlertDialog DLchoice   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.selected_option_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_affiliate).show();

        BTNone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(appo.get(8));
                data.add(appo.get(9));
                data.add(appo.get(10));
                data.add(appo.get(11));
                data.add(appo.get(12));
                data.add(appo.get(13));
                data.add(appo.get(14));
                data.add(appo.get(15));
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                dialogReason(data,DLchoice);
            }
        });

        BTNtwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReason(appo,DLchoice);
            }
        });
    }

    //==================================================================================================================
    //========================== DIALOGO RAZON PARA AGENDAR ====================================================
    private void dialogReason (final ArrayList<String > appointment, AlertDialog DL){
        View                  dialog_reason           = LayoutInflater.from(ctx).inflate(R.layout.dialog_reason_appointment, null);
        final EditText        ETreason                = (EditText)   dialog_reason.findViewById(R.id.ETreason);
        Button                BTNschedule             = (Button)     dialog_reason.findViewById(R.id.BTNschedule);
        Button                BTNcancel               = (Button)     dialog_reason.findViewById(R.id.BTNcancel);

        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNschedule.setText(boostrap.langStrings.get(Constants.schedule_p));
        ETreason.setHint(boostrap.langStrings.get(Constants.reason_appo_p));

        DL.dismiss();
        final AlertDialog DLreason   = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.schedule_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_reason).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boostrap.myPopup != null){
                    boostrap.myPopup.dismiss();
                }
                session.deleteFirstDoctor();
                DLreason.dismiss();
            }
        });

        BTNschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appointment.add(ETreason.getText().toString());
                setAppoinment(appointment,DLreason);
            }
        });
    }
    //==========================================================================================================


    private void searchByDate (final String DATE_CURRENT){
        RequestQueue queueOffices;
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueOffices = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_offices, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONArray jsonRes = jsonObject.getJSONArray(Constants.response);
                                ArrayList<ModelOffices> offices = new ArrayList<>();

                                for (int i = 0; i < jsonRes.length(); i++){
                                    JSONObject item = jsonRes.getJSONObject(i);
                                    ModelOffices off = new ModelOffices();
                                    off.setOffice_id(item.getString("id"));
                                    off.setOffice(item.getString("office"));
                                    off.setCountry(item.getString("country"));
                                    off.setState(item.getString("state"));
                                    off.setBorough(item.getString("borough"));
                                    off.setCity(item.getString("city"));
                                    off.setStreet(item.getString("street"));
                                    off.setNum_ext(item.getString("outdoor_number"));
                                    off.setNum_int(item.getString("interior_number"));
                                    off.setColony(item.getString("colony"));
                                    off.setZipcode(item.getString("zipcode"));
                                    off.setLatitude(item.getString("latitude"));
                                    off.setLongitude(item.getString("longitude"));

                                    ArrayList<String> open_times = new ArrayList<String>();
                                    ArrayList<String> locked_times = new ArrayList<>();
                                    JSONObject jsonTime = item.getJSONObject("times");

                                    if (!DATE_CURRENT.equals("null")){
                                        JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                        JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                        JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                        for(int l = 0; l < jsonOpen.length(); l++) {
                                            open_times.add(jsonOpen.get(l).toString());
                                        }

                                        off.setOpen_time(open_times);
                                        for(int m = 0; m < jsonLocked.length(); m++) {
                                            locked_times.add(jsonLocked.get(m).toString());
                                        }
                                        off.setLocked_time(locked_times);
                                    }
                                    offices.add(off);
                                }
                                updateByDate(offices,DATE_CURRENT);
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.doctor_id, idsession);
                    params.put("date", DATE_CURRENT);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueOffices.add(request);
        }  else {

        }

    }

    //==================================================================================================================

    //==================== EVENTO PARA REGISTRAR CITA ==============================================================================
    private void setAppoinment (final ArrayList<String> data, final AlertDialog DLquestion){
        Log.v("printArray",""+data);
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppointment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponseAppoinment",""+response);
                    DLquestion.dismiss();
                    loading.dismiss();
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResp = jsonObject.getJSONObject(Constants.response);

                                if (boostrap.myPopup != null){
                                    boostrap.myPopup.dismiss();
                                }

                                if (data.get(6).equals("1")){
                                    //======= FALTAN PARAMETROS ========
                                    saveAppoinmentCalendar(data.get(5), data.get(4),data.get(0),data.get(1));
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        JSONObject message = null;
                                        try {
                                            message = jsonObject.getJSONObject("response");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            AlertDialog success = new AlertDialog.Builder(ctx)
                                                    .setMessage(message.getString("message"))
                                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            session.deleteFirstDoctor();
                                                            FragmentManager fm = boostrap.getSupportFragmentManager();
                                                            fm.popBackStack(fragments.RESPONSE,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                            boostrap.setFragment(fragments.MYDATES, null);
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).show();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },2000);
                                break;
                            case 202:
                                session.deleteFirstDoctor();
                                break;
                            case 404:
                                if (boostrap.myPopup != null){
                                    boostrap.myPopup.dismiss();
                                }
                                session.deleteFirstDoctor();
                                Snackbar.make(view_parent, jsonObject.getString("response"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("fdoctor_id", data.get(2));
                    params.put("foffice_id", data.get(3));
                    params.put("fdate", data.get(4));
                    params.put("ftime", data.get(5));
                    params.put("appointment_num", data.get(7));
                    params.put("sdoctor_id", data.get(10));
                    params.put("soffice_id", data.get(11));
                    params.put("sdate", data.get(12));
                    params.put("stime", data.get(12));
                    params.put("s_appointment_num", data.get(15));
                    params.put("reason", data.get(16));
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppointment.add(request);
        }  else {

        }
    }
    //===================================================================================================================================
    //======================  GUARDAR EVENTO EN EL CALENDARIO =======================================
    private boolean saveAppoinmentCalendar (String hourR, String fechaA, String name, String address){
        boolean ban = false;
        if (true){
            long calID = 2;
            long startMillis = 0;
            long endMillis = 0;
            String [] separedDate = fechaA.split("-");
            String [] separedHour = hourR.split(":");
            int year = Integer.parseInt(separedDate[0]);
            int month = Integer.parseInt(separedDate[1]);
            int day = Integer.parseInt(separedDate[2]);
            int hour = Integer.parseInt(separedHour[0]);
            int minute = Integer.parseInt(separedHour[1]);
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(year, (month-1), day, hour, minute);
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(year, (month-1), day, hour, minute+5);
            endMillis = endTime.getTimeInMillis();

            ContentResolver cr = boostrap.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.TITLE, "Cita al médico");
            values.put(CalendarContract.Events.DESCRIPTION, "Cita con el médico "+name+", a las "+hourR+"hrs, ubicado en "+address);
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Mexico_City");

            if (ActivityCompat.checkSelfPermission(boostrap, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(boostrap,"Se requiere de permisos",Toast.LENGTH_SHORT).show();
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            long eventID = Long.parseLong(uri.getLastPathSegment());

            ContentValues reminders = new ContentValues();
            reminders.put(CalendarContract.Reminders.EVENT_ID, eventID);
            reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            reminders.put(CalendarContract.Reminders.MINUTES, 120);
            Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);
            ban = true;
        }

        return ban;
    }

    //============== FORMATO DE FECHA MMM dd, yyyy   ==========================================
    private String formatDate(final String fecha) {
        calAux = Calendar.getInstance();
        String forDate = "";
        Date strDate = null;
        try {
            strDate = sdf.parse(fecha);
            calAux.setTime(strDate);
            forDate = formatDate.format(calAux.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return forDate;
    }
    //=========================================================================================

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int getItemCount() {
        return mOffices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView TVname;
        private TextView TVspeciality;
        private TextView TVaddress;
        private TextView TVclinic;
        private TextView TVamounts;
        private CircleImageView CIVdoctor;
        private RecyclerView mRecyclerHour;
        private Button BTNnextEnable;
        private Button BTNwait;
        private ImageView IVvisa_card, IVamex;
        private LinearLayout LLnoHours;
        public ViewHolder(View itemView) {
            super(itemView);

            TVname          = (TextView)itemView.findViewById(R.id.TVname);
            TVspeciality    = (TextView) itemView.findViewById(R.id.TVspeciality);
            TVaddress       = (TextView) itemView.findViewById(R.id.TVaddresOffice);
            TVclinic        = (TextView) itemView.findViewById(R.id.TVclinic);
            TVamounts       = (TextView) itemView.findViewById(R.id.TVamounts);
            CIVdoctor       = (CircleImageView) itemView.findViewById(R.id.CVdoctor);
            BTNnextEnable   = (Button) itemView.findViewById(R.id.BTNnextEnable);
            BTNwait         = (Button) itemView.findViewById(R.id.BTNwait_list);
            IVvisa_card     = (ImageView) itemView.findViewById(R.id.IVvisa_master);
            IVamex          = (ImageView)   itemView.findViewById(R.id.IVamex);
            mRecyclerHour   = (RecyclerView) itemView.findViewById(R.id.mRecyclerSchedule);
            LLnoHours       = (LinearLayout) itemView.findViewById(R.id.LLnoHours);
            mRecyclerHour.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));
        }
    }

    public void updateByDate (ArrayList<ModelOffices> off, String fecha){
        mOffices.clear();
        this.mOffices = off;
        this.fecha = fecha;
        notifyDataSetChanged();
    }

    public void clear (){
        mOffices.clear();
        notifyDataSetChanged();
    }
}
