package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.PharmacyFind;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapterPharmacy extends RecyclerView.Adapter<RecyclerViewAdapterPharmacy.ViewHolder> {

    private List<PharmacyFind> listPharmacy;
    private Context ctx;
    private int page = 0;
    private Event evento;
    private HashMap <String, String> langString;

    public interface Event{
        void onPager(int i) throws JSONException;
    }


    public RecyclerViewAdapterPharmacy(List<PharmacyFind> listPharmacy, HashMap <String, String> langString, Context ctx, Event evento) {
        this.listPharmacy = listPharmacy;
        this.langString = langString;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_new_pharmacies,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PharmacyFind pharmacy = listPharmacy.get(position);
        if (position == (listPharmacy.size()-1)){
            int aux = listPharmacy.size()/20;
            if (aux != page){
                page = aux;
                try {
                    evento.onPager(page);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (pharmacy.getPicture().length() > 0){
            Picasso.with(ctx).load(pharmacy.getPicture())
                    .resize(60, 60)
                    .centerCrop()
                    .noFade()
                    .into(holder.IVpharmacy);
        } else {
            Picasso.with(ctx).load(R.drawable.icon_pharmacy)
                    .resize(60, 60)
                    .noFade()
                    .centerCrop()
                    .into(holder.IVpharmacy);
        }

        holder.TVfullAddress.setText(pharmacy.getFull_address());

        holder.TVpharmacy_name.setText(pharmacy.getPharmacy_name());
        String num_int = (!pharmacy.getReference().isEmpty())? pharmacy.getInterior_number():langString.get(Constants.without_number_p);
        holder.TVreference.setText(Html.fromHtml("<b>Núm. Int.:</b> "+num_int));
        holder.TVtelephone.setText(Html.fromHtml("<b>Tel.</b> "+pharmacy.getPhone()));
        holder.TVcadena.setText(pharmacy.getNetwork());
    }

    @Override
    public int getItemCount() {
        return listPharmacy.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView IVpharmacy;
        private TextView TVcadena, TVfullAddress, TVpharmacy_name, TVreference, TVtelephone;
        public ViewHolder(View itemView) {
            super(itemView);
            IVpharmacy          = (ImageView) itemView.findViewById(R.id.IVpharmacy);
            TVcadena            = (TextView) itemView.findViewById(R.id.TVcadena);
            TVfullAddress       = (TextView) itemView.findViewById(R.id.TVfullAddress);
            TVreference         = (TextView) itemView.findViewById(R.id.TVreference);
            TVpharmacy_name     = (TextView) itemView.findViewById(R.id.TVpharmacy_name);
            TVtelephone         = (TextView) itemView.findViewById(R.id.TVtelephone);
        }
    }

    public void add(ArrayList<PharmacyFind> f) {
        for (int i = 0; i<f.size(); i++){
            listPharmacy.add(f.get(i));
        }
        notifyDataSetChanged();
    }

    public void updateSearch(ArrayList<PharmacyFind> pham) {
        listPharmacy.clear();
        this.listPharmacy = pham;
        notifyDataSetChanged();
    }

    public void clear() {
        listPharmacy.clear();
        notifyDataSetChanged();
    }
}
