package com.clicdocs.clicdocspacientes.adapters;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public class RecyclerViewAdapterDoctors extends RecyclerView.Adapter<RecyclerViewAdapterDoctors.ViewHolder> {

    private List<DoctorSearch> mDoctors;
    private Context ctx;
    private MainActivity boostrap;
    private View view_parent;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM, yyyy");
    private Calendar myCalendar, calAux;
    private String fecha;
    private Event evento;
    private SessionManager session;
    private int valueRB;
    private RecyclerView mRecyclerWaitList;
    private RequestQueue queueAppoinment, queueSchedule;
    private int page = 0;
    private CircleImageView CIVpictureDoctor;
    private HashMap<String, String> langString;

    public interface Event {
        void onClicViewProfile(String idSession);
        void onClicOffice(String idSession, String name, String speciality, String picture, String enable);
        void setDate(String date) throws ParseException;
        void onPager(int i);
        void onClicShare(String pic, String id);
    }

    public RecyclerViewAdapterDoctors(View view_parent, List<DoctorSearch> mDoctors, Context ctx, MainActivity boostrap, String fecha, Event evento) {
        this.view_parent = view_parent;
        this.mDoctors = mDoctors;
        this.ctx = ctx;
        this.session = new SessionManager(ctx);
        this.boostrap = boostrap;
        this.evento = evento;
        this.fecha = fecha;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout LLprofile;
        private RelativeLayout RLprofile;
        private LinearLayout LLshow;
        private TextView TVname;
        private TextView TVoffice;
        private TextView TVnumberFriends;
        private TextView TVspecialities;
        private TextView TVaddressOffice;
        private TextView TVamounts;
        private TextView TVno_office;
        private ImageView IBshare;
        private ImageView IBfriends;
        private Button BTNwait_list;
        private Button BTNnextEnable;
        private Button BTNmoreOffice;
        private ProgressBar PBloading;
        private CircleImageView CIVdoctor;
        private LinearLayout LLnoHours;
        private ImageView IVaffiliate;

        private RecyclerView mRecyclerHour;

        public ViewHolder(View itemView) {
            super(itemView);
            PBloading           = (ProgressBar) itemView.findViewById(R.id.PBloading);
            LLprofile           = (LinearLayout) itemView.findViewById(R.id.LLprofile);
            RLprofile           = (RelativeLayout) itemView.findViewById(R.id.RLprofile);
            TVname              = (TextView) itemView.findViewById(R.id.TVname);
            IVaffiliate         = (ImageView) itemView.findViewById(R.id.IVaffilate);
            TVnumberFriends     = (TextView) itemView.findViewById(R.id.TVnumberFriends);
            BTNwait_list        = (Button) itemView.findViewById(R.id.BTNwait_list);
            LLshow              = (LinearLayout) itemView.findViewById(R.id.LLshow);
            TVno_office         = (TextView) itemView.findViewById(R.id.TVno_office);
            TVspecialities      = (TextView) itemView.findViewById(R.id.TVspeciality);
            TVaddressOffice     = (TextView) itemView.findViewById(R.id.TVaddresOffice);
            CIVdoctor           = (CircleImageView) itemView.findViewById(R.id.CVdoctor);
            TVoffice            = (TextView) itemView.findViewById(R.id.TVoffice);
            TVamounts           = (TextView) itemView.findViewById(R.id.TVamounts);
            IBshare             = (ImageView) itemView.findViewById(R.id.IBshare);
            IBfriends           = (ImageView) itemView.findViewById(R.id.IBfriends);
            BTNnextEnable       = (Button) itemView.findViewById(R.id.BTNnextEnable);
            BTNmoreOffice       = (Button) itemView.findViewById(R.id.BTNmoreOffice);
            LLnoHours           = (LinearLayout) itemView.findViewById(R.id.LLnoHours);
            mRecyclerHour       = (RecyclerView) itemView.findViewById(R.id.mRecyclerSchedule);
            mRecyclerHour.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));

            TVoffice.setVisibility(View.GONE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_item_doctor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.PBloading.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (position == (mDoctors.size() - 1)) {
                    int aux = mDoctors.size() / 20;
                    if (aux != page) {
                        page = aux;
                        evento.onPager(page);
                    }
                }
                holder.BTNnextEnable.setText(boostrap.langStrings.get(Constants.next_enable_p));
                holder.BTNwait_list.setText(boostrap.langStrings.get(Constants.waiting_list_p));
                holder.BTNmoreOffice.setText(boostrap.langStrings.get(Constants.offices_p));
                holder.PBloading.setVisibility(View.GONE);
                holder.LLshow.setVisibility(View.VISIBLE);
                myCalendar = Calendar.getInstance();
                final DoctorSearch doctors = mDoctors.get(position);
                holder.TVnumberFriends.setText(doctors.getFriends());

                if (doctors.getEnable().equals("1")) {
                    holder.IVaffiliate.setVisibility(View.VISIBLE);
                    Picasso.with(ctx).load(R.drawable.icon_affiliate).noFade().into(holder.IVaffiliate);
                    if (doctors.getMyoffices().size() > 0) {

                        holder.TVamounts.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog amount = new AlertDialog.Builder(ctx)
                                        .setTitle(boostrap.langStrings.get(Constants.cost_p))
                                        .setMessage(boostrap.langStrings.get(Constants.f_appointment_p)+": $ " + doctors.getMyoffices().get(0).getFirst_amount() + "\n" +
                                                boostrap.langStrings.get(Constants.s_appointment_p)+": $ " + doctors.getMyoffices().get(0).getLater_amount())
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                            }
                        });
                        holder.TVoffice.setText(doctors.getMyoffices().get(0).getName_office());
                        holder.TVaddressOffice.setText(doctors.getMyoffices().get(0).getFull_address());

                        holder.BTNwait_list.setVisibility(View.VISIBLE);
                        holder.LLnoHours.setVisibility(View.INVISIBLE);
                        if (doctors.getMyoffices().size() > 1) {
                            holder.BTNmoreOffice.setVisibility(View.VISIBLE);
                        } else {
                            holder.BTNmoreOffice.setVisibility(View.INVISIBLE);
                        }

                    } else {
                        holder.TVamounts.setVisibility(View.INVISIBLE);
                        holder.TVoffice.setText("");
                        holder.TVaddressOffice.setText("");
                        holder.BTNmoreOffice.setVisibility(View.INVISIBLE);
                        holder.BTNwait_list.setVisibility(View.INVISIBLE);
                        holder.LLnoHours.setVisibility(View.VISIBLE);
                        holder.TVno_office.setText(boostrap.langStrings.get(Constants.available_time_p));
                        holder.BTNnextEnable.setText(boostrap.langStrings.get(Constants.next_enable_p));
                    }

                } else {
                    holder.IVaffiliate.setVisibility(View.GONE);
                    if (doctors.getMyoffices().size() > 0) {
                        holder.TVamounts.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog amount = new AlertDialog.Builder(ctx)
                                        .setTitle(boostrap.langStrings.get(Constants.cost_p))
                                        .setMessage(boostrap.langStrings.get(Constants.f_appointment_p)+": $ " + doctors.getMyoffices().get(0).getFirst_amount() + "\n" +
                                                boostrap.langStrings.get(Constants.s_appointment_p)+": $ " + doctors.getMyoffices().get(0).getLater_amount())
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                            }
                        });
                        holder.TVoffice.setText(doctors.getMyoffices().get(0).getName_office());
                        holder.TVaddressOffice.setText(doctors.getMyoffices().get(0).getFull_address());

                        holder.LLnoHours.setVisibility(View.INVISIBLE);

                        if (doctors.getMyoffices().size() > 1) {
                            holder.BTNmoreOffice.setVisibility(View.VISIBLE);
                        } else {
                            holder.BTNmoreOffice.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        holder.TVamounts.setVisibility(View.INVISIBLE);
                        holder.TVoffice.setText("");
                        holder.TVaddressOffice.setText("");
                        holder.BTNmoreOffice.setVisibility(View.INVISIBLE);
                        holder.LLnoHours.setVisibility(View.VISIBLE);
                        holder.TVno_office.setText(boostrap.langStrings.get(Constants.available_time_p));
                        holder.BTNnextEnable.setText(boostrap.langStrings.get(Constants.next_enable_p));
                    }
                    holder.BTNwait_list.setVisibility(View.INVISIBLE);
                }

                if (doctors.getPicture().trim().length() > 0) {
                    Picasso.with(ctx).load(doctors.getPicture())
                            .resize(60, 60)
                            .centerCrop()
                            .noFade()
                            .into(holder.CIVdoctor);
                } else {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .resize(60, 60)
                            .noFade()
                            .centerCrop()
                            .into(holder.CIVdoctor);
                }

                String speciality = boostrap.langStrings.get(Constants.no_specialities_p);
                if (doctors.getSpecialities().size() > 0) {
                    for (int i = 0; i < doctors.getSpecialities().size(); i++) {
                        if (i == 0) {
                            speciality = Miscellaneous.ucFirst(doctors.getSpecialities().get(i).get("specialty"));
                        } else {
                            speciality = speciality + ", " + Miscellaneous.ucFirst(doctors.getSpecialities().get(i).get("specialty"));
                        }
                    }
                }
                holder.TVspecialities.setText(speciality);
                holder.TVname.setText(doctors.getName());
                if (doctors.getMyoffices().size() > 0) {
                    holder.LLnoHours.setVisibility(View.GONE);
                    holder.mRecyclerHour.setVisibility(View.VISIBLE);
                    if (doctors.getMyoffices().get(0).getOpen_time().size() > 0 && doctors.getMyoffices().get(0).getOpen_time().size() != doctors.getMyoffices().get(0).getLocked_time().size()) {
                        RecyclerViewAdapterSchedules adapter = new RecyclerViewAdapterSchedules(doctors.getMyoffices().get(0).getOpen_time(), doctors.getMyoffices().get(0).getLocked_time(), ctx, new RecyclerViewAdapterSchedules.Event() {
                            @Override
                            public void onClic(final String hour, View v) {
                                if (session.profile().get(0) != null) {
                                    if (boostrap.WORD_SEARCH.length() > 0) {
                                        dialogQuestions(holder.TVaddressOffice.getText().toString(), doctors.getName(), fecha, hour, doctors.getId_session(), doctors.getMyoffices().get(0).getOffice_id(), doctors.getEnable(), true);
                                    } else {
                                        if (doctors.getEnable().equals("1")) {
                                            if (session.getFdoctor().get(0) == null) {
                                                dialogQuestions(holder.TVaddressOffice.getText().toString(), doctors.getName(), fecha, hour, doctors.getId_session(), doctors.getMyoffices().get(0).getOffice_id(), doctors.getEnable(), true);
                                            } else {
                                                dialogSecondMedic(holder.TVaddressOffice.getText().toString(), doctors.getName(), fecha, hour, doctors.getId_session(), doctors.getMyoffices().get(0).getOffice_id(), doctors.getEnable(), true);
                                            }
                                        } else {
                                            if (session.getFdoctor().get(0) != null) {
                                                dialogQuestions(holder.TVaddressOffice.getText().toString(), doctors.getName(), fecha, hour, doctors.getId_session(), doctors.getMyoffices().get(0).getOffice_id(), doctors.getEnable(), false);
                                            } else {
                                                dialogQuestions(holder.TVaddressOffice.getText().toString(), doctors.getName(), fecha, hour, doctors.getId_session(), doctors.getMyoffices().get(0).getOffice_id(), doctors.getEnable(), false);
                                            }
                                        }
                                    }
                                } else {
                                    AlertDialog amount = new AlertDialog.Builder(ctx)
                                            .setTitle("CLICDOCS")
                                            .setMessage(boostrap.langStrings.get(Constants.legend_log_in_required_p))
                                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            }).show();
                                }
                            }
                        });
                        holder.mRecyclerHour.setAdapter(adapter);
                    } else {
                        holder.LLnoHours.setVisibility(View.VISIBLE);
                        holder.mRecyclerHour.setVisibility(View.GONE);
                    }

                } else {
                    holder.LLnoHours.setVisibility(View.VISIBLE);
                    holder.mRecyclerHour.setVisibility(View.GONE);
                }


                holder.BTNnextEnable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar cale;
                        cale = Calendar.getInstance();
                        RequestQueue queueNextEnable;
                        if (NetworkUtils.haveNetworkConnection(ctx)) {
                            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                            loading.show();
                            queueNextEnable = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_next_enable, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    loading.dismiss();
                                    Log.v("printNextEnable", "" + response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                            case 200:
                                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                                JSONObject jsonToday = jsonResponse.getJSONObject("today");
                                                Log.v("printNextDate", jsonToday.getString("date"));
                                                Date strDate = sdf.parse(jsonToday.getString("date"));
                                                cale.setTime(strDate);
                                                evento.setDate(jsonToday.getString("date"));
                                                searchByDate(cale.get(Calendar.DAY_OF_WEEK), jsonToday.getString("date"));
                                                break;
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.v("error", error.toString());
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("date", fecha);
                                    params.put("office_id", doctors.getMyoffices().get(0).getOffice_id());
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<>();
                                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                    return headers;
                                }
                            };
                            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queueNextEnable.add(request);
                        } else {

                        }
                    }
                });

                holder.LLprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        evento.onClicViewProfile(doctors.getId_session());
                    }
                });

                holder.RLprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        evento.onClicViewProfile(doctors.getId_session());
                    }
                });

                holder.BTNwait_list.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (session.profile().get(0) != null) {
                            final View dialog_wait_list = LayoutInflater.from(ctx).inflate(R.layout.dialog_wait_list, null);
                            final TextView TVlegend = (TextView) dialog_wait_list.findViewById(R.id.TVlegend);
                            final RadioGroup RBGwaitlist = (RadioGroup) dialog_wait_list.findViewById(R.id.RBGwaitlist);
                            final RadioButton RBmatutino = (RadioButton) dialog_wait_list.findViewById(R.id.RBmatutino);
                            final RadioButton RBvespertino = (RadioButton) dialog_wait_list.findViewById(R.id.RBvespertino);
                            final RadioButton RBambos = (RadioButton) dialog_wait_list.findViewById(R.id.RBambos);
                            final CheckBox CBappoinmentPro = (CheckBox) dialog_wait_list.findViewById(R.id.CBappoinProvi);
                            final TextView TVdateTitle      = (TextView) dialog_wait_list.findViewById(R.id.TVdateTitle);
                            final ProgressBar PBloading = (ProgressBar) dialog_wait_list.findViewById(R.id.PBloading);
                            final LinearLayout LLform = (LinearLayout) dialog_wait_list.findViewById(R.id.LLform);
                            final LinearLayout LLappoinProvi = (LinearLayout) dialog_wait_list.findViewById(R.id.LLappoinmentProvisional);
                            final EditText ETdatePro = (EditText) dialog_wait_list.findViewById(R.id.ETdatePro);
                            final TextView TVhourDisable = (TextView) dialog_wait_list.findViewById(R.id.TVhourDisable);
                            mRecyclerWaitList = (RecyclerView) dialog_wait_list.findViewById(R.id.mRecyclerSchedule);
                            mRecyclerWaitList.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));
                            final Button BTNacccept = (Button) dialog_wait_list.findViewById(R.id.BTNaccept);
                            final Button BTNcancel = (Button) dialog_wait_list.findViewById(R.id.BTNcancel);

                            TVlegend.setText(boostrap.langStrings.get(Constants.legend_diloag_waiting_list_p));
                            RBmatutino.setText(boostrap.langStrings.get(Constants.morning_p));
                            RBvespertino.setText(boostrap.langStrings.get(Constants.evening_p));
                            RBambos.setText(boostrap.langStrings.get(Constants.both_p));
                            CBappoinmentPro.setText(boostrap.langStrings.get(Constants.schedule_provisional_p));
                            TVdateTitle.setText(boostrap.langStrings.get(Constants.date_p));
                            ETdatePro.setHint(boostrap.langStrings.get(Constants.date_p));

                            BTNacccept.setText(boostrap.langStrings.get(Constants.accept_p));
                            BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
                            LLappoinProvi.setVisibility(View.GONE);
                            TVhourDisable.setVisibility(View.GONE);
                            CBappoinmentPro.setEnabled(false);
                            final AlertDialog DLwaitList = new AlertDialog.Builder(ctx)
                                    .setTitle(boostrap.langStrings.get(Constants.waiting_list_p))
                                    .setCancelable(false)
                                    .setView(dialog_wait_list).show();

                            CBappoinmentPro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b) {
                                        LLappoinProvi.setVisibility(View.VISIBLE);
                                        mRecyclerWaitList.setVisibility(View.VISIBLE);
                                        BTNacccept.setVisibility(View.INVISIBLE);
                                        final Calendar cale;
                                        cale = Calendar.getInstance();
                                        RequestQueue queueNextEnable;
                                        if (NetworkUtils.haveNetworkConnection(ctx)) {
                                            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                                            loading.show();
                                            queueNextEnable = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                                            StringRequest request = new StringRequest(Request.Method.POST, Constants.get_next_enable, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    loading.dismiss();
                                                    Log.v("printNextEnable", "" + response);
                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                                            case 200:
                                                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                                                JSONObject jsonToday = jsonResponse.getJSONObject("today");
                                                                ETdatePro.setText(jsonToday.getString("date"));
                                                                ArrayList<String> open_times = new ArrayList<String>();
                                                                ArrayList<String> locked_times = new ArrayList<>();
                                                                JSONArray jsonOpen = jsonToday.getJSONArray("open_times");
                                                                JSONArray jsonLocked = jsonToday.getJSONArray("locked_times");

                                                                for (int l = 0; l < jsonOpen.length(); l++) {
                                                                    open_times.add(jsonOpen.get(l).toString());
                                                                }

                                                                for (int m = 0; m < jsonLocked.length(); m++) {
                                                                    locked_times.add(jsonLocked.get(m).toString());
                                                                }

                                                                RecyclerViewAdapterHourByOffice adapterSheadule = new RecyclerViewAdapterHourByOffice(open_times, locked_times, ctx, new RecyclerViewAdapterHourByOffice.Event() {
                                                                    @Override
                                                                    public void onClic(final String hour) {
                                                                        AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                                                        builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_be_waiting_list_p))
                                                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                                                .setCancelable(false)
                                                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                                        setMeWaitList(doctors.getMyoffices().get(0).getOffice_id(), doctors.getId_session(), String.valueOf(valueRB), ETdatePro.getText().toString(), hour, LLform, PBloading, DLwaitList);
                                                                                        dialog.cancel();
                                                                                    }
                                                                                })
                                                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                                        dialog.cancel();
                                                                                    }
                                                                                });
                                                                        builderAmbos.show();
                                                                    }
                                                                });
                                                                mRecyclerWaitList.setAdapter(adapterSheadule);

                                                                break;

                                                        }

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    Log.v("error", error.toString());
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    HashMap<String, String> params = new HashMap<>();
                                                    params.put("date", "");
                                                    params.put("office_id", doctors.getMyoffices().get(0).getOffice_id());
                                                    return params;
                                                }

                                                @Override
                                                public Map<String, String> getHeaders() throws AuthFailureError {
                                                    HashMap<String, String> headers = new HashMap<>();
                                                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                                                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                                    return headers;
                                                }
                                            };
                                            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                            queueNextEnable.add(request);
                                        } else {

                                        }
                                    } else {
                                        mRecyclerWaitList.setAdapter(null);
                                        ETdatePro.setText("");
                                        mRecyclerWaitList.setVisibility(View.GONE);
                                        LLappoinProvi.setVisibility(View.GONE);
                                        BTNacccept.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                            RBGwaitlist.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup group, int checkedId) {

                                    if (checkedId == R.id.RBmatutino) {
                                        valueRB = 1;
                                    } else if (checkedId == R.id.RBvespertino) {
                                        valueRB = 2;
                                    } else {
                                        valueRB = 3;
                                    }
                                    if (checkedId > 0) {
                                        CBappoinmentPro.setEnabled(true);
                                    }
                                }
                            });

                            ETdatePro.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View view) {
                                    Calendar fecha = new GregorianCalendar();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                    int año = fecha.get(Calendar.YEAR);
                                    int mes = fecha.get(Calendar.MONTH);
                                    int dia = fecha.get(Calendar.DAY_OF_MONTH);
                                    Date d = null;
                                    try {
                                        d = sdf.parse(dia + "/" + (mes + 1) + "/" + año);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    DatePickerDialog dpd = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                                            myCalendar.set(Calendar.YEAR, year);
                                            myCalendar.set(Calendar.MONTH, monthOfYear);
                                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                            try {
                                                setDatePicked(view, TVhourDisable, LLform, PBloading, DLwaitList);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                            myCalendar.get(Calendar.DAY_OF_MONTH));
                                    dpd.getDatePicker().setMinDate(d.getTime());

                                    dpd.show();
                                }
                            });

                            BTNcancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    DLwaitList.dismiss();
                                }
                            });

                            BTNacccept.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    int id = RBGwaitlist.getCheckedRadioButtonId();
                                    if (RBmatutino.isChecked() || RBvespertino.isChecked() || RBambos.isChecked()) {
                                        switch (id) {
                                            case R.id.RBmatutino:
                                                AlertDialog.Builder builderMatutino = new AlertDialog.Builder(ctx);
                                                builderMatutino.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                setMeWaitList(doctors.getMyoffices().get(0).getOffice_id(), doctors.getId_session(), "1", "", "", LLform, PBloading, DLwaitList);
                                                                dialog.cancel();
                                                            }
                                                        })
                                                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builderMatutino.show();
                                                break;
                                            case R.id.RBvespertino:
                                                AlertDialog.Builder builderVespertino = new AlertDialog.Builder(ctx);
                                                builderVespertino.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                setMeWaitList(doctors.getMyoffices().get(0).getOffice_id(), doctors.getId_session(), "2", "", "", LLform, PBloading, DLwaitList);
                                                                dialog.cancel();
                                                            }
                                                        })
                                                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builderVespertino.show();
                                                break;
                                            case R.id.RBambos:
                                                AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                                builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_waiting_list_p))
                                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                setMeWaitList(doctors.getMyoffices().get(0).getOffice_id(), doctors.getId_session(), "3", "", "", LLform, PBloading, DLwaitList);
                                                                dialog.cancel();
                                                            }
                                                        })
                                                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builderAmbos.show();
                                                break;
                                        }
                                    } else {
                                        Toast.makeText(ctx, boostrap.langStrings.get(Constants.selected_option_p), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(ctx, boostrap.langStrings.get(Constants.is_required_login_p), Toast.LENGTH_SHORT).show();
                        }
                    }

                    private void setDatePicked(View v, TextView tvDisable, LinearLayout LLform, ProgressBar PBloading, AlertDialog DLwait) throws JSONException, ParseException {
                        String myFormat = "yyyy-MM-dd";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                        sdf.setTimeZone(myCalendar.getTimeZone());
                        ((EditText) v).setText(formatDate(sdf.format(myCalendar.getTime())));
                        getDoctorSchedulesByOffice(doctors.getMyoffices().get(0).getOffice_id(), sdf.format(myCalendar.getTime()), LLform, PBloading, DLwait, tvDisable);

                    }

                    private void getDoctorSchedulesByOffice(final String idOffice, final String dateAppo, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLwait, final TextView TVdisable) throws JSONException {
                        if (NetworkUtils.haveNetworkConnection(ctx)) {
                            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
                            loading.show();
                            queueSchedule = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
                            StringRequest request = new StringRequest(Request.Method.POST, Constants.offices_time, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.v("printResponseReschedule", "" + response);
                                    loading.dismiss();
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                                            case 200:
                                                JSONObject jsonResponse = jsonObject.getJSONObject("response");
                                                JSONArray jsonOpen = jsonResponse.getJSONArray("open_times");
                                                JSONArray jsonLocked = jsonResponse.getJSONArray("locked_times");

                                                ArrayList<String> open_times = new ArrayList<>();
                                                for (int i = 0; i < jsonOpen.length(); i++) {
                                                    open_times.add(jsonOpen.get(i).toString());
                                                }
                                                ArrayList<String> locked_time = new ArrayList<>();
                                                for (int j = 0; j < jsonLocked.length(); j++) {
                                                    locked_time.add(jsonLocked.get(j).toString());
                                                }
                                                RecyclerViewAdapterHourByOffice adapterSheadule = new RecyclerViewAdapterHourByOffice(open_times, locked_time, ctx, new RecyclerViewAdapterHourByOffice.Event() {

                                                    @Override
                                                    public void onClic(final String hour) {
                                                        AlertDialog.Builder builderAmbos = new AlertDialog.Builder(ctx);
                                                        builderAmbos.setMessage(boostrap.langStrings.get(Constants.legend_be_waiting_list_p))
                                                                .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                                                .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        setMeWaitList(idOffice, doctors.getId_session(), String.valueOf(valueRB), dateAppo, hour, LLform, PBloading, DLwait);
                                                                        dialog.cancel();
                                                                    }
                                                                })
                                                                .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        dialog.cancel();
                                                                    }
                                                                });
                                                        builderAmbos.show();
                                                    }
                                                });
                                                mRecyclerWaitList.setAdapter(adapterSheadule);
                                                break;
                                            default:
                                                AlertDialog success = new AlertDialog.Builder(ctx)
                                                        .setMessage(jsonObject.getString(Constants.response))
                                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                mRecyclerWaitList.setAdapter(null);
                                                                dialogInterface.dismiss();
                                                            }
                                                        }).show();

                                                break;
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.v("error", error.toString());
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("office_id", idOffice);
                                    params.put("date", dateAppo);
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<>();
                                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                                    return headers;
                                }
                            };
                            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queueSchedule.add(request);
                        } else {

                        }
                    }
                });

                final String finalSpeciality = speciality;
                holder.BTNmoreOffice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        evento.onClicOffice(doctors.getId_session(), doctors.getName(), finalSpeciality, doctors.getPicture(), doctors.getEnable());
                    }
                });

                holder.IBshare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (session.profile().get(0) != null) {
                            if (session.profile().get(0).toString().equals(session.secondProfile().get(0))) {
                                if (session.profile().get(3).toString().equals("fb")) {
                                    evento.onClicShare(doctors.getPicture(), doctors.getId_session());
                                } else {
                                    Snackbar.make(view_parent, boostrap.langStrings.get(Constants.login_facebook_p), Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(view_parent, boostrap.langStrings.get(Constants.account_main_p), Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(view_parent, boostrap.langStrings.get(Constants.login_facebook_p), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }, 1000);
    }

    //--------------------------------------------------------------------------------------------------
    private void setMeWaitList(final String idOffice, final String doctor_id, final String turno, final String dateW, final String timeW, final LinearLayout LLform, final ProgressBar PBloading, final AlertDialog DLwait) {
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppoinment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_wait_list, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    DLwait.dismiss();
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject message = jsonObject.getJSONObject(Constants.response);
                                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                builder.setMessage(message.getString("message"))
                                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                builder.show();

                                session.deleteFirstDoctor();
                                break;
                            case 202:
                                Snackbar.make(view_parent, jsonObject.getString("response"), Snackbar.LENGTH_SHORT).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("office_id", idOffice);
                    params.put("doctor_id", doctor_id);
                    params.put("turno", turno);
                    params.put("date", dateW);
                    params.put("time", timeW);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppoinment.add(request);
        } else {

        }
    }

    //--------------------------------------------------------------------------------------------------
    private void searchByDate(final int day, final String DATE_CURRENT) {
        RequestQueue queueSearch;
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueSearch = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.search_doctor, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printSearch", "" + response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonDoctor = jsonObject.getJSONObject("response");
                                JSONArray jsonRows = jsonDoctor.getJSONArray("rows");
                                ArrayList<DoctorSearch> doct = new ArrayList<>();
                                for (int i = 0; i < jsonRows.length(); i++) {
                                    DoctorSearch my = new DoctorSearch();
                                    JSONObject item = jsonRows.getJSONObject(i);
                                    my.setId_session(item.getString("doctor_id"));
                                    my.setName(item.getString("doctor_full_name"));
                                    my.setPicture(item.getString("picture"));
                                    my.setEnable(item.getString("enabled"));
                                    my.setFriends(item.getString("fb_friends"));
                                    my.setF_amount(item.getString("first_amount"));
                                    my.setS_amount(item.getString("later_amount"));


                                    //---------  SPECIALITIES  ------------------------------
                                    ArrayList<HashMap<String, String>> sp = new ArrayList<>();
                                    if (!String.valueOf(item.get("specialities")).equals("")) {
                                        JSONArray jsonSpe = new JSONArray((String) item.get("specialities"));

                                        for (int k = 0; k < jsonSpe.length(); k++) {
                                            JSONObject itemSpecialities = jsonSpe.getJSONObject(k);
                                            HashMap<String, String> speciality = new HashMap<>();
                                            speciality.put("specialty_id", itemSpecialities.getString("id"));
                                            speciality.put("specialty", itemSpecialities.getString("speciality"));
                                            speciality.put("university", itemSpecialities.getString("university"));
                                            sp.add(speciality);
                                        }

                                    }
                                    my.setSpecialities(sp);
                                    //-------------- OFFICE  -------------------------------------------
                                    ArrayList<DoctorSearch.Offices> offices = new ArrayList<>();
                                    if (String.valueOf(item.get("consulting_rooms")).length() > 4) {
                                        JSONArray jsonConsulting = item.getJSONArray("consulting_rooms");
                                        for (int z = 0; z < jsonConsulting.length(); z++) {
                                            JSONObject itemOffice = jsonConsulting.getJSONObject(z);
                                            //Log.v("printNameOffice",itemOffice.getString("office"));
                                            DoctorSearch itemDoctor = new DoctorSearch();
                                            DoctorSearch.Offices myOffices = itemDoctor.off;
                                            myOffices.setOffice_id(itemOffice.getString("id"));
                                            myOffices.setName_office(itemOffice.getString("office"));
                                            myOffices.setFull_address(itemOffice.getString("full_address"));
                                            //myOffices.setStreet(itemOffice.getString("street"));
                                            //myOffices.setColony(itemOffice.getString("colony"));
                                            //myOffices.setNum_ext(itemOffice.getString("outdoor_number"));
                                            //myOffices.setNum_int(itemOffice.getString("interior_number"));
                                            myOffices.setState(itemOffice.getString("state"));
                                            myOffices.setBorough(itemOffice.getString("borough"));
                                            myOffices.setCity(itemOffice.getString("city"));
                                            myOffices.setLatitude(itemOffice.getString("latitude"));
                                            myOffices.setLongitude(itemOffice.getString("longitude"));

                                            //---------- HOURS --------------------------------------
                                            ArrayList<String> open_times = new ArrayList<String>();
                                            ArrayList<String> locked_times = new ArrayList<>();
                                            JSONObject jsonTime = itemOffice.getJSONObject("times");
                                            if (!String.valueOf(jsonTime.get(DATE_CURRENT)).equals("null")) {
                                                JSONObject jsonDate = jsonTime.getJSONObject(DATE_CURRENT);
                                                //Log.v("printJsonDate",""+jsonDate);
                                                JSONArray jsonOpen = jsonDate.getJSONArray("open_times");

                                                JSONArray jsonLocked = jsonDate.getJSONArray("locked_times");

                                                for (int l = 0; l < jsonOpen.length(); l++) {
                                                    open_times.add(jsonOpen.get(l).toString());
                                                }

                                                for (int m = 0; m < jsonLocked.length(); m++) {
                                                    locked_times.add(jsonLocked.get(m).toString());
                                                }
                                            }
                                            myOffices.setLocked_time(locked_times);
                                            myOffices.setOpen_time(open_times);
                                            offices.add(myOffices);
                                        }
                                    }
                                    my.setMyoffices(offices);
                                    doct.add(my);
                                }
                                updateByDate(doct, DATE_CURRENT);
                                break;
                            case 403:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("fields", boostrap.jsonSearch.toString());
                    params.put("date", DATE_CURRENT);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1));
            queueSearch.add(request);

        } else {
            Snackbar.make(view_parent, boostrap.langStrings.get(Constants.noconnection_p), Snackbar.LENGTH_LONG).show();
        }

    }

    public void updateByDate(ArrayList<DoctorSearch> doc, String fecha) {
        mDoctors.clear();
        this.mDoctors = doc;
        this.fecha = fecha;
        notifyDataSetChanged();
    }

    public void clear() {
        mDoctors.clear();
        notifyDataSetChanged();
    }

    public void add(ArrayList<DoctorSearch> d) {
        for (int i = 0; i < d.size(); i++) {
            mDoctors.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    //===================== NUEVO DIALOGO PARA PREGUNTAR SI ES PRIMERA O SEGUNDA CITA ===============================
    private void dialogQuestions(final String address, final String name, final String day, final String hour, final String doctor_id, final String office_id, final String enable, final boolean flag) {
        final View dialog_questions = LayoutInflater.from(ctx).inflate(R.layout.dialog_one_doctor_selected, null);
        TextView TVmessage = (TextView) dialog_questions.findViewById(R.id.TVmessage);
        Button BTNfirst_op = (Button) dialog_questions.findViewById(R.id.BTNf_appo);
        Button BTNsecond_op = (Button) dialog_questions.findViewById(R.id.BTNs_appo);
        Button BTNcancel = (Button) dialog_questions.findViewById(R.id.BTNcancel);

        BTNfirst_op.setText(boostrap.langStrings.get(Constants.yes_first_p));
        BTNsecond_op.setText(boostrap.langStrings.get(Constants.not_sub_p));
        BTNcancel.setText(boostrap.langStrings.get(Constants.not_want_appo_p));

        //Log.v("printFlag", flag + "");
        TVmessage.setText(boostrap.langStrings.get(Constants.legend_schedule_one_p) + "\n" + name + "\n" + boostrap.langStrings.get(Constants.legend_schedule_two_p) + formatDate(day) + "\n" + boostrap.langStrings.get(Constants.legend_schedule_three_p) + hour+" Hr(s)");
        final AlertDialog DLoption = new AlertDialog.Builder(ctx)
                .setTitle("CLICDOCS")
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_questions).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DLoption.dismiss();
            }
        });

        BTNfirst_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                if (flag) {
                    data.add(name);
                    data.add(address);
                    data.add(doctor_id);
                    data.add(office_id);
                    data.add(day);
                    data.add(hour);
                    data.add(enable);
                    data.add("1");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    dialogReason(data, DLoption);
                } else {
                    if (enable.equals("0")) {
                        if (session.getFdoctor().get(0) != null) {
                            data.add(session.getFdoctor().get(1).toString());
                            data.add(session.getFdoctor().get(7).toString());
                            data.add(session.getFdoctor().get(0).toString());
                            data.add(session.getFdoctor().get(4).toString());
                            data.add(session.getFdoctor().get(2).toString());
                            data.add(session.getFdoctor().get(3).toString());
                            data.add(session.getFdoctor().get(5).toString());
                            data.add(session.getFdoctor().get(6).toString());
                            data.add(name);
                            data.add(address);
                            data.add(doctor_id);
                            data.add(office_id);
                            data.add(day);
                            data.add(hour);
                            data.add(enable);
                            data.add("1");
                            dialogReason(data, DLoption);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                            builder.setMessage(boostrap.langStrings.get(Constants.legend_no_affiliate_p))
                                    .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                    .setIcon(R.drawable.ic_warning)
                                    .setCancelable(false)
                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            session.firstDoctor(doctor_id, name, day, hour, office_id, enable, "1", address);
                                            DLoption.dismiss();
                                            //session.firstDoctor(doctors.getId_session(),doctors.getName(),fecha,hour,doctors.getMyoffices().get(0).getOffice_id(),doctors.getEnable(),doctors.getPicture());
                                            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);
                                            View customView = inflater.inflate(R.layout.popup_window_medic, null);

                                            boostrap.myPopup = new PopupWindow(customView,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT);

                                            CIVpictureDoctor = (CircleImageView) customView.findViewById(R.id.CIVdoctorMedic);

                                            Picasso.with(ctx).load(R.drawable.blank).noFade().resize(100, 100).centerCrop().into(CIVpictureDoctor);

                                            boostrap.myPopup.showAsDropDown(view_parent, view_parent.getWidth() - 10, 10);

                                            customView.setOnTouchListener(new View.OnTouchListener() {
                                                int myX, myY;
                                                int touchedX, touchedY;

                                                @Override
                                                public boolean onTouch(View view, MotionEvent event) {
                                                    switch (event.getAction()) {
                                                        case MotionEvent.ACTION_DOWN:
                                                            myX = (int) event.getX();
                                                            myY = (int) event.getY();
                                                            break;
                                                        case MotionEvent.ACTION_MOVE:
                                                            touchedX = (int) event.getRawX() - myX;
                                                            touchedY = (int) event.getRawY() - myY;

                                                            boostrap.myPopup.update(touchedX, touchedY, -1, -1, true);
                                                            break;
                                                    }
                                                    return false;
                                                }
                                            });

                                            CIVpictureDoctor.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    boostrap.ShowDoctorSelected();
                                                }
                                            });
                                            dialog.cancel();
                                        }
                                    });
                            builder.show();
                        }
                    }

                }

                //setAppoinment(appointment, DLquestions);
            }
        });

        BTNsecond_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                if (flag) {
                    data.add(name);
                    data.add(address);
                    data.add(doctor_id);
                    data.add(office_id);
                    data.add(day);
                    data.add(hour);
                    data.add(enable);
                    data.add("2");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    data.add("");
                    dialogReason(data, DLoption);
                } else {
                    if (enable.equals("0")) {
                        if (session.getFdoctor().get(0) != null) {
                            data.add(session.getFdoctor().get(1).toString());
                            data.add(session.getFdoctor().get(7).toString());
                            data.add(session.getFdoctor().get(0).toString());
                            data.add(session.getFdoctor().get(4).toString());
                            data.add(session.getFdoctor().get(2).toString());
                            data.add(session.getFdoctor().get(3).toString());
                            data.add(session.getFdoctor().get(5).toString());
                            data.add(session.getFdoctor().get(6).toString());
                            data.add(name);
                            data.add(address);
                            data.add(doctor_id);
                            data.add(office_id);
                            data.add(day);
                            data.add(hour);
                            data.add(enable);
                            data.add("2");
                            dialogReason(data, DLoption);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(boostrap);
                            builder.setMessage(boostrap.langStrings.get(Constants.legend_no_affiliate_p))
                                    .setTitle(boostrap.langStrings.get(Constants.notice_p))
                                    .setIcon(R.drawable.ic_warning)
                                    .setCancelable(false)
                                    .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            session.firstDoctor(doctor_id, name, day, hour, office_id, enable, "2", address);
                                            DLoption.dismiss();
                                            //session.firstDoctor(doctors.getId_session(),doctors.getName(),fecha,hour,doctors.getMyoffices().get(0).getOffice_id(),doctors.getEnable(),doctors.getPicture());
                                            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);
                                            View customView = inflater.inflate(R.layout.popup_window_medic, null);

                                            boostrap.myPopup = new PopupWindow(customView,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT);

                                            CIVpictureDoctor = (CircleImageView) customView.findViewById(R.id.CIVdoctorMedic);

                                            Picasso.with(ctx).load(R.drawable.blank).noFade().resize(100, 100).centerCrop().into(CIVpictureDoctor);

                                            boostrap.myPopup.showAsDropDown(view_parent, view_parent.getWidth() - 10, 10);

                                            customView.setOnTouchListener(new View.OnTouchListener() {
                                                int myX, myY;
                                                int touchedX, touchedY;

                                                @Override
                                                public boolean onTouch(View view, MotionEvent event) {
                                                    switch (event.getAction()) {
                                                        case MotionEvent.ACTION_DOWN:
                                                            myX = (int) event.getX();
                                                            myY = (int) event.getY();
                                                            break;
                                                        case MotionEvent.ACTION_MOVE:
                                                            touchedX = (int) event.getRawX() - myX;
                                                            touchedY = (int) event.getRawY() - myY;

                                                            boostrap.myPopup.update(touchedX, touchedY, -1, -1, true);
                                                            break;
                                                    }
                                                    return false;
                                                }
                                            });

                                            CIVpictureDoctor.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    boostrap.ShowDoctorSelected();
                                                }
                                            });
                                            dialog.cancel();
                                        }
                                    });
                            builder.show();
                        }
                    }
                }
            }
        });
    }
    //=================================================================================================================

    //=========================== DIALOG SEGUNDO MEDICO =====================================================
    private void dialogSecondMedic(final String address, final String name, final String day, final String hour, final String doctor_id, final String office_id, final String enable, final boolean flag) {
        final View dialog_questions = LayoutInflater.from(ctx).inflate(R.layout.dialog_one_doctor_selected, null);
        TextView TVmessage = (TextView) dialog_questions.findViewById(R.id.TVmessage);
        Button BTNfirst_op = (Button) dialog_questions.findViewById(R.id.BTNf_appo);
        Button BTNsecond_op = (Button) dialog_questions.findViewById(R.id.BTNs_appo);
        Button BTNcancel = (Button) dialog_questions.findViewById(R.id.BTNcancel);


        TVmessage.setText(boostrap.langStrings.get(Constants.legend_schedule_one_p) + "\n" + name + "\n" + boostrap.langStrings.get(Constants.legend_schedule_two_p) + day + "\n" + boostrap.langStrings.get(Constants.legend_schedule_three_p) + hour+" Hr(s)");
        final AlertDialog DLoption = new AlertDialog.Builder(ctx)
                .setTitle("CLICDOCS")
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_questions).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DLoption.dismiss();
            }
        });

        BTNfirst_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(session.getFdoctor().get(1).toString());
                data.add(session.getFdoctor().get(7).toString());
                data.add(session.getFdoctor().get(0).toString());
                data.add(session.getFdoctor().get(4).toString());
                data.add(session.getFdoctor().get(2).toString());
                data.add(session.getFdoctor().get(3).toString());
                data.add(session.getFdoctor().get(5).toString());
                data.add(session.getFdoctor().get(6).toString());
                data.add(name);
                data.add(address);
                data.add(doctor_id);
                data.add(office_id);
                data.add(day);
                data.add(hour);
                data.add(enable);
                data.add("1");
                dialogChoiceDoctor(data, DLoption);
            }
        });

        BTNsecond_op.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(session.getFdoctor().get(1).toString());
                data.add(session.getFdoctor().get(7).toString());
                data.add(session.getFdoctor().get(0).toString());
                data.add(session.getFdoctor().get(4).toString());
                data.add(session.getFdoctor().get(2).toString());
                data.add(session.getFdoctor().get(3).toString());
                data.add(session.getFdoctor().get(5).toString());
                data.add(session.getFdoctor().get(6).toString());
                data.add(name);
                data.add(address);
                data.add(doctor_id);
                data.add(office_id);
                data.add(day);
                data.add(hour);
                data.add(enable);
                data.add("2");
                dialogChoiceDoctor(data, DLoption);


            }
        });
    }

    //=======================================================================================================
//==================== DIALOGO PARA PREGUNTAR SI DESEA AGENDAR CON EL AFFILIADO ==================================================
    private void dialogChoiceDoctor(final ArrayList<String> appo, AlertDialog DL) {
        //Log.v("printSize", "" + appo.size());
        View dialog_affiliate = LayoutInflater.from(ctx).inflate(R.layout.dialog_choice_doctor, null);
        TextView TVlegend = (TextView) dialog_affiliate.findViewById(R.id.TVmessage);
        Button BTNone = (Button) dialog_affiliate.findViewById(R.id.BTNoneDoctor);
        Button BTNtwo = (Button) dialog_affiliate.findViewById(R.id.BTNtwoDoctor);

        BTNone.setText(boostrap.langStrings.get(Constants.if_first_doctor_p));
        BTNtwo.setText(boostrap.langStrings.get(Constants.no_second_option_p));

        DL.dismiss();
        TVlegend.setText(boostrap.langStrings.get(Constants.legend_sec_affiliate_p));
        final AlertDialog DLchoice = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.selected_option_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_affiliate).show();

        BTNone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = new ArrayList<String>();
                data.add(appo.get(8));
                data.add(appo.get(9));
                data.add(appo.get(10));
                data.add(appo.get(11));
                data.add(appo.get(12));
                data.add(appo.get(13));
                data.add(appo.get(14));
                data.add(appo.get(15));
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                data.add("");
                dialogReason(data, DLchoice);
            }
        });

        BTNtwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReason(appo, DLchoice);
            }
        });
    }

    //==================================================================================================================
    //========================== DIALOGO RAZON PARA AGENDAR ====================================================
    private void dialogReason(final ArrayList<String> appointment, AlertDialog DL) {

        View dialog_reason = LayoutInflater.from(ctx).inflate(R.layout.dialog_reason_appointment, null);
        final EditText ETreason = (EditText) dialog_reason.findViewById(R.id.ETreason);
        Button BTNschedule = (Button) dialog_reason.findViewById(R.id.BTNschedule);
        Button BTNcancel = (Button) dialog_reason.findViewById(R.id.BTNcancel);
        BTNcancel.setText(boostrap.langStrings.get(Constants.cancel_p));
        BTNschedule.setText(boostrap.langStrings.get(Constants.schedule_p));
        ETreason.setHint(boostrap.langStrings.get(Constants.reason_appo_p));

        DL.dismiss();
        final AlertDialog DLreason = new AlertDialog.Builder(ctx)
                .setTitle(boostrap.langStrings.get(Constants.schedule_p))
                .setIcon(R.drawable.clic_logo)
                .setCancelable(false)
                .setView(dialog_reason).show();

        BTNcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boostrap.myPopup != null) {
                    boostrap.myPopup.dismiss();
                }
                session.deleteFirstDoctor();
                DLreason.dismiss();
            }
        });

        BTNschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appointment.add(ETreason.getText().toString());
                setAppoinment(appointment, DLreason);
            }
        });
    }
    //==========================================================================================================


    //==================== EVENTO PARA REGISTRAR CITA ==============================================================================
    private void setAppoinment(final ArrayList<String> data, final AlertDialog DLquestion) {
        //Log.v("printArray", data.toString());
        if (NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueAppoinment = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.register_appoinment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.v("printResponseAppoinment", "" + response);
                    DLquestion.dismiss();
                    loading.dismiss();
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                JSONObject jsonResp = jsonObject.getJSONObject(Constants.response);
                                Log.v("jsonAppointment", "" + jsonObject);
                                if (boostrap.myPopup != null) {
                                    boostrap.myPopup.dismiss();
                                }

                                if (data.get(6).equals("1")) {
                                    //======= FALTAN PARAMETROS ========
                                    if (saveAppoinmentCalendar(data.get(5), data.get(4), data.get(0), data.get(1), jsonResp.getString("insert_id"))) {
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                JSONObject message = null;
                                                try {
                                                    message = jsonObject.getJSONObject("response");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    AlertDialog success = new AlertDialog.Builder(ctx)
                                                            .setTitle("CLICDOCS")
                                                            .setCancelable(false)
                                                            .setMessage(message.getString("message"))
                                                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    session.deleteFirstDoctor();
                                                                    FragmentManager fm = boostrap.getSupportFragmentManager();
                                                                    fm.popBackStack(fragments.RESPONSE, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                                    boostrap.setFragment(fragments.MYDATES, null);
                                                                    dialogInterface.dismiss();
                                                                }
                                                            }).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, 2000);

                                    }
                                } else {
                                    JSONObject message = jsonObject.getJSONObject("response");
                                    AlertDialog success = new AlertDialog.Builder(ctx)
                                            .setMessage(message.getString("message"))
                                            .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    session.deleteFirstDoctor();
                                                    FragmentManager fm = boostrap.getSupportFragmentManager();
                                                    fm.popBackStack(fragments.RESPONSE, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                    boostrap.setFragment(fragments.MYDATES, null);
                                                    dialogInterface.dismiss();
                                                }
                                            }).show();
                                }
                                break;
                            case 202:
                                session.deleteFirstDoctor();
                                break;
                            case 404:
                                session.deleteFirstDoctor();
                                if (boostrap.myPopup != null) {
                                    boostrap.myPopup.dismiss();
                                }
                                Snackbar.make(view_parent, jsonObject.getString("response"), Snackbar.LENGTH_SHORT).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("patient_id", session.profile().get(0).toString());
                    params.put("fdoctor_id", data.get(2));
                    params.put("foffice_id", data.get(3));
                    params.put("fdate", data.get(4));
                    params.put("ftime", data.get(5));
                    params.put("appointment_num", data.get(7));
                    params.put("sdoctor_id", data.get(10));
                    params.put("soffice_id", data.get(11));
                    params.put("sdate", data.get(12));
                    params.put("stime", data.get(13));
                    params.put("s_appointment_num", data.get(15).toString());
                    params.put("reason", data.get(16));
                    Log.v("printParamas", "" + params);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueAppoinment.add(request);
        } else {

        }
    }

    //===================================================================================================================================
    //======================  GUARDAR EVENTO EN EL CALENDARIO =======================================
    private boolean saveAppoinmentCalendar(String hourR, String fecha, String name, String address, String idAppo) {
        boolean ban = false;
        Log.v("printCalendar", hourR + "  " + fecha + "   " + name + "   " + address+"  "+idAppo);
        if (true) {

            long calID = 2;
            Log.v("printCalID",""+calID);
            long startMillis = 0;
            long endMillis = 0;
            String [] separedDate = fecha.split("-");
            String [] separedHour = hourR.split(":");
            int year = Integer.parseInt(separedDate[0]);
            int month = Integer.parseInt(separedDate[1]);
            int day = Integer.parseInt(separedDate[2]);
            int hour = Integer.parseInt(separedHour[0]);
            int minute = Integer.parseInt(separedHour[1]);
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(year, (month-1), day, hour, minute);
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(year, (month-1), day, hour, minute+5);
            endMillis = endTime.getTimeInMillis();

            ContentResolver cr = boostrap.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.TITLE, "Cita al médico");
            values.put(CalendarContract.Events.DESCRIPTION, "Cita con el médico "+name+", a la(s) "+hourR+"hr(s), ubicado en "+address);
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Mexico_City");

            if (ActivityCompat.checkSelfPermission(boostrap, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(boostrap,"Se requiere de permisos",Toast.LENGTH_SHORT).show();
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            long eventID = Long.parseLong(uri.getLastPathSegment());
            Log.v("printEventIDCalendar",""+eventID);

            ContentValues reminders = new ContentValues();
            reminders.put(CalendarContract.Reminders.EVENT_ID, eventID);
            reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            reminders.put(CalendarContract.Reminders.MINUTES, 120);
            Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);
            ban = true;
        }

        Log.v("printFlag",String.valueOf(ban));
        return ban;
    }
    //=====================================================================================================

    //============== FORMATO DE FECHA MMM dd, yyyy   ==========================================
    private String formatDate(final String fecha) {
        calAux = Calendar.getInstance();
        String forDate = "";
        Date strDate = null;
        try {
            strDate = sdf.parse(fecha);
            calAux.setTime(strDate);
            forDate = formatDate.format(calAux.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return forDate;
    }
    //=========================================================================================
    @Override
    public int getItemCount() {
        return mDoctors.size();
    }

}
