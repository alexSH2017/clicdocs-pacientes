package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.SubthemesBeans;
import com.clicdocs.clicdocspacientes.utils.Forums;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewAdapterSubthemes extends RecyclerView.Adapter<RecyclerViewAdapterSubthemes.ViewHolder> {

    public interface Event {
        void onClic(SubthemesBeans s);
    }
    private List<SubthemesBeans> subthemeList;
    private Context ctx;
    private Event evento;

    public RecyclerViewAdapterSubthemes(List<SubthemesBeans> subthemeList, Context ctx, Event evento) {
        this.subthemeList = subthemeList;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_subtopic,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final SubthemesBeans subtheme = subthemeList.get(position);
        if (!subtheme.getPicture().isEmpty()){
            Picasso.with(ctx).
                    load(subtheme.getPicture()).
                    centerCrop().
                    noFade().
                    resize(60,60).
                    into(holder.CIVavatar);
        }
        holder.TVtitleSubtheme.setText(subtheme.getSubtheme());
        holder.TVpublicate.setText(subtheme.getNickname());
        holder.TVstamp.setText(subtheme.getTimestamp());
        holder.TVnumcoments.setText(subtheme.getReplies());

        holder.LLevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClic(subtheme);
            }
        });

    }

    @Override
    public int getItemCount() {
        return subthemeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView CIVavatar;
        private TextView TVnameSection, TVtitleSubtheme, TVpublicate,TVstamp, TVnumcoments, TVcomments;
        private LinearLayout LLevent;
        public ViewHolder(View itemView) {
            super(itemView);
            LLevent             = (LinearLayout) itemView.findViewById(R.id.LLevent);
            CIVavatar           = (CircleImageView) itemView.findViewById(R.id.CIVavatar);
            TVnameSection       = (TextView) itemView.findViewById(R.id.TVnameSection);
            TVtitleSubtheme     = (TextView) itemView.findViewById(R.id.TVtitleSubtheme);
            TVpublicate         = (TextView) itemView.findViewById(R.id.TVpublicate);
            TVstamp             = (TextView) itemView.findViewById(R.id.TVstamp);
            TVnumcoments        = (TextView) itemView.findViewById(R.id.TVnumcoments);
            TVcomments          = (TextView) itemView.findViewById(R.id.TVcomments);

        }
    }

    public void remove(){
        subthemeList.clear();
        notifyDataSetChanged();
    }
}
