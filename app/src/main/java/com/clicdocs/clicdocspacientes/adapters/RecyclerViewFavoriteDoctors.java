package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelFavoriteDoc;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Doctors;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewFavoriteDoctors extends RecyclerView.Adapter<RecyclerViewFavoriteDoctors.ViewHolder> {

    private MainActivity boostrap;
    private Context ctx;
    private List<ModelFavoriteDoc> favoriteList;
    private Event evento;

    public interface Event {
        void onClic(ModelFavoriteDoc fav);
    }

    public RecyclerViewFavoriteDoctors(MainActivity boostrap, Context ctx, List<ModelFavoriteDoc> favoriteList, Event evento) {
        this.boostrap = boostrap;
        this.ctx = ctx;
        this.favoriteList = favoriteList;
        this.evento = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_doctorlist,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ModelFavoriteDoc favorite = favoriteList.get(position);
        if (favorite.getPicture().length() > 0){
            Picasso.with(ctx).load(favorite.getPicture())
                    .centerCrop()
                    .noFade()
                    .resize(65, 65)
                    .into(holder.CIVphoto);
        }
        holder.CVdoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClic(favorite);
            }
        });
        holder.TVname.setText(favorite.getName());

        String speciality = boostrap.langStrings.get(Constants.no_specialities_p);
        if (favorite.getSpeciliaty() != null){
            if (favorite.getSpeciliaty().size() > 0) {
                for (int i = 0; i < favorite.getSpeciliaty().size(); i++) {
                    if (i == 0) {
                        speciality = Miscellaneous.ucFirst(favorite.getSpeciliaty().get(i));
                    } else {
                        speciality = speciality + ", " + Miscellaneous.ucFirst(favorite.getSpeciliaty().get(i));
                    }
                }
            }
        }

        holder.TVspecialities.setText(speciality);
    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVname, TVspecialities;
        private CircleImageView CIVphoto;
        private CardView CVdoctor;
        public ViewHolder(View itemView) {
            super(itemView);
            CIVphoto        = (CircleImageView) itemView.findViewById(R.id.CIVphoto);
            TVname          = (TextView) itemView.findViewById(R.id.TVname);
            TVspecialities  = (TextView) itemView.findViewById(R.id.TVspeciality);
            CVdoctor        = (CardView) itemView.findViewById(R.id.CVdoctor);
        }

    }
}
