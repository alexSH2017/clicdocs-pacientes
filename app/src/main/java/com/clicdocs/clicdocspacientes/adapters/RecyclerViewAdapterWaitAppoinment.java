package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelAppoinmentWait;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;



public class RecyclerViewAdapterWaitAppoinment extends RecyclerView.Adapter<RecyclerViewAdapterWaitAppoinment.ViewHolder> {

    private List<ModelAppoinmentWait> appoinmentWaitsList;
    private Context ctx;
    private Event evento;
    private int page;
    private MainActivity boostrap;
    SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd", new Locale("ES", "MX"));

    private Calendar calendar;

    public interface Event{
        void onClicLocation(ModelAppoinmentWait item);
        void onPager(int page);
    }

    public RecyclerViewAdapterWaitAppoinment(MainActivity boostrap, List<ModelAppoinmentWait> appoinmentWaitsList, Context ctx, Event evento) {
        this.boostrap = boostrap;
        this.appoinmentWaitsList = appoinmentWaitsList;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_appoinment_wait,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ModelAppoinmentWait wait = appoinmentWaitsList.get(position);
        if (position == (appoinmentWaitsList.size() - 1)) {
            int aux = appoinmentWaitsList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
        holder.TVappo_one.setText(boostrap.langStrings.get(Constants.appo_p));
        holder.TVappo_sec.setText(boostrap.langStrings.get(Constants.appo_p));
        if (!(wait.getfPicture().isEmpty())){
            holder.CIVfPicture.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(wait.getfPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(holder.CIVfPicture);
                }
            });
        } else {
            holder.CIVfPicture.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(holder.CIVfPicture);
                }
            });
        }

        holder.TVfName.setText(wait.getfName());
        holder.TVfHour.setText(wait.getfHour());
        String [] formatDate = cropDate(wait.getfDate());

        holder.TVfdayOfWeek.setText(formatDate[0]);
        holder.TVfdayOfMonth.setText(formatDate[1]);
        holder.TVfyear.setText(formatDate[2]);

        if (!(wait.getsPicture().isEmpty())){
            holder.CIVsPicture.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(wait.getsPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(holder.CIVsPicture);
                }
            });
        } else {
            holder.CIVsPicture.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(holder.CIVsPicture);
                }
            });
        }

        if (wait.getsHour().length() > 0){
            holder.TVsName.setText(wait.getsName());
            holder.TVsHour.setText(wait.getsHour());
            String [] sformatDate = cropDate(wait.getsDate());

            holder.TVsdayOfWeek.setText(sformatDate[0]);
            holder.TVsdayOfMonth.setText(sformatDate[1]);
            holder.TVsyear.setText(sformatDate[2]);

        } else {
            holder.TVsName.setVisibility(View.GONE);
            holder.TVsHour.setVisibility(View.GONE);
            holder.TVsdayOfMonth.setVisibility(View.GONE);
            holder.TVsdayOfWeek.setVisibility(View.GONE);
            holder.CIVsPicture.setVisibility(View.GONE);
            holder.IVsStatus.setVisibility(View.GONE);
            holder.TVappo_sec.setVisibility(View.GONE);
            holder.Vline.setVisibility(View.GONE);
        }

    }

    public void add(List<ModelAppoinmentWait> d) {
        for (int i = 0; i < d.size(); i++) {
            appoinmentWaitsList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return appoinmentWaitsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView CIVfPicture;
        private TextView TVfName, TVfdayOfWeek, TVfdayOfMonth, TVfyear, TVfHour;

        private CircleImageView CIVsPicture;
        private TextView TVsName, TVsdayOfWeek, TVsdayOfMonth, TVsyear, TVsHour, TVappo_one, TVappo_sec;
        private ImageView IVsStatus;
        private View Vline;

        public ViewHolder(View itemView) {
            super(itemView);
            TVfName     = (TextView) itemView.findViewById(R.id.TVfname);
            TVfHour     = (TextView) itemView.findViewById(R.id.TVfhour);
            CIVfPicture = (CircleImageView) itemView.findViewById(R.id.CIVfpicture);
            TVsName     = (TextView) itemView.findViewById(R.id.TVsname);
            TVsHour     = (TextView) itemView.findViewById(R.id.TVshour);
            CIVsPicture = (CircleImageView) itemView.findViewById(R.id.CIVspicture);
            TVfdayOfWeek     = (TextView) itemView.findViewById(R.id.TVfdayOfWeek);
            TVfdayOfMonth    = (TextView) itemView.findViewById(R.id.TVfdayOfMonth) ;
            TVfyear          = (TextView) itemView.findViewById(R.id.TVfyear);
            TVsdayOfWeek     = (TextView) itemView.findViewById(R.id.TVsdayOfWeek);
            TVsdayOfMonth    = (TextView) itemView.findViewById(R.id.TVsdayOfMonth) ;
            TVsyear          = (TextView) itemView.findViewById(R.id.TVsyear);
            IVsStatus        = (ImageView) itemView.findViewById(R.id.IVsStatus);
            Vline            = (View) itemView.findViewById(R.id.IVline);
            TVappo_one    = (TextView) itemView.findViewById(R.id.TVappo_one);
            TVappo_sec    = (TextView) itemView.findViewById(R.id.TVappo_sec);

        }
    }

    private String [] cropDate (String d){
        SimpleDateFormat formatDate = new SimpleDateFormat(
                "EEEE d MMMM',' yyyy", new Locale(boostrap.langStrings.get(Constants.local_language_p), boostrap.langStrings.get(Constants.local_region_p)));
        calendar = Calendar.getInstance();
        Date strDate = null;
        String [] date={};
        try {
            strDate = sdf.parse(d);
            calendar.setTime(strDate);
            String convertedDate = formatDate.format(calendar.getTime());
            date =convertedDate.split(" ");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
