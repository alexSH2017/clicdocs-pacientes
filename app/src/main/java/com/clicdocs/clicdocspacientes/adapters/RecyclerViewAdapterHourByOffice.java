package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;

import java.util.ArrayList;
import java.util.List;


public class RecyclerViewAdapterHourByOffice extends RecyclerView.Adapter<RecyclerViewAdapterHourByOffice.ViewHolder> {

    private List<String> hourList;
    private List<String> busyHours;
    private Event evento;
    private Context ctx;

    public interface Event {
        void onClic (String hour);
    }

    public RecyclerViewAdapterHourByOffice(List<String> hourList, List<String> busyHours, Context ctx, Event evento) {
        this.hourList = hourList;
        this.busyHours = busyHours;
        this.evento = evento;
        this.ctx = ctx;
    }

    @Override
    public RecyclerViewAdapterHourByOffice.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_new_hour,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewAdapterHourByOffice.ViewHolder holder, int position) {
        final String hora = hourList.get(position);
        if (busyHours != null){
            if (compare(hora)){
                holder.BTNhour.setEnabled(false);
                holder.BTNhour.setText(hora);
                holder.BTNhour.setBackgroundResource(R.color.colordialog);

            } else {
                holder.BTNhour.setEnabled(true);
                holder.BTNhour.setText(hora);
                holder.BTNhour.setBackgroundResource(R.color.azul);

            }
        } else {
            holder.BTNhour.setEnabled(true);
            holder.BTNhour.setText(hora);
            holder.BTNhour.setBackgroundResource(R.color.azul);
        }



        holder.BTNhour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClic(holder.BTNhour.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return hourList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVhour;
        private Button BTNhour;
        public ViewHolder(View itemView) {
            super(itemView);
            BTNhour = (Button) itemView.findViewById(R.id.BTNhour);
        }
    }

    private boolean compare (String hour) {
        boolean flag = false;
        boolean ban = true;
        int i=0;
        while (ban){
            if (i<busyHours.size()){
                if (hour.trim().equals(busyHours.get(i).trim())){
                    ban=false;
                    flag = true;
                }else {
                    flag = false;
                }
            } else {
                ban=false;
            }
            i+=1;
        }
        return flag;
    }

    public void update (List<String> open, List<String> locked,Context cntx){
        hourList.clear();
        busyHours.clear();
        this.ctx = cntx;
        this.hourList = open;
        this.busyHours = locked;
        notifyDataSetChanged();
    }
}
