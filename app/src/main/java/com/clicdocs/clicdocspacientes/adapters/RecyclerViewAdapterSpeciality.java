package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecyclerViewAdapterSpeciality extends RecyclerView.Adapter<RecyclerViewAdapterSpeciality.ViewHolder> {

    private Context ctx;
    private ArrayList<HashMap<String, String>> listSpeciality;
    private HashMap<String, String> langStrings;

    public RecyclerViewAdapterSpeciality(Context ctx, HashMap<String, String > langStrings, ArrayList<HashMap<String, String>> listSpeciality) {
        this.ctx = ctx;
        this.langStrings = langStrings;
        this.listSpeciality = listSpeciality;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_speciality_doctor,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HashMap<String,String>  speciality = listSpeciality.get(position);
        holder.TVspeciality.setText(Html.fromHtml("<b>"+langStrings.get(Constants.speciality_p)+": </b>")+speciality.get("specialty"));
        holder.TVuniversity.setText(speciality.get("university"));
        holder.TVstate.setText(speciality.get("state"));
        holder.TVgraduation.setText(Html.fromHtml("<b>"+langStrings.get(Constants.card_p)+": </b>")+speciality.get("professional_id"));
    }

    @Override
    public int getItemCount() {
        return listSpeciality.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVspeciality, TVuniversity, TVstate, TVgraduation;
        public ViewHolder(View itemView) {
            super(itemView);
            TVspeciality  = (TextView) itemView.findViewById(R.id.TVspeciality);
            TVuniversity  = (TextView) itemView.findViewById(R.id.TVuniversity);
            TVstate       = (TextView) itemView.findViewById(R.id.TVstate);
            TVgraduation  = (TextView) itemView.findViewById(R.id.TVgraduation);
        }
    }
}
