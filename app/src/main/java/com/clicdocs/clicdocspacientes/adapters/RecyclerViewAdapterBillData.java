package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelBillData;

import java.util.List;

/**
 * Created by apc_g on 09/10/2017.
 */

public class RecyclerViewAdapterBillData extends RecyclerView.Adapter<RecyclerViewAdapterBillData.ViewHolder> {

    private List<ModelBillData> listBill;
    private Context ctx;

    public RecyclerViewAdapterBillData(List<ModelBillData> listBill, Context ctx) {
        this.listBill = listBill;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_bill_data,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ModelBillData modelBillData = listBill.get(position);
        holder.TVbusiness.setText(modelBillData.getBusiness_name());
        holder.TVrfc.setText(modelBillData.getRfc());

    }

    @Override
    public int getItemCount() {
        return listBill.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVbusiness;
        private TextView TVrfc;
        public ViewHolder(View itemView) {
            super(itemView);
            TVbusiness        = (TextView)itemView.findViewById(R.id.TVbusiness);
            TVrfc             = (TextView)itemView.findViewById(R.id.TVrfc);

        }
    }

    public void removeItem(int position) {
        listBill.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public ModelBillData getItem(int position) {
        return listBill.get(position);
    }
}
