package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.fragments.pagerfragment.IdentificationFileFragment;
import com.clicdocs.clicdocspacientes.utils.Post;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.net.URLDecoder;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewAdapterPost extends RecyclerView.Adapter<RecyclerViewAdapterPost.ViewHolder> {

    private List<Post> postList;
    private Context ctx;

    public RecyclerViewAdapterPost(List<Post> postList, Context ctx) {
        this.postList = postList;
        this.ctx = ctx;
    }

    public void swapItems(List<Post> todolist){
        this.postList = todolist;
        notifyDataSetChanged();
    }

    public void addItem (Post post){
        postList.add(post);
        notifyItemInserted(postList.size()+1);
    }

    public void clear(){
        postList.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_post,parent, false);
        return new ViewHolder(view);
    }
    public void updateData(List<Post> post) {
        postList.clear();
        postList.addAll(post);
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post posts=postList.get(position);
        if (posts.getImage().length() > 0){
            Picasso.with(ctx).load(posts.getImage())
                    .noFade()
                    .into(holder.CIVavatar);
        } else {
            Picasso.with(ctx).load(R.mipmap.ic_launcher)
                    .resize(50, 50)
                    .noFade()
                    .centerCrop()
                    .into(holder.CIVavatar);
        }
        if (posts.getNickname().toString().length() > 0){
            holder.TVname.setText(""+posts.getNickname());
        } else {
            holder.TVname.setText(""+posts.getName());
        }

        holder.TVdate.setText(""+posts.getTimestamp());
        holder.TVpost.setText(""+posts.getPost());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVname;
        private TextView TVdate;
        private TextView TVpost;
        private CircleImageView CIVavatar;
        public ViewHolder(View itemView) {
            super(itemView);
            TVname  =   (TextView)  itemView.findViewById(R.id.tvName);
            TVdate = (TextView)itemView.findViewById(R.id.tvDate);
            TVpost = (TextView)itemView.findViewById(R.id.tvPost);
            TVdate = (TextView) itemView.findViewById(R.id.tvDate);
            CIVavatar = (CircleImageView) itemView.findViewById(R.id.CVavatar);
        }

    }
}
