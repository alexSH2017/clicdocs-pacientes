package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.clicdocs.clicdocspacientes.R.drawable.calendar;


public class SwipeRecyclerViewApproved extends RecyclerSwipeAdapter<SwipeRecyclerViewApproved.ViewHolder> {

    private MainActivity boostrap;
    private List<Dates> appoinmentList;
    private Context ctx;
    private Event evento;
    private int page;
    SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd", new Locale("ES", "MX"));
    private Calendar calendar;

    public interface Event {
        void onClicLocation (Dates item);
        void onClicCancelAppoinmnet (Dates appoinment, int position);
        void onClicRescheduleAppoinmnet (Dates appoinment);
        void onClicCalendar (Dates item);
        void onPager(int page);
    }

    public SwipeRecyclerViewApproved(MainActivity boostrap, Context ctx, List<Dates> appoinmentList,  Event evento) {
        this.boostrap = boostrap;
        this.appoinmentList = appoinmentList;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public SwipeRecyclerViewApproved.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_appoinment_approved,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SwipeRecyclerViewApproved.ViewHolder viewHolder, final int position) {
        final Dates appoinment=appoinmentList.get(position);
        if (position == (appoinmentList.size() - 1)) {
            int aux = appoinmentList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
        viewHolder.TVappo.setText(boostrap.langStrings.get(Constants.appo_p));
        viewHolder.TVcancel.setText(Miscellaneous.ucFirst(boostrap.langStrings.get(Constants.cancel_p)));
        viewHolder.TVreschedule.setText(Miscellaneous.ucFirst(boostrap.langStrings.get(Constants.reschedule_p)));
        viewHolder.TVcalendar.setText(boostrap.langStrings.get(Constants.add_calendar_p));

        if (!(appoinment.getPicture().isEmpty())){
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(appoinment.getPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(viewHolder.CIVprofile);
                }
            });
        } else {
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(viewHolder.CIVprofile);
                }
            });
        }
        viewHolder.tvNameDoctor.setText(Miscellaneous.ucFirst(appoinment.getName_doctor()));
        viewHolder.TVnameOffice.setText(appoinment.getAddress());

        String [] formatDate = cropDate(appoinment.getDate());

        viewHolder.TVdayOfWeek.setText(formatDate[0]);
        viewHolder.TVdayOfMonth.setText(formatDate[1]);
        viewHolder.TVyear.setText(formatDate[2]);

        viewHolder.TVhour.setText(appoinment.getHour());

        viewHolder.IBlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClicLocation(appoinment);
            }
        });

        viewHolder.LLevents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.swipeApproved.open();
            }
        });

        viewHolder.swipeApproved.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeApproved.findViewById(R.id.LLeventAppoinment));

        //--------------------------EVENT SURFACEVIEW---------------------------------
        /*viewHolder.swipeApproved.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        //------------------------EVENT CANCEL APPOINMENT ------------------------------
        viewHolder.TVcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicCancelAppoinmnet(appoinment, position);

            }
        });

        //------------------------EVENT RESCHEDULE APPOINMENT ------------------------------
        viewHolder.TVreschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicRescheduleAppoinmnet(appoinment);
            }
        });

        //------------------------EVENT CALENDAR ------------------------------
        viewHolder.TVcalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicCalendar(appoinment);
            }
        });

        mItemManger.bindView(viewHolder.itemView, position);
    }

    public void add(List<Dates> d) {
        for (int i = 0; i < d.size(); i++) {
            appoinmentList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return appoinmentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeApproved;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeApproved;
        private ImageView IBlocation;
        private TextView tvNameDoctor;
        private TextView TVnameOffice;
        private TextView TVcalendar;
        private TextView TVdayOfWeek;
        private TextView TVdayOfMonth;
        private TextView TVyear;
        private TextView TVhour;
        private CircleImageView CIVprofile;
        private TextView TVcancel;
        private TextView TVreschedule;
        private TextView TVappo;
        private LinearLayout LLevents;
        public ViewHolder(View itemView) {
            super(itemView);
            swipeApproved     = (SwipeLayout) itemView.findViewById(R.id.swipeApproved);
            IBlocation        = (ImageView) itemView.findViewById(R.id.IBlocation);
            CIVprofile        = (CircleImageView) itemView.findViewById(R.id.CIVprofile);
            tvNameDoctor      = (TextView)itemView.findViewById(R.id.tvNameDoctor);
            TVnameOffice      = (TextView) itemView.findViewById(R.id.TVnameOffice);
            TVcalendar        = (TextView) itemView.findViewById(R.id.TVcalendar);
            TVdayOfWeek       = (TextView) itemView.findViewById(R.id.TVdayOfWeek);
            TVdayOfMonth      = (TextView) itemView.findViewById(R.id.TVdayOfMonth);
            TVyear            = (TextView) itemView.findViewById(R.id.TVyear);
            TVhour            = (TextView) itemView.findViewById(R.id.TVhour);
            TVcancel          = (TextView) itemView.findViewById(R.id.TVcancel);
            TVreschedule      = (TextView) itemView.findViewById(R.id.TVreschedule);
            TVappo            = (TextView) itemView.findViewById(R.id.TVappo);
            LLevents          = (LinearLayout) itemView.findViewById(R.id.LLevents);

        }
    }

    public void removeItem (int pos){
        appoinmentList.remove(pos);
        notifyItemRemoved(pos);
        notifyDataSetChanged();
    }

    public String [] cropDate (String d){
        SimpleDateFormat formatDate = new SimpleDateFormat(
                "EEEE d MMMM',' yyyy", new Locale(boostrap.langStrings.get(Constants.local_language_p), boostrap.langStrings.get(Constants.local_region_p)));
        calendar = Calendar.getInstance();
        Date strDate = null;
        String [] date={};
        try {
            strDate = sdf.parse(d);
            calendar.setTime(strDate);
            String convertedDate = formatDate.format(calendar.getTime());
            date =convertedDate.split(" ");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
