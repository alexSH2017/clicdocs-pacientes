package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelOfficeWait;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewAdapterOfficeWait extends RecyclerView.Adapter<RecyclerViewAdapterOfficeWait.ViewHolder> {

    private List<ModelOfficeWait> officeWaitsList;
    private MainActivity boostrap;
    private Context ctx;
    private SessionManager session;
    private RequestQueue queueUnsubscribe;


    public RecyclerViewAdapterOfficeWait(List<ModelOfficeWait> officeWaitsList, MainActivity boostrap, Context ctx) {
        this.officeWaitsList = officeWaitsList;
        this.boostrap = boostrap;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_office_wait,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ModelOfficeWait officeWait = officeWaitsList.get(position);

        if (!officeWait.getPicture().isEmpty()){
            Picasso.with(ctx).
                    load(officeWait.getPicture()).
                    resize(60,60).
                    noFade().
                    centerCrop().
                    into(holder.CIVprofile);
        } else {
            Picasso.with(ctx).
                    load(R.drawable.blank).
                    resize(60,60).
                    noFade().
                    centerCrop().
                    into(holder.CIVprofile);
        }


        holder.TVname.setText(officeWait.getDoctor_name());

        holder.TVaddress.setText(officeWait.getStreet());

        holder.IVremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderMatutino = new AlertDialog.Builder(ctx);
                builderMatutino.setMessage(boostrap.langStrings.get(Constants.legend_out_waiting_list_p))
                        .setTitle(boostrap.langStrings.get(Constants.notice_p))
                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener()  {
                            public void onClick(DialogInterface dialog, int id) {
                                kicKMeOfWaitList(officeWait.getId_waiting_list(),position);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(boostrap.langStrings.get(Constants.cancel_p), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }); builderMatutino.show();
            }
        });

        if (officeWait.getHours_released() != null){
            holder.TVlegend.setVisibility(View.VISIBLE);
            RecyclerHoursReleased adapter = new RecyclerHoursReleased(officeWait.getHours_released(), ctx, new RecyclerHoursReleased.Event() {
                @Override
                public void onClic(String id_released) {
                    scheduleAppointmnet(id_released,position);
                }
            });
            holder.mRecycler.setAdapter(adapter);
        } else {
            holder.TVlegend.setVisibility(View.GONE);
            holder.mRecycler.setAdapter(null);
        }
    }

    @Override
    public int getItemCount() {
        return officeWaitsList.size();
    }

    public void remove (int i){
        officeWaitsList.remove(i);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView CIVprofile;
        private TextView TVname, TVaddress, TVlegend;
        private ImageView IVremove;
        private RecyclerView mRecycler;
        public ViewHolder(View itemView) {
            super(itemView);
            CIVprofile      = (CircleImageView) itemView.findViewById(R.id.CIVprofile);
            TVname          = (TextView) itemView.findViewById(R.id.TVname);
            TVaddress       = (TextView) itemView.findViewById(R.id.TVaddress);
            TVlegend        = (TextView) itemView.findViewById(R.id.TVlegend);
            IVremove        = (ImageView) itemView.findViewById(R.id.IVremove);
            mRecycler       = (RecyclerView) itemView.findViewById(R.id.mRecycler);
            mRecycler.setLayoutManager(new LinearLayoutManager(ctx,LinearLayoutManager.HORIZONTAL,true));
        }
    }

    private void kicKMeOfWaitList (final String id_wait_list, final int pos){
        Log.v("printRemove",id_wait_list +" "+pos);
        session = new SessionManager(ctx);
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            queueUnsubscribe = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.remove_wait_office, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printResponse",""+response);
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog successful = new AlertDialog.Builder(ctx)
                                        .setMessage(jsonObject.getString(Constants.response))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                remove(pos);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("waiting_list", id_wait_list);
                    params.put("patient_id", session.profile().get(0).toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queueUnsubscribe.add(request);
        }  else {

        }
    }

    private void scheduleAppointmnet(final String id_released, final int position){
        session = new SessionManager(ctx);
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, boostrap.langStrings.get(Constants.loading_p));
            loading.show();
            RequestQueue queue = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.schedule_waiting_list, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    Log.v("printResponse",""+response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                AlertDialog successful = new AlertDialog.Builder(ctx)
                                        .setMessage(boostrap.langStrings.get(Constants.sucess_appo_p))
                                        .setPositiveButton(boostrap.langStrings.get(Constants.accept_p), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                                remove(position);

                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("id_hour_released", id_released);
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_access_token, session.profile().get(5).toString());
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.Ln, (session.getLanguageID().equals("1"))?"spanish":"english" );
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy( 0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);
        }  else {

        }
    }
}
