package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class SwipeRecyclerViewFinished extends RecyclerSwipeAdapter<SwipeRecyclerViewFinished.ViewHolder> {

    private List<Dates> appoinmentList;
    private Context ctx;
    private Event evento;
    private int page;
    private MainActivity boostrap;
    SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd", new Locale("ES", "MX"));
    private Calendar calendar;

    public interface Event {
        void onClicLocation (Dates item);
        void onClicSeeRecipe (Dates dates);
        void onPager(int page);
    }

    public SwipeRecyclerViewFinished(MainActivity boostrap, List<Dates> appoinmentList, Context ctx, Event evento) {
        this.boostrap = boostrap;
        this.appoinmentList = appoinmentList;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public SwipeRecyclerViewFinished.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_swipe_appoinment_finished,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SwipeRecyclerViewFinished.ViewHolder viewHolder, int position) {
        final Dates appoinment = appoinmentList.get(position);
        if (position == (appoinmentList.size() - 1)) {
            int aux = appoinmentList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
        viewHolder.TVseerecipe.setText(boostrap.langStrings.get(Constants.see_pres_p));
        viewHolder.TVappo.setText(boostrap.langStrings.get(Constants.appo_p));
        if (!(appoinment.getPicture().isEmpty())){
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(appoinment.getPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(viewHolder.CIVprofile);
                }
            });
        } else {
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(viewHolder.CIVprofile);
                }
            });
        }

        viewHolder.TVname.setText(Miscellaneous.ucFirst(appoinment.getName_doctor()));
        viewHolder.TVhour.setText(appoinment.getHour());
        viewHolder.TVnameOffice.setText(appoinment.getAddress());

        String [] formatDate = cropDate(appoinment.getDate());

        viewHolder.TVdayOfWeek.setText(formatDate[0]);
        viewHolder.TVdayOfMonth.setText(formatDate[1]);
        viewHolder.TVyear.setText(formatDate[2]);

        viewHolder.TVhour.setText(appoinment.getHour());

        viewHolder.IBlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClicLocation(appoinment);
            }
        });

        viewHolder.LLevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.swipeFinish.open();
            }
        });

        viewHolder.swipeFinish.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeFinish.findViewById(R.id.LLeventAppoinment));

        //--------------------------EVENT SURFACEVIEW---------------------------------
        /*viewHolder.swipeFinish.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicProfile(Integer.valueOf(appoinment.getIdDoctor()));
            }
        });*/


        //------------------------EVENT SEE RECIPE APPOINMENT ------------------------------
        viewHolder.TVseerecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicSeeRecipe(appoinment);
            }
        });

        mItemManger.bindView(viewHolder.itemView, position);
    }

    public void add(List<Dates> d) {
        for (int i = 0; i < d.size(); i++) {
            appoinmentList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return appoinmentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return  R.id.swipeFinished;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SwipeLayout swipeFinish;
        private CircleImageView CIVprofile;
        private TextView TVname,  TVhour, TVseerecipe;
        private ImageView IBlocation;
        private TextView TVnameOffice;
        private TextView TVdayOfWeek;
        private TextView TVdayOfMonth;
        private TextView TVyear;
        private TextView TVappo;
        private LinearLayout LLevent;
        public ViewHolder(View itemView) {
            super(itemView);
            swipeFinish     = (SwipeLayout) itemView.findViewById(R.id.swipeFinished);
            CIVprofile      = (CircleImageView) itemView.findViewById(R.id.CIVprofile);
            TVname          = (TextView) itemView.findViewById(R.id.TVname);
            TVhour          = (TextView) itemView.findViewById(R.id.TVhour);
            TVseerecipe     = (TextView) itemView.findViewById(R.id.TVseerecipe);
            LLevent         = (LinearLayout) itemView.findViewById(R.id.LLevents);
            TVdayOfWeek     = (TextView) itemView.findViewById(R.id.TVdayOfWeek);
            TVdayOfMonth    = (TextView) itemView.findViewById(R.id.TVdayOfMonth) ;
            TVyear          = (TextView) itemView.findViewById(R.id.TVyear);
            TVhour          = (TextView) itemView.findViewById(R.id.TVhour);
            TVappo          = (TextView) itemView.findViewById(R.id.TVappo);
            TVnameOffice    = (TextView) itemView.findViewById(R.id.TVnameOffice);
            IBlocation      = (ImageView) itemView.findViewById(R.id.IBlocation);
        }
    }

    private String [] cropDate (String d){
        SimpleDateFormat formatDate = new SimpleDateFormat(
                "EEEE d MMMM',' yyyy", new Locale(boostrap.langStrings.get(Constants.local_language_p), boostrap.langStrings.get(Constants.local_region_p)));
        calendar = Calendar.getInstance();
        Date strDate = null;
        String [] date={};
        try {
            strDate = sdf.parse(d);
            calendar.setTime(strDate);
            String convertedDate = formatDate.format(calendar.getTime());
            date =convertedDate.split(" ");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
