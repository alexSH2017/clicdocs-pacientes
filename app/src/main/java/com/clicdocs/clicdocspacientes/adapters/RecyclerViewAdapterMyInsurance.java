package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.insuranceBean;

import java.util.ArrayList;


public class RecyclerViewAdapterMyInsurance extends RecyclerView.Adapter<RecyclerViewAdapterMyInsurance.ViewHolder> {

    private ArrayList<insuranceBean> data = new ArrayList<>();
    private Context ctx;

    public RecyclerViewAdapterMyInsurance(ArrayList<insuranceBean> data, Context ctx) {
        this.data = data;
        this.ctx = ctx;
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }

    public insuranceBean getItem(int position) {
        return data.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_insurance, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapterMyInsurance.ViewHolder holder, int position) {
        insuranceBean item = data.get(position);
        holder.TVinsurance.setText(item.getInsurance());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVinsurance;
        public ViewHolder(View itemView) {
            super(itemView);
            TVinsurance = (TextView) itemView.findViewById(R.id.TVinsurance);
        }
    }
}
