package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;

import java.util.ArrayList;
import java.util.HashMap;


public class RecyclerHoursReleased extends RecyclerView.Adapter<RecyclerHoursReleased.ViewHolder> {

    private ArrayList<HashMap<String,String>> hour;
    private Context ctx;
    private Event evento;

    public interface Event{
        void onClic (String id_released);
    }

    public RecyclerHoursReleased(ArrayList<HashMap<String, String>> hour, Context ctx, Event evento) {
        this.hour = hour;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public RecyclerHoursReleased.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_hours_released,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerHoursReleased.ViewHolder holder, int position) {
        final HashMap<String,String> hour_released = hour.get(position);
        holder.TVdate.setText(hour_released.get("date"));
        holder.BTNhour.setText(hour_released.get("hour"));

        holder.BTNhour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClic(hour_released.get("id_hour_released"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return hour.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVdate;
        private Button BTNhour;
        public ViewHolder(View itemView) {
            super(itemView);
            TVdate  = (TextView) itemView.findViewById(R.id.TVdate);
            BTNhour = (Button) itemView.findViewById(R.id.BTNhour);
        }
    }
}
