package com.clicdocs.clicdocspacientes.adapters;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.database.DBhelper;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Forums;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.Pillbox;
import com.google.android.gms.vision.text.Line;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RecyclerViewAdapterPillbox extends RecyclerView.Adapter<RecyclerViewAdapterPillbox.ViewHolder> {

    private List<Pillbox> pillboxList;
    private Context ctx;
    private Event evento;
    private MainActivity boostrap;
    private int page;

    public interface Event {
        void onClicView(Pillbox pillbox);
        void onClicStart(Pillbox pillbox);
        void onClicStop(Pillbox pillbox, int pos);
        void onPager(int page);
    }

    public RecyclerViewAdapterPillbox(MainActivity boostrap,List<Pillbox> pillboxList, Context ctx, Event evento) {
        this.pillboxList = pillboxList;
        this.ctx = ctx;
        this.evento = evento;
        this.boostrap = boostrap;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVname, TVmeasurement, TVfrequency, TVdose, TVduration, TVnote, TVprescription;
        private TextView TVmedicineT, TVmeasurementT, TVdoseT, TVfrequencyT, TVdurationT, TVnoteT;
        private Switch SWstart;
        private LinearLayout LLview;
        public ViewHolder(View itemView) {
            super(itemView);
            TVname          = (TextView) itemView.findViewById(R.id.TVname);
            TVmeasurement   = (TextView) itemView.findViewById(R.id.TVmeasurement);
            TVfrequency     = (TextView) itemView.findViewById(R.id.TVfrequency);
            TVdose          = (TextView) itemView.findViewById(R.id.TVdose);
            TVduration      = (TextView) itemView.findViewById(R.id.TVduration);
            TVnote          = (TextView) itemView.findViewById(R.id.TVnote);

            TVmedicineT     = (TextView) itemView.findViewById(R.id.TVmedicineT);
            TVmeasurementT  = (TextView) itemView.findViewById(R.id.TVmeasurementT);
            TVfrequencyT    = (TextView) itemView.findViewById(R.id.TVfrequencyT);
            TVdoseT         = (TextView) itemView.findViewById(R.id.TVdoseT);
            TVdurationT     = (TextView) itemView.findViewById(R.id.TVdurationT);
            TVnoteT         = (TextView) itemView.findViewById(R.id.TVnoteT);

            SWstart         = (Switch) itemView.findViewById(R.id.SWstart);
            TVprescription  = (TextView) itemView.findViewById(R.id.TVprescription);
            LLview          = (LinearLayout) itemView.findViewById(R.id.LLpill);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.item_pillbox,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Pillbox pillbox=pillboxList.get(position);
        Log.v("printPosition", ""+position+" "+pillboxList.size());
        if (position == (pillboxList.size() - 1)) {
            int aux = pillboxList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
        holder.TVmedicineT.setText(boostrap.langStrings.get(Constants.medicine_p));
        holder.TVmeasurementT.setText(boostrap.langStrings.get(Constants.measure_p));
        holder.TVfrequencyT.setText(boostrap.langStrings.get(Constants.frequency_p));
        holder.TVdoseT.setText(boostrap.langStrings.get(Constants.dose_p));
        holder.TVdurationT.setText(boostrap.langStrings.get(Constants.duration_title_p));
        holder.TVnoteT.setText(boostrap.langStrings.get(Constants.additional_notes_p));
        holder.TVname.setText(pillbox.getMedicine_name());
        holder.TVmeasurement.setText(Miscellaneous.getMeasurement(pillbox.getMeasurement()));
        holder.TVfrequency.setText(boostrap.langStrings.get(Constants.each_p)+" "+Miscellaneous.setFrequency(pillbox.getFrequency())+"hr(s)");
        holder.TVdose.setText(pillbox.getDose());
        holder.TVduration.setText(pillbox.getDuration()+" "+boostrap.langStrings.get(Constants.days_t_p));
        holder.TVprescription.setText(boostrap.langStrings.get(Constants.prescription_p).toUpperCase());
        holder.TVnote.setText(pillbox.getNote());

        if (pillbox.getOrigin().equals("1")){ holder.TVprescription.setVisibility(View.VISIBLE); }
        else {holder.TVprescription.setVisibility(View.GONE);}

        if (pillbox.getEnable().equals("0")){

        } else {
            holder.SWstart.setChecked(true);
            DBhelper dBhelper = new DBhelper(ctx);
            SQLiteDatabase bd = dBhelper.getWritableDatabase();
            Cursor fila = bd.rawQuery("select id_pillbox,number_take from CD_pillbox_table where id_pillbox='"+pillbox.getIdPill()+"'", null);
            if (fila.moveToFirst()) {
                String id = fila.getString(0);
                int number = Integer.parseInt(fila.getString(1));

            }
            bd.close();
        }

        holder.LLview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicView(pillbox);
            }
        });

        holder.SWstart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    evento.onClicStart(pillbox);
                } else {
                    evento.onClicStop(pillbox, position);
                }
            }
        });

    }

    public void removeItem(int position) {
        pillboxList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, pillboxList.size());
    }

    public void add(List<Pillbox> d) {
        for (int i = 0; i < d.size(); i++) {
            pillboxList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    public Pillbox getItem(int position) {
        return pillboxList.get(position);
    }


    @Override
    public int getItemCount() {
        return pillboxList.size();
    }
}
