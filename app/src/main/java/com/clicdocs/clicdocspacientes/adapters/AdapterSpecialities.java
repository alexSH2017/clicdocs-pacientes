package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Forums;

import java.util.List;

public class AdapterSpecialities extends RecyclerView.Adapter<AdapterSpecialities.ViewHolder> {

    private Context ctx;
    private List<String> data;
    private Event evento;

    public interface Event {
        void onClic(String item);
    }

    public AdapterSpecialities(Context ctx, List<String> data, Event evento) {
        this.ctx = ctx;
        this.data = data;
        this.evento = evento;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_speciality, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String speciality = data.get(position);
        holder.TVspeciality.setText(speciality);
        holder.bind(speciality);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView TVspeciality;
        public ViewHolder(View itemView) {
            super(itemView);
            TVspeciality    = (TextView) itemView.findViewById(R.id.TVspeciality);
        }
        public void bind (final String item){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    evento.onClic(item);
                }
            });
        }
    }
}
