package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.beans.ModelSubordinate;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Doctors;
import com.clicdocs.clicdocspacientes.utils.Pillbox;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

//import static com.clicdocs.clicdocspacientes.R.id.SWactive;
import static com.clicdocs.clicdocspacientes.R.id.thing_proto;


public class RecyclerViewAdapterFamilyClicDocs extends RecyclerView.Adapter<RecyclerViewAdapterFamilyClicDocs.ViewHolder> {

    private MainActivity boostrap;
    private SessionManager session;
    private Context ctx;
    private List<ModelSubordinate> subordinatesList;
    private Event evento;
    private View view_parent;

    private static Switch lastChecked = null;
    private static int lastCheckedPos = 0;

    public interface Event {
        void onClic (ModelSubordinate item, int pos);
        void onClicChanceAccount (int pos, boolean ban);
    }

    public RecyclerViewAdapterFamilyClicDocs(View view_parent,MainActivity boostrap, Context ctx, List<ModelSubordinate> subordinatesList, Event evento) {
        this.view_parent = view_parent;
        this.boostrap = boostrap;
        this.ctx = ctx;
        this.subordinatesList = subordinatesList;
        this.session = new SessionManager(ctx);
        this.evento = evento;
    }

    public void clear(){
        subordinatesList.clear();
        notifyDataSetChanged();
    }

    public ModelSubordinate getItem(int position) {
        return subordinatesList.get(position);
    }

    public void removeItem(int position) {
        subordinatesList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, subordinatesList.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_family,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ModelSubordinate subordinate = subordinatesList.get(position);
        holder.BTNenable.setText(boostrap.langStrings.get(Constants.change_profile_p));
        holder.BTNdisable.setText(boostrap.langStrings.get(Constants.back_tutor_p));
        if (subordinate.isSelected){
            holder.BTNdisable.setVisibility(View.VISIBLE);
            holder.BTNenable.setVisibility(View.GONE);
        } else {
            holder.BTNdisable.setVisibility(View.GONE);
            holder.BTNenable.setVisibility(View.VISIBLE);
        }

        holder.bind(subordinate,position);
        if (session.profile().get(0).equals(subordinate.getId_sesion())){
            holder.BTNdisable.setVisibility(View.VISIBLE);
            holder.BTNenable.setVisibility(View.GONE);
        }
        holder.BTNenable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subordinate.isSelected = true;
                Snackbar.make(view_parent,boostrap.langStrings.get(Constants.mess_change_profile_p),Snackbar.LENGTH_LONG).show();
                session.setLogin(subordinate.getId_sesion(),"5",subordinate.getName(),"12345678",subordinate.getPicture(),session.profile().get(5).toString());
                evento.onClicChanceAccount(position, true);
            }
        });

        holder.BTNdisable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subordinate.isSelected = false;
                session.setLogin(session.secondProfile().get(0).toString(),
                        session.secondProfile().get(1).toString(),
                        session.secondProfile().get(2).toString(),
                        session.secondProfile().get(3).toString(),
                        session.secondProfile().get(4).toString(),
                        session.secondProfile().get(5).toString());
                evento.onClicChanceAccount(position, false);
            }
        });

        boostrap.setImageProfile(session.profile().get(4).toString());
        boostrap.setNameProfile(session.profile().get(2).toString());
        holder.TVname.setText(subordinate.getName());
        if (!subordinate.getPicture().isEmpty()){
            Picasso.with(ctx).load(subordinate.getPicture()).resize(65,65).centerCrop().noFade().into(holder.CIVphoto);
        }
    }

    @Override
    public int getItemCount() {
        return subordinatesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView CIVphoto;
        private TextView TVname;
        private Button BTNenable, BTNdisable;
        //private Switch SWactive;
        public ViewHolder(View itemView) {
            super(itemView);
            CIVphoto    = (CircleImageView) itemView.findViewById(R.id.CIVphoto);
            TVname      = (TextView) itemView.findViewById(R.id.TVname);
            BTNenable   = (Button) itemView.findViewById(R.id.BTNenable);
            BTNdisable  = (Button) itemView.findViewById(R.id.BTNdisable);
            //SWactive    = (Switch) itemView.findViewById(R.id.SWactive);

        }
        public void bind (final ModelSubordinate item, final int pos){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    evento.onClic(item,pos);
                }
            });
        }
    }

    public void changeUser (int pos, boolean flag){
        for (int i = 0; i < subordinatesList.size(); i++){
            if (flag){
                if (i != pos){
                    subordinatesList.get(i).isSelected = false;
                }
            } else {
                subordinatesList.get(i).isSelected = false;
            }

        }
        notifyDataSetChanged();
    }

    public boolean validate (int pos){
        boolean bandera = false;
        for (int i = 0; i < subordinatesList.size(); i++){
            if (subordinatesList.get(i).isSelected != true && i != pos){
                subordinatesList.get(i).isSelected = false;
            }
        }
        return bandera;
    }
}
