package com.clicdocs.clicdocspacientes.adapters;


import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.DoctorSearch;
import com.clicdocs.clicdocspacientes.utils.Doctors;
import com.clicdocs.clicdocspacientes.utils.Links;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapterLinks extends RecyclerView.Adapter<RecyclerViewAdapterLinks.ViewHolder>{

    public interface Event{
        void onClic(Links link);
        void onMore(int i) throws JSONException;

    }

    private List<Links> linksList;
    private Context ctx;
    private Event evento;
    private int page = 0;

    public RecyclerViewAdapterLinks(Context ctx, List<Links> linksList, Event evento) {
        this.linksList = linksList;
        this.ctx = ctx;
        this.evento=evento;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView CIVlink;
        private TextView TVtitle;
        private TextView TVdescription;
        private LinearLayout LLlink;

        public ViewHolder(View itemView) {
            super(itemView);
            CIVlink         = (CircleImageView) itemView.findViewById(R.id.CIVlink);
            TVtitle         = (TextView) itemView.findViewById(R.id.TVtitle);
            TVdescription   = (TextView) itemView.findViewById(R.id.TVdescription);
            LLlink          = (LinearLayout) itemView.findViewById(R.id.LLlink);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_links,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Links list= linksList.get(position);
        if (position == (linksList.size()-1)){
            int aux = linksList.size()/20;
            if (aux != page){
                page = aux;
                try {
                    evento.onMore(page);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
            if (list.getImagen().length() > 0){
            Picasso.with(ctx).load(list.getImagen())
                    .noFade()
                    .into(holder.CIVlink);

        }
        holder.TVtitle.setText(list.getTitle());
        holder.TVdescription.setText(list.getDescription());
        holder.LLlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClic(linksList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return linksList.size();
    }

    public void add(List<Links> l) {
        for (int i = 0; i<l.size(); i++){
            linksList.add(l.get(i));
        }
        notifyDataSetChanged();
    }

}
