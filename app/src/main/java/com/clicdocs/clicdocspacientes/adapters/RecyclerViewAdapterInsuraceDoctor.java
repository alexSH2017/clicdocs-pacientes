package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;

import java.util.ArrayList;


public class RecyclerViewAdapterInsuraceDoctor extends RecyclerView.Adapter<RecyclerViewAdapterInsuraceDoctor.ViewHolder>{

    ArrayList<String> data;
    Context ctx;

    public RecyclerViewAdapterInsuraceDoctor(ArrayList<String> data, Context ctx) {
        this.data = data;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_advices, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String item = data.get(position);
        holder.TVinsurance.setText(item);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView TVinsurance;
        public ViewHolder(View itemView) {
            super(itemView);
            TVinsurance= (TextView) itemView.findViewById(R.id.TVadvice);
        }
    }
}
