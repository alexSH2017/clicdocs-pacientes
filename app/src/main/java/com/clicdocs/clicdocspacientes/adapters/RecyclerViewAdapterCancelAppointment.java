package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by apc_g on 30/10/2017.
 */

public class RecyclerViewAdapterCancelAppointment extends RecyclerView.Adapter<RecyclerViewAdapterCancelAppointment.ViewHolder> {

    private List<Dates> datesList;
    private Context ctx;
    private Event evento;
    private int page;
    private MainActivity boostrap;
    SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd", new Locale("ES", "MX"));

    private Calendar calendar;

    public interface Event {
        void onClicLocation (Dates appoinment);
        void onPager(int page);
    }

    public RecyclerViewAdapterCancelAppointment(MainActivity boostrap, List<Dates> datesList, Context ctx, Event evento) {
        this.boostrap = boostrap;
        this.datesList = datesList;
        this.ctx = ctx;
        this.evento = evento;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ctx).inflate(R.layout.adapter_cancel_appointment,parent, false);
        return new RecyclerViewAdapterCancelAppointment.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Dates appoinment=datesList.get(position);
        if (position == (datesList.size() - 1)) {
            int aux = datesList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
        holder.TVappo.setText(boostrap.langStrings.get(Constants.appo_p));
        if (!(appoinment.getPicture().isEmpty())){
            holder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(appoinment.getPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(holder.CIVprofile);
                }
            });
        } else {
            holder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(holder.CIVprofile);
                }
            });
        }

        holder.TVname.setText(Miscellaneous.ucFirst(appoinment.getName_doctor()));
        holder.tvHour.setText(appoinment.getHour());

        holder.TVnameOffice.setText(appoinment.getAddress());

        String [] formatDate = cropDate(appoinment.getDate());

        holder.TVdayOfWeek.setText(formatDate[0]);
        holder.TVdayOfMonth.setText(formatDate[1]);
        holder.TVyear.setText(formatDate[2]);

        holder.IBlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClicLocation(appoinment);
            }
        });

    }

    public void add(List<Dates> d) {
        for (int i = 0; i < d.size(); i++) {
            datesList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return datesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView TVname;
        private TextView tvHour;
        private ImageView IBlocation;
        private TextView TVnameOffice;
        private TextView TVdayOfWeek;
        private TextView TVdayOfMonth;
        private TextView TVyear;
        private TextView TVappo;
        private CircleImageView CIVprofile;
        public ViewHolder(View itemView) {
            super(itemView);ctx = itemView.getContext();
            TVname=(TextView)itemView.findViewById(R.id.TVname);
            tvHour = (TextView) itemView.findViewById(R.id.TVhour);
            TVdayOfWeek     = (TextView) itemView.findViewById(R.id.TVdayOfWeek);
            TVdayOfMonth    = (TextView) itemView.findViewById(R.id.TVdayOfMonth) ;
            TVyear          = (TextView) itemView.findViewById(R.id.TVyear);
            TVnameOffice    = (TextView) itemView.findViewById(R.id.TVnameOffice);
            IBlocation      = (ImageView) itemView.findViewById(R.id.IBlocation);
            CIVprofile = (CircleImageView) itemView.findViewById(R.id.CIVprofile);
            TVappo          = (TextView) itemView.findViewById(R.id.TVappo);
        }
    }

    private String [] cropDate (String d){
        SimpleDateFormat formatDate = new SimpleDateFormat(
                "EEEE d MMMM',' yyyy", new Locale(boostrap.langStrings.get(Constants.local_language_p), boostrap.langStrings.get(Constants.local_region_p)));
        calendar = Calendar.getInstance();
        Date strDate = null;
        String [] date={};
        try {
            strDate = sdf.parse(d);
            calendar.setTime(strDate);
            String convertedDate = formatDate.format(calendar.getTime());
            date =convertedDate.split(" ");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
