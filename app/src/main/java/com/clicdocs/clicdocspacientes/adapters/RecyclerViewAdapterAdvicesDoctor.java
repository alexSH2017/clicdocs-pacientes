package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clicdocs.clicdocspacientes.R;

import java.util.ArrayList;


public class RecyclerViewAdapterAdvicesDoctor extends RecyclerView.Adapter<RecyclerViewAdapterAdvicesDoctor.ViewHolder> {

    private ArrayList<String> data = new ArrayList<>();
    private Context ctx;

    public RecyclerViewAdapterAdvicesDoctor(Context ctx, ArrayList<String> data) {
        this.ctx  = ctx;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_advices, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String item = data.get(position);
        holder.TVadvice.setText(item);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView TVadvice;
        public ViewHolder(View itemView) {
            super(itemView);
            TVadvice = (TextView) itemView.findViewById(R.id.TVadvice);
        }
    }
}
