package com.clicdocs.clicdocspacientes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clicdocs.clicdocspacientes.MainActivity;
import com.clicdocs.clicdocspacientes.R;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Dates;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class SwipeRecyclerViewProcess extends RecyclerSwipeAdapter<SwipeRecyclerViewProcess.ViewHolder> {

    private List<Dates> appoinmentList;
    SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd", new Locale("ES", "MX"));

    private Calendar calendar;
    private Context ctx;
    private Event evento;
    private MainActivity boostrap;
    private int page;

    public interface Event {
        void onClicLocation (Dates item);
        void onClicRejectAppoinmnet (Dates dates, int pos);
        void onClicAcceptAppoinmnet (Dates dates, int pos);
        void onPager(int page);

    }

    public SwipeRecyclerViewProcess(MainActivity boostrap, List<Dates> appoinmentList, Context ctx, Event evento) {
        this.boostrap = boostrap;
        this.appoinmentList = appoinmentList;
        this.ctx = ctx;
        this.evento = evento;
    }

    @Override
    public SwipeRecyclerViewProcess.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_swipe_appoinment_process,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SwipeRecyclerViewProcess.ViewHolder viewHolder, final int position) {
        final Dates appoinment = appoinmentList.get(position);
        viewHolder.TVappo.setText(boostrap.langStrings.get(Constants.appo_p));
        viewHolder.TVcancel.setText(boostrap.langStrings.get(Constants.to_refuse_p));
        viewHolder.TVreschedule.setText(boostrap.langStrings.get(Constants.approve_p));

        if (!(appoinment.getPicture().isEmpty())){
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(appoinment.getPicture())
                            .resize(65, 65)
                            .noFade()
                            .centerCrop()
                            .into(viewHolder.CIVprofile);
                }
            });
        } else {
            viewHolder.CIVprofile.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(ctx).load(R.drawable.blank)
                            .noFade()
                            .centerCrop()
                            .resize(65, 65)
                            .into(viewHolder.CIVprofile);
                }
            });
        }

        viewHolder.TVname.setText(Miscellaneous.ucFirst(appoinment.getName_doctor()));
        viewHolder.TVoffice.setText(appoinment.getName_office());

        String [] formatDate = cropDate(appoinment.getDate());
        viewHolder.TVdayOfWeek.setText(formatDate[0]);
        viewHolder.TVdayOfMonth.setText(formatDate[1]);
        viewHolder.TVyear.setText(formatDate[2]);
        viewHolder.TVhour.setText(appoinment.getHour());

        viewHolder.LLevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.swipeProcess.open();
            }
        });

        viewHolder.IBlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evento.onClicLocation(appoinment);
            }
        });

        viewHolder.swipeProcess.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeProcess.findViewById(R.id.LLeventAppoinment));

        //--------------------------EVENT SURFACEVIEW---------------------------------
        /*viewHolder.swipeProcess.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicProfile(Integer.valueOf(appoinment.getIdDoctor()));
            }
        });*/

        //------------------------EVENT CANCEL APPOINMENT ------------------------------
        viewHolder.TVcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicRejectAppoinmnet(appoinment, position);
            }
        });
        //------------------------EVENT RESCHEDULE APPOINMENT ------------------------------
        viewHolder.TVreschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento.onClicAcceptAppoinmnet(appoinment, position);
            }
        });

        mItemManger.bindView(viewHolder.itemView, position);

        if (position == (appoinmentList.size() - 1)) {
            int aux = appoinmentList.size() / 20;
            if (aux != page) {
                page = aux;
                evento.onPager(page);
            }
        }
    }

    public void add(List<Dates> d) {
        for (int i = 0; i < d.size(); i++) {
            appoinmentList.add(d.get(i));
        }
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        appoinmentList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return appoinmentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeProcess;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SwipeLayout swipeProcess;
        private ImageView IBlocation;
        private CircleImageView CIVprofile;
        private TextView TVname, TVoffice, TVdayOfWeek, TVdayOfMonth, TVyear, TVhour, TVcancel, TVreschedule;
        private LinearLayout LLevent;
        private TextView TVappo;

        public ViewHolder(View itemView) {
            super(itemView);
            swipeProcess = (SwipeLayout) itemView.findViewById(R.id.swipeProcess);
            CIVprofile    = (CircleImageView) itemView.findViewById(R.id.CIVprofile);
            IBlocation      = (ImageView) itemView.findViewById(R.id.IBlocation);
            TVname       = (TextView) itemView.findViewById(R.id.tvNameDoctor);
            TVoffice        = (TextView) itemView.findViewById(R.id.TVnameOffice);
            TVdayOfWeek       = (TextView) itemView.findViewById(R.id.TVdayOfWeek);
            TVdayOfMonth       = (TextView) itemView.findViewById(R.id.TVdayOfMonth);
            TVyear         = (TextView) itemView.findViewById(R.id.TVyear);
            TVhour       = (TextView) itemView.findViewById(R.id.TVhour);
            LLevent      = (LinearLayout) itemView.findViewById(R.id.LLevents);
            TVcancel     = (TextView) itemView.findViewById(R.id.TVcancel);
            TVreschedule = (TextView) itemView.findViewById(R.id.TVreschedule);
            TVappo          = (TextView) itemView.findViewById(R.id.TVappo);
        }
    }

    private String [] cropDate (String d){
        SimpleDateFormat formatDate = new SimpleDateFormat(
                "EEEE d MMMM',' yyyy", new Locale(boostrap.langStrings.get(Constants.local_language_p), boostrap.langStrings.get(Constants.local_region_p)));
        calendar = Calendar.getInstance();
        Date strDate = null;
        String [] date={};
        try {
            strDate = sdf.parse(d);
            calendar.setTime(strDate);
            String convertedDate = formatDate.format(calendar.getTime());
            date =convertedDate.split(" ");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}