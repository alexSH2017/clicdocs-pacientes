package com.clicdocs.clicdocspacientes.database;

import android.provider.BaseColumns;


public final class PillBoxTable {

    public PillBoxTable() {    }

    public static class PillBoxEntry implements BaseColumns {
        // == TABLE
        public static final String TABLE_NAME   = "CD_pillbox_table";

        // == COLUMNS
        public static final String ID_PILLBOX = "id_pillbox";
        public static final String NUMBER_TAKE = "number_take";
        public static final String TIMESTAMP   = "timestamp";
        public static final String ID_SELECT   = "";

        // == QUERIES
        static final String CREATE_TABLE   = "CREATE TABLE " + TABLE_NAME + "("
                + PillBoxEntry._ID + " INTEGER PRIMARY KEY,"
                + PillBoxEntry.ID_PILLBOX + " INTEGER,"
                + PillBoxEntry.NUMBER_TAKE + " INTEGER,"
                + PillBoxEntry.TIMESTAMP + " TEXT)";

        static final String DELETE_RECORDS = "DROP TABLE IF EXISTS " + PillBoxEntry.TABLE_NAME;
    }
}
