package com.clicdocs.clicdocspacientes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DBhelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "CDpillbox.db";

    public DBhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PillBoxTable.PillBoxEntry.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(PillBoxTable.PillBoxEntry.DELETE_RECORDS);
    }

    public void saveRecordsInto(String idPill, String number, String time, SQLiteDatabase db) {
                db.beginTransaction();
                String sql = "INSERT INTO " + PillBoxTable.PillBoxEntry.TABLE_NAME + "(" +
                        PillBoxTable.PillBoxEntry.ID_PILLBOX + ", " +
                        PillBoxTable.PillBoxEntry.NUMBER_TAKE + ", " +
                        PillBoxTable.PillBoxEntry.TIMESTAMP + ") VALUES(?, ?, ?)";
                SQLiteStatement pInsert = db.compileStatement(sql);
                pInsert.bindString(1, idPill);
                pInsert.bindString(2, number);
                pInsert.bindString(3, time);
                pInsert.execute();

                db.setTransactionSuccessful();
                db.endTransaction();

    }
}
