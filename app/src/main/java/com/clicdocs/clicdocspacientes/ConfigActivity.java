package com.clicdocs.clicdocspacientes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clicdocs.clicdocspacientes.fragments.dialogfragments.DialogChangePassword;
import com.clicdocs.clicdocspacientes.fragments.fragments;
import com.clicdocs.clicdocspacientes.utils.Constants;
import com.clicdocs.clicdocspacientes.utils.Miscellaneous;
import com.clicdocs.clicdocspacientes.utils.NetworkUtils;
import com.clicdocs.clicdocspacientes.utils.PopUpManager;
import com.clicdocs.clicdocspacientes.utils.SessionManager;
import com.clicdocs.clicdocspacientes.utils.TrustManagerUtil;
import com.clicdocs.clicdocspacientes.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ConfigActivity extends AppCompatActivity{

    private CardView btnLanguage;
    private CardView btnPhone;
    private CardView btnChangepass;
    private TextView TVlanguage;
    private TextView TVphone;
    private TextView TVchangepass;
    public HashMap<String, String> langStrings;
    private SessionManager session;
    private Context ctx;
    private Toolbar mToolBar;
    private RequestQueue queueLenguage;
    public Validator validator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        mToolBar            = (Toolbar) findViewById(R.id.mToolBar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        validator = new Validator();
        langStrings     = (HashMap<String, String>) getIntent().getExtras().getSerializable(Constants.langString);
        validator.REQUIRED_MESSAGE = langStrings.get(Constants.requier_field_p);
        validator.ONLY_TEXT_MESSAGE = langStrings.get(Constants.only_letters_p);
        validator.ONLY_NUMBER_MESSAGE = langStrings.get(Constants.only_numbers_p);
        validator.EMAIL_MESSAGE = langStrings.get(Constants.invalid_email_p);
        validator.REASON_MESSAGE = langStrings.get(Constants.mess_bussines_p);
        validator.NUMBER_MESSAGE = langStrings.get(Constants.letters_and_num_p);
        validator.GENERAL_MESSAGE = langStrings.get(Constants.letters_o_num_p);
        btnLanguage     = (CardView) findViewById(R.id.btnLanguage);
        btnPhone        = (CardView) findViewById(R.id.btnPhone);
        btnChangepass   = (CardView) findViewById(R.id.btnChangepass);
        TVlanguage      = (TextView) findViewById(R.id.TVlanguage);
        TVphone    = (TextView) findViewById(R.id.TVphone);
        TVchangepass    = (TextView) findViewById(R.id.TVchangepass);
        ctx             = this;
        session = new SessionManager(ctx);
        TVlanguage.setText(langStrings.get(Constants.language_p));
        TVphone.setText(langStrings.get(Constants.emergency_number_p));
        TVchangepass.setText(langStrings.get(Constants.change_pass_p));
        setTitle(langStrings.get(Constants.settings_p));
        if (session.profile().get(3).toString().equals("fb") || !session.profile().get(0).equals(session.secondProfile().get(0))){
            btnChangepass.setVisibility(View.GONE);
        }

        btnLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLanguage();
            }
        });

        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTelephone();
            }
        });

        btnChangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogChangePassword changPass = new DialogChangePassword();
                changPass.show(getSupportFragmentManager(),fragments.CHANGEPASS);
            }
        });

    }

    public void changeLanguage() {
        View view              = LayoutInflater.from(ctx).inflate(R.layout.dialog_lang, null);
        LinearLayout LLform    = (LinearLayout) view.findViewById(R.id.LLform);
        Spinner SPlang         = (Spinner) view.findViewById(R.id.SPlang);
        Button btnSave         = (Button) view.findViewById(R.id.btnSave);
        btnSave.setText(langStrings.get(Constants.save_change_p));
        final AlertDialog lang = new AlertDialog.Builder(ctx)
                .setTitle(langStrings.get(Constants.change_lan_p))
                .setView(view).show();
        getAvailabelLangs(LLform, SPlang, btnSave, lang);
    }

    public void changeTelephone(){
        View view                             = LayoutInflater.from(ctx).inflate(R.layout.dialog_emergency_number, null);
        LinearLayout LLform                   = (LinearLayout) view.findViewById(R.id.LLform);
        final EditText ETemergencyNumber      = (EditText) view.findViewById(R.id.ETemergencyNumber);
        Button BTNchoice                      = (Button) view.findViewById(R.id.BTNchoice);
        BTNchoice.setText(langStrings.get(Constants.save_change_p));
        ETemergencyNumber.setText(session.getPanicTelephone().get(0).toString());
        final AlertDialog DLemergencyNumber = new AlertDialog.Builder(ctx)
                .setTitle(langStrings.get(Constants.change_phone_p))
                .setView(view).show();

        BTNchoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validator validator = new Validator();
                if (!validator.validate(ETemergencyNumber, new String[] {validator.REQUIRED, validator.ONLY_NUMBER})){
                    session.setPanicTelephone(ETemergencyNumber.getText().toString().trim(),"1");
                    Snackbar.make(v,langStrings.get(Constants.change_phone_success_p),Snackbar.LENGTH_SHORT).show();
                    DLemergencyNumber.dismiss();
                }
            }
        });
    }

    public void getAvailabelLangs(final LinearLayout LLform, final Spinner SPlang, final Button btnSave, final AlertDialog dialog) {
        LLform.setVisibility(View.VISIBLE);
        String[] language = { "Español", "English" };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, language);
        SPlang.setAdapter(adapter);
        btnSave.setOnClickListener(setLanguage(SPlang, dialog));

    }

    public View.OnClickListener setLanguage(final Spinner SPlang, final AlertDialog dialog) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getIdoma = Miscellaneous.idioma(String.valueOf(SPlang.getSelectedItemPosition()));
                changeLenguage(getIdoma);
                dialog.dismiss();

            }
        };
    }

    public void changeLenguage(final String idioma){
        if(NetworkUtils.haveNetworkConnection(ctx)) {
            final AlertDialog loading = PopUpManager.showLoadingDialog(ctx, langStrings.get(Constants.loading_p));
            loading.show();
            queueLenguage = Volley.newRequestQueue(ctx, new HurlStack(null, TrustManagerUtil.getSockectFactory(ctx)));
            StringRequest request = new StringRequest(Request.Method.POST, Constants.change_language, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.v("printUser",response+"");
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        switch (Integer.parseInt(jsonObject.get(Constants.Code).toString())) {
                            case 200:
                                session.setLanguageID(idioma);
                                triggerRebirth();
                                break;
                            case 404:
                                Snackbar.make(getWindow().getDecorView().getRootView(),jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                Snackbar.make(getWindow().getDecorView().getRootView(),jsonObject.getString("response").toString(),Snackbar.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("error", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("ln",(idioma.equals("1"))? "spanish":"english");
                    params.put("session_id", session.profile().get(0).toString());
                    params.put("type", "patient");
                    Log.v("printParams",params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put(Constants.key_bundle_id, Constants.bundle_id);
                    headers.put(Constants.key_access_token, session.profile().get(0).toString());
                    return headers;
                }
            };
            queueLenguage.add(request);
        }  else {

        }
    }

    public void triggerRebirth() {
        Intent i = getPackageManager().getLaunchIntentForPackage(getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityCompat.finishAffinity(this);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
